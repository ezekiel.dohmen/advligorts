import numpy as np

def abs_fft(x):
    """
    Return absolute value of fft of x
    Positive frequencies only
    :param x:
    :return:
    """
    absfft = np.abs(np.fft.fft(x))
    absfft = absfft[:len(absfft)//2]
    return absfft

def max_two(index, value):
    """
    Return (index1, mag1, index2, mag2) for the maximum and second maximum


    :param index: list of indices to into value.  Must be same shape as value.
    :param x: a list of values
    :return: index[i1], value[i1], index[i2], value[i2] where i1 and i2 are the indices of the largest
    and second largest items in value
    """
    assert(len(value) > 1)
    i1 = np.argmax(value)
    mag1 = value[i1]

    if i1 == 0:
        i2 = 1
    else:
        i2 = 0
    mag2 = value[i2]

    for i in range(len(value)):
        if i != i1:
            if value[i] > mag2:
                mag2 = value[i]
                i2 = i

    return (index[i1], mag1, index[i2], mag2)
