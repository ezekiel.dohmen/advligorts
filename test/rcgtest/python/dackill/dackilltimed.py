#!/usr/bin/python3
# Test EPICS INPUT w/CONTROL (EIC) part

import epics
import time
import string
import sys
import os
import os.path as path

rcg_dir = os.environ['RCG_DIR']
from epics import PV
from subprocess import call
sys.path.insert(0,path.join(rcg_dir, 'test/rcgtest/python'))
from cdstestsupport import testreport
from cdstestsupport import SFM


tr = testreport('dktTest','DACKILL - TIMED PART',110)
testword = "X2:IOP-SAM_TEST_STAT_6"
testwordepics = PV(testword)
testwordepics.value = 2

# Standard Header for all Test Scripts ****************************************************************
tr.sectionhead('Purpose')
st = 'Test DacKillTimed Part introduced in RCG V2.8. \n'
tr.sectiontext(st)
tr.teststatus == 0

tr.sectionhead('Test Overview')
st = ' \n'
tr.sectiontext(st)

tr.sectionhead('Test Requirements')
st = '	This test is requires the dacKillTimed.py script, which is found in the \n'
st += '	cds/test/scripts/python/dackill directory. \n'
tr.sectiontext(st)

tr.sectionhead('Related MEDM Screens')
st = '	Test screen is at /opt/rtcds/rtscore/advligorts/test/rcgtest/medm/DAC_KILL_TIMED_TEST.adl'
tr.sectiontext(st)
tr.image('dacKillTimedMedm')

tr.sectionhead('Test Model')
st = '	Test Model is x2atstim32.mdl.'
tr.sectiontext(st)
tr.image('dacKillTimedModel')

tr.teststate.value = 'DKT - Initializing'
# Setup EPICS channels to set/monitor *****************************************************************
#DKT external signal connections
bypasstimeset = PV('X2:ATS-TIM32_DK_BP_TIME.VAL')
seitriptimeset = PV('X2:ATS-TIM32_DK_WD_TIME.VAL')
sustriptimeset = PV('X2:ATS-TIM32_DK_DT_TIME.VAL')
dktinput = PV('X2:ATS-TIM32_DK_SIGIN.VAL')
dktoutput = PV('X2:ATS-TIM32_DK_WD_OUT.VAL')
# DAC drives and output values
dacFilt0 = SFM('X2:ATS-TIM32_T1_DAC_FILTER_1')
dacFilt0.inputsw('OFF')
dacFilt0.inputsw('ON')
dacFilt0.offsetsw('ON')
dacFilt0.offset.value = 2000
dacoutput = PV('X2:FEC-110_DAC_OUTPUT_3_0')

#IPO DK Resets
iop_ham3_dkreset = PV('X2:IOP-SAM_DACKILL_HAM3_RESET')
iop_ham4_dkreset = PV('X2:IOP-SAM_DACKILL_HAM4_RESET')

# DKT Settings
dktpanic = PV('X2:ATS-TIM32_DACKILL_PANIC')
dktreset = PV('X2:ATS-TIM32_DACKILL_RESET')
dktbypass = PV('X2:ATS-TIM32_DACKILL_BPSET')


# DKT readback channels 
dktstate = PV('X2:ATS-TIM32_DACKILL_STATE')
dktbptimer = PV('X2:ATS-TIM32_DACKILL_BPTIME')
dktseitriptimer = PV('X2:ATS-TIM32_DACKILL_WDTIME')
dktsustriptimer = PV('X2:ATS-TIM32_DACKILL_DTTIME')

tr.sectionhead('Test Procedure')
tr.sectiontext('')
tr.sectionhead('Step 1:  Initalize Test Settings')


# reset IOP DK
iop_ham3_dkreset.value = 1
iop_ham4_dkreset.value = 1

dktpanic.value = 0
time.sleep(1)
dktbypass.value = 0
dktinput.value = 'ON'
time.sleep(2)
dktreset.value = 1

time.sleep(2)

# Intialize test values
bypasstimeset.value = 20
seitriptimeset.value = 30
sustriptimeset.value = 40

time.sleep(2)

dktreset.value = 1

time.sleep(2)
tptxt = '	- Bypass time = ' + repr(bypasstimeset.value) + ' \n'
tptxt += '	- SEI Trip time = ' + repr(seitriptimeset.value) + ' \n'
tptxt += '	- SUS Trip time = ' + repr(sustriptimeset.value) + ' \n'
tptxt += '	- Input Signal = ' + repr(dktinput.value) + ' (where 0 = Fault, 1 = OK) \n'
tptxt += '	- PANIC mode = ' + repr(dktpanic.value) + ' (where 1 = Panic Mode) \n'
tptxt += '	- BYPASS mode = ' + repr(dktpanic.value) + ' (where 1 = Bypass Mode) \n'
tptxt += '	- Reset the DKT '
tr.sectiontext(tptxt)


tr.sectionhead('Step 2: RCG-0510-T Verify DKT initial state')  # ************************************
tr.teststeptxt = '				  REQ		MEASURED \n'
tr.checkPF('DKT State	',1,dktstate.value,0)
tr.checkPF('DKT Bypass Timer',0,dktbptimer.value,0)
tr.checkPF('DKT SEI Trip Timer',30,dktseitriptimer.value,0)
tr.checkPF('DKT SUS Trip Timer',40,dktsustriptimer.value,0)
tr.sectionstep()

tr.teststate.value = 'DKT - Test BYPASS'
time.sleep(2)
tr.sectionhead('Step 3:  RCG-0511-T Test BYPASS Mode') # *********************************************
tr.teststeptxt = 'a) Check timing, state and output \n'
timenow = tr.gpstime.value
dktbypass.value = 1
time.sleep(2)
maxwait = 60
mstate = dktstate.value
while dktbptimer.value and maxwait:
    time.sleep(.5)
    maxwait -= 1
bypt = tr.gpstime.value - timenow
if maxwait <= 0:
    print("Test failed \n")
tr.teststeptxt += '				  REQ		MEASURED \n'
tr.checkPF('Bypass time	',20,bypt,1)
tr.checkPF('DKT State	',2,mstate,0)
tr.checkPF('DAC Outputs	',2020,dacoutput.value,20)

tr.teststeptxt += '\nb) RCG-0512-T Check Bypass Mode Reset \n'
dktbypass.value = 1
time.sleep(5)
dktreset.value = 1
time.sleep(1)
tr.checkPF('DKT State	',1,dktstate.value,0)
tr.checkPF('DKT Bypass Timer',0,dktbptimer.value,0)
tr.sectionstep()

tr.teststate.value = 'DKT - Test PANIC'
time.sleep(2)
tr.sectionhead('Step 4:  Test PANIC Mode') # *********************************************
tr.teststeptxt = 'a) RCG-0513-T Set DKT PANIC = 1 \n'
tr.teststeptxt += '				  REQ		MEASURED \n'
dktpanic.value = 1
time.sleep(1)
tr.checkPF('DKT State	',0,dktstate.value,0)
tr.checkPF('DKT Bypass Timer',0,dktbptimer.value,0)
tr.checkPF('DKT SEI Trip Timer',0,dktseitriptimer.value,0)
tr.checkPF('DKT SUS Trip Timer',0,dktsustriptimer.value,0)
tr.checkPF('DKT Output	',0,dktoutput.value,0)
tr.checkPF('DAC Outputs	',0,dacoutput.value,0)

tr.teststeptxt += '\nb) RCG-0514-T TRY TO CLEAR DKT PANIC w/RESET \n'
dktreset.value = 1
time.sleep(1)
tr.teststeptxt += '				  REQ		MEASURED \n'
tr.checkPF('DKT State	',0,dktstate.value,0)
tr.checkPF('DKT Bypass Timer',0,dktbptimer.value,0)
tr.checkPF('DKT SEI Trip Timer',0,dktseitriptimer.value,0)
tr.checkPF('DKT SUS Trip Timer',0,dktsustriptimer.value,0)
tr.checkPF('DKT Output	',0,dktoutput.value,0)
tr.checkPF('DAC Outputs	',0,dacoutput.value,0)

tr.teststeptxt += '\nc) RCG-0515-T CLEAR DKT PANIC and RESET \n'
dktpanic.value = 0
time.sleep(1)
dktreset.value = 1
time.sleep(1)
tr.teststeptxt += '				  REQ		MEASURED \n'
tr.checkPF('DKT State	',1,dktstate.value,0)
tr.checkPF('DKT Bypass Timer',0,dktbptimer.value,0)
tr.checkPF('DKT SEI Trip Timer',30,dktseitriptimer.value,0)
tr.checkPF('DKT SUS Trip Timer',40,dktsustriptimer.value,0)

tr.teststeptxt += '\nd) RCG-0516-T Force PANIC in BYPASS MODE \n'
dktbypass.value = 1
time.sleep(5)
dktpanic.value = 1
time.sleep(2)
tr.teststeptxt += '				  REQ		MEASURED \n'
tr.checkPF('DKT State	',0,dktstate.value,0)
tr.checkPF('DKT Bypass Timer',0,dktbptimer.value,0)
tr.checkPF('DKT SEI Trip Timer',0,dktseitriptimer.value,0)
tr.checkPF('DKT SUS Trip Timer',0,dktsustriptimer.value,0)
tr.checkPF('DKT Output	',0,dktoutput.value,0)
tr.checkPF('DAC Outputs	',0,dacoutput.value,0)

tr.teststeptxt += '\ne) RCG-0517-T CLEAR DKT PANIC and RESET \n'
dktpanic.value = 0
time.sleep(1)
dktreset.value = 1
time.sleep(1)
tr.teststeptxt += '				  REQ		MEASURED \n'
tr.checkPF('DKT State	',1,dktstate.value,0)
tr.checkPF('DKT Bypass Timer',0,dktbptimer.value,0)
tr.checkPF('DKT SEI Trip Timer',30,dktseitriptimer.value,0)
tr.checkPF('DKT SUS Trip Timer',40,dktsustriptimer.value,0)
tr.sectionstep()

tr.teststate.value = 'DKT - Test FAULT'
time.sleep(2)
tr.sectionhead('Step 5:  Test FAULT Mode') # *********************************************
tr.teststeptxt = 'a) RCG-0518-T Set input to DKT to fault (0) \n'
dktinput.value = 'OFF'
timenow = tr.gpstime.value
time.sleep(1)
maxwait = 80
mstate = dktstate.value
mout = dktoutput.value

tr.teststeptxt += 'b) Verify DKT State, Output and Time to SEI trip \n'
tr.teststeptxt += '				  REQ		MEASURED \n'
while dktseitriptimer.value and maxwait:
    time.sleep(.5)
    maxwait -= 1
seit = tr.gpstime.value - timenow
timenow = tr.gpstime.value
tr.checkPF('DKT State	',3,mstate,0)
tr.checkPF('DKT Output	',1,mout,0)
tr.checkPF('DKT SEI Timeout	',30,seit,1)

tr.teststeptxt += '\nc) RCG-0519-T Attempt a DKT RESET during failure mode and verify RESET NOT accepted \n'
tr.teststeptxt += '				  REQ		MEASURED \n'
dktreset.value = 1;
time.sleep(1)
tr.checkPF('DKT State	',4,dktstate.value,0)
tr.checkPF('DKT Output	',0,dktoutput.value,0)

tr.teststeptxt += '\nd) RCG-0520-T Verify DKT State, Output and Time to SUS trip \n'
tr.teststeptxt += '				  REQ		MEASURED \n'
time.sleep(1)
mstate = dktstate.value
mout = dktoutput.value
maxwait = 100
while dktsustriptimer.value and maxwait:
        time.sleep(.5)
        maxwait -= 1
sust = tr.gpstime.value - timenow
tr.checkPF('DKT State	',4,mstate,0)
tr.checkPF('DKT Output	',0,mout,0)
tr.checkPF('DKT SUS Timeout	',40,sust,1)
tr.checkPF('DAC Outputs	',0,dacoutput.value,0)
tr.sectionstep()

if(tr.teststatus == 0): 
    tr.sectionhead('TEST SUMMARY:  PASS') # *********************************************
else:
    tr.sectionhead('TEST SUMMARY:  FAIL') #*********************************************
tr.sectionstep()

# Cleanup 
# Reset the DAC filter to original settings.
dacFilt0.inputsw('ON')
dacFilt0.offsetsw('OFF')
# Reset DKT Fault input
dktinput.value = 'ON'
time.sleep(1)
# Reset DKT
dktreset.value = 1
print(tr.teststatus)
if tr.teststatus == 0:
    tr.teststate.value = 'DKT - Test Complete - PASS'
    testwordepics.value = 1
else:
    tr.teststate.value = 'DKT - Test Complete - FAIL'
    testwordepics.value = 0
time.sleep(5)
tr.teststate.value = 'TEST SYSTEM - IDLE'
# Pass any test error back to Jenkins
# End of test ******************************************************************
tr.close()
# Print the output of the test report
call(["cat","/tmp/rcgtest/data/dktTest.dox"])
sys.exit(tr.teststatus)

