#!/usr/bin/env python3

from cdstestsupport import testreport
from epics import PV
from epics import caget, caput

import copy
import os
import os.path
import shutil
import sys
import time

test_site = 'tst'
test_ifo = 'x2'
test_model = 'x2atstim16'
FEC = 112

SDF_CONFIRM = 2.0
SDF_CANCEL = 1.0

SDF_INIT_FULL = 0
SDF_INIT_HAVE_NON_INIT = 1
SDF_INIT_HAVE_MISSING = 2
SDF_INIT_HAVE_MISSING_NON_INIT = 3

prmInit = (
    (SDF_INIT_HAVE_MISSING_NON_INIT, "safe.snap is missing some channels and has some extra"),
    (SDF_INIT_HAVE_MISSING, "safe.snap has extra entries"),
    (SDF_INIT_HAVE_NON_INIT, "safe.snap is missing entries"),
    (SDF_INIT_FULL, "safe.snap is nominal") )

SDF_LOAD_EDB_AND_TABLE = 1
SDF_LOAD_TABLE_ONlY = 2
SDF_LOAD_EDB_ONLY = 4

SDF_VALUE_NO_DIFF = 0
SDF_VALUE_ALL_DIFF = 1
SDF_VALUE_SOME_DIFF = 2
SDF_VALUE_PAGE_DIFF = 3

prmValue = ( 
    (SDF_VALUE_NO_DIFF, "No differences"),
    (SDF_VALUE_ALL_DIFF, "All are changed"),
    (SDF_VALUE_SOME_DIFF, "Half are changed"),
    (SDF_VALUE_PAGE_DIFF, "Changes are mult of 40") )

SDF_MONITOR_NONE = 0
SDF_MONITOR_ALL = 1
SDF_MONITOR_SOME = 2
SDF_MONITOR_PAGE = 3

prmMonitorBits = (
    (SDF_MONITOR_NONE, "No chans monitored"),
    (SDF_MONITOR_ALL, "All chans monitored"),
    (SDF_MONITOR_SOME, "Half chans monitored"),
    (SDF_MONITOR_PAGE, "Monitoring mult of 40") )

SDF_TABLE_DIFFS = 0
SDF_TABLE_NOT_FOUND = 1
SDF_TABLE_NOT_INIT = 2
SDF_TABLE_NOT_MON = 3
SDF_TABLE_FULL_TABLE = 4
SDF_TABLE_DISCONNECTED = 5

prmTableSelect = ( (SDF_TABLE_DISCONNECTED, "Disconnected Chans"),
    (SDF_TABLE_DIFFS, "Settings diffs"), 
    (SDF_TABLE_NOT_FOUND, "Chans not found"), 
    (SDF_TABLE_NOT_INIT, "Chans not init"),
    (SDF_TABLE_NOT_MON, "Chans not mon"),
    (SDF_TABLE_FULL_TABLE, "Full table") )

SDF_SORT_SHOW_ALL = 0
SDF_SORT_SUBSTRING = 1

prmSort = ( (SDF_SORT_SHOW_ALL, "Show all", ''), (SDF_SORT_SUBSTRING, "Sort on substring", 'ADC'), (SDF_SORT_SUBSTRING, "Sort on empty substring", ""), (SDF_SORT_SUBSTRING, "Sort on invalid substring", "X2:") )

SDF_MONITOR_MASK = 0
SDF_MONITOR_ALL = 1

prmMonitor = ( (SDF_MONITOR_MASK, "Mask"), (SDF_MONITOR_ALL, "All") )

PAGE_UP = -1
PAGE_DOWN = 1
ENTRIES_PER_PAGE = 40.0

# list of found failures
# If this list is not empty, then the test failed!
failures = []

def fail_test(explanation: str):
    """
    Fail a specific test.
    :param explanation: A string detailing the failure
    :return:
    """
    global failures

    failures.append(explanation)
    print(explanation)

class cycle_options(object):
    def __init__(self, revert=True, accept=True, monitor=True):
        self.can_revert = revert
        self.can_accept = accept
        self.can_monitor = monitor

class set_point(object):
    def __init__(self, name, value=None, monitored=None, initialized=None):
        self.name = name
        self.value = value
        self.monitored = monitored
        self.initialized = initialized

    def is_sdf_clean(self):
        return self.value != None

    def is_fec_clean(self):
        return self.is_sdf_clean() and self.monitored != None and self.initialized != None

    def as_autoburt(self):
        return self.name

    def as_safe(self):
        if self.is_sdf_clean():
            return self.as_sdf()
        value = self.value
        if value == None:
            value = ''
        return "%s 1 %s" % (self.name, value)

    def as_sdf(self):
        if not self.is_sdf_clean():
            return self.as_safe()
        monitored = self.monitored
        if monitored == None:
            monitored = ''
        return "%s 1 %s %s" % (self.name, self.value, monitored)

    def as_fec(self):
        if not self.is_fec_clean():
            return self.as_sdf()
        return "%s 1 %s %s %s" % (self.name, self.value, self.monitored, self.initialized)

class sdf_file(object):
    def __init__(self):
        self.headers = []
        self.alarms = []
        self.setpoints = []
        self.readbacks = []

    def add_header(self, line):
        self.headers.append(line)

    def add_alarm(self, line):
        self.alarms.append(line)

    def add_setpoint(self, name, value=None, monitored=None, initialized=None):
        self.setpoints.append(set_point(name, value, monitored, initialized))

    def add_readback(self, line):
        self.readbacks.append(line)

    def set_monitored(self, name=None, monitored=None):
        for i in range(len(self.setpoints)):
            if self.setpoints[i].name == name or name == None:
                self.setpoints[i].monitored = monitored

    def set_value(self, name=None, value=None):
        for i in range(len(self.setpoints)):
            if self.setpoints[i].name == name or name == None:
                self.setpoints[i].value = value

    def as_autoburt_f(self, f):
        for entry in self.setpoints:
            f.write(entry.as_autoburt() + "\n")

    def as_autoburt(self, fname):
        with open(fname, 'wt') as f:
            self.as_autoburt_f(f)

    def as_sdf_f(self, f):
        for entry in self.setpoints:
            f.write(entry.as_sdf() + "\n")

    def as_sdf(self, fname):
        with open(fname, 'wt') as f:
            self.as_sdf_f(f)

def validate(value, msg=None):
    global sdfLine0
    if not value:
        out_str = 'A internal test condition failed'
        if msg != None:
            out_str = out_str + " " + msg
        fail_test(out_str)
    if not sdfLine0.connected:
        out_str = 'The canary PV has disconnected, it appears EPICS has failed'
        if msg != None:
            out_str = out_str + " " + msg
        fail_test(out_str)

def set_values(mode, sp_list):
    if not mode in (SDF_VALUE_ALL_DIFF, SDF_VALUE_SOME_DIFF, SDF_VALUE_PAGE_DIFF):
        return sp_list
    for i in range(len(sp_list.setpoints)):
        if mode == SDF_VALUE_SOME_DIFF and i % 2 == 0:
            continue
        elif mode == SDF_VALUE_PAGE_DIFF and i >= 80:
            break   
        val = sp_list.setpoints[i].value
        new_val = 10000.02
        if type(val) == type(''):
            if val == 'SHOW ALL':
                new_val = 'SORT ON SUBSTRING'
            elif val == 'SORT ON SUBSTRING':
                new_val = 'SHOW ALL'
            else:
                slen = len(val)
                tval = val.strip()
                if len(tval) == len(val):
                    new_val = val + ' '
                else:
                    new_val = tval
        elif sp_list.setpoints[i].value == new_val:
            new_val = 20000.04
        sp_list.setpoints[i].value = new_val
    return sp_list

def set_monitored(mode, sp_list):
    for i in range(len(sp_list.setpoints)):
        value = '0'
        
        if mode == SDF_MONITOR_ALL:
            value = '1'
        elif mode == SDF_MONITOR_SOME and i % 2 == 0:
            value = '1'
        elif mode == SDF_MONITOR_PAGE and i < 80:
            value = '1'
        sp_list.setpoints[i].monitored = value
    return sp_list

def add_remove_channels(mode, sp_list):
    removed = []
    added = []
    if mode == SDF_INIT_HAVE_NON_INIT or mode == SDF_INIT_HAVE_MISSING_NON_INIT:
        # drop out the first 60 entries
        size_pre = len(sp_list.setpoints)
        for i in range(60):
            if size_pre >= 60:
                removed.append(sp_list.setpoints[i].name)
        sp_list.setpoints = sp_list.setpoints[60:]
        size_post = len(sp_list.setpoints)
        print("Removing entries from the set point list, size pre/post %d/%d" % (size_pre, size_post))
    if mode == SDF_INIT_HAVE_MISSING or mode == SDF_INIT_HAVE_MISSING_NON_INIT:
        # add in missing entries
        for i in range(60):
            ch_name = f'{test_ifo.upper()}:SEI-MISSING_DUMMY_ENTRY_0000%d' % i
            sp_list.add_setpoint(ch_name, '1.0')
            added.append(ch_name)
    return sp_list, removed, added

def get_paths(SITE, IFO, model):
    paths = {}
    paths['targetdir'] = "/opt/rtcds/%s/%s/target/%s" % (SITE, IFO, model)
    paths['epicsdir'] = "%s/%sepics" % (paths['targetdir'], model)
    paths['autoburtfile'] = "%s/autoBurt.req" % paths['epicsdir']
    paths['burtdir'] = "%s/burt" % paths['epicsdir']
    paths['safesnapfile'] = "%s/safe.snap" % paths['burtdir']
    paths['safesnapfilebackup'] = "%s/safe.snap.sdftest_backup" % paths['burtdir']
    paths['tmpsafefile1'] = "%s/sdf_test_tmp_snap1.snap" % paths['burtdir']
    return paths

def load_sdf_or_burt(fname):
    sdf = sdf_file()
    in_header = False
    auto_burt = False
    if fname.endswith('.req'):
        auto_burt = True
    with open(fname, 'rt') as f:
        for line in f:
            line = line.strip()
            if line.startswith('---'):
                in_header = not in_header
                sdf.add_header(line)
            elif in_header:
                sdf.add_header(line)
            elif line.startswith('RO'):
                sdf.add_readback(line)
            else:
                # this may be a set point 
                parts = line.split()
                if parts[0].find('.') > 0:
                    # alarm
                    sdf.add_alarm(line)
                elif len(parts) >= 3:
                    monitored = None
                    initialized = None
                    if len(parts) >= 4:
                        monitored = parts[3]
                    if len(parts) >= 5:
                        initialized = parts[4]
                    sdf.add_setpoint(parts[0], parts[2], monitored, initialized)
                elif len(parts[0]) > 3 and parts[0][2] == ':' and auto_burt:
                    sdf.add_setpoint(parts[0])
                else:
                    print("Unknown LINE #### %s ###" % line)
    return sdf

def cur_page_num():
    global sdfLine0
    return int(sdfLine0.value/ENTRIES_PER_PAGE)

def max_page_num():
    global sdfTableEntries
    entries = sdfTableEntries.value
    if entries > 0.0:
        entries -= 1.0
    return int(entries/ENTRIES_PER_PAGE)

def can_move_page(direction):
    cur = cur_page_num()
    if direction == PAGE_UP:
        #print "can_move_page(UP) - cur=%s - %s" % (str(cur), str(cur > 0.0))
        return cur > 0.0
    max_page = max_page_num()
    #print "can_move_page(DOWN) - cur=%s max=%s - %s" % (str(cur), str(max_page), str(cur < max_page)) 
    return cur < max_page_num()

def do_page_change(direction):
    global sdfPage
    global sdfLine0

    val = int(sdfLine0.value)
    sdfPage.value = direction
    time.sleep(2)
    newVal = int(sdfLine0.value)
    if newVal == val:
        print('no page change line0 = %d' % int(newVal))
    else:
        print('page changed line0 = %d' % int(newVal))
    return not val == newVal

def page_up():
    return do_page_change(PAGE_UP)

def page_down():
    return do_page_change(PAGE_DOWN)

def test_reset_buttons(opts):
    global sdfLine0
    global sdfTableEntries
    global sdfResetChan
    global sdfResetBits
    global sdfTableLock
    global sdfConfirm

    if sdfTableLock.value != 0.0:
        sdfConfirm.value = SDF_CANCEL
        time.sleep(1.0)
    validate(sdfTableLock.value == 0.0, "There are unexpected changes queued for the table, cannot test reset buttons")
    entryCount = int(sdfTableEntries.value)
    if entryCount == 0:
        return
    sdfResetChan.value = 1
    time.sleep(1)
    expect = 0
    if opts.can_revert:
        expect = 2
    validate(int(sdfResetBits[0].value) & 2 == expect, "test failed on pressing the revert button %s %d" % (str(sdfResetBits[0].value), expect))
    
    sdfResetChan.value = 101
    time.sleep(1)
    expect = 0
    if opts.can_accept:
        expect = 4
        validate(int(sdfResetBits[0].value) & 2 == 0, "test failed, pressing the accept button should clear the revert button")
    validate(int(sdfResetBits[0].value) & 4 == expect, "test failed on pressing the accept button %s %d" % (str(sdfResetBits[0].value), expect))


    sdfResetChan.value = 201
    time.sleep(1)
    expect = 0
    if opts.can_monitor:
        expect = 8
    validate(int(sdfResetBits[0].value) & 8 == expect, "test failed on pressing the monitor button %s %d" % (str(sdfResetBits[0].value), expect))


def test_reset_buttons_on_blanks():
    global sdfLine0
    global sdfTableEntries
    global sdfResetChan
    global sdfResetBits
    global sdfTableLock
    global sdfConfirm

    if sdfTableLock.value != 0.0:
        sdfConfirm.value = SDF_CANCEL
        time.sleep(1.0)
    validate(sdfTableLock.value == 0.0, "There are unexpected changes queued for the table, cannot test reset buttons")
    entryCount = int(sdfTableEntries.value)
    line0 = int(sdfLine0.value)
    lastLine = line0 + int(ENTRIES_PER_PAGE) - 1
    print("blank test %d %d %d %d" % (line0, int(ENTRIES_PER_PAGE), lastLine, entryCount))
    if entryCount < lastLine:
        offset = entryCount - line0 + 1
        blank = line0 + offset

    # noisy systems may have changes come and go, if possible test further down the page to avoid false negatives
    # when the selected blank line becomes non-blank between sampling and pushing the button.
    if offset+5 < int(ENTRIES_PER_PAGE):
        offset += 5
        blank += 5
        
        print('Testing the reset buttons on a blank line %d %d %d' % (blank, offset, line0))
        
    sdfResetChan.value = float(offset+1)
    time.sleep(1)
    validate(sdfTableLock.value == 0.0 and sdfResetBits[offset].value == 0.0, "Reset button on a blank line caused the table to lock on revert blank=%d offset=%d lock=%s bits=%s" % (blank, offset, str(sdfTableLock.value), str(sdfResetBits[offset].value)))
        
    sdfResetChan.value = float(offset+101)
    time.sleep(1)
    validate(sdfTableLock.value == 0.0 and sdfResetBits[offset].value == 0.0, "Reset button on a blank line caused the table to lock on accept blank=%d offset=%d" % (blank, offset))
        
    sdfResetChan.value = float(offset+201)
    time.sleep(1)
    validate(sdfTableLock.value == 0.0 and sdfResetBits[offset].value == 0.0, "Reset button on a blank line caused the table to lock on mon blank=%d offset=%d" % (blank, offset))
    print('Done with blank test')

# given a table selection sort mode (SDF_TABLE_...) return a set of values
# that reflect whether the various buttons can be activated
# returns a tuple of booleans for (revert, accept, monitor)
def get_reset_capablities(mode):
    revert = False
    accept = False
    mon = False
    if mode == SDF_TABLE_DIFFS:
        revert = accept = mon = True
    elif mode == SDF_TABLE_NOT_INIT:
        accept = mon = True
    elif mode == SDF_TABLE_NOT_MON or mode == SDF_TABLE_FULL_TABLE:
        revert = mon = accept = True
    return revert, accept, mon

# Return a list of (channel name, sdf screen offset) retreived from matching
# the entries currently displayed on the sdf screen with the input match_list.
# Returns up to sample_count entries (may return 0)
def get_samples_from_sdf_screen(match_list, sample_count):
    global sdfStat

    sample_list = []
    for cur_offset in range(int(ENTRIES_PER_PAGE)):
        if len(sample_list) > sample_count:
            break
        pvName = sdfStat[cur_offset].value.tostring().strip('\x00')
        if pvName == '':
            break
        if not pvName in match_list:
            continue
        sample_list.append([pvName, cur_offset])
    return sample_list

def cycle_through_pages(options):
# start at page 0
    while can_move_page(PAGE_UP):
        validate(page_up(), 'moving to first page')

    test_reset_buttons(options)

    for i in range(1):
        while can_move_page(PAGE_DOWN):
            validate(page_down(), 'cycling down')
        validate(not page_down(), 'cycling down past end')
        test_reset_buttons_on_blanks()
        while can_move_page(PAGE_UP):
            validate(page_up(), 'cycling up')
        validate(not page_up(), 'cycling up before start')

def wait_for_non_zero_table():
    global sdfTableEntries
    global sdfLine0
    count = 0
    while sdfTableEntries.value == 0.0 and sdfLine0.value == 0.0:
        time.sleep(1)
        count += 1
        if count > 30:
            raise Exception("Timeout while waiting for SDF table")

def basic_paging_permutations(init_str, value_str, mon_str):
    global prmTableSelect
    global sdfTableSort

    global sdfWC
    global sdfWCStr

    global sdfMonAll

    for tableSort in prmTableSelect:
        sdfTableSort.value = tableSort[0]
        time.sleep(0.5)

        opts = cycle_options(*get_reset_capablities(tableSort[0]))

        for subSort in prmSort:
            sdfWCStr.value = subSort[2]
            sdfWC.value = subSort[0]
            time.sleep(1)
            
            for monSort in prmMonitor:
                sdfMonAll.value = monSort[0]
                time.sleep(1)
                print()
                print("------ setting up test ------")
                print("Initialization state", init_str)
                print("Value state", value_str)
                print("Monitoring state", mon_str)
                print("Table selection", tableSort[1])
                print("Substring sort", subSort[1], subSort[2])
                print("Monitor", monSort[1])
                print("------ test cycle -------")
                cycle_through_pages(opts)


tr = testreport('sdf_paging', 'Setpoint Monitor Paging Test', FEC)
# Standard Header for all Test Scripts *********************************************
tr.sectionhead('Purpose')
s = ' Verify the operation of SDF. \n'
tr.sectiontext(s)

tr.sectionhead('Test Numbers')
tr.sectiontext('RCG-1515-T')

tr.sectionhead('Overview')
s = '1) Test paging in the SDF system in the face of changes.\n'

s += '\n'
tr.sectiontext(s)

tr.sectionhead('Test Requirements')
s = ' \n'
s += ' \n'
tr.sectiontext(s)

tr.sectionhead('Test Model')
s = 'This test will use the current x2atstim16 model \n'
s += ' \n'
tr.sectiontext(s)
# tr.image('rampmuxmatrixModel')

tr.sectionhead('Related MEDM Screen')
s = 'The related screens are standard SDF_RESTORE and SDF_TABLE medm screens \n'
tr.sectiontext(s)
# tr.image('rampmuxmatrixMedm')
# END Standard Header for all Test Scripts ************************************************************

tr.sectionhead('Test Procedure')
s = '1) Read in the channel list from the safe.snap file\n'
s += '2) Force the PVs to the safe state.\n'
s += '3) Test paging through the list with no modifications\n'
s += '4) Introduce changes to the system and test the ability to page back and forth through change lists\n'

s += '\n'
tr.sectiontext(s)

#STEP 1 ********************************
tr.sectionhead('Step 1: Read in safe.snap and autoBurt.req files')
paths = get_paths(test_site, test_ifo, test_model)

# if it does not exist copy the safe.snap to the backup file
# else if it does exist copy the backup file over the safe.snap file
if not os.path.exists(paths['safesnapfilebackup']):
    shutil.copyfile(paths['safesnapfile'], paths['safesnapfilebackup'])
else:
    shutil.copyfile(paths['safesnapfilebackup'], paths['safesnapfile'])

auto_burt_list = load_sdf_or_burt(paths['autoburtfile'])
s = '\tloaded %d setpoints from %s\n' % (len(auto_burt_list.setpoints), paths['autoburtfile'])
ref_safe_list = load_sdf_or_burt(paths['safesnapfile'])
s += '\tloaded %d setpoints from %s\n' % (len(ref_safe_list.setpoints), paths['safesnapfile'])
s += '\tAs an optimization of the test connect to all the PVs now\n'

tr.teststate.value = 'SDF PAGING TEST'

tr.teststeptxt = s
tr.sectionstep()

PVs={}

# file loading pvs
sdfFileLoad = PV(f"{test_ifo.upper()}:FEC-{FEC}_SDF_RELOAD")
sdfName = PV(f'{test_ifo.upper()}:FEC-{FEC}_SDF_NAME')
sdfNameLoaded = PV(f'{test_ifo.upper()}:FEC-{FEC}_SDF_LOADED')
sdfEpicsLoaded = PV(f'{test_ifo.upper()}:FEC-{FEC}_SDF_LOADED_EDB')
sdfConfirm = PV(f'{test_ifo.upper()}:FEC-{FEC}_SDF_CONFIRM')

# paging related pvs
sdfPage = PV(f'{test_ifo.upper()}:FEC-{FEC}_SDF_PAGE')
sdfLine0 = PV(f'{test_ifo.upper()}:FEC-{FEC}_SDF_LINE_0')
sdfTableEntries = PV(f'{test_ifo.upper()}:FEC-{FEC}_SDF_TABLE_ENTRIES')

# sorting related pvs
sdfTableSort = PV(f'{test_ifo.upper()}:FEC-{FEC}_SDF_SORT')

# sort substring related pvs
sdfWC = PV(f'{test_ifo.upper()}:FEC-{FEC}_SDF_WILDCARD')
sdfWCStr = PV(f'{test_ifo.upper()}:FEC-{FEC}_SDF_WC_STR')

# monitor mask related pvs
sdfMonAll = PV(f'{test_ifo.upper()}:FEC-{FEC}_SDF_MON_ALL')

# revert/accept/mon button
sdfResetChan = PV(f'{test_ifo.upper()}:FEC-{FEC}_SDF_RESET_CHAN')
sdfResetBits = []
for i in range(int(ENTRIES_PER_PAGE)):
    sdfResetBits.append(PV(f'{test_ifo.upper()}:FEC-{FEC}_SDF_BITS_%d' % i))
sdfTableLock = PV(f'{test_ifo.upper()}:FEC-{FEC}_SDF_TABLE_LOCK')

sdfStat = []
for i in range(int(ENTRIES_PER_PAGE)):
    sdfStat.append(PV(f'{test_ifo.upper()}:FEC-{FEC}_SDF_SP_STAT%d' % i))

# grab all the other channels
for item in auto_burt_list.setpoints:
    PVs[item.name] = PV(item.name)


# debugging output for lists
#for item in auto_burt_list.setpoints:
#    print "auto", item.as_autoburt()
#
#for item in ref_safe_list.setpoints:
#    print "safe", item.as_sdf()


#STEP 2 ******************************
tr.sectionhead('Step 2: Force into a safe state with monitoring on everything')




init_save_list = copy.deepcopy(ref_safe_list)
init_save_list, removed_list, added_list = add_remove_channels(SDF_INIT_HAVE_MISSING_NON_INIT, init_save_list)

init_save_list.as_sdf(paths['safesnapfile'])
#os.system('/usr/bin/ssh controls@x1seiex /opt/rtcds/tst/x1/scripts/startx1seibsctim04')
#time.sleep(10)

#sdfTableSort.value = SDF_TABLE_NOT_INIT
#time.sleep(1)

#print 'Removed list'
#print removed_list

#init_save_list = get_samples_from_sdf_screen(removed_list, 5)
#validate(len(init_save_list) > 0, "could not find any uninitialized values to test")
#print init_save_list

#for entry in init_save_list:
#    sdfResetChan.value = 101 + entry[1]
#    time.sleep(1)

#sdfName.value = os.path.splitext(os.path.basename(paths['tmpsafefile1']))[0]
#sdfNameLoaded.value = os.path.splitext(os.path.basename(paths['tmpsafefile1']))[0]
#time.sleep(1)
#print 'Pushing confirm button - sending a 2.0'
#sdfConfirm.value = SDF_CONFIRM
#time.sleep(2)
#print sdfConfirm
#print sdfConfirm.value

#print 'reading in saved list from %s' % paths['tmpsafefile1']
#init_results = load_sdf_or_burt(paths['tmpsafefile1'])

#for i in range(len(init_save_list)):
#    match = False
#    for entry in init_results.setpoints:
#        if entry.name == init_save_list[i][0]:
#            match = True
#            valPv = PV(entry.name)
#            print "%s = %s" % (entry.name, str(entry.value))
#            break
#    validate(match, 'Could not find initialized value in sdf file, did not really accept the initialization')

init_str = ''
for initEntry in prmInit:
    init_str = initEntry[1]

    running_safe_list = copy.deepcopy(ref_safe_list)
    running_safe_list, removed_list, added_list = add_remove_channels(initEntry[0], running_safe_list)

    print("removed: %s" % str(removed_list))
    print("added: %s" % str(added_list))

    for valueEntry in prmValue:
        value_str = valueEntry[1]

        value_list = copy.deepcopy(running_safe_list)
        value_list = set_values(valueEntry[0], value_list)
    
        value_list.as_sdf(paths['tmpsafefile1'])
        sdfName.value = os.path.splitext(os.path.basename(paths['tmpsafefile1']))[0]
        time.sleep(1)
        sdfFileLoad.value = SDF_LOAD_EDB_AND_TABLE
        time.sleep(3.0)
        print('Loaded sdf file')

        sdfConfirm.value = SDF_CANCEL
        time.sleep(1.0)

        for monEntry in prmMonitorBits:
            mon_str = monEntry[1]

            test_list = copy.deepcopy(value_list)
            test_list = set_monitored(monEntry[0], test_list)

            test_list.as_sdf(paths['tmpsafefile1'])
            sdfName.value = os.path.splitext(os.path.basename(paths['tmpsafefile1']))[0]
            time.sleep(1)
            sdfFileLoad.value = SDF_LOAD_EDB_AND_TABLE #SDF_LOAD_TABLE_ONlY
            print('loading sdf file, table only')
            time.sleep(3.0) 
        
            basic_paging_permutations(init_str, value_str, mon_str)
shutil.copyfile(paths['safesnapfilebackup'], paths['safesnapfile'])
os.unlink(paths['safesnapfilebackup'])

tr.teststate.value = 'TEST SYSTEM - IDLE'

if len(failures) > 0:
    print()
    print("Some failures occurred")
    print("\n".join(failures))
    print()
    tr.teststatus = 1
    tr.teststeptxt = "RCG-1515-T Failed"
else:
    tr.teststeptxt = "RCG-1515-T Passed"

tr.sectionstep()
tr.close()

sys.exit(tr.teststatus)


