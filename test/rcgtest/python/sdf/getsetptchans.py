#!/usr/bin/env python
# Automated test support classes

import epics
import time
import string
import sys
import os

from epics import PV
from subprocess import call
import argparse

parser = argparse.ArgumentParser(description='Check target sources against userapp sources')
parser.add_argument("target")
parser.add_argument("fbdir")
args = parser.parse_args()

print args.target

sdffiles=[]

mytargetdir = args.target
mytargetdir += "/"
myfile = mytargetdir
myfile += args.fbdir
myfile += '/master'
print 'opening file ',myfile

f=open(myfile,"r")

for line in f:
        if ".ini" in line and "#" not in line:
                #print line
                word = line.split("/")
                model = word[7].split('.',1)
                mymodel = model[0].lower()
                safefile = mytargetdir
                safefile += mymodel
                safefile += "/"
                safefile += mymodel
                safefile += "epics/burt/safe.snap"
                if os.path.exists(safefile):
                        sdffiles.append(safefile)
                else:
                        print "NOT Found ",safefile
f.close()

#statname_month_day_year_hr_min_sec
mymonth = time.strftime("%m")
myday = time.strftime("%d")
myyear = time.strftime("%Y")
myhour = time.strftime("%H")
mymin = time.strftime("%M")
mysec = time.strftime("%S")
mynewfile = myyear + '_' + mymonth  + '_' + myday  + '_' + myhour  + '_' + mymin  + '_' + mysec 
print 'Filename is ',mynewfile
myout=open("./totalchans.snap","w")
for sdffile in sdffiles:
        print sdffile
        mystart = 0
        f=open(sdffile,"r")
        for line in f:
                if mystart:
                        word = line.split()
                        myline = word[0] + "\n"
                        if "RO" not in myline and "." not in myline:
                                #tmp = PV(word[0])
                                #myval = tmp.get()
                                #print tmp.type
                                #wline = word[0] + '\t' + tmp.type + '\t' + repr(myval) + '\n'
                                myout.write(line)
                if "End BURT header" in line:
                        mystart = 1
        f.close()
myout.close()

