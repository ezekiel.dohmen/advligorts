#!/usr/bin/env python
# Automated test support classes

import epics
import time
import string
import sys
import os

import nds2
from epics import PV
from subprocess import call
import argparse

parser = argparse.ArgumentParser(description='Check target sources against userapp sources')
parser.add_argument("target")
parser.add_argument("fbdir")
args = parser.parse_args()

print args.target

sdffiles=[]

mytargetdir = args.target
mytargetdir += "/"
myfile = mytargetdir
myfile += args.fbdir
myfile += '/master'
print 'opening file ',myfile

f=open(myfile,"r")

for line in f:
	if ".ini" in line and "#" not in line:
		#print line
		word = line.split("/")
		model = word[7].split('.',1)
		mymodel = model[0].lower()
		safefile = mytargetdir
		safefile += mymodel
		safefile += "/"
		safefile += mymodel
		safefile += "epics/burt/safe.snap"
		if os.path.exists(safefile):
			print "Found ",safefile
			sdffiles.append(safefile)
		else:
			print "NOT Found ",safefile
f.close()
for model in sdffiles:
	print model
