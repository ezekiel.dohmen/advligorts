#!/usr/bin/perl -w 
##
##

my %tests = (
	'daqfilter' 	=> {
		'name' 		=> 'daq filter',
		'command_path' 	=> 'perl/daqfilters/daqFiltTest.pl',
		},
	'decfilter' 	=> {
		'name' 		=> 'decimation filter',
		'command_path' 	=> 'perl/decfilters/decFiltTest.pl',
             	},
       	'dackilltimed' 	=> {
		'name' 		=> 'DACKILL TIMED',
		'command_path' 	=> 'python/dackill/dackilltimed.py',
		},
        'duotone' => {
        	'name' 		=> 'duotone',
		'command_path' 	=> 'perl/duotone/duotoneTest.pl',
		},
        'matrix' => {
		'name' 		=> 'matrix',
		'command_path' 	=> 'perl/matrix.pl',
		},
	'simparts' => {
		'name' 		=> 'Simulink parts',
		'command_path' 	=> 'perl/simPartsTest.pl',
		},
	'osc' => {
		'name' 		=> 'oscillator part',
		'command_path' 	=> 'perl/osc/oscTest.pl',
		},
	'fmc' => {
		'name' 		=> 'filter module w/control',
		'command_path' 	=> 'perl/fmc/fmctest.pl',
		},
	'fmc2' => {
		'name' 		=> 'filter module w/control 2',
		'command_path' 	=> 'perl/fmc2/fmc2test.pl',
		},
	'sfm' => {
		'name' 		=> 'standard IIR filter',
		'command_path' 	=> 'perl/sfm/sfmTest.pl',
		},
	'inputfilter' => {
		'name' 		=> 'input filter',
		'command_path' 	=> 'perl/inputfilter/inputfilterTest.pl',
		},
	'ioptest' => {
        	'name' 		=> 'IOP',
		'command_path' 	=> 'perl/rcgcore/ioptest.pl',
		},
	'dac' => {
		'name' 		=> 'DAC',
		'command_path' 	=> 'python/dactest/dactest.py',
		},
	'epics_in_ctl' => {
		'name' 		=> 'EPICS INPUT W/Control',
		'command_path' 	=> 'python/epicsparts/epicsInCtrl.py',
		},
	'exctp' => {
		'name' 		=> 'EXC/TP',
		'command_path' 	=> 'python/exctp/extpTest.py',
		},
	'rampmuxmatrix' => {
		'name' 		=> 'RampMuxMatrix',
		'command_path' 	=> 'python/rampmuxmatrix/rampmuxmatrix.py',
		},
	'phase' => {
		'name' 		=> 'Phase part',
		'command_path' 	=> 'python/phase/phase.py',
		},
	'binaryIO' => {
		'name' 		=> 'binary IO',
		'command_path' 	=> 'python/binaryIO/biotest.py',
		},
	'sdf_paging' => {
		'name'		=> 'SDF Paging',
		'command_path'  => 'python/sdf/sdftest_ca-sdf.py'
	},
);

foreach $testname (keys %tests) {
	$tests{$testname}{'result'} = 'NOT-TESTED';
}

sub run_test {
	my ($testname, ) = @_;
	my $name = $tests{$testname}{'name'};
	my $command_path = "$rcgdir/" . $tests{$testname}{'command_path'};

	print "Starting $name test\n";
	system("$command_path $rcgtestdir");
	if ($?) {
	    $tests{$testname}{'result'} = 'FAIL';
	} else {
	    $tests{$testname}{'result'} = 'PASS';
	}
}

# Test groups

sub testIO {
	run_test('ioptest');

	sleep(60);

	run_test('binaryIO');
	run_test('dac');
	run_test('duotone');
}

sub matrixtest {
	run_test('matrix');
	run_test('rampmuxmatrix');
}

sub testfilters() {
	run_test('decfilter');
	sleep(5);
	run_test('daqfilter');
	sleep(5);
	run_test('fmc2');
	sleep(5);
	run_test('fmc');
	sleep(5);
	run_test('inputfilter');
	sleep(5);
	run_test('sfm');
}

sub miscparts {
	run_test('dackilltimed');
	run_test('osc');
	run_test('phase');
	run_test('exctp');
	run_test('epics_in_ctl');
	run_test('simparts');
}

sub sdftests {
	run_test('sdf_paging');
}

sub all {
	testIO();
	sleep(5);
	testfilters();
	sleep(5);
	matrixtest();
	sleep(5);
	miscparts();
	# sleep(5);
	# sdftests();
}

my $test2run = $ARGV[0];

# when true, fork a process to handle
# tests while the main process quits
my $fork_flag=1;

if (@ARGV > 1)
{
	my $flag = $ARGV[1];
	print "flag = $flag\n";

	# wait for tests to finish (dont fork).  Scripts want this, but medms don't
	if($flag eq "-w")
	{
		$fork_flag=0;
	}
}

$rcgdir = $ENV{'RCG_DIR'};
$rcgdir .="/test/rcgtest";

$rcgtestdir = "/tmp/rcgtest";
$rcgtestdatadir = $rcgtestdir . "/data";
$rcgtestimagesdir = $rcgtestdir . "/images";
$rcgtesttmpdir = $rcgtestdir . "/tmp";
$rcgtestdocdir = $rcgtestdir . "/doc";
$rcgtestlatexdir = $rcgtestdocdir . "/latex";

#Create test data directories if they do not exist
unless( -e $rcgtestdir or mkdir $rcgtestdir) {
    die "unable to create test results directories\n";
}
unless( -e $rcgtestdatadir or mkdir $rcgtestdatadir) {
    die "unable to create test data directories\n";
}
unless( -e $rcgtestimagesdir or mkdir $rcgtestimagesdir) {
    die "unable to create test images directories\n";
}
unless( -e $rcgtesttmpdir or mkdir $rcgtesttmpdir) {
    die "unable to create test tmp directories\n";
}
unless( -e $rcgtestdocdir or mkdir $rcgtestdocdir) {
    die "unable to create test doc directories\n";
}
unless( -e $rcgtestlatexdir or mkdir $rcgtestlatexdir) {
    die "unable to create test doc directories\n";
}

if ($fork_flag)
{
    # run in the background
    print "running test in background\n";
    my $pid = fork;
    exit if $pid;
}
else
{
    print "waiting for test to complete\n";
}
print "running $test2run\n";
#Determine which test to run
for($test2run)
{
    print "Preparing to run $test2run test(s)\n";
    if($_ eq "testIO") {
		testIO();
    }
    elsif($_ eq "matrixtest") {
		matrixtest();
    }
    elsif($_ eq "testfilters") {
		testfilters();
    }
    elsif($_ eq "miscparts") {
		miscparts();
    }
    elsif($_ eq "sdftests") {
    		sdftests();
    }
    elsif($_ eq "all") {
    	all();
    }
    else {
    	if (exists $tests{$test2run}) {
    		run_test($test2run);
    	}
    	else
    	{
        	print "Unknown test\n";
        }
    }

}

$test_summary_path = "$rcgdir/doc/test_summary.txt";
print "writing test summary to $test_summary_path\n";
open(SUM,">",$test_summary_path) || die "Could not open $test_summary_path: $!\n";
$time = localtime();
print SUM "RCG Test output summary $time\n";
foreach $testname (keys %tests) {
    my $name = $tests{$testname}{'name'};
    my $result = $tests{$testname}{'result'};
    print SUM "$name, $result\n";
}
close(SUM)
