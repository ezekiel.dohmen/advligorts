import subprocess
import sys
import os.path as path

base_path = "/var/opt/rcgtest"

def open_log():
    """
    Open the log file for appending
    :return: None
    """
    global base_path
    log_file = path.join(base_path, "last_test.log")
    return open(log_file, "at")

def clear_log():
    """
    Erase existing log file
    :return: None
    """
    log_file = path.join(base_path, "last_test.log")
    with open(log_file, "wt") as f:
        pass

def append_to_log(line):
    """
    Append some text to the log file
    :param line: A string
    :return: None
    """
    with open_log() as f:
        f.write(line + "\n")

def log(line):
    """
    Log a line to the last log file
    :param line: A string.  Newline will be added.
    :return: None
    """
    print(line)
    append_to_log(line)


def error(msg):
    """
    Display and log an error message
    :param msg:
    :return:
    """
    line = "*** ERROR: " + msg
    append_to_log(line)
    sys.stderr.write(line + "\n")


def run_log(*args, **kwargs):
    """
    Log the output of a call to subprocess.run

    :param args: passed to subprocess.run
    :param kwargs: passed to subprocess.run
    :return: return of subprocess.run
    """
    return subprocess.run(stdout=open_log(),stderr=open_log(), *args, **kwargs)


def system_run_log(args, **kwargs):
    """
    Log the output of a call to subprocess.run
    Run in a login shell to get out of the python environment

    :param args: a list passed to subprocess.run
    :param kwargs: passed to subprocess.run
    :return: return of subprocess.run
    """
    command = f"/bin/bash -l -c '{' '.join(args)}'"
    return subprocess.run(command, stdout=open_log(), stderr=open_log(), shell=True, **kwargs)
