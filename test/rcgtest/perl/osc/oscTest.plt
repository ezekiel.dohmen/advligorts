set term png
unset logscale; set logscale y
set title "Power Spectrum"
set xlabel 'Frequency (Hz)'
set ylabel 'Magnitude '
set style line 100 lt 1 lc rgb "gray" lw 2
set style line 101 lt 1 lc rgb "gray" lw 1
set grid xtics ls 100
set grid ytics ls 100
set output "/tmp/rcgtest/images/osc10.png"
plot "/tmp/rcgtest/tmp/osc10reference.data" using 1:2 smooth unique title "Reference Data", "/tmp/rcgtest/tmp/osc10.data" using 1:2 smooth unique title "Test Data"
set output "/tmp/rcgtest/images/osc2000.png"
plot "/tmp/rcgtest/tmp/osc2000reference.data" using 1:2 smooth unique title "Reference Data", "/tmp/rcgtest/tmp/osc2000.data" using 1:2 smooth unique title "Test Data"
set output "/tmp/rcgtest/images/osc6500.png"
plot "/tmp/rcgtest/tmp/osc6500reference.data" using 1:2 smooth unique title "Reference Data", "/tmp/rcgtest/tmp/osc6500.data" using 1:2 smooth unique title "Test Data"

