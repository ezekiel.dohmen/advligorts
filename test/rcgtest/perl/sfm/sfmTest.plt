set term png
set title "Transfer Function"
set xlabel 'Frequency (Hz)'
set ylabel 'Magnitude '
set style line 100 lt 1 lc rgb "gray" lw 2
set style line 101 lt 1 lc rgb "gray" lw 1
set grid xtics ls 100
set grid ytics ls 100

set output "/tmp/rcgtest/images/sfmLP200.png"
plot "/tmp/rcgtest/tmp/sfmLP200.data" using 1:4 smooth unique title "Test Data", "/tmp/rcgtest/tmp/sfmLP200reference.data" using 1:4 smooth unique title "Reference Data"
set output "/tmp/rcgtest/images/sfmNotch.png"
plot "/tmp/rcgtest/tmp/sfmNotch.data" using 1:4 smooth unique title "Test Data", "/tmp/rcgtest/tmp/sfmNotchReference.data" using 1:4 smooth unique title "Reference Data"
set output "/tmp/rcgtest/images/sfm3030.png"
plot "/tmp/rcgtest/tmp/sfm3030.data" using 1:4 smooth unique title "Test Data", "/tst/rcgtest/tmp/sfm3030Reference.data" using 1:4 smooth unique title "Reference Data"
set output "/tmp/rcgtest/images/sfmEL50.png"
plot "/tmp/rcgtest/tmp/sfmEL50.data" using 1:4 smooth unique title "Test Data", "/tmp/rcgtest/tmp/sfmEL50Reference.data" using 1:4 smooth unique title "Reference Data"

set title "Time Series"
set xlabel 'Cycle Count'
set ylabel 'Magnitude (Counts)'
set output "/tmp/rcgtest/images/sfmRamp.png"
plot "/tmp/rcgtest/tmp/rampReference.data" using 1:2 smooth unique title "Reference Data", "/tmp/rcgtest/tmp/ramp.data" using 1:2 smooth unique title "Test Data"
