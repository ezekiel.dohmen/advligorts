################################################################
# EPICS Channel Access
#
# epSwitch - Toggles switches , requires a base name, which switch and ON/OFF/
# TOGGLE in that order. Returns 1 for success and 0 for failure.
#     Example:
#       my @switch = ("C1:LSC-MICH_CORR","FM2","ON");
#       my $val = epSwitch(@switch);
#
# epStep - Steps up/down the channels given to the routine. 
#  Example:
#     my @rcrds = ("C1:LSC-GAIN1", "C1:LSC-GAIN2");
#     my @stepsizes = (1.0, "+3dB")
#     my $numofsteps = 4
#     my @vals = epStep(@rcrds,@stepsizes,$numofsteps);
#
# epServo - Servos a channel. Applies a simple integrator(pole at zero).
# Example:
# my $chan = "C1:LSC-CHAN";
# my $option = "-t 10"; 
# my $isok = epServo($chan,$option);
#     
#      The options are as follows :- 
#       -r 'readback': readback (error) channel
#       -g 'gain' : gain between readback and channel
#       -s 'value' : set value
#       -f 'ugf' : unity gain frequency (Hz)
#       -t 'duration' : timeout (sec)
#       -c 'channel' : 2nd control channel (common)
#       -d 'channel' : 2nd control channel (differential)
#       -h : help
#
#
# returns 1 for success and 0 for failure. Also the option part is optional.
#
# epRead - returns values for a list of record names
#   the last value in the list indicates success
#   Example:
#     my @rcrds = ("C1:LSC-PD1_DC_TRAMP", "C1:LSC-PD1_DC_GAIN");
#     my @vals = epRead(@rcrds);
#     my $isok = pop(@vals);
# 
# epWrite - returns success
#   arguments are a list of names, and a list of values
#   Example:
#     my @rcrds = ("C1:LSC-PD1_DC_TRAMP", "C1:LSC-PD1_DC_GAIN");
#     my @vals = (5, 1);
#     my $isok = epWrite(@rcrds, @vals);
# 
# epSave - save the values of argument records for later restore
#   arguments and returns are as epRead
#   (see also epRestore)
#
# epRestore - restore record values saved with epSave
#
# epTestBit - test whether a bit is set.  Returns a 0 or 1.
#   Example:
#   my $val = epTestBit("C1:LSC-LA_PARAM_INT_06", 7)
#
# epSetBit  - set a bit.  returns 1 if successful.
#   Example:
#   my $val = epSetBit("C1:LSC-LA_PARAM_INT_06", 4)
# 
# epResetBit - reset a bit.  returns 1 if successful.
#   Example:
#   my $val = epResetBit("C1:LSC-LA_PARAM_INT_06", 3)
#
################################################################

package EpicsTools;
use Exporter;
@ISA = ('Exporter');
@EXPORT = qw(&epSwitch &epStep &epRead &epWrite &epSave &epRestore &epTestBit &epSetBit &epResetBit &epServo);

# require careful code
use strict;

# define environment
# my $gdsdir = "/cvs/cds/gds/gds/bin";
# my $read = "$gdsdir/ezcaread";
# my $write = "$gdsdir/ezcawrite";

# Use whatever the ENVIRONMENT provides us
#my $gdsdir = "/cvs/cds/gds/gds/bin";
my $read = "ezcaread";
my $write = "ezcawrite";
my $switch = "ezcaswitch";
my $step = "ezcastep";
my $servo = "ezcaservo";

# records with saved states
my @saved_names;
my @saved_vals;

################################ parse_reply
sub parse_reply {
    my $rply = $_[0];
    my $cmd = $_[1];
    my $rcrd = $_[2];
    my $val = $_[3];
    my @rtn = (0, 0, 0);
    my @vals;

    chop($rply);
    @vals = split(" = ", $rply);

    if (@vals != 2) {
	if (@_ > 2) {
	    print "ERROR: $cmd failed for $rcrd.  Response was:\n";
	    print "$rply\n";
	} else {
	    print "ERROR: $cmd failed.  Response was:\n";
	    print "$rply\n";
	}
    } elsif (@_ > 2 && $rcrd ne $vals[0]) {
	print "ERROR: $cmd failed for $rcrd.  Response was:\n";
	print "$rply\n";
    } elsif (@_ > 3 && $val != $vals[1]) {
	print "ERROR: $cmd failed for $rcrd.  Response was:\n";
	print "$rply\n";
    } else {
      $rtn[0] = 1;
      $rtn[1] = $vals[1];
      $rtn[2] = $vals[0];
    }

    return @rtn;
}

################################ parse_switchreply
sub parse_switchreply {
    my $rply = $_[0];
    my $what = $_[1];
    my @rtn = (0, 0, 0);
    my @vals;

    chop($rply);
    @vals = split("=",$rply);

    if(@vals)
    {
	if($vals[0] =~ "Usage:")
	{
	    print "ERROR:$what failed. Response was:\n";
	    print "$rply\n";
	}
	elsif($vals[0] =~ "accessible")
	{
	    print "ERROR: No such channel found, response was:\n";
	    print "$rply\n";
	}

    }
    else
    {
	$rtn[0] = 1;
	$rtn[1] = $vals[1];
	$rtn[2] = $vals[0];
    }

    return @rtn;
}


################################ epServo
sub epServo {
    my $str; 
    $str = $_[0];
    if(@_ > 1)
    {
	$str = "$str $_[1]";
    }
    
    my $out = `$servo $str`;
    my @rply = parse_swithreply($out,$servo);
    if(!$rply[0])
    {
	return(0);
    }
    return(1);
}

################################ epSwitch
sub epSwitch {
    my $rcrd;
    my $button;
    my $action;
    my $str;
    my @rply;
    
    $rcrd = $_[0];
    $button = $_[1];
    $action = $_[2];

    $str = `$switch $rcrd $button $action`;

    @rply = parse_switchreply($str, $switch);
    
    if(!$rply[0])
    {
	return (0);
    }
    
    return (1);
}

################################ Catch the Cntrl-C
$SIG{INT} = sub {

    my $out = `ps -ef | grep ezcastep`;
    my @words = split(" ",$out);
    $out = $words[1];
    #my $temp = `kill $out`;
    print "\nprocess id $out killed\n";
};
    


################################ epStep
sub epStep {
    
    my @stuff;
    my $numofsteps;
    my $str;
    my $i;

    $numofsteps = pop(@_);
    my $len = @_;
    if($_[$len/2] < 0)
    {
	$str = "$_[0] +$_[$len/2],$numofsteps";
    }
    else
    {
	$str = "$_[0] $_[$len/2],$numofsteps";
    }

    for($i = 1; $i < $len/2 ; $i++)
    {
	if($_[$i+$len/2] < 0)
	{
	    $str = "$str $_[$i] +$_[$i+$len/2]";
	}
	else
	{
	    $str = "$str $_[$i] $_[$i+$len/2]";
	}

    }

    my $out = `$step $str`;
    my @rply = parse_switchreply($out, $step);
    
    if(!$rply[0])
    {
	return (0);
    }
    
    return (1);

}

################################ epRead
sub epRead {
    my @vals;
    my @rply;
    my $str;

    foreach my $rcrd (@_) {
	$str = `$read $rcrd`;
	if ($?) { return (0); }
	@rply = parse_reply($str, "read", $rcrd);
	if (!$rply[0]) {
	    push(@vals, 0);
	    return @vals;
	}
	push(@vals, $rply[1]);
    }

    push(@vals, 1);
    return @vals;
}

################################ epWrite
sub epWrite {
    my $n = int(@_ / 2);
    my @rply;
    my $str;

    for (my $i = 0; $i < $n; $i++) {
	$str = `$write $_[$i] $_[$i + $n]`;
	@rply = parse_reply($str, "write", $_[$i], $_[$i + $n]);
	if (!$rply[0])
	{ return 0; }
    }
    return 1;
}

################################ epSave
sub epSave {
    my @vals = epRead(@_);
    my $isok = pop(@vals);

    # add to saved list
    if( $isok ) {
	push(@saved_names, @_);
	push(@saved_vals, @vals);
    }

    # return
    push(@vals, $isok);
    return @vals;
}

################################ epRestore
sub epRestore {
    return epWrite(@saved_names, @saved_vals);
}

sub epTestBit {
    my $rcrd;
    my $bit;
    my @rply;
    my $str;


    $rcrd = $_[0];
    $bit  = $_[1];

    $str = `$read $rcrd`;
    @rply = parse_reply($str, "read", $rcrd);

    if(!$rply[0])
    {return "epTestBit: FAILED";}

    return (($rply[1] & (1 << $bit)) >> $bit);

}


sub epSetBit {
    my $rcrd;
    my $bit;
    my $bin;
    my @rply;
    my $str;


    $rcrd = $_[0];
    $bit  = $_[1];


    $bin = 1 << $bit;
    $str = `$write -i -r $rcrd -l OR $rcrd $bin`; 

    chop($str);
    @rply = split(' ',$str);

    # crudely Parse the reply (parse_reply does not work for logical operations)

    if (($rply[0] ne $rcrd))
    {return 0;}
    
    return 1;

}


sub epResetBit {
    my $rcrd;
    my $bit;
    my $bin;
    my @rply;
    my $str;


    $rcrd = $_[0];
    $bit  = $_[1];

    $bin = ~(1 << $bit);
    $str = `$write -i -r $rcrd -l AND $rcrd $bin`;    

    chop($str);
    @rply = split(' ',$str);

    # crudely Parse the reply (parse_reply does not work for logical operations)

    if (($rply[0] ne $rcrd))
    {return 0;}
    
    return 1;

}


################################ end properly
1;

