################################################################
# TDS Tools
#
# tdsAvg - returns values for a list of channels
#   the last value in the list indicates success
#   Example:
#     my $time = 2;
#     my @rcrds = ("C1:LSC-PD1_DC_INMON", "C1:LSC-PD1_DC_OUT16");
#
#     my @vals = tdsAvg($time, @rcrds);
#     my $isok = pop(@vals);
# 
# tdsSine - starts an excitation, returns pid of tdssine and fid of pipe
#   Example: (see tdsDemod example)
#
# tdsStop - stop an excitation, returns nothing
#   Example: (see tdsDemod example)
#
# tdsDemod - returns demodulation values for a list of channels
#   Example:
#     my $freq = $ARGV[0];
#     my $amp  = $ARGV[1];
#     my $ncyc = $ARGV[2];
#     my $navg = $ARGV[3];
#     my $base = $ARGV[4];
#     my $time = 2 * $ncyc * $navg / $freq;
#     my @rcrds = ($base . "_EXC", $base . "_IN1", $base . "_IN2");
#
#     # start excitation
#     my ($pid, $fid) = tdsSine($freq, $amp, $base . "_EXC", $time);
#     $pid or die;
#
#     # demodulate
#     my @vals = tdsDemod($freq, $ncyc, $navg, @rcrds);
#     pop(@vals) or die;
#
#     # stop excitation
#     tdsStop($pid, $fid);
#
#     # evaluate results
#     my @rOL = tdsRatio($vals[2], $vals[1]);
#     my @rCL = tdsRatio($vals[0], $vals[2]);
#     my @rIN = tdsRatio($vals[0], $vals[1]);
#
# tdsRatio - returns the ratio of two tdsDemod result strings
#   Example: (see tdsDemod example)
#
# tdsRead - returns the values for a list of channels
#   the last vule in the list indicates succes.
#   Example:
#     my @rcrds = ("C1:SUS-MC1_ASCPIT_GAIN", "C1:SUS-MC1_ASCYAW_GAIN");
#
#     my @vals = tdsRead(@rcrds);
#     my $isok = pop(@vals);
#
# tdsWrite - writes the values for a list of channels
#   will return 1 if succesful.
#   Example:
#     my $rcrd1 = ("C1:SUS-MC1_ASCPIT_GAIN", 3);
#     my $rcrd2 = ("C1:SUS-MC2_ASCPIT_GAIN", 4);
#
#     my @vals = tdsWrite($rcrd1, $rcrd2);
#     my $isok = pop(@vals);
#
################################################################
package TdsTools;
use Exporter;
@ISA = ('Exporter');
@EXPORT = qw(&tdsAvg &tdsSine &tdsStop &tdsDemod &tdsRatio &tdsRead &tdsWrite);

# require careful code
use strict;

# define environment
my $tdsdir = "";

# Use the whatever the ENVIRONMENT gives us, so this can run
# across multiple operating systems.
my $tdsavg   = "tdsavg";
my $tdsdmd   = "tdsdmd";
my $tdssine  = "tdssine";
my $tdsread  = "tdsread";
my $tdswrite = "tdswrite";

################################ tdsAvg
sub tdsAvg {
    my @vals;
    my $str;
    my $nvals;

    # call tdsAvg
    my $cmd = "$tdsavg @_";
    $str = `$cmd`;
    chop($str);
    @vals = split("\n", $str);

    # check return values
    $nvals = scalar(@vals);
    if( ($nvals != $#_) || $? ) {
	print "$str\n";
	return 0;
    }

    push(@vals, 1);
    return @vals;
}

################################ tdsSine
sub tdsSine {
    my $pid;
    my $fid;
    my $str;

    $pid = open($fid, "$tdssine @_ |");
    $str = <$fid>;
    if( !($str =~ m/START/) ) {
	print "ERROR: tdsSine failed to start: $str";
	return 0;
    }

    # return pid
    return ($pid, $fid);
}

################################ tdsStop
sub tdsStop {
    my $pid = $_[0];
    my $fid = $_[1];

    kill("INT", $pid);
    waitpid($pid, 0);
    return close($fid);
}

################################ tdsDemod
sub tdsDemod {
    my @vals;
    my $str;
    my $nvals;

    # call tdsDemod
    $str = `$tdsdmd @_`;
        chop($str);
    @vals = split("\n", $str);

    # check return values
    $nvals = scalar(@vals);
    if( ($nvals != $#_ - 2) || $? ) {
	print "$vals[0]\n";
	return 0;
    }

    push(@vals, 1);
    return @vals;
}

################################ tdsRatio
# Ex.   tdsRatio(num, den)
#   - where num and den are tdsdemod outputs
# and tdsRatio gives back (coherence, magnitude, phase, real, imag)
#
# Code checked by Bram and Rana, Nov28, 2006
sub tdsRatio {
    my $pi = 3.14159265358979323846;

    # check arguments
    if( @_ != 2 ) {
	print "ERROR: tdsRatio takes 2 arguments.  Got:\n";
	die "@_\n";
    }

    my @num = split(" ", shift(@_));
    if( @num != 7 ) {
	print "ERROR: tdsRatio bad first argument.  Got:\n";
	die "@num\n";
    }

    my @den = split(" ", shift(@_));
    if( @den != 7 ) {
	print "ERROR: tdsRatio bad second argument.  Got:\n";
	die "@den\n";
    }

    # some complex math (not worth using Math::Complex)
    my $mag = $den[1];
    my $dre = $den[3];
    my $dim = $den[4];
    my $nre = $num[3];
    my $nim = $num[4];
    my @rslt;

    if( $mag == 0.0  ) {
	@rslt = (0, 0, 0, 0, 0);
	return @rslt;
    }

    my $rre = (($nre * $dre) + ($nim * $dim)) / ($mag * $mag);
    my $rim = (($nim * $dre) - ($nre * $dim)) / ($mag * $mag);

    # build result
    push(@rslt, shift(@num) * shift(@den));	# coherence
    push(@rslt, shift(@num) / shift(@den));	# magnitude
    push(@rslt, shift(@num) - shift(@den));	# phase
    push(@rslt, $rre);				# real
    push(@rslt, $rim);				# imag

    # phase check
    while( $rslt[2] < -$pi )
    { $rslt[2] += 2 * $pi; }
    while( $rslt[2] > $pi )
    { $rslt[2] -= 2 * $pi; }

    return @rslt;
}

################################ tdsRead
sub tdsRead {
    my @vals;
    my $str;
    my $nvals;

    # call tdsRead
    $str = `$tdsread @_`;
    chop($str);
    @vals = split("\n", $str);

    push(@vals, !$?);
    return @vals;
}

################################ tdsWrite
 sub tdsWrite {
    my @vals;
    my $str;
    my $nvals;

    # call tdsDemod
    $str = `$tdswrite @_`;
    chop($str);
    @vals = split("\n", $str);

    # check return values
    $nvals = scalar(@vals);
    if( ($nvals != 0) || $?) {
	print "$str\n";
	return 0;
    }

    push(@vals, 1);
    return @vals;
}

################################ end properly
1;

