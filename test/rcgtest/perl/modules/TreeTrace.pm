package TreeTrace;
use strict;
use warnings;
use File::Basename;

# 
# this module contains the subroutines &tree_searching_subroutine,
# &next_branch, and &expand_arguments
# 
# &tree_searching_subroutine takes one argument, which is the name
# of an MEDM screen to start with, passes that argument to &next_branch,
# and eventually returns two hashes of hashes of arrays - each key of the
# outer hash is the name of a called screen, each key of the inner hash
# is a macro string that the called screen is called with, and each element
# of the array is the name of a calling screen that uses the macro string
# to call the called screen
# 

our $current_medm_screen;
our $current_medm_screen_args;
our $next_medm_screen;
our $next_medm_screen_args;
our $medm_screen_filehandle;

our $begin;
our $length;
our $args;

sub tree_searching_subroutine {
    &next_branch($_[0]);

    return(1);
}

sub next_branch {
    local $medm_screen_filehandle;
    local $current_medm_screen;
    local $current_medm_screen_args;
    local $next_medm_screen;
    local $next_medm_screen_args = "";
    
    if (defined $_[0]) {$current_medm_screen = $_[0];}
    if (defined $_[1]) {$current_medm_screen_args = $_[1];}
    
    open ($medm_screen_filehandle, $current_medm_screen) or die;
    
    # discard the first three lines because they contain a pointer to the filename
    # and directory where the file was last edited, which may be different than the
    # current ones
    <$medm_screen_filehandle>; <$medm_screen_filehandle>; <$medm_screen_filehandle>;
    
    while (<$medm_screen_filehandle>) {
	# files for the MEDM screens end in "adl"
	chomp;
	
	next if ($_ !~ /adl\"$/);
	# parse the file name and path from the line of text
	local $begin = index($_, '"') + 1;
	local $length = rindex($_, '"') - $begin;
	local $next_medm_screen = substr($_, $begin, $length);
	
        # check for macro arguments on the next line
	$_ = <$medm_screen_filehandle>;
	if ($_ =~ "args") {
	    $begin = index($_, '"') + 1;
	    $length = rindex($_, '"') - $begin;
	    $next_medm_screen_args = substr($_, $begin, $length);
	}
	else {
	    $next_medm_screen_args = "<no macros>";
	}
	
	# check for macros in file name and path
 	if ((defined($next_medm_screen)) && (index($next_medm_screen, '$') > -1) && (defined($current_medm_screen_args))) {
	    $next_medm_screen = &expand_arguments($next_medm_screen, $current_medm_screen_args);
	}
	
	# convert relative addresses
	if ($next_medm_screen !~ /^\//) {
	    $next_medm_screen = dirname($current_medm_screen)."/".$next_medm_screen;
	}
	
        #check for macros in next MEDM screen arguments
 	if ((defined($next_medm_screen_args)) && (defined($current_medm_screen_args)) && (index($next_medm_screen_args, '$') > -1)) {
	    $next_medm_screen_args = &expand_arguments($next_medm_screen_args, $current_medm_screen_args);
	}
	
	# check for a missing screen
	if (-r $next_medm_screen) {
	    if (! defined $Main::active_medm_screens{$next_medm_screen}{$next_medm_screen_args}) {
		push @{$Main::active_medm_screens{$next_medm_screen}{$next_medm_screen_args}}, $current_medm_screen;
		&next_branch($next_medm_screen, $next_medm_screen_args);
		next;
	    }
            # eliminate loops	    
	    else {
		push @{$Main::active_medm_screens{$next_medm_screen}{$next_medm_screen_args}}, $current_medm_screen;
		next;
	    }
	}
	else {
	    push @{$Main::missing_medm_screens{$next_medm_screen}{$next_medm_screen_args}}, $current_medm_screen;
	    next;
	}
    }
    close $medm_screen_filehandle;
}

# this subroutine takes two arguments, the first is a filename containing macros, and
# the second is a string of the macro definitions
# 
# the macro definition string is of the form a=b,c=d,e=f

sub expand_arguments{
    my $screen = shift;
    my $args = shift;

    $screen =~ s/\s*//g; # remove whitespace
    $args =~ s/\s*//g;

    my $comma;
    my $equals;
    my %args_hash;
    my $this_arg;
    my @array;
    
    while (index($args, "=") != -1) {
	$equals = index($args, "=");
	$this_arg = substr($args, 0, $equals);
	$equals += 1;
	$args = substr($args, $equals);
	
	if (index($args, ",") != -1) {
	    $comma = index($args, ",");
	    $args_hash{$this_arg} = substr($args, 0, $comma);
	    $comma += 1;
	    $args = substr($args, $comma);
	}
	else {
	    $args_hash{$this_arg} = $args;
	}
    }
    
    @array = sort keys %args_hash;
    foreach my $macro_string (@array) {

	next if (index($args_hash{$macro_string}, "\$") != -1);


	while (index($screen, "\$(".$macro_string.")" ) != -1) {
	    my $begin = index($screen, "\$(".$macro_string.")");
	    my $length = length("\$(".$macro_string.")");
	    substr($screen, $begin, $length) = $args_hash{$macro_string};
	}
    }
    return($screen);
}

1;
