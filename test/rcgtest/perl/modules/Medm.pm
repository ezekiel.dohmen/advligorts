package Medm;


# this package contains an assortment of subroutines that are used
# in the generation of MEDM screens


# the fg_color subroutine takes one argument, which is the name of an
# EPICS subsystem, and returns a color which may be used for the
# foreground for its screens and buttons
sub fg_color {
    my $sys = $_[0];
    my    $button_fg_color = 14;       # default value
    
    if ($sys eq "asc") {$button_fg_color = 14;}
    elsif ($sys eq "burt") {$button_fg_color = 30;}
    elsif ($sys eq "cds") {$button_fg_color = 14;}
    elsif ($sys eq "cpu") {$button_fg_color = 0;}
    elsif ($sys eq "daq") {$button_fg_color = 30;}
    elsif ($sys eq "gds") {$button_fg_color = 20;}
    elsif ($sys eq "hpi") {$button_fg_color = 0;}
    elsif ($sys eq "ifo") {$button_fg_color = 50;}
    elsif ($sys eq "ioo") {$button_fg_color = 14;}
    elsif ($sys eq "iop") {$button_fg_color = 0;}
    elsif ($sys eq "isi") {$button_fg_color = 0;}
    elsif ($sys eq "isc") {$button_fg_color = 14;}
    elsif ($sys eq "llo") {$button_fg_color = 14;}
    elsif ($sys eq "lsc") {$button_fg_color = 14;}
    elsif ($sys eq "meas") {$button_fg_color = 0;}
    elsif ($sys eq "medm") {$button_fg_color = 0;}
    elsif ($sys eq "net") {$button_fg_color = 14;}
    elsif ($sys eq "oaf") {$button_fg_color = 0;}
    elsif ($sys eq "omc") {$button_fg_color = 30;}
    elsif ($sys eq "ops") {$button_fg_color = 30;}
    elsif ($sys eq "pem") {$button_fg_color = 14;}
    elsif ($sys eq "psl") {$button_fg_color = 0;}
    elsif ($sys eq "pzt") {$button_fg_color = 30;}
    elsif ($sys eq "rfm") {$button_fg_color = 0;}
    elsif ($sys eq "sus") {$button_fg_color = 0;}
    elsif ($sys eq "tcs") {$button_fg_color = 14;}
    elsif ($sys eq "ve" ) {$button_fg_color = 14;}
    elsif ($sys eq "video") {$button_fg_color = 0;}

    return $button_fg_color;
}


# the bg_color subroutine takes one argument, which is the name of an
# EPICS subsystem, and returns a color which may be used for the
# background for its screens and buttons
sub bg_color {
    my $sys = $_[0];
    $button_bg_color = 55;       # default value
    
    if ($sys eq "asc") {$button_bg_color = 31;}
    elsif ($sys eq "burt") {$button_bg_color = 29;}
    elsif ($sys eq "cds") {$button_bg_color = 50;}
    elsif ($sys eq "cpu") {$button_bg_color = 15;}
    elsif ($sys eq "daq") {$button_bg_color = 13;}
    elsif ($sys eq "gds") {$button_bg_color = 55;}
    elsif ($sys eq "hpi") {$button_bg_color = 28;}
    elsif ($sys eq "ifo") {$button_bg_color = 14;}
    elsif ($sys eq "ioo") {$button_bg_color = 40;}
    elsif ($sys eq "iop") {$button_bg_color = 36;}
    elsif ($sys eq "isi") {$button_bg_color = 25;}
    elsif ($sys eq "isc") {$button_bg_color = 50;}
    elsif ($sys eq "llo") {$button_bg_color = 32;}
    elsif ($sys eq "lsc") {$button_bg_color = 30;}
    elsif ($sys eq "meas") {$button_bg_color = 24;}
    elsif ($sys eq "medm") {$button_bg_color = 34;}
    elsif ($sys eq "net") {$button_bg_color = 51;}
    elsif ($sys eq "oaf") {$button_bg_color = 29;}
    elsif ($sys eq "omc") {$button_bg_color = 21;}
    elsif ($sys eq "ops") {$button_bg_color = 29;}
    elsif ($sys eq "pem") {$button_bg_color = 60;}
    elsif ($sys eq "psl") {$button_bg_color = 20;}
    elsif ($sys eq "pzt") {$button_bg_color = 14;}
    elsif ($sys eq "rfm") {$button_bg_color = 19;}
    elsif ($sys eq "sus") {$button_bg_color = 59;}
    elsif ($sys eq "tcs") {$button_bg_color = 35;}
    elsif ($sys eq "ve" ) {$button_bg_color = 35;}
    elsif ($sys eq "video") {
	$button_bg_color = 37;
    }
    return $button_bg_color;
}


# the addcolors subroutine returns the color mapping used
# by the MEDM screens
sub colorpalette {
    my $palette = "\"color map\" {
	ncolors=65
	colors {
		ffffff,
		ececec,
		dadada,
		c8c8c8,
		bbbbbb,
		aeaeae,
		9e9e9e,
		919191,
		858585,
		787878,
		696969,
		5a5a5a,
		464646,
		2d2d2d,
		000000,
		00d800,
		1ebb00,
		339900,
		2d7f00,
		216c00,
		fd0000,
		de1309,
		be190b,
		a01207,
		820400,
		5893ff,
		597ee1,
		4b6ec7,
		3a5eab,
		27548d,
		fbf34a,
		f9da3c,
		eeb62b,
		e19015,
		cd6100,
		ffb0ff,
		d67fe2,
		ae4ebc,
		8b1a96,
		610a75,
		a4aaff,
		8793e2,
		6a73c1,
		4d52a4,
		343386,
		c7bb6d,
		b79d5c,
		a47e3c,
		7d5627,
		58340f,
		99ffff,
		73dfff,
		4ea5f9,
		2a63e4,
		0a00b8,
		ebf1b5,
		d4db9d,
		bbc187,
		a6a462,
		8b8239,
		73ff6b,
		52da3b,
		3cb420,
		289315,
		1a7309,
	}
}\n";
    return $palette;
}

1;
