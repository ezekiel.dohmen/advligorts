################################################################
# PERLDOC
=head1 NAME

GuardTools.pm - a PERL module for running guardian scripts.

=head1 DESCRIPTION

This module contains functions for the Guardian scripts.

=head1 Methods

=cut
################################################################

package GuardTools;
use Exporter;
use POSIX qw(strftime _exit :sys_wait_h);#used to write timestamp localtime to BURT file snapshot

@ISA = ('Exporter');
@EXPORT = qw(&guardSystemDir &guardListStates &guardComment &guardLog 
&guardKillSubscripts &guardError &guardState &guardStatus &guardRequest 
&guardPrompt &guardReadBurt &guardMakeBurt &guardRestoreBurt 
&guardVerifyState &guardRecordAlarms &guardAlarm &guardInheritance &guardAddState &guardDeleteState);

# This uses the CaTools code to do channel access
# It assumes the @INC path has been set to include the guardian directory
use CaTools;

# Uses the stdenv LIGO tools.
use stdenv;
INIT_ENV($ENV{IFO});

# require careful code
use strict;

use IO::Handle;

# Set the timeout for channel access
my $CA_timeout = 1;

############################## guardSystemDir ##############
=over 4

=item B<guardSystemDir>

Generate a directory from a Subsystem and a type. Directories
are defined for:
I<scripts>
I<burtfiles>
I<logs>

Example:
 my $SubSys = 'M1:HPI-HAMX';
 my $dir = guardSystemDir($SubSys, 'scripts');
 print $dir; # prints '/opt/rtcds/userapps/release/hpi/m1/scripts'

=cut
############################################################ guardSystemDir
sub guardSystemDir {
	sub usage {
		print <<EN;
		\$dir = guardSystemDir(\$SubSys, \$DirType)

		Returns a directory where \$SubSys maps to 
		<IFO>:<SYS>-<UNIT> or <IFO>:<SYS>

		and \$DirType maps to 
			scripts:       /opt/rtcds/userapps/release/<SYS>/<IFO>/scripts/<UNIT>
			logs:          \$RTCDSDIR/target/lc(<IFO><SYS><UNIT>)/logs
			burtfiles:     /opt/rtcds/userapps/release/<SYS>/<IFO>/burtfiles/<UNIT>
		or 
                        scripts:       /opt/rtcds/userapps/release/<SYS>/<IFO>/scripts/
                        logs:          \$RTCDSDIR/target/lc(<IFO><SYS>)/logs
                        burtfiles:     /opt/rtcds/userapps/release/<SYS>/<IFO>/burtfiles/

EN
	}
	# Load in arguments
	my ($SubSys, $DirType)   = @_;
	$SubSys = uc($SubSys);
	$DirType = lc($DirType);  

	# Split SubSys so we have individual components
	my ($IFO,$SYS,$UNIT);
	if (index($SubSys,"-")<=0){
    		($IFO,$SYS) = split(/[:]/,lc($SubSys));
	}
	else{
    		($IFO,$SYS,$UNIT) = split (/[-:]/, lc($SubSys));
	}

	my $userapps_dir = '/opt/rtcds/userapps/release';
	$userapps_dir = "/opt/rtcds/${site}/${ifo}/userapps/release" unless (-d $userapps_dir);

	# Find the appropriate directory type
	my $dir = "";
	if ($DirType eq "scripts") { 
		if(defined $UNIT){$dir = "${userapps_dir}/${SYS}/${IFO}/scripts/${UNIT}";}
    		else{$dir = "${userapps_dir}/${SYS}/${IFO}/scripts";}
	}		
	elsif ($DirType eq "burtfiles") {
		if(defined $UNIT){$dir = "${userapps_dir}/${SYS}/${IFO}/burtfiles/${UNIT}";}
    		else{$dir = "${userapps_dir}/${SYS}/${IFO}/burtfiles";}
	}
	elsif ($DirType eq "logs") {
		if(defined $UNIT){$dir = "${RTCDSDIR}/target/${IFO}${SYS}${UNIT}/logs";}
    		else{$dir = "${RTCDSDIR}/target/${IFO}${SYS}/logs";}
	}
	else {
		print "Error! Bad SubSys: $SubSys and DirType: $DirType\n";
		&usage;    
	}

	return $dir;
};

############################## guardListStates #############
=item B<guardListStates>

Generate a list of valid states from a SubSystem based on the files in
the scripts directory given by guardSystemDir.  If an executable "Inheritance"
exists,  guardListStates will include the Inheritance.


Example:
 my $SubSys = 'M1:HPI-HAMX'; 
 my @States = guardListStates($SubSys);

=back
=cut
############################################################ guardListStates
sub guardListStates {
	my $SubSys = uc(shift);
	# Read the burtfiles directory for states
	my $DirName = guardSystemDir($SubSys, 'burtfiles');
	my @ValidStates;

	# Open the scripts directory
	opendir( DIR, $DirName ) or die "Error in opening scripts directory: $DirName.\n";
	while( my $filename = readdir(DIR) ) {
		# A valid state must:
		if ( $filename =~ m/^verify_/ &&		#   have a verify script
			$filename !~ m/~$/        ) 		#   not be an emacs file
		{
			$filename =~ /^verify_(.*)/;
			push(@ValidStates, $1);
		}
	}
	closedir(DIR);

	# Follow the inheritance tree.
	if(-e "$DirName/Inheritance") {
		$DirName = `${DirName}/Inheritance`;
		chomp($DirName);
		opendir( INHERITDIR, "${DirName}" ) or die "Error in opening scripts directory: $DirName.\n";
		while ( my $filename = readdir(INHERITDIR) ) {
			# A valid state must:
			if ( $filename =~ /^verify_/ &&  	#   have a verify script
				$filename !~ /~$/        ) 	#   not be an emacs file
			{
				$filename =~ /^verify_(.*)/;
				# add the inherited state but don't duplicate
				push(@ValidStates, $1) unless grep(/^$1$/,@ValidStates);
			}
		}
		closedir(INHERITDIR);
	}

	return @ValidStates;
};

############################## guardComment ################
=item B<guardComment>

Send a comment to the comment field and the guardian log.
Limited to 40 characters by epics
Requires the SubSys and the comment

Example:
   my $SubSys = 'M1:HPI-HAMX';
   guardComment($SubSys, "You look funny");

=cut
############################################################ guardComment
sub guardComment {
	my $SubSys = uc(shift);
	my $separator = "_";
	if (index($SubSys, "-")<=0){
			$separator = "-";
	}
	my $comment = join(' ', @_);
	# Shorten to fit in EPICS text block
	my $short_comment = substr($comment, 0, 38); 
	caPut("${SubSys}${separator}GUARD_COMMENT", $short_comment);
	guardLog($SubSys, $comment);
};

############################## guardLog ####################
=item B<guardLog>

Sends a comment to the guardian log located in guardian.txt

Example:
   my $SubSys = 'M1:HPI-HAMX'
   guardLog($SubSys, "You smell funny, too");

=cut
############################################################ guardLog
sub guardLog {
	my ($SubSys, $comment) = @_;
	my $separator = "_";
        if (index($SubSys, "-")<=0){
                        $separator = "-";
        }
	my $logdir = $SubSys;
	if ( $SubSys =~ m/[A-Z][0-9]:[A-Z]{3}-[A-Z0-9]+/ ){
		$logdir = guardSystemDir($SubSys, 'logs');
	}
	my $nowloc = localtime;
	my $nowgps = time;
	my $logfile = $logdir . "/guardian.txt"; 
	open(LOG,">>${logfile}") or return "Couldn't open ${logfile} for appending data\n";
	print LOG "[${nowloc} - ${nowgps}] ${comment}\n";
	close LOG;
	return $comment;
};

############################## guardError ##################
=item B<guardError>

Calls a guard comment and sets ${SubSys}_GUARD_STATUS to 1

Example:
   guardError("No verify script");

=cut
############################################################ guardError
sub guardError {
	my ($SubSys, $comment) = @_;
	my $separator = "_";
        if (index($SubSys, "-")<=0){
                        $separator = "-";
        }
	guardComment($SubSys, $comment);
	caPut("${SubSys}${separator}GUARD_STATUS", 1);
};

############################## guardState ##################
=item B<guardState>

With a single argument,  returns the current state of the system
With a second argument, it sets the state of the system
-1 is error, 0 is ok

Example:
   print "$SubSys is in state: ".guardState(${SubSys});
   $newstate = "BLAH";
   guardState(${SubSys}, $newstate);
   print "$SubSys is now in state $newstate";

=cut
############################################################ guardState
sub guardState {
	my $SubSys = uc(shift);
	my $separator = "_";
        if (index($SubSys, "-")<=0){
                        $separator = "-";
        }
	if (@_ == 0) {
		my ($state) = caGet("${SubSys}${separator}GUARD_STATE");
		if (length($state) > 0)  {
			return $state;
		} else {
			return -1;
		}
	} else {
		my $newstate = shift;
		caPut("${SubSys}${separator}GUARD_STATE", $newstate);
		return 0;  
	}
};

############################## guardStatus #################
=item B<guardStatus>

With a single argument, returns the current status of the system as a number. 
0 is OK,  1 is an error
With two arguments,  sets the system status

Example:
   print STDERR "HELP!\n" if( guardStatus(${SubSys}) );
   print "Setting status to 0\n";
   guardStatus($SubSys, 0);

=cut
############################################################ guardStatus
sub guardStatus {	
	if (@_ == 1) {
		my ($SubSys) = @_;
                my $separator = "_";
        	if (index($SubSys, "-")<=0){
               	$separator = "-";
        	} 
		my ($status) = caGet("${SubSys}${separator}GUARD_STATUS");
		if ( (length($status)>0) &&( $status eq 'OK' || $status == 0)) {
			return 0;
		} else {
			return 1;
		}
	} elsif (@_ == 2) { 
		my ($SubSys, $status) = @_;
		my $separator = "_";
                if (index($SubSys, "-")<=0){
                $separator = "-";
                }
		caPut("${SubSys}${separator}GUARD_STATUS", $status);
	} else {
		print STDERR "Bad GuardStatus call.\n";
	}  
};

############################## guardRequest ################
=item B<guardRequest>

Takes the name of a system and the requested state 
and makes a state change request

Example:
	$System = "M1:SUS-MC2";
	$NewState = "SAFE";
	guardRequest($System, $NewState);

=cut
############################################################ guardRequest
sub guardRequest {
	my ($SubSys, $newState) = @_;
	my $separator = "_";
	if (index($SubSys, "-")<=0){
                        $separator = "-";
        }
	caPut("${SubSys}${separator}GUARD_REQUEST", $newState); 
};

############################## guardPrompt #################
=item B<guardPrompt>

Creates a user-input prompt on the screen. Returns the input.
Default value is optional.

Example:
	my $userInput = &guardPrompt("To be or not to be?","That is the question.")

=cut
############################################################ guardPrompt
sub guardPrompt {
	my($prompt, $default) = @_;
	my $defaultValue = $default ? "[$default]" : "";
	print "$prompt $defaultValue: ";
	chomp(my $input = <STDIN>);
	return $input ? $input : $default;
};

############################## guardKillSubscripts #########
=item B<guardKillSubscripts>

This function kills any currently running subscripts 
Used in transition scripts in the case that the state is not verified

=cut
############################################################ guardKillSubscripts
sub guardKillSubscripts{
	my $SubSys = shift;
        my $separator = "_";
        if (index($SubSys, "-")<=0){
                        $separator = "-";
        }

	# get the sub-pid, check whether it is 0. If so, exit
	# if it is nonzero, but does not exist, exit
	my ($subpid) = caGet("${SubSys}${separator}GUARD_SUBPID");
	return 1 unless ($subpid && (kill 0, $subpid));

	# check whether we're running manually
	# if so, tell the user they can't kill subscripts
	my $parent = getppid;
	my ($guard_pid) = caGet("${SubSys}${separator}GUARD_PID");
	if ($parent == $guard_pid){
		kill -9, $subpid;
		return 0;
	} else {
		print "Running manually, cannot kill sub-scripts.\n";
		return 1;
	}
};

############################## guardReadBurt ###############
=item B<guardReadBurt>

Reads in a BURT-generated file and parses it.
Returns a list of references to the arrays of channel data
and comment headers. The BURT file is assumed to be of the
format:

--- Start BURT header
Everything in between is part of the header
--- End BURT header
(RO|RNO|'') Channel Type Value

See the BURT documentation for more details on the file format.

B<Arguments:>
  guardReadBurt($SubSys,$infile)
  $infile is a BURT file in guardSystemDir($SubSys, 'burtfiles')
Example:
	my ($r1,$r2) = guardReadBurt($SubSys,"burtFile.snap")
	my @chanvals = @$r1;
	my @comments = @$r2;
Note that each element in the chanvals array is a hashref with the following keys:
readonly, channel, type, value

=cut
############################################################ guardReadBurt
sub guardReadBurt {
	#shift in the arguments
	my ($SubSys, $infile) = @_;
	my $sys_dir = guardSystemDir($SubSys, 'burtfiles');
	my @infiles;

	# we go file hunting. First, we look for an inherited burt file
	# If we find one, we add it do the infile list. We then look for
	# a local burt file. If one is found, it's added to the list, and 
	# we go to process. Otherwise, we process only with the inherited
	# file. 
	# If we don't find an inherited file, we look for a local file.
	# If we don't find that, we look for a local default.
	# If we don't find that, we look for an inherited default.
	# If we don't find that, we die
	# define a inherit flag so we know to prepend the subsystem
	my $is_inheritance_file = 0;
	my $readonly = 0;
	unless (defined $infile){
		$infile = guardState($SubSys) . ".snap";
		$readonly = 1;
	} 
	my $inherit_dir;
	if (-e "${sys_dir}/Inheritance"){	# find the inheritance directory
		$inherit_dir = `${sys_dir}/Inheritance`;
		chomp($inherit_dir);
	}
	if (defined $inherit_dir && -e "${inherit_dir}/$infile"){ # if there's an inheritance snapshot there
		$is_inheritance_file = 1;
		push @infiles, "${inherit_dir}/$infile";
		if (-e "${sys_dir}/$infile") { # If there's also a locally defined snapshot
			push @infiles, "${sys_dir}/$infile";
		}
	} elsif (-e "${sys_dir}/$infile") { # If no inherited snapshot, use local snapshot
		push @infiles, "${sys_dir}/$infile";
	} elsif ($readonly && -e "${sys_dir}/defaultChannelList.req") { # if no local snaphsot, use local default list
		push @infiles, "${sys_dir}/defaultChannelList.req";
	} elsif (defined $inherit_dir && $readonly && -e "${inherit_dir}/defaultChannelList.req") { # if no local defualt list, use inherited default list
		$is_inheritance_file = 1;
		push @infiles, "${inherit_dir}/defaultChannelList.req";
	} elsif (-e $infile){
		push @infiles, $infile;
	} else {
		die "Could not find a burt file $infile.";
	}

	print guardLog($SubSys,"Executing guardReadBurt on infiles @infiles\n");

	my @comments;
	# use a hash to ensure that all channels are unique
	my %chanvalhash; 

	# main foreach loop, allows for multiple infiles to be read
	foreach my $file (@infiles){
		my $insideHeader = 0;#this is to keep track of lines that are part of header

		#open file for reading or die on error
		open(my $fh, "<", $file) or die guardLog($SubSys,"    Couldn't open ${file}: $!");
		guardLog($SubSys,"    Reading in channels from $file\n");

		#line by line
		while (<$fh>) {
			chomp;#remove \n, whitespace, trim
			$insideHeader = 1-$insideHeader if(/^--- Start/);
			if($insideHeader == 0){#not inside Header
				my ($r1,$r2,$r3,$r4) = split(/\s+/);
				my ($ron,$ch,$type,$val);
				#because some channels are not "read-only"
				if($r1 =~ /^(RO|RON)$/){
					if(defined $r4) { 
						($ron,$ch,$type,$val) = (1,$r2,$r3,$r4); 
					} else { 
						($ron,$ch,$type,$val) = (1,$r2,1,$r3);   
					}
				} elsif(defined $r3) {
					($ron,$ch,$type,$val) = (0,$r1,$r2,$r3);
				} else {
					($ron,$ch,$type,$val) = (0,$r1,1,$r2);
				}
				# if it's an inheritance file, prepend the subsystem prefix
				$ch = "${SubSys}_$ch" if $is_inheritance_file;
				# add all of the values to the channel list
				$chanvalhash{$ch} = {readonly=>$ron,channel=>$ch,type=>$type,value=>$val};
			} else {#inside header
				push(@comments,$_);
			}
			$insideHeader = 1-$insideHeader if(/^--- End/);
		}
		close($fh);
		$is_inheritance_file = 0;
	}
	my @uniquechanvals = values %chanvalhash;
	return (\@uniquechanvals,\@comments);
};


############################## guardMakeBurt ###############
=item B<guardMakeBurt>

Makes a BURT file with snapshot of channels from $infile.
Returns nothing. If no $outfile specified, writes a file
to guardSystemDir($SubSys, 'burtfiles') with the name of the 
current state, e.g. DAMPED.snap. If no $infile specified, 
uses built-in default list of channels. If the comment 
headers contain a 'Time: (.*)' string, it will substitute 
in the current time stamp of when file was created.

B<Arguments:>
 guardMakeBurt($SubSys,$outfile='$STATE.snap',$infile='')
 $outfile is name of file to be written, will be stored in guardSystemDir('burtfiles')
 $infile is a BURT file in guardSystemDir($SubSys, 'burtfiles')

Examples :
	guardMakeBurt($SubSys); # writes a file, e.g. SAFE.snap
		# to guardSystemDir($SubSys, 'burtfiles') with values
		# from defaultChannelList.req.
	guardMakeBurt($SubSys,"outFile.txt","inFile.txt")

=cut
############################################################ guardMakeBurt
sub guardMakeBurt {
	my ($SubSys,$outfile,$infile) = @_;
	my $separator = "_";
        if (index($SubSys, "-")<=0){
                        $separator = "-";
        }
	if(@_ == 0){
		die "Not enough args.\n";
	}
	my $SubSysDir = guardSystemDir($SubSys, 'burtfiles');
	if(@_ == 1){
		(my $state) = caGet("${SubSys}${separator}GUARD_BURT_SAVE");
		die "Error: no state name specified in BURT save field." if ($state eq '');
		$outfile = "${SubSysDir}/$state.snap";
	}
	# commented out for work with guardReadBURT
	# $infile = "defaultChannelList.req" if !defined($infile);


	my @chs = ();
	my @vals = ();

	if(defined $infile){
		guardLog($SubSys,"Executing guardMakeBurt($SubSys,$outfile,$infile)\n");
	} else {
		guardLog($SubSys,"Executing guardMakeBurt($SubSys,$outfile)\n");
	}

	#read the $infile
	my ($r1, $r2) = guardReadBurt($SubSys,$infile);
	my @chanvals = @$r1;
	my @comments = @$r2;

	#update all values in @chanvals with the current snapshot values
	#this assumes that we get back all the values for all channels we gave it
	my @channel_names = map $_->{channel}, @chanvals;
	my $number_channel_names = @channel_names;
	guardLog($SubSys, "   There are $number_channel_names channels in the snapshot.");
	my @currVals = caGet(@channel_names);
	for(my $ii = 0; $ii<@currVals; $ii++){
		$chanvals[$ii]->{value} = $currVals[$ii];
	}

	guardLog($SubSys,"    Dumping snapshot of specified channels into $outfile \n");

	#open for writing
	open(my $burt, ">", $outfile) or die guardLog($SubSysDir,"    Couldn't open ${outfile}: $!");
	my $datestring = `date +%c`;

	#write comments to file
	if(@comments == 11){
		foreach (@comments) {
			#update current timestamp if the line is there
			# \s is whitespace, \K is anchor for rhs
			s/^Time:\s*\K(.*)$/$datestring/;
			print $burt $_,"\n";
		}
	} else {
		my $datestring = `date +%c`;	
		chomp $datestring;
		print $burt <<HEADER
--- Start BURT header
Time:     $datestring
Login ID: controls (controls)
Eff  UID: 1001
Group ID: 1001
Keywords:
Comments:
Type:     Absolute
Directory /opt/rtcds/userapps/guardian
Req File: /opt/rtcds/userapps/guardian
--- End BURT header
HEADER
		;
	}

	#go through all channels and write to file
	foreach (@chanvals) {
		my $ron = $_->{readonly}?'RO ':'';
		my ($ch,$type,$val) = ($_->{channel},$_->{type},$_->{value});

		print $burt sprintf("%s%s %d %s\n",$ron,$ch,$type,$val);
	}
	close($burt);	
	caPut("${SubSys}${separator}GUARD_BURT_SAVE", '');
};

############################## guardRestoreBurt ############
=item B<guardRestoreBurt>

Restores values to the channels defined in the input file. 
Returns a list of hash-refs of badly-defined channels.

B<Arguments:>
  guardRestoreBurt($SubSys,$snapFile)
  $snapFile is in guardSystemDir($SubSys, 'burtfiles')
Example:
 my @badChans = guardRestoreBurt($SubSys,"burtFile.txt")

=cut
############################################################ guardRestoreBurt
sub guardRestoreBurt {
	# Usage: guardRestoreBurt( $SubSys, $FileName ) 
	# This file reads the file in the data directory $Filename
	# and restores all of the EPICS values found there
	#
	# Bad channels are listed in the guardian log file. 

	my ($SubSys, $state) = @_;
	my $snapFile = uc($state) . ".snap";
	if ($state =~ m/(.+)\.(.+)/){
		$snapFile = $state;
	}
	my $SubSysDir = guardSystemDir($SubSys, 'burtfiles');
	guardLog($SubSys, "Executing guardRestoreBurt(${SubSys}, ${snapFile}");

	my ($r1,$r2) = guardReadBurt($SubSys, $snapFile);
	# guardReadBurt returns references to array,  so dereference here:
	my @chanvals = @$r1;
	my @comments = @$r2;

	my (@goodChans,@goodVals,@badchanvals);

	foreach(@chanvals){
		#go through all channel data
		unless(defined $_->{value}){
			#it better have a value defined for putting
			guardLog($SubSys, "    Bad Channel: ".$_->{channel}."\n");
			push(@badchanvals,$_);
		} elsif($_->{readonly} == 0 ){
			#it must also be not readonly
			push(@goodChans,$_->{channel});
			push(@goodVals,$_->{value});
		}
	}

	guardLog($SubSys, "    Restoring the system with values defined in ${snapFile}");
	
	my $nChannels = @goodChans;
	guardLog($SubSys, "         - updating $nChannels channels");

	if(@chanvals == 0){
		print "Error: no channel/value pairs found.\n";
		return 0;
	}
	else{
		caPut(@goodChans, @goodVals,{wait=>'no_wait'});
	}
	return @badchanvals;
};

############################## guardCleanBurt ##############
=item B<guardCleanBurt>

Takes in a Burt file and removes excess lines and bad channels.
Arguments: guardCleanBurt($SubSys,$dirtyFile,$cleanFile)
Assumes that $dirtyFile and $cleanFile are in 
guardSystemDir($SubSys, 'burtfiles')

Example:
 guardCleanBurt($SubSys,"muddyFile.txt","cleanOutput.txt")

=cut
############################################################ guardCleanBurt
sub guardCleanBurt {
	my ($SubSys,$dirtyFile,$cleanFile) = @_;
	my $SubSysDir = guardSystemDir($SubSys, 'burtfiles');
	guardLog($SubSys, "Executing guardCleanBurt(${SubSysDir}, ${dirtyFile}, ${cleanFile})");

	my ($r1,$r2) = guardReadBurt($SubSys,$dirtyFile);
	my @chs = @$r1;
	my @vals = @$r2;

	my @currVals = caGet(@chs);

	my @goodChs = ();
	my @badChs = ();
	my @goodVals = ();
	for(my $ii = 0; $ii < @chs; $ii++){
		if($vals[$ii] =~ "NaN" ||  $currVals[$ii] =~ ""){
			push(@badChs,$chs[$ii]);
		} else {
			push(@goodChs,$chs[$ii]);
			push(@goodVals,$vals[$ii]);
		}
	}

	guardLog($SubSys, "    Cleaning up the channel list now and dumping into ${SubSysDir}${cleanFile}");
	$cleanFile = "${SubSysDir}/${cleanFile}";
	open(BURT, ">$cleanFile");
	for(my $ii = 0;$ii<@goodChs;$ii++){
		my($ch,$val) = ($goodChs[$ii],$goodVals[$ii]);
		write(BURT);
	}
	close(BURT);
};

############################## guardVerifyState ############
=item B<guardVerifyState>
Changed by CK May 5 2012 to check alarm checksum instead of BURT snapshot
rev 2152
Verifies that the system has the alarm checksum stored in verify_STATE
Verifies no alarms going off
returns 0 if OK
returns -1 if checksum doesn't match
returns the number of alarms if chksum matches but there are bad values
prints channels with alarms to the log file
Arguments: guardVerifyState($SubSys, $state)

Example: my $alarms = guardVerifyState("S1:ISI-TST", "DAMPED");

=cut
############################################################ guardVerifyState
# ~~ requires Perl5.010 or higher
sub guardVerifyState{
	my ($SubSys, $state) = @_;
	my $separator = "_";
        if (index($SubSys, "-")<=0){
                        $separator = "-";
        }
	unless(defined $state) { 
		($state) = caGet("${SubSys}${separator}GUARD_REQUEST");	
	} 

	my $verify_filename = guardSystemDir($SubSys, 'burtfiles') . "/verify_$state";
	open my $vfh, "<$verify_filename" or die "Couldn't open file $verify_filename.";
	my ($stored_checksum) = <$vfh>;
	close $vfh; chomp $stored_checksum;
	my ($dcu_id) = caGet("${SubSys}${separator}DCU_ID");
	my $FEC_NAME = substr($SubSys, 0, 3) . "FEC-${dcu_id}";
	my ($cur_checksum) = caGet("${FEC_NAME}_GRD_ALH_CRC");
	$cur_checksum = sprintf("0x%x", $cur_checksum);
	if($stored_checksum ne $cur_checksum){
		guardLog($SubSys, "Expected checksum $stored_checksum. Current checksum is $cur_checksum.");
		return -1;
	}	
	my ($setpt_alms, $rdbk_alms) = caGet("${FEC_NAME}_GRD_SP_ERR_CNT", "${FEC_NAME}_GRD_RB_ERR_CNT");
	my @setpt_alarms, my @rdbk_alarms;
	for(my $ii = 0; $ii < $setpt_alms; ++$ii){
		push @setpt_alarms, caGet("${FEC_NAME}_GRD_SP_STAT".$ii);	
		push @setpt_alarms, "\n";
	}
	for(my $jj = 0; $jj < $rdbk_alms; ++$jj){
		push @rdbk_alarms, caGet("${FEC_NAME}_GRD_RB_STAT".$jj);	
		push @rdbk_alarms, "\n";
	}
	if($setpt_alms){
		guardLog($SubSys, 'The following setpoints are in alarm:');
		guardLog($SubSys, @setpt_alarms);
	}
	if($rdbk_alms){
		guardLog($SubSys, 'The following readbacks are in alarm:');
		guardLog($SubSys, @rdbk_alarms);
	}
	my $total = $setpt_alms + $rdbk_alms;
	return $total;
}

############################## guardAlarm ############
=item B<guardAlarm>

Checks the alarm status of the system.  The alarms are set for each available 
field that handles alarms.  The .HSV and .LSV set the severity of the high and 
low alarms ('MAJOR' is a good severity).  The .HIGH and .LOW fields set the 
high and low alarm thresholds,  respectively.

Arguments: guardAlarm($SubSys)
Returns:   0    if no alarm
		   N    number of alarms

Example: my $alarm = guardAlarm("L1:SUS-MC1");

=cut
############################################################ guardAlarm
# ~~ requires Perl5.010 or higher
sub guardAlarm {
	my $pr_debug = 0;
	my ($SubSys) = @_;
	my $separator = "_";
        if (index($SubSys, "-")<=0){
                        $separator = "-";
        }
	my ($FEC_ID) = caGet("${SubSys}${separator}DCU_ID");
	my $FEC_STR = substr($SubSys, 0,3) . 'FEC-' . $FEC_ID;

	print "Checking $SubSys:\n" if $pr_debug;

	# The FILTER_STATUS is for variables associated with a FILTER
	# The SETPOINT is for EPICS input values
	# The STAT_ERR is for EPICS readback values
	my @chans = ( "${FEC_STR}_FILTER_STATUS_COUNT", 
		"${FEC_STR}_SETPOINT_STATUS_COUNT",
		"${FEC_STR}_STAT_ERR_CNT");
	my @id_fld = ( "_FILTER_ID_", "_SETPOINT_ID_", "_FESTAT_");
	my @vals = caGet(@chans);

	my $n_alarms = 0;
	for(my $ii=0; $ii<3; $ii++) {
		print "  $chans[$ii]: $vals[$ii]\n" if $pr_debug;
		$n_alarms += $vals[$ii];
		for(my $jj=0; $jj<$vals[$ii]; $jj++) {
			my $errchan = ${FEC_STR}.$id_fld[$ii].($jj+1);
			my ($err) = caGet($errchan);
			print "    $errchan:  $err\n" if $pr_debug;		
			guardComment(${SubSys}, "Alarm:".$err);
		}
	}
	return $n_alarms;
}

############################## guardInheritance ############
=item B<guardInheritance>

Author: Adam J Mullavey
Created: 12th September 2012

Generates the 'Inheritance' file for a subsystem, which echos the Inheritance directory. It also creates the Inheritance directory if it doesn't exist.

Arguments: guardInheritance($SubSys,SubSysType)

Example: guardInheritance("L1:SUS-MC1","HSTS");

=cut
############################################################ guardInheritance

sub guardInheritance{
    my ($SubSys, $SubSysType) = @_;

    my $subsystype = lc($SubSysType);
    my $sys = lc(substr($SubSys,3,3));
    my $subsys = lc(substr($SubSys,7));
    
    my @dirs = qw(burtfiles scripts);
    my ($iburt_dir,$iscripts_dir) = map {"$ENV{USERAPPS_DIR}/$sys/common/". $_ ."/$subsystype"} @dirs;
    
    my $burt_dir = guardSystemDir($SubSys,'burtfiles');
    my $scripts_dir = guardSystemDir($SubSys,'scripts');

    my $bf_inh_file = "$burt_dir/Inheritance";
    my $sc_inh_file = "$scripts_dir/Inheritance";

    # Make the inheritance directories if they don't already exist
    unless(-d $iburt_dir){mkdir $iburt_dir};
    unless(-d $iscripts_dir){mkdir $iscripts_dir};

    # Make an executable inheritance file for the burtfiles
    open BF_INH, ">$bf_inh_file";
    print BF_INH "#/usr/bin/env bash\n echo $iburt_dir";
    close BF_INH; 
    chmod 0775, $bf_inh_file;

    # Make an executable inheritance file for the scripts
    open SC_INH, ">$sc_inh_file";
    print SC_INH "#/usr/bin/env bash\n echo $iscripts_dir";
    close SC_INH;
    chmod 0775, $sc_inh_file;
}

############################## guardAddState ############
=item B<guardAddState>

Adds or updates a state of the given subsystem
Takes the current state of the system, makes a BURT snapshot
Then, if a state_alarms.txt file exists, it generates a guardian
snapshot file using generate_alarms.pl. Otherwise, it defaults to 
copying the alarms from the new snapshot into a guardian snapshot file.

Arguments: guardAddState($SubSys,"STATE")
Returns:   0 if ok, 1 if failed

Example: my $failure = guardAddState("L1:SUS-MC1", "DAMPED");

=cut
############################################################ guardAddState
sub guardAddState{
	my ($SubSys, $state) = @_;
	my $separator = "_";
        if (index($SubSys, "-")<=0){
                        $separator = "-";
        }
	my $subsys_str = lc(substr($SubSys, 0, 2) . substr($SubSys,3,3) . substr($SubSys, 7));
	# if called upon via the guard burt screen
	if ($state eq 'BURTSAVE'){
		($state) = caGet("${SubSys}${separator}GUARD_BURT_SAVE");
	}

	# find the guardian directory, back compatible with 2.3
	my $guardian_dir = "/opt/rtcds/userapps/release/guardian";
	unless(-e $guardian_dir){
		$guardian_dir = "/opt/rtcds/${site}/${ifo}/userapps/release/guardian";
	}
	
	my $burt_dir = guardSystemDir($SubSys, 'burtfiles');

	my $statelogfile = $state."_log.txt";

	# open statelog.txt file, and append to.
	open LOG, ">>$burt_dir/$statelogfile";
	
	# write standard error and standard output to logfile 
	STDOUT->fdopen(\*LOG,'w');
	STDERR->fdopen(\*LOG,'w');

        # open an xterm window to show additions to state log file (AM120905)
	my $xpid = fork;
	if($xpid==0){
	    exec "xterm -e tail -f '$burt_dir/$statelogfile'";
	}

	#print LOG "Writing to state $state.\n";
	print "Writing to state $state.\n";

	# Find the autoBurt.req file
	my $autoburt_file = "/opt/rtcds/${site}/${ifo}/target/${subsys_str}/${subsys_str}epics/autoBurt.req";
	#die "Error: couldn't find $autoburt_file." unless (-e $autoburt_file);
	unless(-e $autoburt_file){
	    print LOG "Error: couldn't find $autoburt_file.\n";
	    die
	}

	# Put the desired state into the state and request fields for the snapshot (AM120626)
	guardState($SubSys, $state);
	guardRequest($SubSys, $state);
	
	sleep(1);

	# Get a snapshot and write it to the right place
	#print LOG "Writing snapshot of current state...\n";
	print "Writing snapshot of current state...\n";
	system("burtrb -f $autoburt_file -o $burt_dir/$state.snap");

	# Run the generate alarms script to get a .guardsnap file
	#print LOG "Generating a guardian snapshot file...\n";
	print "Generating a guardian snapshot file...\n";
	system("$guardian_dir/generate_alarms.pl $SubSys $state");
	unless(-e "$burt_dir/$state.guardsnap"){
		unlink "$burt_dir/$state.snap" if (-e "$burt_dir/$state.snap");
		die "Error: could not create state $state - no .guardsnap file found.";
	}

	# Write the newly created guardian snapshot file (with alarm values)
	# to the system
	#print LOG "Writing guardsnap file with alarms to system...\n";
	print "Writing guardsnap file with alarms to system...\n";
	system("burtwb -f $burt_dir/$state.guardsnap");
	sleep(1);

	# Record the alarm checksum to a verify_STATE file
	#print LOG "Saving alarm checksum to verify_$state...\n";
	print "Saving alarm checksum to verify_$state...\n";
	guardRecordAlarms($SubSys, $state);

	# Re-write the snapshot file with the proper alarms
	#print LOG "Saving new snapshot file with correct alarms...\n";
	print "Saving new snapshot file with correct alarms...\n";
	system("burtrb -f $autoburt_file -o $burt_dir/$state.snap");
	
	# print "The states for the $SubSys are:\n";
	# my @all_states = guardListStates($SubSys);
	# my @states;
	# #only include non-transit states and not undefined
	# foreach $state (@all_states){
	#     if($state !~ m/TRANSIT/ && $state ne 'UNDEFINED'){ 
	# 	print ">>> $state\n";
	# 	push @states, $state; 
	#     }
	# }

        print "$state\n"; 

    #Commenting out burt patch file functionality JCB 2013/03/08
	print "Make the burt patch files\n";
	my @all_states = guardListStates($SubSys);
	foreach my $sysstate (@all_states){
	    if($sysstate !~ m/^TRANSIT/ and $sysstate ne $state and $sysstate ne "UNDEFINED"){
	    #if($sysstate ne $state){
		system("$guardian_dir/makeBurtPatch.pl $SubSys $sysstate $state");
   		system("$guardian_dir/makeBurtPatch.pl $SubSys $state $sysstate");
	    }
	}

	#select STDOUT;

	print "The states for system $SubSys are:\n";
	# Re-make the guardian MEDM screen with the new state
	system("$guardian_dir/guardMakeMEDM $SubSys");
	

	close LOG;
}

############################## guardDeleteState ############
=item B<guardDeleteState>

Deletes a state from a model by removing the burtfile, goto_, verify_, and transit_
scripts to and from that state.
This also calls the guardMakeMEDM script to re-make the guardian medm screen
and accompanying burtfile screen

Arguments: guardDeleteState($SubSys,"STATE")
Returns:   0 if ok, 1 if failed (state does not exist)

Example: my $failure = guardDeleteState("L1:SUS-MC1", "DAMPED");

=cut
############################################################ guardDeleteState
sub guardDeleteState{
	my ($SubSys, $state) = @_;

	# find the guardian directory, back compatible with 2.3
	my $guardian_dir = "/opt/rtcds/userapps/release/guardian";
	unless(-e "$guardian_dir/GuardTools.pm"){
		$guardian_dir = "/opt/rtcds/${site}/${ifo}/userapps/release/guardian";
	}

	# Delete verify_STATE, STATE.snap, STATE.guardsnap
	# Leave state_alarms.txt in place
	my $burt_dir = guardSystemDir($SubSys, 'burtfiles');
	if(-e "$burt_dir/${state}.snap"){
		unlink "$burt_dir/${state}.snap";
	}
	if(-e "$burt_dir/${state}.guardsnap"){
		unlink "$burt_dir/${state}.guardsnap";
	}
	if(-e "$burt_dir/verify_${state}"){
		unlink "$burt_dir/verify_${state}";
	}

	# Remove scripts associated with that state 
	# recover_STATE, goto_STATE, transit either direction
	my $scripts_dir = guardSystemDir($SubSys, 'scripts');
	opendir(my $dh, $scripts_dir) or die "Couldn't open $scripts_dir.";
	my @state_scripts = grep { /transit_$state\_.+/ || /transit_.+_$state/ ||
							   /goto_$state/        || /recover_$state/ } readdir($dh);
	foreach my $script (@state_scripts) {
		unlink "$scripts_dir/$script";
	}

	# Re-make MEDM screen
	system("$guardian_dir/guardMakeMEDM $SubSys");
}
############################## guardRecordAlarms ##################
=item B<guardRecordAlarms>

Writes the checksum value for a state of alarms to the proper verify_STATE
text file in guardSystemDir($SubSys, 'burtfiles')

Example:
   guardRecordAlarms($SubSys, $state);

=cut
############################################################ guardRecordAlarms
sub guardRecordAlarms {
	my ($SubSys, $state) = @_;
	my $separator = "_";
        if (index($SubSys, "-")<=0){
                        $separator = "-";
        }
	my $burtdir = guardSystemDir($SubSys, 'burtfiles');
	open my $state_fh, ">$burtdir/verify_$state" or die "Couldn't open $burtdir/verify_$state";
	my ($DCU_ID) = caGet("${SubSys}${separator}DCU_ID");
	my $FEC_STR = substr($SubSys, 0,3) . 'FEC-' . $DCU_ID;
	my ($checksum) = caGet("${FEC_STR}_GRD_ALH_CRC");
	$checksum = sprintf("0x%x", $checksum);
	print $state_fh $checksum;
	close $state_fh;
};

# end properly
1;

=head1 AUTHORS

 SJW Sept. 20, 2010
 GHS July 21, 2011
 Updated: July 22, 2011
 GHS July 25, 2011
 CJK August 14, 2011
 GHS August 31, 2011
=cut
