#!/usr/bin/env python3
import x1unittest_pybind as model #Built by rtslib
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from math import isclose

mod = model.create_instance()


mod.record_model_var("UNWRAP_PHASE_TEST_OUTPUT_1", mod.get_model_rate_Hz())

#n = np.zeros((400,), dtype=complex)
#n[40:60] = np.exp(1j*np.random.uniform(0, 2*np.pi, (20,)))
n = np.fft.ifft([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0])
data = np.fft.ifft(n)


mod.set_excitation_point_generator("UNWRAP_PHASE_TEST_EXC_0", model.CannedSignalGenerator( canned_data=np.angle(data), repeat=False))

unwrapped_data = np.unwrap(np.angle(data))

mod.run_model( len(unwrapped_data) ) 

unwrap_out = mod.get_recorded_var("UNWRAP_PHASE_TEST_OUTPUT_1")

for i in range(len(data)):
    if not isclose(unwrap_out[i], unwrapped_data[i], rel_tol=1e-12, abs_tol=0.0):
        print(f"Error: Mismatch between part an scipy library: part: {unwrap_out[i]}, library: {unwrapped_data[i]}")
        exit(1)

p0 = plt.figure(0)
plt.plot(unwrapped_data)
plt.title("np unwrapped data")

p1 = plt.figure(1)
plt.plot(unwrap_out)
plt.title("Part unwrapped data")

plt.show()
