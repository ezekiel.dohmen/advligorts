#!/usr/bin/env python3
import x1unittest_pybind as model #Built by rtslib
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sig
import math



# Define sin and cos Generators
# We must define get_next_sample() and reset() to be a Generator
#
class MySinGenerator(model.Generator):
    def __init__(self, freq_hz, amplitude, sample_rate_hz) -> None:
        super().__init__()
        self.freq_hz = freq_hz
        self.amplitude = amplitude
        self.sample_rate_hz = sample_rate_hz
        self.cur_ts = 0.0
        

    def get_next_sample(self) -> float:
        res = math.sin(2*math.pi*self.freq_hz*self.cur_ts) * self.amplitude
        self.cur_ts += 1.0/self.sample_rate_hz
        return res

    def reset(self) -> None:
        self.cur_ts = 0
        return
    
class MyCosGenerator(model.Generator):
    def __init__(self, freq_hz, amplitude, sample_rate_hz) -> None:
        super().__init__()
        self.freq_hz = freq_hz
        self.amplitude = amplitude
        self.sample_rate_hz = sample_rate_hz
        self.cur_ts = 0.0
        

    def get_next_sample(self) -> float:
        res = math.cos(2*math.pi*self.freq_hz*self.cur_ts) * self.amplitude
        self.cur_ts += 1.0/self.sample_rate_hz
        return res

    def reset(self) -> None:
        self.cur_ts = 0
        return

mod = model.create_instance()

sin_gen = MySinGenerator(freq_hz=100, amplitude=1, sample_rate_hz=2048) 
cos_gen = MyCosGenerator(freq_hz=100, amplitude=1, sample_rate_hz=2048) 

wfsPhase_sin_gen = MySinGenerator(freq_hz=100, amplitude=1, sample_rate_hz=2048) 
wfsPhase_cos_gen = MyCosGenerator(freq_hz=100, amplitude=1, sample_rate_hz=2048) 

#Set up generators that will feed the testpoints
mod.set_excitation_point_generator("OSC_PHASE_IN1", sin_gen)
mod.set_excitation_point_generator("OSC_PHASE_IN2", cos_gen)

mod.set_excitation_point_generator("OSC_WFS_PHASE_IN1", wfsPhase_sin_gen)
mod.set_excitation_point_generator("OSC_WFS_PHASE_IN2", wfsPhase_cos_gen)


#Mark part outputs so they are recorded
mod.record_model_var("OSC_PHASE_OUT1", mod.get_model_rate_Hz())
mod.record_model_var("OSC_PHASE_OUT2", mod.get_model_rate_Hz())
mod.record_model_var("OSC_PHASE_IN1_EPICS", mod.get_model_rate_Hz())
mod.record_model_var("OSC_PHASE_IN2_EPICS", mod.get_model_rate_Hz())

mod.record_model_var("OSC_WFS_PHASE_OUT1", mod.get_model_rate_Hz())
mod.record_model_var("OSC_WFS_PHASE_OUT2", mod.get_model_rate_Hz())
mod.record_model_var("OSC_WFS_PHASE_IN1_EPICS", mod.get_model_rate_Hz())
mod.record_model_var("OSC_WFS_PHASE_IN2_EPICS", mod.get_model_rate_Hz())


#Configure PHASE part
phase_shift_rad = math.pi
mod.set_var("OSC_ROTATOR_1", math.sin(phase_shift_rad))
mod.set_var("OSC_ROTATOR_2", math.cos(phase_shift_rad))

#Configure WfsPhase part
phase_r = math.pi
phase_d = math.pi/8
if phase_d == 0:
    phase_d = 0.1
mod.set_var("OSC_WFS_PHASE_1_1",  math.sin(phase_r + phase_d)/ math.sin(phase_d))
mod.set_var("OSC_WFS_PHASE_1_2",  math.cos(phase_r + phase_d)/ math.sin(phase_d))
mod.set_var("OSC_WFS_PHASE_2_1",  math.sin(phase_r)/ math.sin(phase_d))
mod.set_var("OSC_WFS_PHASE_2_2",  math.cos(phase_r)/ math.sin(phase_d))



#Run model
mod.run_model( 20 ) 

#Collect part outputs/inputs
in1 = mod.get_recorded_var("OSC_PHASE_IN1_EPICS")
in2 = mod.get_recorded_var("OSC_PHASE_IN2_EPICS")
out1 = mod.get_recorded_var("OSC_PHASE_OUT1")
out2 = mod.get_recorded_var("OSC_PHASE_OUT2")

wfs_in1 = mod.get_recorded_var("OSC_WFS_PHASE_IN1_EPICS")
wfs_in2 = mod.get_recorded_var("OSC_WFS_PHASE_IN2_EPICS")
wfs_out1 = mod.get_recorded_var("OSC_WFS_PHASE_OUT1")
wfs_out2 = mod.get_recorded_var("OSC_WFS_PHASE_OUT2")


p1 = plt.figure("cdsPhase Part")
plt.plot(in1, label='In1')
plt.plot(in2, label='In2')

plt.plot(out1, label='Out1', linestyle='dotted')
plt.plot(out2, label='Out2', linestyle='dotted')

plt.title("Configured to 90 Degrees")
plt.legend()

p1 = plt.figure("cdsWfsPhase Part")
plt.plot(wfs_in1, label='In1')
plt.plot(wfs_in2, label='In2')

plt.plot(wfs_out1, label='Out1', linestyle='dotted')
plt.plot(wfs_out2, label='Out2', linestyle='dotted')

plt.title(f"Configured r = {phase_r} Rad, d = {phase_d} Rad")
plt.legend()


plt.show()