#!/usr/bin/env python3
import x1unittest_pybind as model #Built by rtslib
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sig


mod = model.create_instance()

# Use some of the C++ defined generators
mod.set_adc_channel_generator(0, 0, model.CannedSignalGenerator( canned_data=[0, 0 ,0 ,0 , 10, 0, 0, 0, 0, -10], repeat=True))
mod.set_adc_channel_generator(0, 2, model.ConstantGenerator( value=1000.0 ))
mod.set_adc_channel_generator(0, 4, model.RampGenerator(startVal=0, step=1.5))
mod.set_adc_channel_generator(0, 6, model.GaussianNoiseGenerator(mean=0, stdDev=3))


# Define a custom python Generator
# We must define get_next_sample() and reset() to be a Generator
#
class MyCustomGenerator(model.Generator):
    def __init__(self, samples_in_cycle, pos_edge_val) -> None:
        super().__init__()
        self.samples_in_cycle = samples_in_cycle/2
        self.running_count = 0
        self.pos_edge_val = pos_edge_val
        self.is_on = 0

    def get_next_sample(self) -> float:
        res =  self.is_on 
        self.running_count += 1
        if self.running_count == self.samples_in_cycle:
            self.is_on = self.pos_edge_val - self.is_on #Toggle clock
            self.running_count = 0
        return res

    def reset(self) -> None:
        self.running_count = 0
        self.is_on = 0
        return

# Warning: we need to keep a refrence to our python generator or we have an 
# issue when run_model tries to call methods on the base rts:Generator
cust_gen = MyCustomGenerator(samples_in_cycle=10, pos_edge_val=1) #Toggle every 5 samples, between 0,1
mod.set_adc_channel_generator(0, 8, cust_gen)


mod.get_filter_module_by_name("FM0").enable_input().enable_output().set_gain(1).set_ramp_time_s(0.0)
mod.get_filter_module_by_name("FM2").enable_input().enable_output().set_gain(1).set_ramp_time_s(0.0)
mod.get_filter_module_by_name("FM4").enable_input().enable_output().set_gain(1).set_ramp_time_s(0.0)
mod.get_filter_module_by_name("FM6").enable_input().enable_output().set_gain(1).set_ramp_time_s(0.0)
mod.get_filter_module_by_name("FM8").enable_input().enable_output().set_gain(1).set_ramp_time_s(0.0)

mod.record_dac_output(True) #This allows up to lookup DAC output after a call to run Model

mod.run_model( int(mod.get_model_rate_Hz()/10) ) #Run for 1/10 th of a sec

# Trend DAC card 0 chan 0 over the simulation
dac_output = mod.get_dac_output_by_id(card=0, chan=0); #Get card 0, chan 0 output
p1 = plt.figure(0)
plt.plot(dac_output)
plt.title("Card 0, Chan 0 - Canned Signal")

dac_output = mod.get_dac_output_by_id(0, 1)
p1 = plt.figure(1)
plt.plot(dac_output)
plt.title("Card 0, Chan 1 - Constant Generator")

dac_output = mod.get_dac_output_by_id(0, 2)
p1 = plt.figure(2)
plt.plot(dac_output)
plt.title("Card 0, Chan 4 - Ramp Generator")

dac_output = mod.get_dac_output_by_id(0, 3)
p1 = plt.figure(3)
plt.plot(dac_output)
plt.title("Card 0, Chan 6 - Gaussian")

dac_output = mod.get_dac_output_by_id(0, 4)
p1 = plt.figure(4)
plt.plot(dac_output)
plt.title("Card 0, Chan 8 - Custom Generator")

plt.show()

