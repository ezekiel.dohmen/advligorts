#!/usr/bin/env python3
import x1unittest_pybind as model #Built by rtslib
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sig


mod = model.create_instance()

mod.set_ipc_receiver_generator( "X1:SHMEM_RECV_ONLY_IPC", model.RampGenerator(startVal=0, step=1.0))
mod.set_excitation_point_generator( "SHMEM_SND_ONLY_EX", model.RampGenerator(startVal=0, step=1.0) )
mod.set_ipc_receiver_generator( "X1:PCIE_RECEIVER_IPC", model.RampGenerator(startVal=0, step=1.0))
mod.set_excitation_point_generator( "PCIE_SENDER_EX", model.RampGenerator(startVal=0, step=1.0) )
mod.set_ipc_receiver_generator( "X1:RFM_RECEIVER_IPC", model.RampGenerator(startVal=0, step=1.0))
mod.set_excitation_point_generator( "RFM_SENDER_EX", model.RampGenerator(startVal=0, step=1.0) )


# Set up variable recording
mod.record_model_var("SHMEM_RECV_TP", mod.get_model_rate_Hz()) 
mod.record_model_var("X1:SHMEM_SND_ONLY_IPC", mod.get_model_rate_Hz()) 
mod.record_model_var("PCIE_RECEIVER_TP", mod.get_model_rate_Hz()) 
mod.record_model_var("X1:PCIE_SENDER_IPC", mod.get_model_rate_Hz()) 
mod.record_model_var("RFM_RECEIVER_TP", mod.get_model_rate_Hz()) 
mod.record_model_var("X1:RFM_SENDER_IPC", mod.get_model_rate_Hz()) 


mod.run_model( int(mod.get_model_rate_Hz() * 1) ) #Run for 1 second

# Trend DAC card 0 chan 0 over the simulation
SHMEM_ipc_recv_data = mod.get_recorded_var("SHMEM_RECV_TP")
SHMEM_ipc_send_data = mod.get_recorded_var("X1:SHMEM_SND_ONLY_IPC")
PCIE_ipc_recv_data = mod.get_recorded_var("PCIE_RECEIVER_TP")
PCIE_ipc_send_data = mod.get_recorded_var("X1:PCIE_SENDER_IPC")
RFM_ipc_recv_data = mod.get_recorded_var("RFM_RECEIVER_TP")
RFM_ipc_send_data = mod.get_recorded_var("X1:RFM_SENDER_IPC")

if (SHMEM_ipc_recv_data != SHMEM_ipc_send_data or
    SHMEM_ipc_send_data != PCIE_ipc_recv_data or
    PCIE_ipc_recv_data != PCIE_ipc_send_data or
    PCIE_ipc_send_data != RFM_ipc_recv_data or
    RFM_ipc_recv_data != RFM_ipc_send_data )  :
    print("Error all data is not equal!")
    exit(1)
   

p1 = plt.figure(1)
plt.plot(SHMEM_ipc_recv_data)
plt.plot(SHMEM_ipc_send_data)
plt.plot(PCIE_ipc_recv_data)
plt.plot(PCIE_ipc_send_data)
plt.plot(RFM_ipc_recv_data)
plt.plot(RFM_ipc_send_data)
plt.title("IPC Receiver from external model sender")


plt.show()

