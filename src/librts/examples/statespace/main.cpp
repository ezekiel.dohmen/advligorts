#include "Model.hh"


#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{

    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    if(model_ptr == nullptr)
    {
        std::cout << "Error coult not create Model. Exiting..."  << std::endl;
        return 2;
    }

    unsigned num_parts = model_ptr->get_num_statespace_parts();
    std::cout << "There are " << num_parts << " statespace parts in the model\n";

    for(int i =0; i < 100; ++i) {
        if ( int res = model_ptr->load_statespace_from_file("examples/data/statespace_config.json") != num_parts )
        {
            std::cout << "There was an error loading config file, ret: " << res << "\n";
            return 0;
        }
    }

    //Configure modules
    //
    model_ptr->set_var("STATESPACE_TEST_1_1_IN", 0);
    model_ptr->record_model_var("STATESPACE_TEST_1_1_OUT", model_ptr->get_model_rate_Hz());
    //std::cout << model_ptr->get_var<int>("EX_EPICS_BIN_IN").value() << std::endl;


    //Run Model
    //
    model_ptr->run_model(20);
    //Maybe change config
    //model_ptr->...()
    //Run some more
    //model_ptr->runModel(1024);

 
    std::optional<std::vector<double>> outs = model_ptr->get_recorded_var<double>("STATESPACE_TEST_1_1_OUT");

    for(int i=0; i < outs.value().size(); ++i) {
        std::cout << outs.value().at(i) << " ";
    }
    std::cout << std::endl;


    return 0;
}
