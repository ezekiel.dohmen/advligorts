#ifndef LIGO_DOLPHIN_BUFFERS_HH
#define LIGO_DOLPHIN_BUFFERS_HH

#include <memory>
#include <string>

namespace rts
{

    class DolphinBuffers
    {
        public:
            static std::unique_ptr<DolphinBuffers> create_instance(std::string model_name, size_t pcie_sz, size_t rfm_sz);
            static std::unique_ptr<DolphinBuffers> create_instance_shmem(std::string model_name, size_t pcie_sz, size_t rfm_sz);
            virtual ~DolphinBuffers(); 

            volatile void* get_PCIE_write_ptr();
            volatile void* get_PCIE_read_ptr();

            volatile void* get_RFM_write_ptr();
            volatile void* get_RFM_read_ptr();

        private:

            DolphinBuffers() {};

            int _pcie_fd;
            int _rfm_fd;
            bool _is_using_shmem = false;

            std::string _pcie_shmem_name;
            std::string _rfm_shmem_name;

            std::unique_ptr< char[] > _pcie_sp;
            std::unique_ptr< char[] > _rfm_sp;

            volatile void * _pcie_ptr;
            volatile void * _rfm_ptr;
    };



}
#endif //LIGO_DOLPHIN_BUFFERS_HH