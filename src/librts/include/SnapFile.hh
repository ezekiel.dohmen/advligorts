#ifndef LIGO_SNAP_FILE_HH
#define LIGO_SNAP_FILE_HH

#include <string>
#include <vector>
#include <map>
#include <tuple>
#include <iterator>
#include <optional>

namespace rts
{

    class SnapFile
    {
        public:
            SnapFile();
            virtual ~SnapFile();
            int load(const std::string & pathname);

            std::optional<double> getVal(const std::string & var_name);
            static std::optional<double> enumToVal(const std::string & enum_val);

            typedef std::map< std::string, std::tuple< std::string, std::string > >::iterator iterator;
            //typedef std::map< std::string, std::tuple< std::string, std::string > >::const_iterator citerator;
            iterator begin() { return _loaded_snap.begin(); }
            iterator end() { return _loaded_snap.end(); }

        private:

            static std::map< std::string, int > _enum_val_to_num;

            bool endsWith(std::string_view str, std::string_view suffix);


            //tuple: chan name, value, monitor flag
            std::map< std::string, std::tuple< std::string, std::string > > _loaded_snap;

    };


}

#endif // LIGO_SNAP_FILE_HH
