#ifndef LIGO_AWG_GENERATOR_HH
#define LIGO_AWG_GENERATOR_HH

#include "Generator.hh"
#include "awgstandalone.h"

namespace rts
{
    class AwgGenerator : public Generator
    {
        public:
            AwgGenerator(int slot, int64_t gpsTime, int cycle, int model_rate_Hz)
                : _slot(slot), _gpsTime(gpsTime), _cycle(cycle), _model_rate_Hz(model_rate_Hz)
            {
                _cycles_per_epoch = _model_rate_Hz/NUMBER_OF_EPOCHS;
            }
            virtual double get_next_sample()
            {
                double sample;
                awgbuf_t *buf = get_awgbuf(_cycle/_cycles_per_epoch);
                if (!buf->ready || !buf->buf[_slot].otype || buf->buf[_slot].pagelen != _cycles_per_epoch)
                {
                    sample = 0.0;
                }
                else if ((int64_t)buf->time != _gpsTime)
                {
                    throw std::runtime_error("AwgGenerator lost sync with AWG");
                }
                else
                {
                    sample = buf->buf[_slot].page[_cycle % _cycles_per_epoch];
                }
                _cycle++;
                _cycle %= _model_rate_Hz;
                if (_cycle == 0)
                {
                    _gpsTime++;
                }
                return sample;
            }
            virtual void reset()
            {
            }
            int get_slot()
            {
                return _slot;
            }
            virtual ~AwgGenerator() {};
        private:
            int _slot;
            int64_t _gpsTime;
            int _cycle;
            int _model_rate_Hz;
            int _cycles_per_epoch;
    };
}


#endif //LIGO_AWG_GENERATOR_HH
