#include "DolphinBuffers.hh"

#include <unistd.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <string.h>

using namespace rts;


std::unique_ptr<DolphinBuffers> DolphinBuffers::create_instance_shmem(std::string model_name, size_t pcie_sz, size_t rfm_sz)
{

    std::unique_ptr<DolphinBuffers> me_ptr (new DolphinBuffers());

    me_ptr->_pcie_shmem_name = model_name + "_dolphin_pcie";
    me_ptr->_rfm_shmem_name = model_name + "_dolphin_rfm";

    me_ptr->_pcie_fd = shm_open(me_ptr->_pcie_shmem_name.c_str(), O_CREAT | O_RDWR, 0666);
    if (me_ptr->_pcie_fd == 0) return nullptr;
    ftruncate(me_ptr->_pcie_fd, pcie_sz);
    me_ptr->_pcie_ptr = mmap(0, pcie_sz, PROT_WRITE, MAP_SHARED, me_ptr->_pcie_fd, 0);
    if ( me_ptr->_pcie_ptr == (void *) -1 )
    {
        shm_unlink(me_ptr->_pcie_shmem_name.c_str());
        return nullptr;
    }


    me_ptr->_rfm_fd = shm_open(me_ptr->_rfm_shmem_name.c_str(), O_CREAT | O_RDWR, 0666);
    if (me_ptr->_rfm_fd == 0) return nullptr;
    ftruncate(me_ptr->_rfm_fd, rfm_sz);
    me_ptr->_rfm_ptr = mmap(0, rfm_sz, PROT_WRITE, MAP_SHARED, me_ptr->_rfm_fd, 0);
    if ( me_ptr->_rfm_ptr == (void *) -1 )
    {
        shm_unlink(me_ptr->_pcie_shmem_name.c_str());
        shm_unlink(me_ptr->_rfm_shmem_name.c_str());
        return nullptr;
    }

    me_ptr->_is_using_shmem = true;

    memset((void*)me_ptr->_pcie_ptr, 0, pcie_sz);
    memset((void*)me_ptr->_rfm_ptr, 0, rfm_sz);

    return me_ptr;
}


std::unique_ptr<DolphinBuffers> DolphinBuffers::create_instance(std::string model_name, size_t pcie_sz, size_t rfm_sz)
{

    std::unique_ptr<DolphinBuffers> me_ptr (new DolphinBuffers());

    me_ptr->_pcie_shmem_name = model_name + "_dolphin_pcie";
    me_ptr->_rfm_shmem_name = model_name + "_dolphin_rfm";


    me_ptr->_pcie_sp = std::make_unique< char[] >(pcie_sz);
    me_ptr->_pcie_ptr = (volatile void *) me_ptr->_pcie_sp.get();
    if(me_ptr->_pcie_ptr == nullptr) return nullptr;
    

    me_ptr->_rfm_sp = std::make_unique< char[] >(rfm_sz);
    me_ptr->_rfm_ptr = (volatile void *) me_ptr->_rfm_sp.get();
    if(me_ptr->_rfm_ptr == nullptr) return nullptr;

    me_ptr->_is_using_shmem = false;

    memset((void*)me_ptr->_pcie_ptr, 0, pcie_sz);
    memset((void*)me_ptr->_rfm_ptr, 0, rfm_sz);

    return me_ptr;
}

DolphinBuffers::~DolphinBuffers()
{
    if ( _is_using_shmem ) {
        shm_unlink(_pcie_shmem_name.c_str());
        shm_unlink(_rfm_shmem_name.c_str());
    }
}

volatile void* DolphinBuffers::get_PCIE_write_ptr()
{
    return _pcie_ptr;
}

volatile void* DolphinBuffers::get_PCIE_read_ptr()
{
    return _pcie_ptr;
}

volatile void* DolphinBuffers::get_RFM_write_ptr()
{
    return _rfm_ptr;
}

volatile void* DolphinBuffers::get_RFM_read_ptr()
{
    return _rfm_ptr;
}
