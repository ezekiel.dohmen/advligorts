project (awgstandalone)
add_library(awgstandalone OBJECT
        ../../gds/awgtpman/awg.c
        ../../gds/awgtpman/awgfunc.c
        ../../gds/awgtpman/gdserr.c
        ../../gds/awgtpman/gdsstring.c
        ../../gds/awgtpman/gdsprm.c
        ../../gds/awgtpman/gdsrand.c
        ../../gds/awgtpman/tconv.c
        ../../gds/awgtpman/big_buffers.c
        awgstandalone.c
        )

target_include_directories(awgstandalone PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}/../../gds/awgtpman
        ${CMAKE_CURRENT_SOURCE_DIR}/../../gds/awgtpman/dtt
        ${CMAKE_CURRENT_SOURCE_DIR}/../../../src/include
        ${CMAKE_CURRENT_SOURCE_DIR}/../../../src/shmem
        )

target_compile_definitions(awgstandalone PRIVATE
        LIGO_GDS
        _ADVANCED_LIGO
        MAX_CHNNAME_SIZE=60
        _NO_KEEP_ALIVE
        _NO_TESTPOINTS
        AWGSTANDALONE
        )

set_property(TARGET awgstandalone PROPERTY POSITION_INDEPENDENT_CODE ON)

