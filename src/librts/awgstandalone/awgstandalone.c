#include <stddef.h>
#include "tconv.h"
#include "big_buffers.h"


// global vars needed from awgtpman.c
char archive_storage[256];
char *archive = archive_storage;

/**
 * Model rate in Hz.  This may not be properly defined
 * in the case where only  a frequency multiplier was given.
 */
int model_rate_Hz = 0;

/**
 * Number of values in a page, where a page will hold all data for one
 * epoch for one channel at the model rate, so page_size is model_rate_Hz / epochs_per_sec
 */
int page_size = 0;


// functions needed from gdsheartbeat.c
/*----------------------------------------------------------------------*/
/*                                                                      */
/* Internal Procedure Name: getTimeAndEpoch                             */
/*                                                                      */
/* Procedure Description: obtains the current time and divides it       */
/*                        TAI in sec and an epoch count                 */
/*                                                                      */
/* Procedure Arguments: _tai : pointer to TAI time variable             */
/*                      _epoch: pointer to epoch variable               */
/*                                                                      */
/* Procedure Returns: void                                              */
/*                                                                      */
/*----------------------------------------------------------------------*/
   static void getTimeAndEpoch (taisec_t* _tai, int* _epoch)
   {
      tainsec_t tain;   /* tai in nsec */
      tai_t     taibd;  /* broken down time */
      taisec_t  tai;    /* tai of epoch in sec */
      int       epoch;  /* epoch */

      /* obtain current time and calculate epoch */
      tain = TAInow();
      TAIsec (tain, &taibd);
      tai = taibd.tai;
      epoch = (taibd.nsec + _EPOCH / 10) / _EPOCH;
      if (epoch >= NUMBER_OF_EPOCHS) {
         epoch -= NUMBER_OF_EPOCHS;
         tai++;
      }
      if (_tai != NULL) {
         *_tai = tai;
      }
      if (_epoch != NULL) {
         *_epoch = epoch;
      }
   }

/*----------------------------------------------------------------------*/
/*                                                                      */
/* External Procedure Name: synchWithHeartbeatEx                        */
/*                                                                      */
/* Procedure Description: blocks until the next heartbeat               */
/*                                                                      */
/* Procedure Arguments: tai: variable to store TAI of heartbeat         */
/*                      epoch: variable to store epoch count            */
/*                                                                      */
/* Procedure Returns: 0 when successful or error number when failed     */
/*                                                                      */
/*----------------------------------------------------------------------*/
   int syncWithHeartbeatEx (taisec_t* tai, int* epoch)
   {
      getTimeAndEpoch (tai, epoch);
      return 0;
   }


void awgstandalone_set_model_rate_Hz(int rate)
{
    model_rate_Hz = rate;
}

awgbuf_t *get_awgbuf(int epoch)
{
    return &awgbuf.bufs[epoch];
}

