#!/usr/bin/env bash

if [ -z ${BASH_SOURCE[0]} ]
then
        echo "ERROR: BASH_SOURCE not defined. Are you using another shell?"
fi

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
export PYTHONPATH=$PYTHONPATH:$SCRIPT_DIR/../build/cmake/python/pybind/
