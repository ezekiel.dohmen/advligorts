#!/usr/bin/env python3

#import sys
#sys.path.append('../../build/cmake/python/pybind/')
import x1unittest_pybind as model #Built by rtslib

import unittest
import numpy as np
from scipy import signal
from math import isclose

class TestUnwrapPhase(unittest.TestCase):



    @classmethod
    def setUpClass(cls):
        pass
        

    def test_positive_unwrap(self):
        mod = model.create_instance()
        mod.record_model_var("UNWRAP_PHASE_TEST_OUTPUT_1", mod.get_model_rate_Hz())
        input_complex = np.fft.ifft([0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        phase_data = np.angle(input_complex)
        mod.set_excitation_point_generator("UNWRAP_PHASE_TEST_EXC_0", model.CannedSignalGenerator( canned_data=phase_data, repeat=False))
        mod.run_model( len(phase_data) ) 
        np_unwrapped = np.unwrap(phase_data)
        unwrap_out = mod.get_recorded_var("UNWRAP_PHASE_TEST_OUTPUT_1")
        for i in range(len(phase_data)):
            self.assertTrue(isclose(unwrap_out[i], np_unwrapped[i], rel_tol=1e-12, abs_tol=0.0))

    def test_negative_unwrap(self): 
        mod = model.create_instance()
        mod.record_model_var("UNWRAP_PHASE_TEST_OUTPUT_1", mod.get_model_rate_Hz())
        input_complex = np.fft.ifft([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0])
        phase_data = np.angle(input_complex)
        mod.set_excitation_point_generator("UNWRAP_PHASE_TEST_EXC_0", model.CannedSignalGenerator( canned_data=phase_data, repeat=False))
        mod.run_model( len(phase_data) ) 
        np_unwrapped = np.unwrap(phase_data)
        unwrap_out = mod.get_recorded_var("UNWRAP_PHASE_TEST_OUTPUT_1")
        for i in range(len(phase_data)):
            self.assertTrue(isclose(unwrap_out[i], np_unwrapped[i], rel_tol=1e-12, abs_tol=0.0))

    def test_random_unwrap(self): 
        for tests in range(20):
            mod = model.create_instance( )
            mod.record_model_var("UNWRAP_PHASE_TEST_OUTPUT_1", mod.get_model_rate_Hz())

            n = np.zeros((400,), dtype=complex)
            n[40:60] = np.exp(1j*np.random.uniform(0, 2*np.pi, (20,)))
            input_complex = np.fft.ifft(n)

            phase_data = np.angle(input_complex)
            mod.set_excitation_point_generator("UNWRAP_PHASE_TEST_EXC_0", model.CannedSignalGenerator( canned_data=phase_data, repeat=False))
            mod.run_model( len(phase_data) ) 
            np_unwrapped = np.unwrap(phase_data)
            unwrap_out = mod.get_recorded_var("UNWRAP_PHASE_TEST_OUTPUT_1")
            for i in range(len(phase_data)):
                self.assertTrue(isclose(unwrap_out[i], np_unwrapped[i], rel_tol=1e-12, abs_tol=0.0))

            del mod #Clean up singleton for next loop


if __name__ == '__main__':
    unittest.main()






