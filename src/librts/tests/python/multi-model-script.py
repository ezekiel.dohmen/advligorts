#!/usr/bin/env python3


# Import TWO models into the same script, this is what we are really testing here
import x1unittest_pybind as unittest_model 
import x1tutorial_pybind as tutorial_model

import unittest
import numpy as np
from scipy import signal
from math import isclose

class TestUnwrapPhase(unittest.TestCase):


    @classmethod
    def setUpClass(self):
        pass
        

    def test_nominal(self):
        mod1 = unittest_model.create_instance()
        mod2 = tutorial_model.create_instance()
        self.assertTrue( mod1.get_model_rate_Hz() > 0 ) 
        self.assertTrue( mod2.get_model_rate_Hz() > 0 )



if __name__ == '__main__':
    unittest.main()






