

#include "Model.hh"
#include "ConstantGenerator.hh"
#include "RampGenerator.hh"
#include "CannedSignalGenerator.hh"

#include <iostream>
#include <cmath>

#include "Catch_Multiversion.hpp"

TEST_CASE( "canned generator test", "[general]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    std::vector<double> data = {1,2,3,4,5,6,7};
    model_ptr->set_excitation_point_generator( "TEST_EXCITATION", std::make_unique<rts::CannedSignalGenerator>(data, true) );

    model_ptr->record_model_var("EXCITATION_EPICS_OUT", model_ptr->get_model_rate_Hz());

    // Run the model
    model_ptr->run_model( 1024 );

    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("EXCITATION_EPICS_OUT");
    REQUIRE(epics_out);

    int c = 0;
    for( unsigned i = 0; i < epics_out.value().size(); ++i)
    {
        REQUIRE( data[c] == epics_out.value()[i] );
        c = (c+1) % data.size();
    }

}

TEST_CASE( "no repeat canned generator test", "[general]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    std::vector<double> data = {1,2,3,4,5,6,7};
    model_ptr->set_excitation_point_generator( "TEST_EXCITATION", std::make_unique<rts::CannedSignalGenerator>(data, false) );

    model_ptr->record_model_var("EXCITATION_EPICS_OUT", model_ptr->get_model_rate_Hz());

    // Run the model
    model_ptr->run_model( 37 );

    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("EXCITATION_EPICS_OUT");
    REQUIRE(epics_out);

    //First 7 vals are buffer
    for( unsigned i = 1; i < 7; ++i)
    {
        REQUIRE( i == epics_out.value()[i-1] );
    }
    //Last val get repeated after end
    for( unsigned i = 7; i < epics_out.value().size(); ++i)
    {
        REQUIRE( 7 == epics_out.value()[i]);
    }

}

