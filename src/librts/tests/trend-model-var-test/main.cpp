
#include "Model.hh"
#include "RampGenerator.hh"

#include <iostream>
#include <cmath>

#include "Catch_Multiversion.hpp"


TEST_CASE( "Trend 10 times a sec for one sec", "[trendvars]" )
{


    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE(model_ptr);

    //Set inputs
    //
    model_ptr->set_adc_channel_generator( 0, 30, std::make_shared<rts::RampGenerator>(0, 1) );

    //Configure modules
    //
    model_ptr->record_model_var("RAMP_EPICS_OUT", 10); //The 2nd parameter records at 10 Hz


    //Configure record points
    //
    model_ptr->record_dac_output(false);

    //Run Model
    //
    model_ptr->run_model(model_ptr->get_model_rate_Hz()*1);//Run for 1 sec

    //Examine Recorded Outputs
    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("RAMP_EPICS_OUT");
    REQUIRE(epics_out);

    REQUIRE( epics_out.value().size() == 10 ); 

}


TEST_CASE( "Record at 16Hz for 10 sec", "[trendvars]" )
{


    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE(model_ptr);

    //Set inputs
    //
    model_ptr->set_adc_channel_generator( 0, 30, std::make_shared<rts::RampGenerator>(0, 1) );

    //Configure modules
    //
    model_ptr->record_model_var("RAMP_EPICS_OUT", 16); //The 2nd parameter records at 16 Hz


    //Configure record points
    //
    model_ptr->record_dac_output(false);

    //Run Model
    //
    model_ptr->run_model(model_ptr->get_model_rate_Hz()*10);//Run for 1 sec

    //Examine Recorded Outputs
    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("RAMP_EPICS_OUT");
    REQUIRE(epics_out);

    REQUIRE( epics_out.value().size() == 16*10 );

}



TEST_CASE( "Record at model rate for 20 sec", "[trendvars]" )
{


    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE(model_ptr);

    //Set inputs
    //
    model_ptr->set_adc_channel_generator( 0, 30, std::make_shared<rts::RampGenerator>(0, 1) );

    //Configure modules
    //
    model_ptr->record_model_var("RAMP_EPICS_OUT", model_ptr->get_model_rate_Hz()); //Record at model rate (default)


    //Configure record points
    //
    model_ptr->record_dac_output(false);

    //Run Model
    //
    model_ptr->run_model(model_ptr->get_model_rate_Hz()*20);//Run for 20 sec

    //Examine Recorded Outputs
    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("RAMP_EPICS_OUT");
    REQUIRE(epics_out);

    REQUIRE( epics_out.value().size() ==  20*model_ptr->get_model_rate_Hz());

}


TEST_CASE( "Record at model rate for 2 1 second runs", "[trendvars]" )
{
    //
    // This test verifies that the recorded var only holds the last run_model()'s data
    //

    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE(model_ptr);

    //Set inputs
    //
    model_ptr->set_adc_channel_generator( 0, 30, std::make_shared<rts::RampGenerator>(0, 1) );

    //Configure modules
    //
    model_ptr->record_model_var("RAMP_EPICS_OUT", model_ptr->get_model_rate_Hz()); //Record at model rate (default)


    //Configure record points
    //
    model_ptr->record_dac_output(false);

    //Run Model
    //
    model_ptr->run_model(model_ptr->get_model_rate_Hz()*1);//Run for 1 sec

    model_ptr->run_model(model_ptr->get_model_rate_Hz()*1);//Run for 1 sec

    //Examine Recorded Outputs
    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("RAMP_EPICS_OUT");
    REQUIRE(epics_out);

    //We only expect the last runs data
    REQUIRE( epics_out.value().size() ==  1*model_ptr->get_model_rate_Hz());

}

TEST_CASE( "Record multicycle test", "[trendvars]" )
{
    //
    // This test verifies that the recorded var holds 
    // both runs of data because multicycle_record_control(true) 
    // is called
    //

    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE(model_ptr);

    model_ptr->multicycle_record_control(true);

    //Set inputs
    //
    model_ptr->set_adc_channel_generator( 0, 30, std::make_shared<rts::RampGenerator>(0, 1) );

    //Configure modules
    //
    model_ptr->record_model_var("RAMP_EPICS_OUT", model_ptr->get_model_rate_Hz()); //Record at model rate (default)


    //Configure record points
    //
    model_ptr->record_dac_output(false);

    //Run Model
    //
    model_ptr->run_model(model_ptr->get_model_rate_Hz()*1);//Run for 1 sec

    model_ptr->run_model(model_ptr->get_model_rate_Hz()*1);//Run for 1 sec

    //Examine Recorded Outputs
    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("RAMP_EPICS_OUT");
    REQUIRE(epics_out);

    //We only expect the last runs data
    REQUIRE( epics_out.value().size() ==  2*model_ptr->get_model_rate_Hz());

}



TEST_CASE( "Trend testpoint", "[trendvars]" )
{


    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE(model_ptr);

    //Set inputs
    //
    model_ptr->set_adc_channel_generator( 0, 31, std::make_shared<rts::RampGenerator>(0, 1) );

    //Configure modules
    //
    model_ptr->record_model_var("TP_TEST", model_ptr->get_model_rate_Hz());


    //Run Model
    //
    model_ptr->run_model(model_ptr->get_model_rate_Hz()*2);//Run for 2 sec

    //Examine Recorded Outputs
    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("TP_TEST");
    REQUIRE(epics_out);

    REQUIRE( epics_out.value().size() ==  model_ptr->get_model_rate_Hz()*2);

}


