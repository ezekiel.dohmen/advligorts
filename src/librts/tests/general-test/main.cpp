#include "Model.hh"
#include "ConstantGenerator.hh"
#include "RampGenerator.hh"


#include <iostream>
#include <cmath>

#include "Catch_Multiversion.hpp"

TEST_CASE( "Double creation", "[general]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    std::unique_ptr<rts::Model> model2_ptr = rts::Model::create_instance();
    REQUIRE( model2_ptr == nullptr);

}

TEST_CASE( "excitation test", "[general]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    model_ptr->set_excitation_point_generator( "TEST_EXCITATION", std::make_shared<rts::RampGenerator>(0, 1) );

    model_ptr->record_model_var("EXCITATION_EPICS_OUT", model_ptr->get_model_rate_Hz());

    model_ptr->run_model(model_ptr->get_model_rate_Hz() * 1);

    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("EXCITATION_EPICS_OUT");
    REQUIRE(epics_out);

    double sample = epics_out.value()[0];
    for( unsigned i = 1; i < epics_out.value().size(); ++i)
    {
        REQUIRE( sample == epics_out.value()[i] - 1 );
        sample = epics_out.value()[i];
    }
}

TEST_CASE( "SHMEM IPC sender test", "[general, ipc]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    model_ptr->set_excitation_point_generator( "SHMEM_SND_ONLY_EX", std::make_shared<rts::RampGenerator>(0, 1) );

    model_ptr->record_model_var("X1:SHMEM_SND_ONLY_IPC", model_ptr->get_model_rate_Hz());

    model_ptr->run_model(model_ptr->get_model_rate_Hz() * 1);

    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("X1:SHMEM_SND_ONLY_IPC");
    REQUIRE(epics_out);

    double sample = epics_out.value()[0];
    for( unsigned i = 1; i < epics_out.value().size(); ++i)
    {
        INFO("i = " << i);
        REQUIRE( sample == epics_out.value()[i] - 1 );
        sample = epics_out.value()[i];
    }
}


TEST_CASE( "SHMEM IPC receiver test", "[general, ipc]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    model_ptr->set_ipc_receiver_generator( "X1:SHMEM_RECV_ONLY_IPC", std::make_shared<rts::RampGenerator>(0, 1) );

    model_ptr->record_model_var("SHMEM_RECV_TP", model_ptr->get_model_rate_Hz());

    model_ptr->run_model(model_ptr->get_model_rate_Hz() * 1);

    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("SHMEM_RECV_TP");
    REQUIRE(epics_out);

    double sample = epics_out.value()[0];
    for( unsigned i = 1; i < epics_out.value().size(); ++i)
    {
        INFO("i = " << i);
        REQUIRE( sample == epics_out.value()[i] - 1 );
        sample = epics_out.value()[i];
    }
}


TEST_CASE( "PCIE IPC sender test", "[general, ipc]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    model_ptr->set_excitation_point_generator( "PCIE_SENDER_EX", std::make_shared<rts::RampGenerator>(0, 1) );

    model_ptr->record_model_var("X1:PCIE_SENDER_IPC", model_ptr->get_model_rate_Hz());

    model_ptr->run_model(model_ptr->get_model_rate_Hz() * 1);

    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("X1:PCIE_SENDER_IPC");
    REQUIRE(epics_out);

    double sample = epics_out.value()[0];
    for( unsigned i = 1; i < epics_out.value().size(); ++i)
    {
        INFO("i = " << i);
        REQUIRE( sample == epics_out.value()[i] - 1 );
        sample = epics_out.value()[i];
    }
}

TEST_CASE( "PCIE IPC receiver test", "[general, ipc]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    model_ptr->set_ipc_receiver_generator( "X1:PCIE_RECEIVER_IPC", std::make_shared<rts::RampGenerator>(0, 1) );

    model_ptr->record_model_var("PCIE_RECEIVER_TP", model_ptr->get_model_rate_Hz());

    model_ptr->run_model(model_ptr->get_model_rate_Hz() * 1);

    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("PCIE_RECEIVER_TP");
    REQUIRE(epics_out);

    double sample = epics_out.value()[0];
    for( unsigned i = 1; i < epics_out.value().size(); ++i)
    {
        INFO("i = " << i);
        REQUIRE( sample == epics_out.value()[i] - 1 );
        sample = epics_out.value()[i];
    }
}

TEST_CASE( "RFM IPC sender test", "[general, ipc]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    model_ptr->set_excitation_point_generator( "RFM_SENDER_EX", std::make_shared<rts::RampGenerator>(0, 1) );

    model_ptr->record_model_var("X1:RFM_SENDER_IPC", model_ptr->get_model_rate_Hz());

    model_ptr->run_model(model_ptr->get_model_rate_Hz() * 1);

    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("X1:RFM_SENDER_IPC");
    REQUIRE(epics_out);

    double sample = epics_out.value()[0];
    for( unsigned i = 1; i < epics_out.value().size(); ++i)
    {
        INFO("i = " << i);
        REQUIRE( sample == epics_out.value()[i] - 1 );
        sample = epics_out.value()[i];
    }
}

TEST_CASE( "RFM IPC receiver test", "[general, ipc]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    model_ptr->set_ipc_receiver_generator( "X1:RFM_RECEIVER_IPC", std::make_shared<rts::RampGenerator>(0, 1) );

    model_ptr->record_model_var("RFM_RECEIVER_TP", model_ptr->get_model_rate_Hz());

    model_ptr->run_model(model_ptr->get_model_rate_Hz() * 5);

    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("RFM_RECEIVER_TP");
    REQUIRE(epics_out);

    double sample = epics_out.value()[0];
    for( unsigned i = 1; i < epics_out.value().size(); ++i)
    {
        INFO("i = " << i);
        REQUIRE( sample == epics_out.value()[i] - 1 );
        sample = epics_out.value()[i];
    }
}
