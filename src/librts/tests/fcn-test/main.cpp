
#include "Model.hh"
#include "ConstantGenerator.hh"
#include "RampGenerator.hh"


#include <iostream>
#include <cmath>

#include "Catch_Multiversion.hpp"


#define TEST_SUBSYSTEM "Fcn_Test_"
#define TO_RAD(X) (X * (M_PI / 180.0f))
#define TO_DEG(X) (X * (180.0f / M_PI))

TEST_CASE( "simple fcn tests", "[general]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);


    model_ptr->record_model_var(TEST_SUBSYSTEM "SIN_0_OUT", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "SIN_1_OUT", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "SINDEG_0_OUT", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "SINDEG_1_OUT", model_ptr->get_model_rate_Hz());

    model_ptr->record_model_var(TEST_SUBSYSTEM "COS_0_OUT", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "COS_1_OUT", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "COSDEG_0_OUT", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "COSDEG_1_OUT", model_ptr->get_model_rate_Hz());

    model_ptr->record_model_var(TEST_SUBSYSTEM "LOG10_1_OUT", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "LOG10_10_OUT", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "LOG10_1024_OUT", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "LOG10_FLOAT_OUT", model_ptr->get_model_rate_Hz());

    model_ptr->record_model_var(TEST_SUBSYSTEM "FABS_1_OUT", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "FABS_2_OUT", model_ptr->get_model_rate_Hz());

    model_ptr->record_model_var(TEST_SUBSYSTEM "SQRT_2_OUT", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "SQRT_4_OUT", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "SQRT_FLOAT_OUT", model_ptr->get_model_rate_Hz());

    model_ptr->record_model_var(TEST_SUBSYSTEM "COMPLEX_USE_1_OUT", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "CUSTOM_FUNC0_OUT", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "CUSTOM_FUNC2_OUT", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "CUSTOM_FUNC3_OUT", model_ptr->get_model_rate_Hz());


    model_ptr->run_model(10000);

    //sin() tests
    std::optional< std::vector<double> > out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "SIN_0_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::sin(0)).epsilon( std::numeric_limits<float>::epsilon() ));

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "SIN_1_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::sin(1)).epsilon( std::numeric_limits<float>::epsilon() ));

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "SINDEG_0_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::sin(TO_RAD(0))).epsilon( std::numeric_limits<float>::epsilon() ));

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "SINDEG_1_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::sin(TO_RAD(1))).epsilon( std::numeric_limits<float>::epsilon() ));


    //cos() tests
    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "COS_0_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::cos(0)).epsilon( std::numeric_limits<float>::epsilon() ));

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "COS_1_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::cos(1)).epsilon( std::numeric_limits<float>::epsilon() ));

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "COSDEG_0_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::cos(TO_RAD(0))).epsilon( std::numeric_limits<float>::epsilon() ));

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "COSDEG_1_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::cos(TO_RAD(1))).epsilon( std::numeric_limits<float>::epsilon() ));

    //log10() tests
    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "LOG10_1_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::log10(1)).epsilon( std::numeric_limits<float>::epsilon() ));

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "LOG10_10_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::log10(10)).epsilon( std::numeric_limits<float>::epsilon() ));

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "LOG10_1024_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::log10(1024)).epsilon( std::numeric_limits<float>::epsilon() ));

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "LOG10_FLOAT_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::log10(333.21)).epsilon( std::numeric_limits<float>::epsilon() ));


    //fabs() tests
    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "FABS_1_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::fabs(-1.567)).epsilon( std::numeric_limits<float>::epsilon() ));

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "FABS_2_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::fabs(1.567)).epsilon( std::numeric_limits<float>::epsilon() ));

    //sqrt() tests
    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "SQRT_2_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::sqrt(2.0)).epsilon( std::numeric_limits<float>::epsilon() ));

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "SQRT_4_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::sqrt(4)).epsilon( std::numeric_limits<float>::epsilon() ));

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "SQRT_FLOAT_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::sqrt(2.4)).epsilon( std::numeric_limits<float>::epsilon() ));

    //complex use case
    //lsin3+lcos4+llog10(fcn_test_mux18[2])*lfabs(fcn_test_mux18[3])/lsqrt(fcn_test_mux18[4]);
    
    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "COMPLEX_USE_1_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::sin(2.4)+std::cos(36)+std::log10(1000)*std::fabs(-9.55)/std::sqrt(4.4))
                                 .epsilon( std::numeric_limits<float>::epsilon() ));

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "CUSTOM_FUNC0_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::ceil(0.93)).epsilon( std::numeric_limits<float>::epsilon() ));

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "CUSTOM_FUNC2_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx(std::log2(10)*4.4 + std::round(33.5) + std::sin(4.9))
                                 .epsilon( std::numeric_limits<float>::epsilon() ));

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "CUSTOM_FUNC3_OUT");
    REQUIRE(out);
    REQUIRE(out.value()[9999] == Approx( 0.93 * (180.0 / M_PI))
                                 .epsilon( std::numeric_limits<float>::epsilon() ));


}






