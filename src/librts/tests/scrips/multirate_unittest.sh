#!/usr/bin/env bash

set -e

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)


for VAR in '256' '2K' '4096' '16K' '32K' '64K'
do
    cd ${SCRIPT_DIR}/../../ && make clean && RCG_OVERRIDE_CDS_PARAMETERS="host=x1cymac:ifo=X1:rate="$VAR":dcuid=11:requireIOcnt=0" make x1unittest && make test

done

