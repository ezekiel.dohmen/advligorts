
#include "Model.hh"
#include "RampGenerator.hh"

#include <iostream>
#include <cmath>

#include "Catch_Multiversion.hpp"


TEST_CASE( "Test the statespace part", "[statespace]" )
{


    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE(model_ptr);


    //Load the config over and over
    for(int i=0; i < 100; ++i) {
        REQUIRE( model_ptr->load_statespace_from_file("../../../../examples/data/statespace_config.json") > 0);
    }

    //Run Model
    //
    model_ptr->run_model(model_ptr->get_model_rate_Hz()*1);//Run for 1 sec

    //Impluse on input
    REQUIRE( model_ptr->set_var("STATESPACE_TEST_1_1_IN", 10) );
    model_ptr->run_model( 1 );
    REQUIRE( model_ptr->set_var("STATESPACE_TEST_1_1_IN", 0) );

    REQUIRE( model_ptr->record_model_var( "STATESPACE_TEST_1_1_OUT", model_ptr->get_model_rate_Hz() ));
    model_ptr->run_model(model_ptr->get_model_rate_Hz()*5);

    double max = 0.0;
    auto data =  model_ptr->get_recorded_var<double>("STATESPACE_TEST_1_1_OUT");
    for( const auto & d : data.value() ) {
        if ( d > max ) max = d;
    }
    REQUIRE( max > 3.0);
    REQUIRE ( model_ptr->stop_recording_model_var("STATESPACE_TEST_1_1_OUT") );

    model_ptr->run_model(model_ptr->get_model_rate_Hz()*100);//Run for 100 sec

    //Verify we have settled to around 0
    model_ptr->run_model( 1 );
    REQUIRE( model_ptr->get_var<double>("STATESPACE_TEST_1_1_OUT").value_or(-1000) < 0.0001 );
    REQUIRE( model_ptr->get_var<double>("STATESPACE_TEST_1_1_OUT").value_or(-1000) > -0.0001 );


}


