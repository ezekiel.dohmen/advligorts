
#include "Model.hh"
#include "ConstantGenerator.hh"
#include "RampGenerator.hh"


#include <iostream>
#include <cmath>

#include "Catch_Multiversion.hpp"


#define TEST_SUBSYSTEM "LogicalOperator_Test_"


auto my_and = [](int t1, int t2) { return t1 && t2; };
auto my_or = [](int t1, int t2) { return t1 || t2; };
auto my_nand = [](int t1, int t2) { return !(t1 && t2); };
auto my_nor = [](int t1, int t2) { return !(t1 || t2); };
auto my_xor = [](int t1, int t2) { return t1 ^ t2; };

auto my_not = [](int t1) { return !t1; };


template<class T, class F> bool logical(T a, T b, F f)
{
    return f(a, b);
}

template<class T, class F> bool logical(T a, F f)
{
    return f(a);
}



template<class F>
void run_test(std::unique_ptr<rts::Model> & model_ptr, std::string part_name, F operation_func, int in1, int in2)
{

    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_1", in1));
    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_2", in2));

    model_ptr->run_model(1);

    REQUIRE(  model_ptr->get_var<int>(TEST_SUBSYSTEM + part_name + "_OUT_1").value()  == logical(in1, in2, operation_func)  );

}

template<class F>
void two_input_gen(std::unique_ptr<rts::Model> & model_ptr, std::string part_name, F operation_func)
{
    for(int i = 0; i < 2; ++i) {
        for(int j = 0; j < 2; ++j) {
            run_test(model_ptr, part_name, operation_func, i, j);
        }
    }
}

template<class F>
void one_input_gen(std::unique_ptr<rts::Model> & model_ptr, std::string part_name, F operation_func)
{
    for(int i = 0; i < 2; ++i) {
        REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_1", i));

        model_ptr->run_model(1);

        REQUIRE(  model_ptr->get_var<int>(TEST_SUBSYSTEM + part_name + "_OUT_1").value()  == logical(i, operation_func)  );

    }
}


TEST_CASE( "simple LogicalOperator tests", "[general]" )
{
    
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    two_input_gen(model_ptr, "AND", my_and);
    two_input_gen(model_ptr, "OR", my_or);
    two_input_gen(model_ptr, "NAND", my_nand);
    two_input_gen(model_ptr, "NOR", my_nor);
    two_input_gen(model_ptr, "XOR", my_xor);

    one_input_gen(model_ptr, "NOT", my_not);

}






