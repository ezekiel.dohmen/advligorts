
#include "Model.hh"
#include "ConstantGenerator.hh"
#include "RampGenerator.hh"


#include <iostream>
#include <cmath>

#include "Catch_Multiversion.hpp"


#define TEST_SUBSYSTEM "OSC_"

TEST_CASE( "Regular osc test", "[OSC]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    double sin_freq_hz = 64; //Power of two, so we gen integer shift

    //OSC_FIXED_PHASE_FREQ, OSC_FIXED_PHASE_CLKGAIN, OSC_FIXED_PHASE_SINGAIN, OSC_FIXED_PHASE_COSGAIN
    //OSC_FIXED_PHASE_TRAMP, OSC_FIXED_PHASE_CLK_OUT, OSC_FIXED_PHASE_COS_OUT, OSC_FIXED_PHASE_SIN_OUT

    model_ptr->run_model( model_ptr->get_model_rate_Hz() * 100);


    REQUIRE( model_ptr->record_model_var(TEST_SUBSYSTEM "REGULAR_CLK_OUT", model_ptr->get_model_rate_Hz()));// Same as sin output but with own gain
    REQUIRE( model_ptr->record_model_var(TEST_SUBSYSTEM "REGULAR_SIN_OUT", model_ptr->get_model_rate_Hz()));
    REQUIRE( model_ptr->record_model_var(TEST_SUBSYSTEM "REGULAR_COS_OUT", model_ptr->get_model_rate_Hz()));

    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "REGULAR_FREQ", sin_freq_hz));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "REGULAR_CLKGAIN", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "REGULAR_SINGAIN", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "REGULAR_COSGAIN", 1));

    model_ptr->run_model(300);

    std::optional< std::vector<double> > clk_out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "REGULAR_CLK_OUT");
    REQUIRE(clk_out);
    std::optional< std::vector<double> > sin_out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "REGULAR_SIN_OUT");
    REQUIRE(sin_out);
    std::optional< std::vector<double> > cos_out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "REGULAR_COS_OUT");
    REQUIRE(cos_out);

    const double samples_in_cycle = model_ptr->get_model_rate_Hz() / sin_freq_hz;
    const double expected_shift_deg = 90;
    const int expected_shift_samples = (int)( (expected_shift_deg/360) * samples_in_cycle);

    REQUIRE(expected_shift_samples >= 1); //If this fails the number the sin_freq_hz might be be too high for the model rate

    for ( int i = 0; i < sin_out.value().size() - expected_shift_samples; ++i)
    {
        INFO("i = " << i << " cos: " << cos_out.value()[i] << " sin: " << sin_out.value()[i+expected_shift_samples] << " expected_shift: " << expected_shift_samples
             << " diff: " << cos_out.value()[i]-sin_out.value()[i+expected_shift_samples]  );
        REQUIRE( fabs(cos_out.value()[i] - sin_out.value()[i+expected_shift_samples]) < 1e-14);
    }

}


TEST_CASE( "Fixed phase osc test", "[OSC]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    double sin_freq_hz = 64; //Power of two, so we gen integer shift

    //OSC_FIXED_PHASE_FREQ, OSC_FIXED_PHASE_CLKGAIN, OSC_FIXED_PHASE_SINGAIN, OSC_FIXED_PHASE_COSGAIN
    //OSC_FIXED_PHASE_TRAMP, OSC_FIXED_PHASE_CLK_OUT, OSC_FIXED_PHASE_COS_OUT, OSC_FIXED_PHASE_SIN_OUT

    model_ptr->run_model( model_ptr->get_model_rate_Hz() * 100);


    REQUIRE( model_ptr->record_model_var(TEST_SUBSYSTEM "FIXED_PHASE_CLK_OUT", model_ptr->get_model_rate_Hz()));// Same as sin output but with own gain
    REQUIRE( model_ptr->record_model_var(TEST_SUBSYSTEM "FIXED_PHASE_SIN_OUT", model_ptr->get_model_rate_Hz()));
    REQUIRE( model_ptr->record_model_var(TEST_SUBSYSTEM "FIXED_PHASE_COS_OUT", model_ptr->get_model_rate_Hz()));

    REQUIRE( model_ptr->set_var("OSC_FIXED_PHASE_FREQ", sin_freq_hz)); 
    REQUIRE( model_ptr->set_var("OSC_FIXED_PHASE_CLKGAIN", 1));
    REQUIRE( model_ptr->set_var("OSC_FIXED_PHASE_SINGAIN", 1));
    REQUIRE( model_ptr->set_var("OSC_FIXED_PHASE_COSGAIN", 1));

    model_ptr->run_model(300);

    std::optional< std::vector<double> > clk_out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "FIXED_PHASE_CLK_OUT");
    REQUIRE(clk_out);
    std::optional< std::vector<double> > sin_out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "FIXED_PHASE_SIN_OUT");
    REQUIRE(sin_out);
    std::optional< std::vector<double> > cos_out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "FIXED_PHASE_COS_OUT");
    REQUIRE(cos_out);

    const double samples_in_cycle = model_ptr->get_model_rate_Hz() / sin_freq_hz;
    const double expected_shift_deg = 90;
    const int expected_shift_samples = (int)( (expected_shift_deg/360) * samples_in_cycle);
  
    REQUIRE(expected_shift_samples >= 1);

    for ( int i = 0; i < sin_out.value().size() - expected_shift_samples; ++i)
    {
        INFO("i = " << i << " cos: " << cos_out.value()[i] << " sin: " << sin_out.value()[i+expected_shift_samples] << " expected_shift: " << expected_shift_samples
             << " diff: " << cos_out.value()[i]-sin_out.value()[i+expected_shift_samples]  );
        REQUIRE( fabs(cos_out.value()[i] - sin_out.value()[i+expected_shift_samples]) < 1e-14);
    }

}






