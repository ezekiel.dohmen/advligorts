
#include "Model.hh"


#include <iostream>
#include <cmath>

#include "Catch_Multiversion.hpp"


#define TEST_SUBSYSTEM "MathFunction_Test_"


template<class F>
void run_test(std::unique_ptr<rts::Model> & model_ptr, std::string part_name, double value, F operation_func)
{

     REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_1", value));

     model_ptr->run_model(1);

     REQUIRE(model_ptr->get_var<double>(TEST_SUBSYSTEM + part_name + "_OUT_1").value() ==  Approx(operation_func(value)));
}



TEST_CASE( "simple MathFunction tests", "[general]" )
{
    
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);


    double (*square_func) ( double arg ) = [](double arg) { return arg * arg; }; 
    run_test(model_ptr, "SQUARE", 4.5, square_func );
    run_test(model_ptr, "SQUARE", 0, square_func );
    run_test(model_ptr, "SQUARE", -3487.3476, square_func );

    //Store the function pointer so the compiler knows what overload we want
    //double (*sqrt_func) ( double arg ) = std::sqrt;

    double (*recip_func) ( double arg ) = [](double arg) { return 1.0 / arg; };
    run_test(model_ptr, "RECIPROCAL", 5, recip_func );
    run_test(model_ptr, "RECIPROCAL", 23.5, recip_func );
    run_test(model_ptr, "RECIPROCAL", -33.3, recip_func );

    //The model adds 1 to keep the input to log valid, so we also add 1 here
    double (*log10_func) ( double arg ) = [](double arg) { return std::log10(arg+1); };
    run_test(model_ptr, "LOG10", 5, log10_func );
    run_test(model_ptr, "LOG10", 23.5, log10_func );

    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM "MOD_INPUT_1", 35.9)); //Truncates to 35
    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM "MOD_INPUT_2", 4.9)); //Truncates to 4
    model_ptr->run_model(1);
    REQUIRE(model_ptr->get_var<int>(TEST_SUBSYSTEM "MOD_OUT_1").value() == 35 % 4 );


}


TEST_CASE( "atan2 tests", "[general]" )
{

    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);
    const std::string subsys = "ATAN2_TEST_";


    REQUIRE(model_ptr->set_var(subsys + "ATAN2_INPUT_0", 0));
    REQUIRE(model_ptr->set_var(subsys + "ATAN2_INPUT_1", 0));
    model_ptr->run_model(1);
    REQUIRE(model_ptr->get_var<double>(subsys +  "ATAN2_OUTPUT_0").value() ==  Approx( 0.0 ));


    REQUIRE(model_ptr->set_var(subsys + "ATAN2_INPUT_0", 3.0));
    REQUIRE(model_ptr->set_var(subsys + "ATAN2_INPUT_1", 5.342));
    model_ptr->run_model(1);
    REQUIRE(model_ptr->get_var<double>(subsys +  "ATAN2_OUTPUT_0").value() ==  Approx( atan2(3, 5.342) ));

    REQUIRE(model_ptr->set_var(subsys + "ATAN2_INPUT_0", -43.1));
    REQUIRE(model_ptr->set_var(subsys + "ATAN2_INPUT_1", -55.1));
    model_ptr->run_model(1);
    REQUIRE(model_ptr->get_var<double>(subsys +  "ATAN2_OUTPUT_0").value() ==  Approx( atan2(-43.1, -55.1) ));


    REQUIRE(model_ptr->set_var(subsys + "ATAN2_INPUT_0", 3.1));
    REQUIRE(model_ptr->set_var(subsys + "ATAN2_INPUT_1", 1.1));
    model_ptr->run_model(1);
    REQUIRE(model_ptr->get_var<double>(subsys +  "ATAN2_OUTPUT_0").value() ==  Approx( atan2(3.1, 1.1) ));




}


