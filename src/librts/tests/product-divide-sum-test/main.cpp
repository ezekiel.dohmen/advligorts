
#include "Model.hh"


#include <iostream>
#include <cmath>


#include "Catch_Multiversion.hpp"


#define TEST_SUBSYSTEM "Product_Divide_Sum_Abs_Test_"



TEST_CASE( "simple MathFunction tests", "[general]" )
{
    
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);


    //Product testing
    std::string part_name = "PRODUCT";
    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_1", 5.0));
    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_2", -25.36));
    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_3", 22.3));
    model_ptr->run_model(1);
    REQUIRE(model_ptr->get_var<double>(TEST_SUBSYSTEM + part_name + "_OUT_1").value() ==  (5.0));
    REQUIRE(model_ptr->get_var<double>(TEST_SUBSYSTEM + part_name + "_OUT_2").value() == 5.0*(-25.36));
    REQUIRE(model_ptr->get_var<double>(TEST_SUBSYSTEM + part_name + "_OUT_3").value() == 5.0*(-25.36)*22.3);


    //Divide testing
    part_name = "DIVIDE";
    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_1", 5.0));
    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_2", -25.36));
    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_3", -1.0));
    model_ptr->run_model(1);
    REQUIRE(model_ptr->get_var<double>(TEST_SUBSYSTEM + part_name + "_OUT_1").value() == 5.0/(-25.36) );
    REQUIRE(model_ptr->get_var<double>(TEST_SUBSYSTEM + part_name + "_OUT_2").value() == (-25.36)/5.0 );
    REQUIRE(model_ptr->get_var<double>(TEST_SUBSYSTEM + part_name + "_OUT_3").value() == (-25.36)/5.0/(-1.0) );
    REQUIRE(model_ptr->get_var<double>(TEST_SUBSYSTEM + part_name + "_OUT_4").value() == ((-1)/5.0/(-25.36)) );

    //Test div zero out 
    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_1", 0.0));
    model_ptr->run_model(1);
    REQUIRE(model_ptr->get_var<double>(TEST_SUBSYSTEM + part_name + "_OUT_1").value() == 0.0 );
    REQUIRE(model_ptr->get_var<double>(TEST_SUBSYSTEM + part_name + "_OUT_2").value() == 0.0 );
    REQUIRE(model_ptr->get_var<double>(TEST_SUBSYSTEM + part_name + "_OUT_3").value() == 0.0 );
    REQUIRE(model_ptr->get_var<double>(TEST_SUBSYSTEM + part_name + "_OUT_4").value() == 0.0 );


    //Sum/Difference testing
    part_name = "ADD";
    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_1", 5.5));
    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_2", -33.5));
    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_3", 22.1));
    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_4", -9));
    model_ptr->run_model(1);
    REQUIRE(model_ptr->get_var<double>(TEST_SUBSYSTEM + part_name + "_OUT_1").value() == 5.5+(-33.5) );
    REQUIRE(model_ptr->get_var<double>(TEST_SUBSYSTEM + part_name + "_OUT_2").value() == (-5.5)+(-33.5)+22.1-(-9) );


    //Abs testing, can't do NaN test because NaN != NaN
    part_name = "ABS";
    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_1", 5.5));
    model_ptr->run_model(1);
    REQUIRE(model_ptr->get_var<double>(TEST_SUBSYSTEM + part_name + "_OUT_1").value() == Approx( std::fabs(5.5)) );
    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_1", -5.5));
    model_ptr->run_model(1);
    REQUIRE(model_ptr->get_var<double>(TEST_SUBSYSTEM + part_name + "_OUT_1").value() == Approx( std::fabs( -5.5) ) );
    REQUIRE(model_ptr->set_var(TEST_SUBSYSTEM + part_name + "_INPUT_1", std::numeric_limits<double>::infinity() * -1 ));
    model_ptr->run_model(1);
    REQUIRE(model_ptr->get_var<double>(TEST_SUBSYSTEM + part_name + "_OUT_1").value() == std::fabs( std::numeric_limits<double>::infinity() * -1 ) );
}






