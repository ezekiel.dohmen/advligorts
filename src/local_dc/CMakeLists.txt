add_executable(local_dc local_dc.c)

target_link_libraries(local_dc PUBLIC
        simpleargs
        util
        driver::gpsclock
        driver::shmem)

configure_file(test_local_dc_posix_shmem.sh.in test_local_dc_posix_shmem.sh @ONLY)
configure_file(test_local_dc_stopped_model.sh.in test_local_dc_stopped_model.sh @ONLY)
configure_file(test_local_dc_with_systab.sh.in test_local_dc_with_systab.sh @ONLY)

add_executable(test_local_dc_unit_tests tests/test_local_dc_unit_tests.c)
target_include_directories(test_local_dc_unit_tests PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(test_local_dc_unit_tests PUBLIC driver::shmem)

add_executable(check_for_dcu_existence tests/check_for_dcu_existence.cc)
target_include_directories(check_for_dcu_existence PUBLIC
        ${Boost_INCLUDE_DIRS})
target_link_libraries(check_for_dcu_existence PUBLIC
        cds-shmem
        simpleargs)
target_requires_cpp11(check_for_dcu_existence PUBLIC)

integration_test(
    NAME test_local_dc
    SCRIPT "tests/test_local_dc.py"
)
integration_test(

	    NAME test_local_dc_with_stopped_models
	    SCRIPT "test_local_dc_stopped_model.py"

	)

#integration_test(
#
#	    NAME test_local_dc_unit_tests
#	    SCRIPT "test_local_dc_unit_tests.py"
#
#	)

integration_test(

	    NAME test_local_dc_with_systab
	    SCRIPT "test_local_dc_with_systab.py"

	)
#add_test(NAME "test_local_dc"
#        COMMAND /bin/bash ./test_local_dc.sh
#        WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")

add_test(NAME "test_local_dc_posix_shmem"
        COMMAND /bin/bash ./test_local_dc_posix_shmem.sh
        WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")

#add_test(NAME "test_local_dc_with_stopped_models"
#        COMMAND /bin/bash ./test_local_dc_stopped_model.sh
#        WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")

#add_test(NAME "test_local_dc_with_systab"
#        COMMAND /bin/bash ./test_local_dc_with_systab.sh
#        WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")

add_test(NAME "test_local_dc_unit_tests"
        COMMAND ./test_local_dc_unit_tests
        WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")

install(TARGETS local_dc DESTINATION bin)

