










                        # Not Working. can't create files in Shared memory with the intergration modue yet














import os
import os.path
import time
import typing
import integration

integration.Executable(name="fe_stream_check", hints=["../fe_stream_test",], description="verification program")
integration.Executable(name="fe_simulated_streams", hints=["../fe_stream_test/fe_simulation",], description="data stream generator")
integration.Executable(name="local_dc", hints=["../local_dc",], description="local_dc")

ini_dir = integration.state.temp_dir('ini_files')
master_file = os.path.join(ini_dir, "master")

mod5ini = os.path.join(ini_dir, "mod5.ini")
mod5par = os.path.join(ini_dir, "tpchn_mod5.par")
mod6ini = os.path.join(ini_dir, "mod6.ini")
mod6par = os.path.join(ini_dir, "tpchn_mod6.par")
mod7ini = os.path.join(ini_dir, "mod7.ini")
mod7par = os.path.join(ini_dir, "tpchn_mod7.par")
mod250ini = os.path.join(ini_dir, "mod250.ini")
mod250par = os.path.join(ini_dir, "tpchn_mod250.par")
mod255ini = os.path.join(ini_dir, "mod255.ini")
mod255par = os.path.join(ini_dir, "tpchn_mod255.par")



fe_simulated_streams = integration.Process("fe_simulated_streams",
            ["-S",
             "-i", ini_dir,
             "-M", master_file,
             "-k", "300",
             "-s", "shm://",
             "-D", "5,6,7,250,255"])

local_dc = integration.Process("local_dc", [
                                            "-m", "100",
                                            "-s", "shm://mod5 shm://mod6 shm://mod7 shm://mod250 shm://mod255"
                                            "-d", ini_dir,
                                            "-b", "shm://local_dc"
                                            ])


fe_stream_check = integration.Process("fe_stream_check",
            ["-m", "shm://local_dc",
             "-s", "100",
             "-v",
             "-c", mod5ini, mod5par,
             "-c", mod6ini, mod6par,
             "-c", mod7ini, mod7par,
             "-c", mod250ini, mod250par,
             "-c", mod255ini, mod255par
                ])

#functions...

def check_ok(timeout:float):
    end_time = time.time() + timeout
    def do_check():
        global fe_stream_check
        while True:
            state = fe_stream_check.state()
            if state == integration.Process.STOPPED:
                fe_stream_check.ignore()
                print("the check finished in a stopped state")
                return
            elif state == integration.Process.FAILED:
                raise RuntimeError('Data check failed')
            print("state is {0}".format(state))

            if time.time() > end_time:
                fe_stream_check.stop()
                raise RuntimeError('Timed out while waiting for nds check to complete')
            time.sleep(0.5)

    return do_check


def echo(msg):
    def wrapper():
        print(msg)
    return wrapper

integration.Sequence(
    [
        integration.state.preserve_files,
        echo(ini_dir),
        integration.require_readable(files=["/dev/gpstime", "/dev/mbuf"], description="required device files"),
        fe_simulated_streams.run,
        integration.wait(),
        local_dc.run,
        integration.wait(),
        echo("local dc is really up now"),
        integration.wait(),
        fe_stream_check.run,
        check_ok(20)

    ]
)






