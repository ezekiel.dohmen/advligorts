//
// Created by jonathan.hanks on 2/7/18.
// This file is pieces of logic pulled out of the bison/yacc file comm.y.
// They have been pulled out to make it easier to consume with external
// tools and debuggers.  Also the hope is that removing all the ${1}...
// makes the code easier to reason about.
//
#include "comm_impl.hh"

#include <sstream>

#include "FlexLexer.h"
#include "y.tab.h"
#include "MyLexer.hh"
#include "config.h"

#include "daqd.hh"
#include "checksum_crc32.hh"
#include "network_helpers.hh"
#include "raii.hh"

extern daqd_c daqd;

namespace
{
    class StartWriterErrorMessage
    {
    public:
        StartWriterErrorMessage( bool strict, ostream* yyout )
            : strict_{ strict }, yyout_{ yyout }
        {
        }

        void
        operator( )( const char* strict_message,
                     const char* human_friendly_message )
        {
            if ( strict_ )
            {
                *yyout_ << strict_message << flush;
            }
            else
            {
                *yyout_ << human_friendly_message << endl;
            }
        }

        void
        operator( )( int error_code, const char* human_friendly_message )
        {
            if ( strict_ )
            {
                *yyout_ << setw( 4 ) << setfill( '0' ) << hex << error_code
                        << dec << flush;
            }
            else
            {
                *yyout_ << human_friendly_message << endl;
            }
        }

    private:
        bool     strict_;
        ostream* yyout_;
    };
} // namespace

namespace comm_impl
{
    void
    start_write_impl( void*       lexer,
                      int         writerType_2_,
                      const char* optionalAddress_3_,
                      int         optionalStart_4_,
                      int         optionalStop_5_,
                      const char* optionalBroadcast_6_,
                      int         decimateOption_7_,
                      int         channelNames_9_ )
    {
        int      no_data_connection;
        ostream* yyout = ( (my_lexer*)lexer )->get_yyout( );
        int      ifd = ( (my_lexer*)lexer )->get_ifd( );
        int      strict_lexer = ( (my_lexer*)lexer )->strict;

        StartWriterErrorMessage error_message( strict_lexer, yyout );

        // Check if no main buffer
        if ( !daqd.b1 )
        {
            error_message( S_DAQD_NO_MAIN, "Main is not started" );
            return;
        }

        if ( daqd.offline_disabled && ( optionalStart_4_ || optionalStop_5_ ) )
        { // Check if off-line data request is disabled
            error_message( S_DAQD_NO_OFFLINE,
                           "Off-line data request disabled" );
            return;
        }

        // Check errors in channel names here
        if ( ( (my_lexer*)lexer )->error_code )
        {
            error_message( ( (my_lexer*)lexer )->error_code,
                           "Network writer is not started" );
            return;
        }

        void* mptr = malloc( sizeof( net_writer_c ) );
        if ( !mptr )
        {
            error_message( S_DAQD_MALLOC, "Virtual memory exhausted" );
            return;
        }

        DEBUG1( cerr << "Creating net_writer tagged " << ifd << endl );
        net_writer_c* nw = new ( mptr ) net_writer_c( ifd );
        if ( !nw->alloc_vars( channelNames_9_ ? MAX_CHANNELS
                                              : my_lexer::max_channels ) )
        {
            error_message( S_DAQD_MALLOC, "Virtual memory exhausted" );

            nw->~net_writer_c( );
            free( (void*)nw );
            return;
        }
        int errc;

        nw->broadcast = (long)optionalBroadcast_6_;
        nw->set_mcast_interface( optionalBroadcast_6_ );

        nw->no_averaging = ( decimateOption_7_ == DOWNSAMPLE );
        system_log( 1, "no_average=%d\n", nw->no_averaging );

        if ( ( writerType_2_ ) == NAME_WRITER )
        {
            // Bail out if frame saver is not running, no point to start
            // it hanging around, i guess (???) Bail out on a request
            // with any channel specs (only `all' is valid) Bail out on
            // a request with any periods Bail out if there is no
            // circular buffer, ie, if !daqd.fsd.cb

            if ( !daqd.frame_saver_tid )
            {
                error_message( S_DAQD_NO_OFFLINE,
                               "name writer is not started, since frame saver "
                               "is not running" );
            }

            if ( ( optionalStart_4_ ) || ( optionalStop_5_ ) ||
                 !( channelNames_9_ ) )
            {
                error_message( S_DAQD_ERROR, "invalid command" );
            }

            // FIXME
            // Should I try to construct this circular buffer on the
            // first request?
            if ( !daqd.fsd.cb )
            {
                error_message( S_DAQD_NOT_SUPPORTED,
                               "main filesys map doesn't have a circular "
                               "buffer constructed" );
            }

            if ( !daqd.frame_saver_tid || ( optionalStart_4_ ) ||
                 ( optionalStop_5_ ) || !( channelNames_9_ ) || !daqd.fsd.cb )
            {
                nw->~net_writer_c( );
                free( (void*)nw );
                return;
            }
            nw->writer_type = net_writer_c::name_writer;
        }
        else if ( ( writerType_2_ ) == FRAME_WRITER )
        {
            int not_supported = ( optionalStart_4_ ) ^ ( optionalStop_5_ ) ||
                ( ( optionalStart_4_ ) && ( optionalStop_5_ ) &&
                  !( channelNames_9_ ) ); // only format `start
                                          // frame-writer
            // 123123 10 all' supported

            // Decimation is not supported
            for ( int i = 0; i < ( (my_lexer*)lexer )->num_channels; i++ )
            {
                not_supported |= ( (my_lexer*)lexer )->channels[ i ].req_rate;
            }
            if ( not_supported )
            {
                error_message( S_DAQD_NOT_SUPPORTED,
                               "frame write call format is not supported" );

                nw->~net_writer_c( );
                free( (void*)nw );
            }
            nw->writer_type = net_writer_c::frame_writer;
        }
        else if ( ( writerType_2_ ) == FAST_WRITER )
        {
            if ( ( optionalStart_4_ ) || ( optionalStop_5_ ) )
            {
                error_message( S_DAQD_NOT_SUPPORTED, "invalid command" );

                nw->~net_writer_c( );
                free( (void*)nw );
                return;
            }
            nw->writer_type = net_writer_c::fast_writer;
        }
        else
        {
            nw->writer_type = net_writer_c::slow_writer;
        }

        // optionalAddress_3_ is the string in the format
        // `127.0.0.1:9090' If the string is not present -- use control
        // connection for data transport If there is no IP address,
        // usethe port and extract IP address from the request and open
        // data connection to this address using specified port.
        if ( optionalAddress_3_ )
        {
            long ip_addr = -1;

            if ( !net_helper::is_valid_dec_number( optionalAddress_3_ ) )
            {
                ip_addr = net_helper::ip_str( optionalAddress_3_ );
            }

            // Extract IP address
            if ( ip_addr == -1 )
            {
                if ( ( ip_addr = net_writer_c::ip_fd(
                           ( (my_lexer*)lexer )->ifd ) ) == -1 )
                {
                    std::ostringstream os;
                    os << "Couldn't do getpeername(); errno=" << errno;
                    std::string msg = os.str( );
                    error_message( DAQD_GETPEERNAME, msg.c_str( ) );

                    nw->~net_writer_c( );
                    free( (void*)nw );
                    return;
                }
            }
            nw->srvr_addr.sin_addr.s_addr = (u_long)ip_addr;
            nw->srvr_addr.sin_family = AF_INET;
            nw->srvr_addr.sin_port = net_helper::port_str( optionalAddress_3_ );
            no_data_connection = 0;
        }
        else
        {
            no_data_connection = 1;
        }

        if ( nw->writer_type == net_writer_c::name_writer )
        {
            DEBUG1( cerr << "name writer on the frame files" << endl );
            nw->transmission_block_size = nw->block_size =
                daqd.fsd.cb->block_size( );
            nw->num_channels = 0;
            nw->pvec[ 0 ].vec_idx = 0;
            nw->pvec[ 0 ].vec_len = daqd.fsd.cb->block_size( );
            nw->pvec_len = 1;
        }
        else
        {
            int fast_net_writer = nw->writer_type == net_writer_c::fast_writer;

            if ( channelNames_9_ )
            {
                DEBUG1( cerr << "all channels selected" << endl );
                nw->num_channels = daqd.num_channels
                    // Do not send gds channels to broadcaster for now
                    - daqd.num_gds_channels - daqd.num_gds_channel_aliases;
                unsigned int j = 0;
                for ( unsigned int i = 0; i < daqd.num_channels; i++ )
                {
                    if ( ( !IS_GDS_ALIAS( daqd.channels[ i ] ) ) &&
                         ( !IS_GDS_SIGNAL( daqd.channels[ i ] ) ) )
                    {
                        nw->channels[ j++ ] = daqd.channels[ i ];
                    }
                }
            }
            else
            {
                memcpy( nw->channels,
                        ( (my_lexer*)lexer )->channels,
                        sizeof( nw->channels[ 0 ] ) *
                            ( (my_lexer*)lexer )->num_channels );
                nw->num_channels = ( (my_lexer*)lexer )->num_channels;
            }

            nw->block_size = 0;
            DEBUG1( cerr << "num_channels=" << nw->num_channels << endl );

            if ( fast_net_writer )
            {
                // Check if any channel rate is less than 16
                for ( int i = 0; i < nw->num_channels; i++ )
                {
                    if ( nw->channels->req_rate && nw->channels->req_rate < 16 )
                    {
                        error_message( S_DAQD_INVALID_CHANNEL_DATA_RATE,
                                       "invalid channel data rate -- "
                                       "minimum "
                                       "rate for fast net-writer is 16" );
                        nw->~net_writer_c( );
                        free( (void*)nw );
                        return;
                    }
                }
            }
            // See if there are any GDS alias channels
            // If there are any, need to find real GDS
            // channels carrying the data, then update
            // the offset
            {
                int na = 0;
                // char *alias[nw -> num_channels];
                // unsigned int tpnum[nw -> num_channels];
                long_channel_t* ac[ nw->num_channels ];
                channel_t*      gds[ nw->num_channels ];

                for ( int i = 0; i < nw->num_channels; i++ )
                {
                    // printf("channel %d tp_node=%d\n", i, nw ->
                    // channels [i].tp_node);
                    if ( IS_GDS_ALIAS( nw->channels[ i ] ) )
                    {
                        ac[ na ] = nw->channels + i;
                        // tpnum [na] = nw -> channels [i].chNum;
                        // alias [na] = nw -> channels [i].name;
                        na++;
                    }
                }
                if ( na )
                {
                    int res = -1;
                    // res = daqd.gds.req_names (alias, tpnum, gds,na);
                    if ( daqd.tp_allowed( net_writer_c::ip_fd(
                             ( (my_lexer*)lexer )->ifd ) ) )
                    {
                        res = daqd.gds.req_tps( ac, gds, na );
                    }

                    if ( res )
                    {
                        // if
                        // (daqd.tp_allowed(net_writer_c::ip_fd(((my_lexer
                        // *)lexer) -> ifd))) daqd.gds.clear_tps(ac,
                        // na);
                        if ( strict_lexer )
                        {
                            *yyout << S_DAQD_NOT_FOUND << flush;
                        }
                        else
                        {
                            *yyout << "failed to find GDS signal for one "
                                      "or more aliases"
                                   << endl;
                        }

                        nw->~net_writer_c( );
                        free( (void*)nw );
                        return;
                    }
                }
                nw->clear_testpoints = 1;
            }

            for ( int i = 0; i < nw->num_channels; i++ )
            {
                if ( fast_net_writer )
                {
                    if ( nw->channels[ i ].req_rate )
                    {
                        nw->transmission_block_size += nw->channels[ i ].bps *
                            nw->channels[ i ].req_rate / 16;
                    }
                    else
                    {
                        nw->transmission_block_size +=
                            nw->channels[ i ].bytes / 16;
                    }

                    nw->block_size += nw->channels[ i ].bytes / 16;
                    for ( int j = 0; j < 16; j++ )
                    {
                        nw->pvec16th[ j ][ nw->pvec_len ].vec_idx =
                            nw->channels[ i ].offset +
                            nw->channels[ i ].bytes / 16 * j;
                        nw->pvec16th[ j ][ nw->pvec_len ].vec_len =
                            nw->channels[ i ].bytes / 16;
                    }
                    nw->pvec_len++;

                    // Contruct another scattered data array to be used
                    // for data decimation in the
                    // net_writer_c::consumer() `dec_vec [].vec_idx' is
                    // the offset in the net-writer circ buffer data
                    // block
                    if ( !nw->dec_vec_len )
                    { // Start the vector
                        nw->dec_vec[ 0 ].vec_idx = 0;
                        nw->dec_vec[ 0 ].vec_len = nw->channels[ i ].bytes / 16;
                        nw->dec_vec[ 0 ].vec_rate =
                            nw->channels[ i ].req_rate / 16;
                        nw->dec_vec[ 0 ].vec_bps = nw->channels[ i ].bps;
                        nw->dec_vec_len++;
                    }
                    else if ( nw->channels[ i - 1 ].req_rate ==
                                  nw->channels[ i ].req_rate &&
                              nw->channels[ i - 1 ].bytes ==
                                  nw->channels[ i ].bytes &&
                              nw->channels[ i - 1 ].bps ==
                                  nw->channels[ i ].bps )
                    {

                        // Increase data element length for contiguous
                        // channels with the same rate, same requested
                        // rate and the same number bytes per sample. We
                        // should be able to treat these channels as one
                        // channel with the increased data rate.

                        nw->dec_vec[ nw->dec_vec_len - 1 ].vec_len +=
                            nw->channels[ i ].bytes / 16;
                        nw->dec_vec[ nw->dec_vec_len - 1 ].vec_rate +=
                            nw->channels[ i ].req_rate / 16;
                    }
                    else
                    {

                        // Start new vector if rate or BPS changes

                        nw->dec_vec[ nw->dec_vec_len ].vec_idx =
                            nw->dec_vec[ nw->dec_vec_len - 1 ].vec_idx +
                            nw->dec_vec[ nw->dec_vec_len - 1 ].vec_len;
                        nw->dec_vec[ nw->dec_vec_len ].vec_len =
                            nw->channels[ i ].bytes / 16;
                        nw->dec_vec[ nw->dec_vec_len ].vec_rate =
                            nw->channels[ i ].req_rate / 16;
                        nw->dec_vec[ nw->dec_vec_len ].vec_bps =
                            nw->channels[ i ].bps;
                        nw->dec_vec_len++;
                    }
                }
                else
                {
                    if ( nw->channels[ i ].req_rate )
                    {
                        nw->transmission_block_size +=
                            nw->channels[ i ].bps * nw->channels[ i ].req_rate;
                    }
                    else
                    {
                        nw->transmission_block_size += nw->channels[ i ].bytes;
                    }

                    nw->block_size += nw->channels[ i ].bytes;
                    // Construct put vector array
                    if ( !nw->pvec_len )
                    { // Start vector
                        nw->pvec[ 0 ].vec_idx = nw->channels[ i ].offset;
                        nw->pvec[ 0 ].vec_len = nw->channels[ i ].bytes;
                        nw->pvec_len++;
                    }
                    else if ( nw->pvec[ nw->pvec_len - 1 ].vec_idx +
                                  nw->pvec[ nw->pvec_len - 1 ].vec_len ==
                              nw->channels[ i ].offset )
                    {
                        // Increase vector element length for contiguous
                        // channels
                        nw->pvec[ nw->pvec_len - 1 ].vec_len +=
                            nw->channels[ i ].bytes;
                    }
                    else
                    { // Start new vector if there is a gap in channel
                        // sequence
                        nw->pvec[ nw->pvec_len ].vec_idx =
                            nw->channels[ i ].offset;
                        nw->pvec[ nw->pvec_len ].vec_len =
                            nw->channels[ i ].bytes;
                        nw->pvec_len++;
                    }

                    // Contruct another scattered data array to be used
                    // for data decimation in the
                    // net_writer_c::consumer() `dec_vec [].vec_idx' is
                    // the offset in the net-writer circ buffer data
                    // block
                    if ( !nw->dec_vec_len )
                    { // Start the vector
                        nw->dec_vec[ 0 ].vec_idx = 0;
                        nw->dec_vec[ 0 ].vec_len = nw->channels[ i ].bytes;
                        nw->dec_vec[ 0 ].vec_rate = nw->channels[ i ].req_rate;
                        nw->dec_vec[ 0 ].vec_bps = nw->channels[ i ].bps;
                        nw->dec_vec_len++;
                    }
                    else if ( nw->channels[ i - 1 ].req_rate ==
                                  nw->channels[ i ].req_rate &&
                              nw->channels[ i - 1 ].bytes ==
                                  nw->channels[ i ].bytes &&
                              nw->channels[ i - 1 ].bps ==
                                  nw->channels[ i ].bps )
                    {

                        // Increase data element length for contiguous
                        // channels with the same rate and the same
                        // number bytes per sample. We should be able to
                        // treat these channels as one channel with the
                        // increased data rate.

                        nw->dec_vec[ nw->dec_vec_len - 1 ].vec_len +=
                            nw->channels[ i ].bytes;
                        nw->dec_vec[ nw->dec_vec_len - 1 ].vec_rate +=
                            nw->channels[ i ].req_rate;
                    }
                    else
                    {

                        // Start new vector if rate or BPS changes

                        nw->dec_vec[ nw->dec_vec_len ].vec_idx =
                            nw->dec_vec[ nw->dec_vec_len - 1 ].vec_idx +
                            nw->dec_vec[ nw->dec_vec_len - 1 ].vec_len;
                        nw->dec_vec[ nw->dec_vec_len ].vec_len =
                            nw->channels[ i ].bytes;
                        nw->dec_vec[ nw->dec_vec_len ].vec_rate =
                            nw->channels[ i ].req_rate;
                        nw->dec_vec[ nw->dec_vec_len ].vec_bps =
                            nw->channels[ i ].bps;
                        nw->dec_vec_len++;
                    }
                }
            }

            nw->decimation_requested = !nw->broadcast;
            system_log( 5, "decimation flag=%d", nw->decimation_requested );
            nw->block_size += nw->num_channels * sizeof( int );
            if ( fast_net_writer )
            {
                if ( !nw->broadcast )
                {
                    unsigned long status_ptr = sizeof( int ) + daqd.block_size -
                        17 * sizeof( int ) * daqd.num_channels;
                    for ( int i = 0; i < nw->num_channels; i++ )
                    {
                        for ( int j = 0; j < 16; j++ )
                        {
                            nw->pvec16th[ j ][ nw->pvec_len ].vec_idx =
                                j * sizeof( int ) + status_ptr +
                                17 * sizeof( int ) * nw->channels[ i ].seq_num;
                            nw->pvec16th[ j ][ nw->pvec_len ].vec_len =
                                sizeof( int );
                        }
                        nw->pvec_len++;
                    }
                }
            }
            else
            {
                // append one element per channel to save the status
                // word
                //
                unsigned long status_ptr =
                    daqd.block_size - 17 * sizeof( int ) * daqd.num_channels;
                for ( int i = 0; i < nw->num_channels; i++ )
                {
                    nw->pvec[ nw->pvec_len ].vec_idx = status_ptr +
                        17 * sizeof( int ) * nw->channels[ i ].seq_num;
                    nw->pvec[ nw->pvec_len ].vec_len = sizeof( int );
                    nw->pvec_len++;
                }
            }
            if ( !fast_net_writer )
            {
                DEBUG1( cerr << "pvec->vec_idx\tpvec->vec_len" << endl );
                DEBUG1( for ( int j = 0; j < nw->pvec_len; j++ ) {
                    cerr << nw->pvec[ j ].vec_idx << '\t'
                         << nw->pvec[ j ].vec_len << endl;
                } );
            }
            DEBUG1( cerr << "dec_vec->vec_idx\tdec_vec->vec_len\tdec_vec->"
                            "vec_rate\tdec_vec->vec_bps"
                         << endl );
            DEBUG1( for ( int k = 0; k < nw->dec_vec_len; k++ ) {
                cerr << nw->dec_vec[ k ].vec_idx << '\t'
                     << nw->dec_vec[ k ].vec_len << '\t'
                     << nw->dec_vec[ k ].vec_rate << '\t'
                     << nw->dec_vec[ k ].vec_bps << endl;
            } );
        } // not name writer
        circ_buffer* this_cb;

        if ( nw->writer_type == net_writer_c::name_writer )
        {
            this_cb = daqd.fsd.cb;
        }
        else
        {
            this_cb = daqd.b1;
            nw->need_send_reconfig_block = true;
        }

        if ( !( errc = nw->start_net_writer( yyout,
                                             ( (my_lexer*)lexer )->ofd,
                                             no_data_connection,
                                             this_cb,
                                             &daqd.fsd,
                                             optionalStart_4_,
                                             optionalStop_5_,
                                             0 ) ) )
        {
            if ( !no_data_connection )
            {
                if ( strict_lexer )
                {
                    *yyout << S_DAQD_OK << flush;
                    // send writer ID
                    *yyout << setfill( '0' )
                           << setw( sizeof( unsigned long ) * 2 ) << hex
                           << (unsigned long)nw << dec << flush;
                }
                else
                {
                    *yyout << "started" << endl;
                    *yyout << "writer id: " << (unsigned long)nw << endl;
                }
            }
        }
        else
        { // a problem
            nw->~net_writer_c( );
            free( (void*)nw );

            std::ostringstream os;
            os << "error " << errc;
            std::string msg = os.str( );
            error_message( errc, msg.c_str( ) );
        }
    }

    void
    configure_channels_body_begin_end( )
    {
        int i, j, k, offs;
        int rm_offs = 0;

        auto skip_checks =
            daqd.parameters( ).get< bool >( "allow_any_dcuid", false );

        // Configure channels from files
        if ( daqd.configure_channels_files( ) )
        {
            exit( 1 );
        }
        int cur_dcu = -1;

        // compute the checksum of the channels two ways
        // the broadcaster mode uses a different set of active channels
        // which changes the channel hash with respect to the other daqds.
        // So to help compare state this computes two hashes, one that
        // all daqds will see and one with the broadcast settings.
        checksum_crc32 csum;
        checksum_crc32 csum_bcast;
        // Save channel offsets and assign trend channel data struct
        for ( i = 0, offs = 0; i < daqd.num_channels + 1; i++ )
        {
            int active_before_bcast_check = 0;
            int t = 0;
            if ( i == daqd.num_channels )
            {
                t = 1;
            }
            else
            {
                t = cur_dcu != -1 && cur_dcu != daqd.channels[ i ].dcu_id;
            }

            if ( cur_dcu >= 0 && !skip_checks )
            {
                if ( !IS_TP_DCU( cur_dcu ) && !IS_MYRINET_DCU( cur_dcu ) )
                {
                    std::cerr << "requested DCUID not in the standard range: "
                              << cur_dcu << std::endl;
                    exit( 1 );
                }
            }

            if ( IS_TP_DCU( daqd.channels[ i ].dcu_id ) && t )
            {
                // Remember the offset to start of TP/EXC DCU data
                // in main buffer
                daqd.dcuTPOffset[ daqd.channels[ i ].ifoid ]
                                [ daqd.channels[ i ].dcu_id ] = offs;
            }

            // Finished with cur_dcu: channels sorted by dcu_id
            if ( IS_MYRINET_DCU( cur_dcu ) && t )
            {
                int dcu_size =
                    daqd.dcuSize[ daqd.channels[ i - 1 ].ifoid ][ cur_dcu ];
                daqd.dcuDAQsize[ daqd.channels[ i - 1 ].ifoid ][ cur_dcu ] =
                    dcu_size;

                // When compiled with USE_BROADCAST we do not allocate
                // contigious memory in the move buffer for the test points. We
                // allocate this memory later at the end of the buffer.

                // Save testpoint data offset
                daqd.dcuTPOffset[ daqd.channels[ i - 1 ].ifoid ][ cur_dcu ] =
                    offs;
                daqd.dcuTPOffsetRmem[ daqd.channels[ i - 1 ].ifoid ]
                                    [ cur_dcu ] = rm_offs;

                // Allocate testpoint data buffer for this DCU
                int tp_buf_size =
                    DAQ_DCU_BLOCK_SIZE * DAQ_NUM_DATA_BLOCKS_PER_SECOND -
                    dcu_size * DAQ_NUM_DATA_BLOCKS_PER_SECOND;
                offs += tp_buf_size;
                daqd.block_size += tp_buf_size;
                rm_offs += DAQ_DCU_BLOCK_SIZE - dcu_size;
                DEBUG1( cerr << "Configured MYRINET DCU, block size="
                             << tp_buf_size << endl );
                DEBUG1( cerr << "Myrinet DCU size " << dcu_size << endl );
                daqd.dcuSize[ daqd.channels[ i - 1 ].ifoid ][ cur_dcu ] =
                    DAQ_DCU_BLOCK_SIZE;
            }
            if ( !IS_MYRINET_DCU( cur_dcu ) && t )
            {
                daqd.dcuDAQsize[ daqd.channels[ i - 1 ].ifoid ][ cur_dcu ] =
                    daqd.dcuSize[ daqd.channels[ i - 1 ].ifoid ][ cur_dcu ];
            }
            if ( i == daqd.num_channels )
            {
                continue;
            }

            cur_dcu = daqd.channels[ i ].dcu_id;

            daqd.channels[ i ].bytes =
                daqd.channels[ i ].sample_rate * daqd.channels[ i ].bps;

            if ( IS_GDS_ALIAS( daqd.channels[ i ] ) )
            {
                daqd.channels[ i ].offset = 0;
            }
            else
            {
                daqd.channels[ i ].offset = offs;

                offs += daqd.channels[ i ].bytes;
                daqd.block_size += daqd.channels[ i ].bytes;

                daqd.dcuSize[ daqd.channels[ i ].ifoid ]
                            [ daqd.channels[ i ].dcu_id ] +=
                    daqd.channels[ i ].bytes / DAQ_NUM_DATA_BLOCKS_PER_SECOND;
                daqd.channels[ i ].rm_offset = rm_offs;
                rm_offs +=
                    daqd.channels[ i ].bytes / DAQ_NUM_DATA_BLOCKS_PER_SECOND;
                /* For the EXC DCUs: add the status word length */
                if ( IS_EXC_DCU( daqd.channels[ i ].dcu_id ) )
                {
                    daqd.dcuSize[ daqd.channels[ i ].ifoid ]
                                [ daqd.channels[ i ].dcu_id ] += 4;
                    rm_offs += 4;
                    daqd.channels[ i ].rm_offset += 4;
                }
            }

            active_before_bcast_check = daqd.channels[ i ].active;
            if ( !daqd.broadcast_set.empty( ) )
            {
                // Activate configured DMT broadcast channels
                daqd.channels[ i ].active =
                    daqd.broadcast_set.count( daqd.channels[ i ].name );
            }

            if ( daqd.channels[ i ].trend )
            {
                if ( daqd.trender.num_channels >=
                     trender_c::max_trend_channels )
                {
                    system_log(
                        1,
                        "too many trend channels. No trend on `%s' channel",
                        daqd.channels[ i ].name );
                }
                else
                {
                    daqd.trender.channels[ daqd.trender.num_channels ] =
                        daqd.channels[ i ];
                    daqd.trender.block_size += sizeof( trend_block_t );
                    if ( daqd.trender.channels[ daqd.trender.num_channels ]
                             .data_type == _32bit_complex )
                    {
                        /* Change data type from complex to float but leave bps
                         * field at 8 */
                        /* Trend loop function checks bps variable to detect
                         * complex data */
                        daqd.trender.channels[ daqd.trender.num_channels ]
                            .data_type = _32bit_float;
                    }

                    daqd.trender.num_channels++;
                }
            }
            // the unmodified hash, this differs between
            // broadcasters and all other daqds
            hash_channel( csum_bcast, daqd.channels[ i ] );
            {
                // 'undo' the potential broadcast change to create
                // the 'normal' channel hash.
                channel_t tmp = daqd.channels[ i ];
                tmp.active = active_before_bcast_check;
                hash_channel( csum, tmp );
            }
        }
        PV::set_pv( PV::PV_CHANNEL_LIST_CHECK_SUM, csum.result( ) );
        PV::set_pv( PV::PV_CHANNEL_LIST_CHECK_SUM_BCAST, csum_bcast.result( ) );

        if ( !daqd.broadcast_set.empty( ) )
        {
            std::vector< std::string > missing_broadcast_channels{ };
            std::for_each( daqd.broadcast_set.begin( ),
                           daqd.broadcast_set.end( ),
                           [ &missing_broadcast_channels ](
                               const std::string& cur_broadcast_channel ) {
                               const channel_t* start = daqd.channels;
                               const auto       end = start + daqd.num_channels;

                               auto it = std::find_if(
                                   start,
                                   end,
                                   [ &cur_broadcast_channel ](
                                       const channel_t& cur_channel ) -> bool {
                                       // only active non tp channels are marked
                                       // with trend != 0, so check the name,
                                       // tp, and active state
                                       return cur_broadcast_channel ==
                                           cur_channel.name &&
                                           cur_channel.trend;
                                   } );
                               if ( it == end )
                               {
                                   missing_broadcast_channels.emplace_back(
                                       cur_broadcast_channel );
                               }
                           } );
            if ( !missing_broadcast_channels.empty( ) )
            {
                std::cerr << "The GDS broadcast list requests channels that "
                             "are not available"
                          << std::endl;
                std::copy(
                    missing_broadcast_channels.begin( ),
                    missing_broadcast_channels.end( ),
                    std::ostream_iterator< std::string >( std::cerr, "\n" ) );
                std::cerr << std::endl;
                throw std::runtime_error( "The GDS broadcast list requests "
                                          "channels that are not available" );
            }
        }

        // Calculate memory size needed for channels status storage in the main
        // buffer and increment main block size
        daqd.block_size += 17 * sizeof( int ) * daqd.num_channels;

        daqd.trender.num_trend_channels = 0;
        // Assign trend output channels
        for ( i = 0, offs = 0; i < daqd.trender.num_channels; i++ )
        {
            int l = strlen( daqd.trender.channels[ i ].name );
            if ( l > ( MAX_CHANNEL_NAME_LENGTH - 6 ) )
            {
                printf( "Channel %s length %d over the limit of %d\n",
                        daqd.trender.channels[ i ].name,
                        (int)strlen( daqd.trender.channels[ i ].name ),
                        MAX_CHANNEL_NAME_LENGTH - 6 );
                exit( 1 );
            }
            for ( int j = 0; j < trender_c::num_trend_suffixes; j++ )
            {
                daqd.trender.trend_channels[ daqd.trender.num_trend_channels ] =
                    daqd.trender.channels[ i ];

                // Add a suffix to the channel name
                strcat( daqd.trender
                            .trend_channels[ daqd.trender.num_trend_channels ]
                            .name,
                        daqd.trender.sufxs[ j ] );

                // Set the sample rate always to one, since
                // we have fixed trend calculation period equal to one second
                daqd.trender.trend_channels[ daqd.trender.num_trend_channels ]
                    .sample_rate = 1;

                switch ( j )
                {
                case 0:
                case 1:
                    if ( daqd.trender.channels[ i ].data_type !=
                             _64bit_double &&
                         daqd.trender.channels[ i ].data_type != _32bit_float )
                    {
                        daqd.trender
                            .trend_channels[ daqd.trender.num_trend_channels ]
                            .data_type = _32bit_integer;
                    }
                    else
                    {
                        daqd.trender
                            .trend_channels[ daqd.trender.num_trend_channels ]
                            .data_type = daqd.trender.channels[ i ].data_type;
                    }
                    break;
                case 2: // `n'
                    daqd.trender
                        .trend_channels[ daqd.trender.num_trend_channels ]
                        .data_type = _32bit_integer;
                    break;
                case 3: // `mean'
                case 4: // `rms'
                    daqd.trender
                        .trend_channels[ daqd.trender.num_trend_channels ]
                        .data_type = _64bit_double;
                    break;
                }

                daqd.trender.trend_channels[ daqd.trender.num_trend_channels ]
                    .bps =
                    daqd.trender
                        .bps[ daqd.trender.channels[ i ].data_type ][ j ];
                daqd.trender.trend_channels[ daqd.trender.num_trend_channels ]
                    .bytes =
                    daqd.trender
                        .trend_channels[ daqd.trender.num_trend_channels ]
                        .bps *
                    daqd.trender
                        .trend_channels[ daqd.trender.num_trend_channels ]
                        .sample_rate;
                daqd.trender.trend_channels[ daqd.trender.num_trend_channels ]
                    .offset = daqd.trender.toffs[ j ] + offs;
                daqd.trender.num_trend_channels++;
            }
            offs += sizeof( trend_block_t );
        }

        DEBUG1( cerr << "Configured " << daqd.num_channels << " channels"
                     << endl );
        DEBUG1( cerr << "Configured " << daqd.trender.num_channels
                     << " trend channels" << endl );
        DEBUG1( cerr << "Configured " << daqd.trender.num_trend_channels
                     << " output trend channels" << endl );
        DEBUG1( cerr << "comm.y: daqd block_size=" << daqd.block_size << endl );
    }

    bool
    configure_test_points( const std::string& par_file )
    {
        daqd.gds.set_gds_server( par_file );
        return daqd.gds.gds_initialize( ) == 0;
    }

    void
    clear_crc( )
    {
        for ( int i = 0; i < daqd.data_feeds; i++ )
            for ( int j = 0; j < DCU_COUNT; j++ )
            {
                daqd.dcuCrcErrCnt[ i ][ j ] = 0;
            }
        daqd.producer1.clearStats( );
    }

    void
    start_broadcast( ostream*           yyout,
                     const std::string& broadcast_address,
                     const std::string& network_address,
                     int                port )
    {
        const int no_data_connection{ 0 };

        int ifd = 1; //( (my_lexer*)lexer )->get_ifd( );

        if ( broadcast_address.empty( ) )
        {
            throw std::runtime_error( "Invalid ip address specified" );
        }

        // Check if no main buffer
        if ( !daqd.b1 )
        {
            throw std::runtime_error( "the main thread is not started" );
        }

        DEBUG1( cerr << "Creating net_writer tagged " << ifd << endl );
        auto nw = raii::c_unique_ptr< net_writer_c >::make( ifd );

        if ( !nw->alloc_vars( MAX_CHANNELS ) )
        {
            throw std::runtime_error( "Virtual memory exhausted" );
        }
        int errc;

        nw->broadcast = 1;
        nw->set_mcast_interface( network_address, port );

        nw->no_averaging = false;
        nw->writer_type = net_writer_c::frame_writer;

        // broadcast_address is the string in the format
        // `127.0.0.1' If the string is not present -- use control
        // connection for data transport If there is no IP address,
        // usethe port and extract IP address from the request and open
        // data connection to this address using specified port.

        long ip_addr = net_helper::ip_str( broadcast_address.c_str( ) );
        if ( ip_addr == -1 )
        {
            throw std::runtime_error( "Invalid ip address specified" );
        }

        nw->srvr_addr.sin_addr.s_addr = (u_long)ip_addr;
        nw->srvr_addr.sin_family = AF_INET;
        nw->srvr_addr.sin_port = port;

        const int fast_net_writer = 0;

        DEBUG1( cerr << "all channels selected" << endl );
        nw->num_channels = daqd.num_channels
            // Do not send gds channels to broadcaster for now
            - daqd.num_gds_channels - daqd.num_gds_channel_aliases;
        unsigned int j = 0;
        for ( unsigned int i = 0; i < daqd.num_channels; i++ )
        {
            if ( ( !IS_GDS_ALIAS( daqd.channels[ i ] ) ) &&
                 ( !IS_GDS_SIGNAL( daqd.channels[ i ] ) ) )
            {
                nw->channels[ j++ ] = daqd.channels[ i ];
            }
        }

        nw->block_size = 0;
        DEBUG1( cerr << "num_channels=" << nw->num_channels << endl );

        // See if there are any GDS alias channels
        // If there are any, need to find real GDS
        // channels carrying the data, then update
        // the offset
        {
            int na = 0;
            // char *alias[nw -> num_channels];
            // unsigned int tpnum[nw -> num_channels];
            long_channel_t* ac[ nw->num_channels ];
            channel_t*      gds[ nw->num_channels ];

            for ( int i = 0; i < nw->num_channels; i++ )
            {
                // printf("channel %d tp_node=%d\n", i, nw ->
                // channels [i].tp_node);
                if ( IS_GDS_ALIAS( nw->channels[ i ] ) )
                {
                    throw std::runtime_error(
                        "Found TP enabled in broadcast mode" );
                }
            }
            nw->clear_testpoints = 1;
        }

        for ( int i = 0; i < nw->num_channels; i++ )
        {
            if ( nw->channels[ i ].req_rate )
            {
                nw->transmission_block_size +=
                    nw->channels[ i ].bps * nw->channels[ i ].req_rate;
            }
            else
            {
                nw->transmission_block_size += nw->channels[ i ].bytes;
            }

            nw->block_size += nw->channels[ i ].bytes;
            // Construct put vector array
            if ( !nw->pvec_len )
            { // Start vector
                nw->pvec[ 0 ].vec_idx = nw->channels[ i ].offset;
                nw->pvec[ 0 ].vec_len = nw->channels[ i ].bytes;
                nw->pvec_len++;
            }
            else if ( nw->pvec[ nw->pvec_len - 1 ].vec_idx +
                          nw->pvec[ nw->pvec_len - 1 ].vec_len ==
                      nw->channels[ i ].offset )
            {
                // Increase vector element length for contiguous
                // channels
                nw->pvec[ nw->pvec_len - 1 ].vec_len += nw->channels[ i ].bytes;
            }
            else
            { // Start new vector if there is a gap in channel
                // sequence
                nw->pvec[ nw->pvec_len ].vec_idx = nw->channels[ i ].offset;
                nw->pvec[ nw->pvec_len ].vec_len = nw->channels[ i ].bytes;
                nw->pvec_len++;
            }

            // Contruct another scattered data array to be used
            // for data decimation in the
            // net_writer_c::consumer() `dec_vec [].vec_idx' is
            // the offset in the net-writer circ buffer data
            // block
            if ( !nw->dec_vec_len )
            { // Start the vector
                nw->dec_vec[ 0 ].vec_idx = 0;
                nw->dec_vec[ 0 ].vec_len = nw->channels[ i ].bytes;
                nw->dec_vec[ 0 ].vec_rate = nw->channels[ i ].req_rate;
                nw->dec_vec[ 0 ].vec_bps = nw->channels[ i ].bps;
                nw->dec_vec_len++;
            }
            else if ( nw->channels[ i - 1 ].req_rate ==
                          nw->channels[ i ].req_rate &&
                      nw->channels[ i - 1 ].bytes == nw->channels[ i ].bytes &&
                      nw->channels[ i - 1 ].bps == nw->channels[ i ].bps )
            {

                // Increase data element length for contiguous
                // channels with the same rate and the same
                // number bytes per sample. We should be able to
                // treat these channels as one channel with the
                // increased data rate.

                nw->dec_vec[ nw->dec_vec_len - 1 ].vec_len +=
                    nw->channels[ i ].bytes;
                nw->dec_vec[ nw->dec_vec_len - 1 ].vec_rate +=
                    nw->channels[ i ].req_rate;
            }
            else
            {

                // Start new vector if rate or BPS changes

                nw->dec_vec[ nw->dec_vec_len ].vec_idx =
                    nw->dec_vec[ nw->dec_vec_len - 1 ].vec_idx +
                    nw->dec_vec[ nw->dec_vec_len - 1 ].vec_len;
                nw->dec_vec[ nw->dec_vec_len ].vec_len =
                    nw->channels[ i ].bytes;
                nw->dec_vec[ nw->dec_vec_len ].vec_rate =
                    nw->channels[ i ].req_rate;
                nw->dec_vec[ nw->dec_vec_len ].vec_bps = nw->channels[ i ].bps;
                nw->dec_vec_len++;
            }
        }

        nw->decimation_requested = !nw->broadcast;
        system_log( 5, "decimation flag=%d", nw->decimation_requested );
        nw->block_size += nw->num_channels * sizeof( int );

        // append one element per channel to save the status
        // word
        //
        unsigned long status_ptr =
            daqd.block_size - 17 * sizeof( int ) * daqd.num_channels;
        for ( int i = 0; i < nw->num_channels; i++ )
        {
            nw->pvec[ nw->pvec_len ].vec_idx =
                status_ptr + 17 * sizeof( int ) * nw->channels[ i ].seq_num;
            nw->pvec[ nw->pvec_len ].vec_len = sizeof( int );
            nw->pvec_len++;
        }

        circ_buffer* this_cb;

        this_cb = daqd.b1;
        nw->need_send_reconfig_block = true;

        if ( nw->start_net_writer(
                 yyout, 0, no_data_connection, this_cb, &daqd.fsd, 0, 0, 0 ) !=
             0 )
        {
            throw std::runtime_error( "Unable to start net writer" );
        }
        nw.release( );
    }

} // namespace comm_impl