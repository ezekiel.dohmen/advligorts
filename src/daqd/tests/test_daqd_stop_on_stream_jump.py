
import os.path

import integration

integration.Executable(name="fe_simulated_streams", hints=["../fe_stream_test/fe_simulation",], description="data stream generator")
integration.Executable(name="daqd", hints=[], description="daqd")
integration.Executable(name="fe_stream_check_nds", hints=["../fe_stream_test",], description="verification program")

ini_dir = integration.state.temp_dir('ini_files')
master_file = os.path.join(ini_dir, "master")
testpoint_file = ""
daqdrc_file = os.path.join(ini_dir, "daqdrc")

integration.transform_text_file(input='daqdrc_live_test', output=daqdrc_file,
                                substitutions=[('MASTER', master_file),
                                               ('TESTPOINT', testpoint_file)])


fe_simulated_streams = integration.Process("fe_simulated_streams",
                                           ["-i", ini_dir,
                                            "-M", master_file,
                                            "-b", "local_dc",
                                            "-m", "140",
                                            "-k", "300",
                                            "-R", "247"])

fe_simulated_streams2 = integration.Process("fe_simulated_streams",
                                           ["-i", ini_dir,
                                            "-M", master_file,
                                            "-b", "local_dc",
                                            "-m", "140",
                                            "-k", "300",
                                            "-R", "247"])

daqd = integration.Process("daqd",
                           ["-c", daqdrc_file])

fe_stream_check_nds = integration.Process("fe_stream_check_nds", ["-c", "100", "-w", "/tmp/wait_check"])


def test_stopped():
    state = fe_stream_check_nds.state()
    return state != integration.Process.RUNNING


def fe_simulated_streams_stopped():
    print("fe_simulated_streams == {0}".format(fe_simulated_streams.state()))
    return fe_simulated_streams.state() in (integration.Process.STOPPED,)


def fe_simulated_streams2_started():
    print("fe_simulated_streams2 == {0}".format(fe_simulated_streams2.state()))
    return fe_simulated_streams2.state() in (integration.Process.RUNNING,)



def daqd_stopped():
    print("daqd_state == {0}".format(daqd.state()))
    return daqd.state() in (integration.Process.STOPPED,)


def wait_long():
    import time
    time.sleep(1.0)
    print(".")
    time.sleep(1.0)
    print(".")
    time.sleep(1.0)
    print(".")


def echo(msg):
    def wrapper():
        print(msg)
    return wrapper


integration.Sequence(
 [
     # integration.state.preserve_files,
     integration.require_readable(files=["/dev/gpstime", "/dev/mbuf"], description="required device files"),
     fe_simulated_streams.run,
     integration.wait(),
     daqd.run,
     integration.wait_tcp_server(port=8088, timeout=20),
     echo("daqd is up now"),
     integration.wait(),

     echo("about to start fe_stream_check_nds"),
     fe_stream_check_nds.run,
     echo("fe_stream_check_nds running"),
     fe_stream_check_nds.ignore,
     integration.wait_for(test_stopped, timeout=180, step=1.25),
     echo("fe_stream_check_nds done"),

     fe_simulated_streams.ignore,
     echo("about to stop the data stream"),
     # stop the data stream
     fe_simulated_streams.stop,
     integration.wait_for(fe_simulated_streams_stopped, 5, 0.25),
     echo("simulated streams stopped"),
     # introduce a pause in the data stream to test that we fail on a time jump
     wait_long,
     # only now when we restart the new data stream do we expect daqd to fail
     daqd.ignore,
     echo("starting 2nd invocation of simulated streams"),
     fe_simulated_streams2.ignore,
     fe_simulated_streams2.run,
     integration.wait_for(fe_simulated_streams2_started, 5, 0.1),
     integration.wait_for(daqd_stopped, 15, 0.25),
 ]
)

