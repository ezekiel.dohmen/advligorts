
import glob
import os
import os.path

import integration

integration.Executable(name="fe_simulated_streams", hints=["../fe_stream_test/fe_simulation",], description="data stream generator")
integration.Executable(name="daqd", hints=[], description="daqd")

ini_dir = integration.state.temp_dir('ini_files')
master_file = os.path.join(ini_dir, "master")
mod_file = os.path.join(ini_dir, "mod5.ini")
par_file = os.path.join(ini_dir, "tpchn_mod5.par")
testpoint_file = ""
daqdrc_file = os.path.join(ini_dir, "daqdrc")

integration.transform_text_file(input='daqdrc_live_test', output=daqdrc_file,
                                substitutions=[('MASTER', master_file),
                                               ('TESTPOINT', testpoint_file),])

fe_simulated_streams = integration.Process("fe_simulated_streams",
                                           ["-i", ini_dir,
                                            "-M", master_file,
                                            "-b", "local_dc",
                                            "-m", "100",
                                            "-k", "300",
                                            "-R", "5"])
daqd = integration.Process("daqd",
                           ["-c", daqdrc_file],
                           exit_codes=(1,))


def do_ini_manipulation(input_name: str):
    temp_name = input_name + ".tmp"
    names = ['X1:CHANNEL_A',]
    with open(input_name, 'rt') as f:
        with open(temp_name, 'wt') as output:
            for line in f:
                line = line.strip()
                if len(names) > 0 and line.startswith('[') and line != '[default]':
                    value = names.pop()
                    line = "[" + value + "]"
                output.write(line + '\n')
    os.unlink(input_name)
    os.rename(temp_name, input_name)


def insert_duplicates():
    global mod_file
    global par_file
    do_ini_manipulation(mod_file)
    do_ini_manipulation(par_file)

def daqd_not_running():
    state = daqd.state()
    return state not in (integration.Process.UNINITIALIZED, integration.Process.RUNNING)

def ini_files_available():
    global ini_dir
    global master_file
    inis = glob.glob(os.path.join(ini_dir, "*.ini"))
    pars = glob.glob(os.path.join(ini_dir, "*.par"))
    return (len(inis) == len(pars)) and (len(inis)== 5) and os.path.exists(master_file)


integration.Sequence(
 [
  integration.require_readable(files=["/dev/gpstime", "/dev/mbuf"], description="required device files"),
  fe_simulated_streams.run,
  integration.wait_for(predicate=ini_files_available, description="ini and par files must exist", timeout=10),
  insert_duplicates,
  daqd.run,
  daqd.ignore,
  integration.wait_for(predicate=daqd_not_running, description="daqd must fail", timeout=10),
 ]
)
