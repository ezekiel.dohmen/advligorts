#include "catch.hpp"

#include "raii.hh"

namespace
{
    class SimpleDefault
    {
    public:
        int val{ 42 };
    };

    class NoteDestruction
    {
    public:
        explicit NoteDestruction( int* counter_location )
            : counter{ counter_location }
        {
        }
        ~NoteDestruction( )
        {
            if ( counter )
            {
                ( *counter )++;
            }
        }
        int* counter{ nullptr };
    };
} // namespace

TEST_CASE( "a c_unique_ptr defaults to a nullptr" )
{
    raii::c_unique_ptr< int > p{ };

    REQUIRE( p.get( ) == nullptr );
    REQUIRE( static_cast< bool >( p ) == false );
}

TEST_CASE(
    "c_unique_ptr::make constructs an object using the given parameters" )
{
    auto p1 = raii::c_unique_ptr< int >::make( 5 );
    REQUIRE( *p1 == 5 );
    REQUIRE( static_cast< bool >( p1 ) == true );

    auto p2 = raii::c_unique_ptr< SimpleDefault >::make( );
    REQUIRE( p2->val == 42 );
}

TEST_CASE( "a c_unique_ptr cleans up by calling the destructor" )
{
    int destructor_called{ 0 };

    REQUIRE( destructor_called == 0 );
    {
        auto p1 =
            raii::c_unique_ptr< NoteDestruction >::make( &destructor_called );
        REQUIRE( p1.get( ) != nullptr );
        REQUIRE( destructor_called == 0 );
    }
    REQUIRE( destructor_called == 1 );
}

TEST_CASE( "a reset will call the destructor on a c_unique_ptr if it is there" )
{
    int destructor_called{ 0 };

    REQUIRE( destructor_called == 0 );
    {
        auto p1 =
            raii::c_unique_ptr< NoteDestruction >::make( &destructor_called );
        REQUIRE( p1.get( ) != nullptr );
        REQUIRE( destructor_called == 0 );
        p1.reset( );
        REQUIRE( p1.get( ) == nullptr );
        REQUIRE( destructor_called == 1 );
        destructor_called = -1;
    }
    REQUIRE( destructor_called == -1 );
}

TEST_CASE(
    "a release will not call the destructor on a c_unique_ptr if it is there" )
{
    int destructor_called{ 0 };
    NoteDestruction* p_release = nullptr;

    try
    {

        REQUIRE( destructor_called == 0 );
        {
            auto p1 = raii::c_unique_ptr< NoteDestruction >::make(
                &destructor_called );
            auto p_actual = p1.get( );
            REQUIRE( p1.get( ) != nullptr );
            REQUIRE( destructor_called == 0 );
            p_release = p1.release( );
            REQUIRE( p1.get( ) == nullptr );
            REQUIRE( p_actual == p_release );
            REQUIRE( destructor_called == 0 );
            destructor_called = -1;
        }
        REQUIRE( destructor_called == -1 );
        p_release->~NoteDestruction();
        free(p_release);
        p_release = nullptr;
    }
    catch(...)
    {
        if (p_release)
        {
            p_release->~NoteDestruction();
            free(p_release);
        }
        throw;
    }
}

TEST_CASE(
    "It is safe to call reset or release on a c_unique_ptr that has a nullptr" )
{
    raii::c_unique_ptr< int > p1{ };
    REQUIRE( p1.get( ) == nullptr );
    p1.reset( );
    REQUIRE( p1.get( ) == nullptr );
    p1.release( );
    REQUIRE( p1.get( ) == nullptr );
}

TEST_CASE( "c_unique_ptr::swap swaps the values w/o calling the destructor" )
{
    int destructor_called{ 0 };
    {
        auto p1 =
            raii::c_unique_ptr< NoteDestruction >::make( &destructor_called );

        auto p2 =
            raii::c_unique_ptr< NoteDestruction >::make( &destructor_called );
        REQUIRE( p1.get( ) != nullptr );
        REQUIRE( p2.get( ) != nullptr );
        REQUIRE( p1.get( ) != p2.get( ) );
        auto ptr1 = p1.get( );
        auto ptr2 = p2.get( );
        REQUIRE( ptr1 != ptr2 );

        REQUIRE( destructor_called == 0 );
        p1.swap( p2 );
        REQUIRE( destructor_called == 0 );
        REQUIRE( p1.get( ) != nullptr );
        REQUIRE( p2.get( ) != nullptr );
        REQUIRE( p1.get( ) != p2.get( ) );
        REQUIRE( p1.get( ) == ptr2 );
        REQUIRE( p2.get( ) == ptr1 );
    }
    REQUIRE( destructor_called == 2 );
}