import os
import os.path
import time
import typing

#just not there for some reaoson and can't install it.
import integration


integration.Executable(name="fe_simulated_streams", hints=["../fe_stream_test/fe_simulation",], description="data stream generator")
integration.Executable(name="daqd", hints=[], description="daqd")
integration.Executable(name="fe_stream_check_nds", hints=["../fe_stream_test",], description="verification program")


ini_dir = integration.state.temp_dir('ini_files')
master_file = os.path.join(ini_dir, "master")
testpoint_file = ""
daqdrc_file = os.path.join(ini_dir, "daqdrc")

integration.transform_text_file(input='daqdrc_live_test', output=daqdrc_file,
         substitutions=[('MASTER', master_file),
            ('TESTPOINT', testpoint_file)])



fe_simulated_streams = integration.Process("fe_simulated_streams",
            ["-i", ini_dir,
             "-M", master_file,
             "-b", "local_dc",
             "-m", "140",
             "-k", "700",
             "-R", "10"])

daqd = integration.Process("daqd", ["-c", daqdrc_file])

fe_stream_check = integration.Process("fe_stream_check_nds",
            ["-c", "100",
             "-t", "-2"])


#functions...

def check_ok(timeout:float):
    end_time = time.time() + timeout
    def do_check():
        global fe_stream_check
        while True:
            state = fe_stream_check.state()
            if state == integration.Process.STOPPED:
                fe_stream_check.ignore()
                print("the check finished in a stopped state")
                return
            elif state == integration.Process.FAILED:
                raise RuntimeError('Data check failed')
            print("state is {0}".format(state))

            if time.time() > end_time:
                raise RuntimeError('Timed out while waiting for nds check to complete')
            time.sleep(0.5)

    return do_check


def echo(msg):
    def wrapper():
        print(msg)
    return wrapper



integration.Sequence(
    [
        # integration.state.preserve_files,
        integration.require_readable(files=["/dev/gpstime", "/dev/mbuf"], description="required device files"),
        fe_simulated_streams.run,
        integration.wait(),
        daqd.run,
        integration.wait_tcp_server(port=8088, timeout=20),
        echo("daqd is really up now"),
        integration.wait(5),
        fe_stream_check.run,
        check_ok(20),
    ]
)