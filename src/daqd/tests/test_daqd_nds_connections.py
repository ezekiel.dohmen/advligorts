import os
import os.path
import time
import typing
import integration

integration.Executable(name="fe_simulated_streams", hints=["../fe_stream_test/fe_simulation",], description="data stream generator")
integration.Executable(name="daqd", hints=["daqd"], description="")



#NEED JOHNATHONS HELP AS THE THE PACKAGE CAN'T FIND THE NDS1 EXECUTABLE



integration.Executable(name="test_nds1_connections", hints=["test_nds1_connections"], description="test_nds1_connections")


ini_dir = integration.state.temp_dir('ini_files')
master_file = os.path.join(ini_dir, "master")
testpoint_file = ""
daqdrc_file = os.path.join(ini_dir, "daqdrc")


integration.transform_text_file(input='daqdrc_live_test', output=daqdrc_file,
         substitutions=[('MASTER', master_file),
            ('TESTPOINT', testpoint_file)])


fe_simulated_streams = integration.Process("fe_simulated_streams",
            ["-i", ini_dir,
             "-M", master_file,
             "-b", "local_dc",
             "-m", "140",
             "-k", "700",
             "-R", "100"])

daqd = integration.Process("daqd", ["-c", daqdrc_file])

test_nds1_connections = integration.Process("test_nds1_connections")



#functions...

def check_ok(timeout:float):
    end_time = time.time() + timeout
    def do_check():
        global test_nds1_connections
        while True:
            state = test_nds1_connections.state()
            if state == integration.Process.STOPPED:
                test_nds1_connections.ignore()
                print("the check finished in a stopped state")
                return
            elif state == integration.Process.FAILED:
                raise RuntimeError('Data check failed')
            print("state is {0}".format(state))

            if time.time() > end_time:
                raise RuntimeError('Timed out while waiting for nds check to complete')
            time.sleep(0.5)

    return do_check


def echo(msg):
    def wrapper():
        print(msg)
    return wrapper



integration.Sequence([

    #integration.state.preserve_files,
    integration.require_readable(files=["/dev/gpstime", "/dev/mbuf"], description="required device files"),
    fe_simulated_streams.run,
    integration.wait(),
    daqd.run,
    integration.wait_tcp_server(port=8088, timeout=20),
    echo("daqd is really up now"),
    test_nds1_connections.run,
    #check_ok(20)




])




