//
// Created by jonathan.hanks on 5/26/20.
//

#ifndef DAQD_TRUNK_DECLARATIVE_CONFIG_HH
#define DAQD_TRUNK_DECLARATIVE_CONFIG_HH

/**
 * Define what we want a config to look like.
 *
 * The goal is to define a configuration structure
 * that is declarative and sorts out all dependencies.
 * Not a imperative one where previous state and
 * commands matter.
 *
 * This should not be constrained much by what
 * the daqd is actually like.  However it needs
 * to be able to express the range of the daqd.
 */

#include <array>
#include <fstream>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include <boost/variant.hpp>
#include "declarative_services.hh"

namespace declarative_config
{

    using parameter_t = boost::variant< int, double, bool, std::string >;
    using parameter_map = std::map< std::string, parameter_t >;
    using service_ptr = std::shared_ptr< service_base >;
    using service_list = std::vector< service_ptr >;

    /*!
     * @brief retrieve a string version of the parameter value
     * @param param the parameter to extract the value from
     * @return A representation of the value in a std::string
     */
    std::string as_string( const parameter_t& param );

    struct detector_t
    {
        std::string             name{ };
        std::string             prefix{ };
        double                  longitude{ 0.0 };
        double                  latitude{ 0.0 };
        double                  elevation{ 0.0 };
        std::array< double, 2 > azimuths{ 0.0, 0.0 };
        std::array< double, 2 > altitudes{ 0.0, 0.0 };
        std::array< double, 2 > midpoints{ 0.0, 0.0 };
    };

    struct channel_directory_t
    {
        std::string master_file_path{ "/etc/advligorts/master" };
        std::string testpoint_par_path{ "/etc/advligorts/testpoint.par" };
    };

    struct config_t
    {
        parameter_map       parameters{ };
        detector_t          detector{ };
        channel_directory_t inputs{ };
        service_list        services{ };

        service_ptr find_service( const std::string& name );
    };

    /*!
     * @brief examine a filename to determine if it is likely a declarative
     * config file.
     * @return returns true if fname endswith ".yaml".
     */
    bool is_declarative_config_file( const std::string& fname );

    /*!
     * @brief There are some frame writing defaults that
     * take information from the detector structure, and
     * are not thus part of the parsing phase.
     * @param config The config object to add detector specific
     * overrides to.
     */
    void overlay_derived_defaults( config_t& config );

    /*!
     * @brief parse a config file into config_t structure
     * @param stream input stream
     * @return the config described in a config_t structure
     */
    config_t parse( std::istream& stream );

    inline config_t
    parse( const std::string& fname )
    {
        std::ifstream input( fname );
        auto          conf = parse( input );
        overlay_derived_defaults( conf );
        return conf;
    }
} // namespace declarative_config

#endif // DAQD_TRUNK_DECLARATIVE_CONFIG_HH
