//
// Created by jonathan.hanks on 6/20/23.
//

#ifndef DAQD_TRUNK_DAQD_CONFIG_IMPL_HH
#define DAQD_TRUNK_DAQD_CONFIG_IMPL_HH

#include "daqd_config.hh"

class daqd_c;

/**
 * Given a daqd_config::config_t object configure the daqd.
 * @param config
 */
void apply_config_to_daqd( const daqd_config::config_t& config );

#endif // DAQD_TRUNK_DAQD_CONFIG_IMPL_HH
