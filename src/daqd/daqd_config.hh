#ifndef DAQD_CONFIG_HH
#define DAQD_CONFIG_HH

/**
 * As part of the declarative config this file defines
 * what is actuall in the daqd.  This defines things like
 * there is a frame writer thread, a main thread, a profiler.
 *
 * It defines an ordering for services such that things can
 * be specified in any order, but run in a valid order that
 * the daqd will work with.
 */

#include <ostream>
#include <vector>

#include "declarative_config.hh"

namespace daqd_config
{
    enum class action
    {
        CONFIGURE_CHANNELS = 0,
        CONFIGURE_TESTPOINTS = 10,
        CONFIGURE_LOCATION = 20,
        ENABLE_GDS_BROADCAST = 30,
        MAIN = 40,
        EPICS = 50,
        PROFILER = 60,
        FRAME_WRITER = 70,
        TRENDER = 80,
        STREND_FRAME_WRITER = 90,
        MTREND_FRAME_WRITER = 100,
        RAW_MTREND_WRITER = 110,
        TREND_PROFILER = 120,
        GDS_BROADCAST = 130,
        PRODUCER = 140,
        DAQD_PROTOCOL = 150,
        DAQD_TEXT_PROTOCOL = 160,
        CONFIGURE_NDS1_PARAMS = 170,
        FINAL_CRC = 180,
    };

    inline std::ostream&
    operator<<( std::ostream& os, action a )
    {
        os << static_cast< int >( a );
        return os;
    }

    struct config_t
    {
        std::map< std::string, std::string > general_parameters{ };
        std::vector< action >                actions{ };
        declarative_config::detector_t       detector{ };
    };

    config_t to_config( const declarative_config::config_t& general_config );

    config_t parse( const std::string& fname );

    config_t parse( std::istream& input );
} // namespace daqd_config

#endif // DAQD_CONFIG_HH