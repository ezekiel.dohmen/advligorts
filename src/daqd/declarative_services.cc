//
// Created by jonathan.hanks on 6/16/23.
//
#include "declarative_services.hh"

#include <functional>
#include <map>
#include "raii.hh"

#include <boost/algorithm/string.hpp>

namespace
{
    using sptr = std::unique_ptr< declarative_config::service_base >;

    using service_factory_t = std::map< std::string, std::function< sptr( ) > >;
    using service_factory_entry = typename service_factory_t::value_type;

    const service_factory_t&
    service_factories( )
    {
        using namespace declarative_config;
        static std::map< std::string, std::function< sptr( ) > > factories{
            std::make_pair(
                "frame_writer",
                []( ) -> sptr {
                    return raii::make_unique_ptr< service_frame_writer >( );
                } ),
            std::make_pair(
                "strend_writer",
                []( ) -> sptr {
                    return raii::make_unique_ptr< service_strend_writer >( );
                } ),
            std::make_pair(
                "mtrend_writer",
                []( ) -> sptr {
                    return raii::make_unique_ptr< service_mtrend_writer >( );
                } ),
            std::make_pair( "raw_mtrend_writer",
                            []( ) -> sptr {
                                return raii::make_unique_ptr<
                                    service_raw_mtrend_writer >( );
                            } ),
            std::make_pair( "nds1",
                            []( ) -> sptr {
                                return raii::make_unique_ptr< service_nds1 >( );
                            } ),
            std::make_pair(
                "epics",
                []( ) -> sptr {
                    return raii::make_unique_ptr< service_epics >( );
                } ),
            std::make_pair(
                "gds_broadcast",
                []( ) -> sptr {
                    return raii::make_unique_ptr< service_gds_broadcast >( );
                } ),
            std::make_pair(
                "daqd_control",
                []( ) -> sptr {
                    return raii::make_unique_ptr< service_daqd_control >( );
                } ),
        };
        return factories;
    }
} // namespace

namespace declarative_config
{
    std::unique_ptr< service_base >
    service_base::get_service( const std::string& name )
    {
        auto factories = service_factories( );
        auto it = factories.find( name );
        if ( it == factories.end( ) )
        {
            return nullptr;
        }
        return it->second( );
    }

    std::vector< std::string >
    service_base::get_services( )
    {
        std::vector< std::string > services{ };
        auto                       factories = service_factories( );
        services.reserve( factories.size( ) );
        std::transform(
            factories.begin( ),
            factories.end( ),
            std::back_inserter( services ),
            []( const service_factory_entry& entry ) -> std::string {
                return entry.first;
            } );
        return services;
    }

    std::string
    service_base::at_first( const std::string& index ) const
    {
        auto                       input = at( index );
        std::vector< std::string > dest{ };
        boost::split( dest, input, boost::is_any_of( "," ) );
        return ( dest.empty( ) ? "" : dest.front( ) );
    }
} // namespace declarative_config