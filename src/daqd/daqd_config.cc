#include "daqd_config.hh"

#include <boost/variant.hpp>

#include <algorithm>
#include <deque>
#include <iostream>
#include <fstream>

namespace daqd_config
{
    struct param_string : public boost::static_visitor< std::string >
    {
        std::string
        operator( )( int val ) const
        {
            std::ostringstream os{ };
            os << val;
            return os.str( );
        }
        std::string
        operator( )( double val ) const
        {
            std::ostringstream os{ };
            os << val;
            return os.str( );
        }
        std::string
        operator( )( bool val ) const
        {
            return ( val ? "true" : "false" );
        }
        std::string
        operator( )( const std::string& val ) const
        {
            return val;
        }
    };

    struct service_dependency
    {
        service_dependency( ) = default;
        service_dependency( action what, std::initializer_list< action > req )
            : service{ what }, requires{ req }
        {
        }
        service_dependency( const service_dependency& ) = default;
        service_dependency( service_dependency&& ) noexcept = default;
        service_dependency& operator=( const service_dependency& ) = default;
        service_dependency&
        operator=( service_dependency&& ) noexcept = default;

        action                service{ action::MAIN };
        std::vector< action > requires{ };
    };

    std::vector< service_dependency >
    make_service_dependency_list(
        const std::initializer_list< service_dependency >& cont )
    {
        std::vector< service_dependency > deps{ };
        for ( const auto& cur : cont )
        {
            for ( const auto& entry : cur.requires )
            {
                auto it =
                    std::find_if( deps.begin( ),
                                  deps.end( ),
                                  [ &entry ]( service_dependency& d ) -> bool {
                                      return d.service == entry;
                                  } );
                if ( it == deps.end( ) )
                {
                    std::cerr << "entry = " << (int)entry << "\n";

                    throw std::runtime_error( "Not all dependencies are in the "
                                              "service dependency list" );
                }
            }
            deps.emplace_back( cur );
        }
        return deps;
    }

    static const std::vector< service_dependency > service_dependencies =
        make_service_dependency_list( {
            { action::CONFIGURE_CHANNELS, {} },
            { action::CONFIGURE_TESTPOINTS, { action::CONFIGURE_CHANNELS } },
            { action::CONFIGURE_LOCATION, {} },
            { action::ENABLE_GDS_BROADCAST, {} },
            { action::MAIN,
              { action::CONFIGURE_CHANNELS, action::CONFIGURE_LOCATION } },
            { action::PROFILER, { action::MAIN } },
            { action::EPICS, {} },
            { action::PRODUCER, { action::MAIN, action::PROFILER } },
            { action::FRAME_WRITER, { action::PRODUCER } },
            { action::TRENDER,
              {
                  action::PRODUCER,
              } },
            { action::STREND_FRAME_WRITER, { action::TRENDER } },
            { action::MTREND_FRAME_WRITER, { action::TRENDER } },
            { action::RAW_MTREND_WRITER, { action::TRENDER } },
            { action::TREND_PROFILER, { action::TRENDER } },
            { action::GDS_BROADCAST,
              { action::ENABLE_GDS_BROADCAST, action::FRAME_WRITER } },
            { action::DAQD_PROTOCOL, {} },
            { action::DAQD_TEXT_PROTOCOL, {} },
            { action::CONFIGURE_NDS1_PARAMS,
              { action::PRODUCER, action::TRENDER, action::DAQD_PROTOCOL } },
            { action::FINAL_CRC, { action::PRODUCER } },
        } );

    void
    add_to_actions( config_t& config, action to_add )
    {
        std::deque< action > actions{ to_add };
        while ( !actions.empty( ) )
        {
            action cur_action = actions.front( );
            actions.pop_front( );
            auto it = std::find(
                config.actions.begin( ), config.actions.end( ), cur_action );
            if ( it == config.actions.end( ) )
            {
                config.actions.emplace_back( cur_action );
                auto dep_it = std::find_if(
                    service_dependencies.begin( ),
                    service_dependencies.end( ),
                    [ cur_action ]( const service_dependency& cur ) -> bool {
                        return cur.service == cur_action;
                    } );
                if ( dep_it != service_dependencies.end( ) )
                {
                    std::copy( dep_it->requires.begin( ),
                               dep_it->requires.end( ),
                               std::back_inserter( actions ) );
                }
            }
        }
        std::sort( config.actions.begin( ), config.actions.end( ) );
    }

    void
    add_to_parameters( config_t&                         config,
                       declarative_config::service_base& service )
    {
        for ( const auto& key : service.fields( ) )
        {
            auto final_key = service.name( ) + ":" + key;
            config.general_parameters[ final_key ] = service.at( key );
        }
    }

    config_t
    to_config( const declarative_config::config_t& input )
    {
        config_t config{ };

        // clang-format off
        std::map<std::string, std::vector<action>> daqd_service_map{
            std::make_pair("config_number_client", std::vector<action>{action::CONFIGURE_CHANNELS}),
            std::make_pair("frame_writer", std::vector<action>{action::FRAME_WRITER}),
            std::make_pair("strend_writer", std::vector<action>{action::STREND_FRAME_WRITER, action::TREND_PROFILER}),
            std::make_pair("mtrend_writer", std::vector<action>{action::MTREND_FRAME_WRITER, action::TREND_PROFILER}),
            std::make_pair("raw_mtrend_writer", std::vector<action>{action::RAW_MTREND_WRITER, action::TREND_PROFILER}),
            std::make_pair("nds1", std::vector<action>{action::CONFIGURE_NDS1_PARAMS, action::DAQD_PROTOCOL, action::TRENDER, action::TREND_PROFILER, action::CONFIGURE_TESTPOINTS}),
            std::make_pair("epics", std::vector<action>{action::EPICS}),
            std::make_pair("gds_broadcast", std::vector<action>{action::GDS_BROADCAST}),
            std::make_pair("daqd_control", std::vector<action>{action::DAQD_PROTOCOL, action::DAQD_TEXT_PROTOCOL}),
        };
        // clang-format on

        for ( const auto& param : input.parameters )
        {
            config.general_parameters.insert( std::make_pair(
                param.first,
                boost::apply_visitor( param_string( ), param.second ) ) );
        }

        // add all default service parameters
        auto all_services = declarative_config::service_base::get_services( );
        for ( const auto& service_name : all_services )
        {
            auto service =
                declarative_config::service_base::get_service( service_name );
            if ( !service )
            {
                continue;
            }
            add_to_parameters( config, *service );
        }
        // add specific service overrides
        for ( const auto& service : input.services )
        {
            if ( !service )
            {
                continue;
            }
            add_to_parameters( config, *service );
            auto it = daqd_service_map.find( service->name( ) );
            ;
            if ( it == daqd_service_map.end( ) )
            {
                continue;
            }
            std::cout << "processing service " << service->name( ) << "\n";
            for ( const auto& entry : it->second )
            {
                add_to_actions( config, entry );
            }
        }

        add_to_actions( config, action::CONFIGURE_CHANNELS );
        add_to_actions( config, action::CONFIGURE_LOCATION );
        add_to_actions( config, action::FINAL_CRC );

        config.general_parameters[ "input:main_ini" ] =
            input.inputs.master_file_path;
        config.general_parameters[ "input:tp_ini" ] =
            input.inputs.testpoint_par_path;

        config.detector = input.detector;

        return config;
    }

    config_t
    parse( const std::string& fname )
    {
        std::ifstream input( fname );
        if ( !input.is_open( ) )
        {
            throw std::runtime_error( "Error opening yaml config file" );
        }
        return parse( input );
    }

    config_t
    parse( std::istream& input )
    {
        auto general_config = declarative_config::parse( input );
        declarative_config::overlay_derived_defaults( general_config );
        return to_config( general_config );
    }

    void
    apply_config( const daqd_config::config_t& config )
    {
    }
} // namespace daqd_config