#include "portableInline.h"
#include "util/inlineMath.h"

typedef struct unwrapPhaseCtx_t
{
    int k;
    int firstRun;

    double  uLast;
    double  uThis;

} unwrapPhaseCtx_t;

LIGO_INLINE double unwrapPhase(double cur_sample, double reset, unwrapPhaseCtx_t * ctx) 
{
    /*      This block of code is supposed to take the output of the atan code for the SPI and unwrap any phase
     *       jumps of greater than pi. This is similar to the MATLAB simulink unwrap block except that the reset
     *       is a seperate input instead of either continuously running or going frame to frame. This means that
     *       the system can be reset to zero fringes at any point.
     *
     *       Daniel Clark - Stanford University 2011.06.27
     *
     */


    double uwrapout;
    double compare;



    /*      Some special cases for the first time the code is run - this means that we will miss a phase jump
     *       if it happens between the first and second data point */
    if (ctx->firstRun == 1) {
        ctx->uLast = cur_sample;
        ctx->k = 0.0;
        ctx->firstRun = 0;
    }

    /*      Reset the phase / fringe counter to zero if desired */
    if (reset > 0.5) {
        ctx->k = 0;
    }


    /*      Collect the difference of the last two data points to determine if unwrapping is necessary */
    compare = cur_sample - ctx->uLast;

    /*      Now take the absolute value of the difference */
    if (compare < 0.0) {
        compare = compare * -1.0;
    }

    /*      Use the last value of k if no unwrapping is needed */
    if (compare < M_PI) {
        uwrapout = cur_sample + M_HALF_PI * 4.0 * ctx->k;
    }
    else {
        if (cur_sample < ctx->uLast) {
            ctx->k += 1;
        }
        else {
            ctx->k -= 1;
        }
        uwrapout = cur_sample + M_HALF_PI * 4.0 * ctx->k;
    }
    ctx->uLast = cur_sample;
    return uwrapout;
}

