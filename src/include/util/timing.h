#ifndef LIGO_TIMING_H
#define LIGO_TIMING_H

#include "util/fixed_width_types.h"

#if defined(__KERNEL__)

#include <asm/cpufeature.h>
#include <asm/msr.h> //rdtsc_ordered()
#include <asm/tsc.h> //tsc_khz

#define CYC2NS_SCALE (1000000ULL * 1>>27 / tsc_khz)

#define LIGO_TIMER_t uint64_t

//
// We are making these functions statix inline bacause rdtsc_ordered()
// is a static function, and the compiler will complain if we aren't
//

/** @brief Returns a monotonic count of nanoseconds from an unspecified
 *         starting point.
 *
 *  This is the kernelspace implementation and uses the TSC register, which 
 *  counts the number of CPU cycles since it was laster reset. We multiply 
 *  by 1e6 first so we can keep the math fixed point, and factor in the CPU
 *  frequency with tsc_khz. 
 *
 *  TODO: How do we end up with ns?
 *
 *  @return uint64_t A number of ns
 */
static inline uint64_t getMonotonic_ns_utin64( void ) 
{
    return (rdtsc_ordered() * 1000000ULL) / tsc_khz ; 
}

/** @brief Fills the LIGO_TIMER_t * with the start time recorded
 *         when this function is called.
 *
 *  @param start_tsc [out] This is filled with the current time (tsc)
 *
 *  @return None
 */
static inline void timer_start( LIGO_TIMER_t * start_tsc)
{
    *start_tsc = rdtsc_ordered();
}

/** @brief Returns the time elapsed from when the start_tsc
 *         was last filled with timer_start()
 *
 *  @param start_tsc [in] The start time you are using to
 *                       calculate the elapsed time (tsc)
 *
 *  @return The time elapsed from when the start_tsc was
 *          filled to now, in ns
 */
static inline uint64_t timer_tock_ns( LIGO_TIMER_t * start_tsc)
{
    return ((rdtsc_ordered() - *start_tsc) * 1000000ULL ) / tsc_khz ;
}

/** @brief Returns the time elapsed from when the start_tsc
 *         was last filled with timer_start(), and also stores
 *         that result in the start_tsc parameter
 *
 *  @param start_ns [in/out] This is filled with duration (ns)
 *                           between the time stored in start_tsc when first
 *                           passed in and now.
 *
 *  @return Returns the elapsed time from that start time passed in and now
 */

static inline uint64_t timer_end_ns( LIGO_TIMER_t * start_tsc)
{
    *start_tsc = ((rdtsc_ordered() - *start_tsc) * 1000000ULL ) / tsc_khz ;
    return *start_tsc; //This is now the duration in ns
}

/** @brief Returns difference in ns, between the first and second tsc
 *         time captures. end_tsc - start_tsc
 *
 *  @param start_ns [in] A tsc count captured with timer_start()
 *  @param end_ns [in] A tsc count captured with timer_start()                    
 *                           
 *  @return Returns the elapsed time between the start time and the end time
 */

static inline uint64_t timer_duration_ns( LIGO_TIMER_t * start_tsc, LIGO_TIMER_t * end_tsc )
{
    return ((*end_tsc - *start_tsc) * 1000000ULL ) / tsc_khz ; 
}

#else //

#include <time.h>

#define LIGO_TIMER_t struct timespec


/** @brief Returns a monotonic count of nanoseconds from an unspecified
 *         starting point.
 *         
 *  This is the userspace implementation and simply uses the clock_gettime()
 *  function provided by the standard library.
 *
 *  @return uint64_t A number of ns
 */
static inline uint64_t getMonotonic_ns_utin64( void ) 
{
    struct timespec monotime;
    clock_gettime(CLOCK_MONOTONIC_RAW, &monotime);
    return (monotime.tv_sec * 1000000000ULL) + monotime.tv_nsec; 
}


static inline void timer_start( LIGO_TIMER_t * start)
{
    clock_gettime(CLOCK_MONOTONIC, start);
}

static inline uint64_t timer_tock_ns( LIGO_TIMER_t * start)
{
    struct timespec tend={0,0};
    clock_gettime(CLOCK_MONOTONIC, &tend);
    return ((double)tend.tv_sec*1.0e9 + tend.tv_nsec) - 
           ((double)start->tv_sec*1.0e9 + start->tv_nsec);
}

static inline uint64_t timer_end_ns( LIGO_TIMER_t * start)
{
    struct timespec tend={0,0};
    uint64_t result = 0;
    clock_gettime(CLOCK_MONOTONIC, &tend);
    result = (tend.tv_sec - start->tv_sec) * 1.0e9;
    result += tend.tv_nsec - start->tv_nsec;
    return result; //This is now the duration in ns
}

static inline void udelay( int us)
{
    struct timespec sleeptime={.tv_sec=0, .tv_nsec=us*1000};//us to ns
    //clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &sleeptime, NULL);
    nanosleep(&sleeptime, NULL);
}


#endif


#endif //LIGO_TIMING_H
