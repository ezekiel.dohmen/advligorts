#ifndef LIGO_PRINTL_H
#define LIGO_PRINTL_H


#ifdef __KERNEL__
#include <linux/kernel.h> //snprintf, etc
#include <linux/printk.h>
#define printl printk

#else //__KERNEL__

#include <stdio.h>
#define printl printf
#endif //!__KERNEL__


#endif //LIGO_PRINT_H
