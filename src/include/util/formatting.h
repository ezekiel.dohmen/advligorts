#ifndef LIGO_FORMATTING_H_INCLUDED
#define LIGO_FORMATTING_H_INCLUDED

/**
 * This headers defines the following functions for 
 * kernel or userspace.
 *
 * sprintf
 * snprintf
 * vsnprintf
 *
 * sscanf
 *
 *
 */ 

#ifdef __KERNEL__
#include <linux/kernel.h> //snprintf, etc
#else //__KERNEL__

#include <stdio.h>
#endif //!__KERNEL__



#endif
