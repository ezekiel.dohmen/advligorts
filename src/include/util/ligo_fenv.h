/* Copyright (C) 1997-2023 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <https://www.gnu.org/licenses/>.  */

//
// This is ported from glibc and works in kernel space for LIGO real time modules
//
#ifndef LIGO_FENV_H
#define LIGO_FENV_H


#include "drv/rts-logger.h"
#include "fe_state_word.h"

/* Define bits representing the exception.  We use the bit positions
   of the appropriate bits in the FPU control word.  */
enum
  {
    FE_INVALID =
#define FE_INVALID	0x01
      FE_INVALID,
    __FE_DENORM = 0x02,
    FE_DIVBYZERO =
#define FE_DIVBYZERO	0x04
      FE_DIVBYZERO,
    FE_OVERFLOW =
#define FE_OVERFLOW	0x08
      FE_OVERFLOW,
    FE_UNDERFLOW =
#define FE_UNDERFLOW	0x10
      FE_UNDERFLOW,
    FE_INEXACT =
#define FE_INEXACT	0x20
      FE_INEXACT
  };

#define FE_ALL_EXCEPT \
	(FE_INEXACT | FE_DIVBYZERO | FE_UNDERFLOW | FE_OVERFLOW | FE_INVALID)

/* The ix87 FPU supports all of the four defined rounding modes.  We
   use again the bit positions in the FPU control word as the values
   for the appropriate macros.  */
enum
  {
    FE_TONEAREST =
#define FE_TONEAREST	0
      FE_TONEAREST,
    FE_DOWNWARD =
#define FE_DOWNWARD	0x400
      FE_DOWNWARD,
    FE_UPWARD =
#define FE_UPWARD	0x800
      FE_UPWARD,
    FE_TOWARDZERO =
#define FE_TOWARDZERO	0xc00
      FE_TOWARDZERO
  };


/* Type representing exception flags.  */
typedef unsigned short int fexcept_t;


/* Type representing floating-point environment.  This structure
   corresponds to the layout of the block written by the `fstenv'
   instruction and has additional fields for the contents of the MXCSR
   register as written by the `stmxcsr' instruction.  */
typedef struct
  {
    unsigned short int __control_word;
    unsigned short int __glibc_reserved1;
    unsigned short int __status_word;
    unsigned short int __glibc_reserved2;
    unsigned short int __tags;
    unsigned short int __glibc_reserved3;
    unsigned int __eip;
    unsigned short int __cs_selector;
    unsigned int __opcode:11;
    unsigned int __glibc_reserved4:5;
    unsigned int __data_offset;
    unsigned short int __data_selector;
    unsigned short int __glibc_reserved5;
#ifdef __x86_64__
    unsigned int __mxcsr;
#endif
  }
fenv_t;

/* If the default argument is used we use this value.  */
#define FE_DFL_ENV	((const fenv_t *) -1)

#ifdef __USE_GNU
/* Floating-point environment where none of the exception is masked.  */
# define FE_NOMASK_ENV	((const fenv_t *) -2)
#endif

/* Type representing floating-point control modes.  */
typedef struct
{
    unsigned short int __control_word;
    unsigned short int __glibc_reserved;
    unsigned int __mxcsr;
} femode_t;

/* Default floating-point control modes.  */
# define FE_DFL_MODE	((const femode_t *) -1L)


/*************************************************************
 *
 * Start of function prototypes
 *
 *************************************************************/


#ifdef __cplusplus
extern "C" {
#endif

int check_fpu_exceptions( void );
void clear_fpu_exceptions(void);
int feclearexcept (int excepts);
int fedisableexcept (int excepts);
int feenableexcept (int excepts);
int __fegetenv (fenv_t *envp);
int fegetexcept (void);
int fegetmode (femode_t *modep);
int __fegetround (void);
int __feholdexcept (fenv_t *envp);
int fesetexcept (int excepts);
int __fesetround (int round);
int fegetexceptflag (fexcept_t *flagp, int excepts);
int __feraiseexcept (int excepts);
int fesetexceptflag (const fexcept_t *flagp, int excepts);
int fetestexcept (int excepts);

#ifdef __cplusplus
}
#endif



#endif //LIGO_FENV_H
