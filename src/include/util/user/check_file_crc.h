//
// Created by jonathan.hanks on 3/30/22.
//

#ifndef DAQD_TRUNK_CHECK_FILE_CRC_H
#define DAQD_TRUNK_CHECK_FILE_CRC_H

#include <stdio.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

#include "crc.h"


/// Common routine to check file CRC.
///	@param[in] *fName	Name of file to check.
///	@return File CRC or -1 if file not found.
static inline int
checkFileCrc( const char* fName )
{
    char         cbuf[ 128 ];
    char*        cp = 0;
    int          flen = 0;
    int          clen = 0;
    unsigned int crc = 0;
    FILE*        fp = NULL;

    if ( !fName )
    {
        return -1;
    }
    fp = fopen( fName, "r" );
    if ( fp == NULL )
    {
        return -1;
    }

    while ( ( cp = fgets( cbuf, 128, fp ) ) != NULL )
    {
        clen = strlen( cbuf );
        flen += clen;
        crc = crc_ptr( cbuf, clen, crc );
    }
    crc = crc_len( flen, crc );
    fclose( fp );
    return crc;
}

#ifdef __cplusplus
}
#endif

#endif // DAQD_TRUNK_CHECK_FILE_CRC_H
