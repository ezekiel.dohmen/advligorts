#ifndef LIGO_INLINE_MATH_STDLIB_H
#define LIGO_INLINE_MATH_STDLIB_H

#include "portableInline.h"

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <math.h>

#define M_TWO_PI (2.0*M_PI)

//NOTE on denormal optimization, ARM32 defaults these to our desired settings, ARM64 still might need to be set
#if defined(__amd64__) || defined(__x86_64__) || defined(_M_X64) || defined(i386) || defined(__i386__) || defined(__i386) || defined(_M_IX86)
#include <pmmintrin.h>
#include <xmmintrin.h>
#endif


//
// Map the ligo math functions to the versions provided by the stdlib
//
#define lsqrt sqrt
#define l2sqrt sqrt
#define l2xr exp2
#define lrndint round
#define lmullog210(x) ((x)*log2(10))
#define llog10 log10
#define lfabs fabs
#define latan2 atan2
#define lceil ceil
#define lfloor floor
#define lfmon fmod

//Define anything else that LIGO needs
#define M_HALF_PI 1.570796326794896619231

#ifdef __APPLE__
#define sincos __sincos
#endif


LIGO_INLINE void
fz_daz( void )
{
#if defined(__amd64__) || defined(__x86_64__) || defined(_M_X64) || defined(i386) || defined(__i386__) || defined(__i386) || defined(_M_IX86)
    _MM_SET_DENORMALS_ZERO_MODE(_MM_DENORMALS_ZERO_ON);
    _MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);
#endif
}


#endif //LIGO_INLINE_MATH_STDLIB_H
