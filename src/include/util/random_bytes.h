#ifndef LIGO_RAMDOM_BYTES_H
#define LIGO_RAMDOM_BYTES_H

/****************************************************************************
 *
 * This header provides a kernel/usp compatible compatible call for
 * retrieving cryptographically secure random bytes.
 *
 * NOTE: These calls are SLOW and should not be used in performance sensitive
 *       loops. .5-1us measured on a Intel(R) Xeon(R) CPU E5-1660 v4 @ 3.20GHz
 *
 * You will see these used to seed faster PRNGs that will be used to
 * generate random numbers during critical loops.
 *
 * @author EJ Dohmen
 * @date 13 April 2022
 *
 ****************************************************************************/

#ifdef __KERNEL__

#include <linux/random.h>

static inline void ligo_get_random_bytes(void *buf, int nbytes)
{
    get_random_bytes(buf, nbytes);
    return;
}

#else //Userspace

#ifdef __APPLE__
#include <stdlib.h>
static inline void ligo_get_random_bytes(void *buf, int nbytes)
{
    arc4random_buf(buf, nbytes);
    return;
}

#else // x86
#include <sys/random.h>
static inline void ligo_get_random_bytes(void *buf, int nbytes)
{
    getrandom(buf, nbytes, GRND_NONBLOCK);
    return;
}

#endif // __APPLE__
#endif



#endif //LIGO_RAMDOM_BYTES_H
