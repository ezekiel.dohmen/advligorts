#ifndef LIGO_MACROS_HH
#define LIGO_MACROS_HH

#include "util/fixed_width_types.h"

// Returns the number of elements in an array, with a bit of error checking that will
// divide by 0 at compile time if there is an issue
#define COUNT_OF(x) ((sizeof(x)/sizeof(0[x])) / ((size_t)(!(sizeof(x) % sizeof(0[x])))))

// Rounds up the first parameter (N) to the next multiple of the second parameter S
// ROUND_UP(12, 5) -> 15
// ROUND_UP(12, 2) -> 12
#define ROUND_UP(N, S) ((((N) + (S) - 1) / (S)) * (S))

// Rounds down to the first parameter (N) to the nearest multiple of the second parameter S
#define ROUND_DOWN(N,S) ((N / S) * S)

// Checks if number is power of two, returns 1 is so
#define IS_POW_OF_2(num) ( (num != 0) && ((num & (num - 1)) == 0))


#endif //LIGO_MACROS_HH
