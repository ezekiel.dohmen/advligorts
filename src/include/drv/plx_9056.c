/// \file plx_9056.c
/// \brief File contains common PLX chip DMA routines for ADC and DAC modules

#include "drv/plx_9056.h"

#include <linux/delay.h> //udelay()


//
// Globals
// Used by: include/drv/gsc18ai64.c, include/drv/gsc18ai32.c, include/drv/gsc20ao8, include/drv/gsc18ao8
//
#ifndef USER_SPACE
volatile PLX_9056_DMA* adcDma[ MAX_ADC_MODULES ]; ///< DMA struct for GSA ADC
dma_addr_t adc_dma_handle[ MAX_ADC_MODULES ]; ///< PCI add of ADC DMA memory

volatile PLX_9056_DMA *dacDma[MAX_DAC_MODULES]; /* DMA struct for GSA DAC */
dma_addr_t dac_dma_handle[MAX_DAC_MODULES];     /* PCI add of DAC DMA memory */
#endif



// *****************************************************************************
/// \brief Function checks status of DMA DONE bit for an ADC module.
///     @param[in] module ID of ADC board to read.
/// @return Status of ADC module DMA DONE bit (0=not complete, 1= complete)
// *****************************************************************************
int
plx9056_check_dma_done( int module )
{
    // Return 0 if DMA not complete
    if ( ( adcDma[ module ]->DMA_CSR & PLX_DMA_DONE ) == 0 )
        return ( 0 );
    // Return 1 if DMA is complete
    else
        return ( 1 );
}

// *****************************************************************************
/// \brief Function if DMA from ADC module is complete.
///< Code will remain in loop until DMA is complete.
///     @param[in] module ID of ADC board to read.
///     @param[in] Time, in usec, to wait between checks.
///     @param[in]  Number of checks before timeout.
/// @param[out] data Status of DMA DONE bit.
/// @return ADC DMA Status (-1= timeout, 0=complete
/// Note: This function not presently used.
// *****************************************************************************
int
plx9056_wait_dma_done( int module, int timeinterval, int time2wait)
{
    int ii = 0;
    do
    {
        ii++;
        udelay( timeinterval );
    } while ( ( (adcDma[ module ]->DMA_CSR & PLX_DMA_DONE) == 0 ) &&
              ( ii < time2wait ) );
    // If DMA did not complete, return error
    if ( ii >= time2wait )
        return -1;
    else
        return 0;
}

// *****************************************************************************
/// \brief Function performs DMA setup for ADC cards.
/// Called only once at startup
///     @param[in] module ID of ADC board.
///     @param[in] Number of bytes to be transferred.
// *****************************************************************************
void
plx9056_adc_dma_setup( int module, int dma_bytes )
{
    /// Set DMA mode such that completion does not cause interrupt on bus.
    adcDma[ module ]->DMA0_MODE = PLX_DMA_MODE_NO_INTR;
    /// Load PCI address (remapped local memory) to which data is to be
    /// delivered.
    adcDma[ module ]->DMA0_PCI_ADD = (int)adc_dma_handle[ module ];
    /// Set the PCI address of board where data will be transferred from.
    adcDma[ module ]->DMA0_LOC_ADD = PLX_DMA_LOCAL_ADDR;
    /// Set the number of bytes to be transferred.
    adcDma[ module ]->DMA0_BTC = dma_bytes;
    /// Set the DMA direction ie ADC to computer memory.
    adcDma[ module ]->DMA0_DESC = PLX_DMA_TO_PCI;
}

// *****************************************************************************
/// \brief Function changes the DMA size of an existing DMA setup
/// called between cycle 1 and cycle 2 for fast ADCs
///     @param[in] module ID of ADC board
///     @param[in] new size for the DMA xfer in number of bytes
void
plx9056_adc_dma_set_size( int module, int dma_bytes )
{
    adcDma[ module ]->DMA0_BTC = dma_bytes;
}

// *****************************************************************************
/// \brief Function sets up starts Demand DMA for ADC modules.
/// Called once on startup.
///     @param[in] module ID of ADC board.
// *****************************************************************************
void
plx9056_adc_dma_enable( int module )
{
    /// Set DMA mode and direction in PLX controller chip on module.
    adcDma[ module ]->DMA0_MODE = PLX_DMA_MODE_NO_INTR | PLX_DEMAND_DMA;
    /// Enable DMA
    adcDma[ module ]->DMA_CSR = PLX_DMA_START;
}

// *****************************************************************************
/// \brief Function starts DMA for ADC modules.
/// Called at end of every ADC read to arm ADC DMA for next read.
///     @param[in] module ID of ADC board.
// *****************************************************************************
void
plx9056_adc_dma_start( int module )
{
    adcDma[ module ]->DMA_CSR = PLX_DMA_START;
}

// *****************************************************************************
// Following are DAC DMA Funtions ********************************************
// *****************************************************************************

// *****************************************************************************
/// \brief Function sets up DMA for 18 and 20bit DAC modules.
/// Called once on startup.
///     @param[in] module ID of DAC board.
// *****************************************************************************
void
plx9056_dac_1820_dma_setup( int modNum )
{
    // dacDma[ modNum ]->DMA1_MODE = GSAI_DMA_MODE_NO_INTR;
    dacDma[ modNum ]->DMA1_MODE = PLX_DMA_MODE_NO_INTR;
    dacDma[ modNum ]->DMA1_PCI_ADD = (int)dac_dma_handle[ modNum ];
    dacDma[ modNum ]->DMA1_LOC_ADD = GSAO1820_OUTBUF_LOCAL_ADDRESS;
#ifdef OVERSAMPLE_DAC
    dacDma[ modNum ]->DMA1_BTC = GSAO1820_BTC * OVERSAMPLE_TIMES;
#else
    dacDma[ modNum ]->DMA1_BTC = GSAO1820_BTC;
#endif
    dacDma[ modNum ]->DMA1_DESC = PLX_DMA_FROM_PCI;

}

// *****************************************************************************
/// \brief Function sets up DMA for 16bit DAC modules.
/// Called once on startup.
///     @param[in] module ID of DAC board.
// *****************************************************************************
void
plx9056_dac_16_dma_setup( int modNum )
{
    dacDma[ modNum ]->DMA1_MODE = PLX_DMA_MODE_NO_INTR;
    dacDma[ modNum ]->DMA1_PCI_ADD = (int)dac_dma_handle[ modNum ];
    dacDma[ modNum ]->DMA1_LOC_ADD = GSAO16_OUTBUF_LOCAL_ADDRESS;
#ifdef OVERSAMPLE_DAC
    dacDma[ modNum ]->DMA1_BTC = GSAO16_BTC * OVERSAMPLE_TIMES;
#else
    dacDma[ modNum ]->DMA1_BTC = GSAO16_BTC;
#endif
    dacDma[ modNum ]->DMA1_DESC = PLX_DMA_FROM_PCI;
}

// *****************************************************************************
/// \brief Function starts DMA for ADC modules.
/// Called at end of every ADC read to arm ADC DMA for next read.
///     @param[in] module ID of DAC board.
// *****************************************************************************
void
plx9056_dac_dma_start( int modNum )
{
    dacDma[ modNum ]->DMA_CSR = PLX_DMA1_START;

}
