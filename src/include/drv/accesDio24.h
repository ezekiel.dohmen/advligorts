#ifndef LIGO_ACCESDIO24_H
#define LIGO_ACCESDIO24_H

#include "drv/cdsHardware.h"
#include <linux/pci.h>

#define ACC_TID     0x0C51

#ifdef __cplusplus
extern "C" {
#endif

int accesDio24Init( CDS_HARDWARE* pHardware, struct pci_dev* diodev );
unsigned int accesDio24ReadInputRegister( CDS_HARDWARE* pHardware, int modNum );
void accesDio24WriteOutputRegister( CDS_HARDWARE* pHardware, int modNum, int data );

#ifdef __cplusplus
}
#endif

#endif //LIGO_ACCESDIO24_H
