#ifndef LIGO_CONTEC6464_H
#define LIGO_CONTEC6464_H

#include "drv/cdsHardware.h"

#include <linux/pci.h>

#define C_DIO_6464L_PE  0x8682

#ifdef __cplusplus
extern "C" {
#endif

int contec6464Init( CDS_HARDWARE* , struct pci_dev* );
unsigned int contec6464WriteOutputRegister( CDS_HARDWARE* , int, unsigned int );
unsigned int contec6464ReadOutputRegister( CDS_HARDWARE* , int );
unsigned int contec6464ReadInputRegister( CDS_HARDWARE*,  int );

#ifdef __cplusplus
}
#endif


#endif //LIGO_CONTEC6464_H

