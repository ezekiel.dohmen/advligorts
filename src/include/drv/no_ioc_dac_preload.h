#ifndef LIGO_NO_IOC_DAC_PRELOAD_H
#define LIGO_NO_IOC_DAC_PRELOAD_H

#include "portableInline.h"

#ifdef __cplusplus
extern "C" {
#endif

//
// When we have no IO cards (virtual IO) this version
// of iop_dac_preload() is called, it does nothing 
// but it is put in its own header for now because controllerIop.c
// and verify_slots.c both call it.
//
// The original comment was: dummy placeholder function that should never be called when there's no IOC
//
// But it has the possibility of being called or we wouldn't need to define it...

LIGO_INLINE int iop_dac_preload( int card ) 
{
    return 0;
}


#ifdef __cplusplus
}
#endif



#endif // LIGO_NO_IOC_DAC_PRELOAD_H
