/// 	\file ligoPcieTiming64.c

#include "ligoPcieTiming.h"
#include "controller.h" //IOC_CLK_SLOW, MAX_UDELAY
#include "drv/ligo28ao32/ligo28ao32.h"
#include "drv/ligo28ao32/ligo28ao32_private.h"
#include "drv/rts-logger.h"



#include <linux/delay.h> //udelay()


void
lptc_enable_all_slots( CDS_HARDWARE* pCds )
{
    unsigned int   regval;
    int            ii, cur_lptc;
    volatile LPTC_REGISTER* lptcPtr;

    for ( cur_lptc = 0; cur_lptc < pCds->card_count[ LPTC ]; cur_lptc++ )
    {
        lptcPtr = (LPTC_REGISTER*)pCds->lptc[ cur_lptc ];
        for ( ii = 0; ii < LPTC_IOC_SLOTS; ii++ )
        {
            switch ( pCds->ioc_config[ ii ] )
            {
            case GSC_18AI64SSC:
            case GSC_18AI32SSC1M:
#if MODEL_RATE_HZ >= 524288
                lptcPtr->slot_info[ ii ].config =
                    ( LPTC_SCR_ADC_SET | IOC_CLK_FAST );
#else
                lptcPtr->slot_info[ ii ].config =
                    ( LPTC_SCR_ADC_SET | IOC_CLK_SLOW );
#endif
                break;
            case GSC_16AI64SSA:
                lptcPtr->slot_info[ ii ].config =
                    ( LPTC_SCR_ADC_SET | IOC_CLK_SLOW );
                break;
            case GSC_16AO16:
            case GSC_18AO8:
            case GSC_20AO8:
                lptcPtr->slot_info[ ii ].config =
                    ( LPTC_SCR_DAC_SET | IOC_CLK_SLOW );
                break;
            case LIGO_28AO32:
                lptcPtr->slot_info[ ii ].config = 
                    (LPTC_SCR_TIM_SIG | LPTC_SCR_LVDS );
                break;
            default:
                lptcPtr->slot_info[ ii ].config =
                    ( LPTC_SCR_ADC_SET | IOC_CLK_SLOW );
                break;
            }
            if ( ii == 0 )
                lptcPtr->slot_info[ ii ].config |= LPTC_SCR_ADC_DT_ENABLE;
            udelay( MAX_UDELAY );
            regval = lptcPtr->slot_info[ ii ].config;
        }
    }
    udelay( MAX_UDELAY );
    udelay( MAX_UDELAY );
}

int
lptc_start_clock( CDS_HARDWARE* pCds )
{
    int            ii, cur_lptc;
    volatile LPTC_REGISTER* lptcPtr;

    if(pCds->card_count[ LPTC ] < 1) return -1;

    for ( cur_lptc = 0; cur_lptc < pCds->card_count[ LPTC ]; cur_lptc++ )
    {
        lptcPtr = (LPTC_REGISTER*)pCds->lptc[ cur_lptc ];
        lptcPtr->bp_config = LPTC_CMD_START_CLK_ALL;

        // Update lptc status to epics
        for ( ii = 0; ii < LPTC_IOC_SLOTS; ii++ )
            pLocalEpics->epicsOutput.lptMon[ ii ] =
                lptc_get_slot_status( &cdsPciModules, ii );
        for ( ii = 0; ii < LPTC_IOC_SLOTS; ii++ )
            pLocalEpics->epicsOutput.lptMon[ ( ii + LPTC_IOC_SLOTS ) ] =
                lptc_get_slot_config( &cdsPciModules, ii );
        pLocalEpics->epicsOutput.lpt_bp_config =
            lptc_get_bp_config( &cdsPciModules );
        pLocalEpics->epicsOutput.lpt_bp_status =
            lptc_get_bp_status( &cdsPciModules );
        pLocalEpics->epicsOutput.lpt_status =
            lptc_get_lptc_status( &cdsPciModules );
    }

    return lptcPtr->bp_status;
}


void
lptc_status_update( CDS_HARDWARE* pCds )
{
    int ii;

    // Update lptc status to epics
    for ( ii = 0; ii < LPTC_IOC_SLOTS; ii++ )
        pLocalEpics->epicsOutput.lptMon[ ii ] =
            lptc_get_slot_status( pCds, ii );
    for ( ii = 0; ii < LPTC_IOC_SLOTS; ii++ )
        pLocalEpics->epicsOutput.lptMon[ ( ii + LPTC_IOC_SLOTS ) ] =
            lptc_get_slot_config( &cdsPciModules, ii );
    pLocalEpics->epicsOutput.lpt_bp_config =
        lptc_get_bp_config( &cdsPciModules );
    pLocalEpics->epicsOutput.lpt_bp_status =
        lptc_get_bp_status( &cdsPciModules );
    pLocalEpics->epicsOutput.lpt_status =
        lptc_get_lptc_status( &cdsPciModules );
}

int
lptc_stop_clock( CDS_HARDWARE* pCds )
{
    int            cur_lptc;
    volatile LPTC_REGISTER* lptcPtr;

    if(pCds->card_count[ LPTC ] < 1) return -1;

    for ( cur_lptc = 0; cur_lptc < pCds->card_count[ LPTC ]; cur_lptc++ )
    {
        lptcPtr = (LPTC_REGISTER*)pCds->lptc[ cur_lptc ];
        lptcPtr->bp_config = LPTC_CMD_STOP_CLK_ALL;
        udelay( MAX_UDELAY );
        udelay( MAX_UDELAY );
    }
    return lptcPtr->bp_status;
}

void
lptc_dac_duotone( CDS_HARDWARE* pCds, int setting )
{
    volatile LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    if ( setting )
    {
        lptcPtr->slot_info[ 0 ].config |= LPTC_SCR_DAC_DT_ENABLE;
        lptcPtr->slot_info[ 1 ].config |= LPTC_SCR_DAC_DT_ENABLE;
    }
    else
    {
        lptcPtr->slot_info[ 0 ].config &= ~( LPTC_SCR_DAC_DT_ENABLE );
        lptcPtr->slot_info[ 1 ].config &= ~( LPTC_SCR_DAC_DT_ENABLE );
    }
}

void
lptc_slot_clk_set( CDS_HARDWARE* pCds, int slot, int enable )
{
    int cur_lptc;

    for ( cur_lptc = 0; cur_lptc < pCds->card_count[ LPTC ]; cur_lptc++ )
    {
        volatile LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
        if ( enable )
            lptcPtr->slot_info[ slot ].config |= LPTC_SCR_CLK_ENABLE;
        else
            lptcPtr->slot_info[ slot ].config &= ~( LPTC_SCR_CLK_ENABLE );
    }
}

void
lptc_slot_clk_disable_all( CDS_HARDWARE* pCds )
{
    int slot = 0;
    int cur_lptc;

    for ( cur_lptc = 0; cur_lptc < pCds->card_count[ LPTC ]; cur_lptc++ )
    {
        volatile LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ cur_lptc ];
        for ( slot = 0; slot < LPTC_IOC_SLOTS; slot++ ) {
            if ( pCds->ioc_config[ slot ] != LIGO_28AO32 ) {
                lptcPtr->slot_info[ slot ].config &= ~( LPTC_SCR_CLK_ENABLE );
            }
        }
    }
}

void
lptc_slot_clk_enable_all( CDS_HARDWARE* pCds )
{
    int slot = 0;
    int cur_lptc;

    for ( cur_lptc = 0; cur_lptc < pCds->card_count[ LPTC ]; cur_lptc++ )
    {
        volatile LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ cur_lptc ];
        for ( slot = 0; slot < LPTC_IOC_SLOTS; slot++ ) {
            if ( pCds->ioc_config[ slot ] != LIGO_28AO32 ) {
                lptcPtr->slot_info[ slot ].config |= LPTC_SCR_CLK_ENABLE;
            }
        }
    }
}

int lptc_get_duotone_info( CDS_HARDWARE* pCds, double * dt_offset_usec_ptr, int * dt_data_count_ptr )
{
    int timing_board_id = 0;
    int timing_firmware_raw_id = 0;
    int timing_firmware_id = 0;
    int timing_firmware_rev = 0;

    // get firmware version info
    timing_firmware_raw_id = lptc_get_software_id(pCds);
    timing_board_id = timing_firmware_raw_id >> 4;
    timing_firmware_id = timing_firmware_raw_id & 0xf;
    timing_firmware_rev = lptc_get_software_rev(pCds);

    if (timing_board_id == LPTC_BOARD_ID)
    {
        RTSLOG_INFO( "LPTC revision_byte=0x%x subversion_rev=0x%x\n",
                timing_firmware_id,
                timing_firmware_rev );
        if ( timing_firmware_id >= 2  )
        {
            // offset is corrected in LPTC firmware 0x2fa
            RTSLOG_INFO("Setting expected duotone offset to 0 usec\n");
            *dt_offset_usec_ptr = 0.0;

            // set dt data length to 11*15.26 usec to get a data span
            // centered on 0 usec.
            *dt_data_count_ptr = 11;
        }
        else if ( timing_firmware_id == 1 ) 
        {
            //Nothing special needed
        }
        else
        {
            RTSLOG_WARN( "LPTC firmware revision not recognized\n" );
            return -1;
        }
    }
    else
    {
        RTSLOG_ERROR("LPTC board id 0x%x not recognized", timing_board_id);
        return -1;
    }
    return 0;
}

/**
 * \brief Waits for all DACs to synchronize
 *
 * Waits for all DACs that get timing information over LVDS
 *        (instead of just a raw clk) to synchronize, or times
 *        out after waiting 2 seconds.
 *
 * @param pCds Passes the HW info, so we can loop over all DACs
 *
 * @return It returns -1 if any DAC did not synchronize, 0 if all DAC are synchronized
 */
int lptc_wait_for_dac_sync( CDS_HARDWARE* pCds)
{
    int cur_dac = 0, count=0;
    while(pCds->dacDrivers[cur_dac] != NULL)
    {
        if ( pCds->dac_info[ cur_dac ].card_type == LIGO_28AO32 )
        {
            while ( ligo28ao32_is_locked(pCds->dacDrivers[cur_dac]) == 0 && count++ < 100 )
            {
                udelay(MAX_UDELAY);
            }
            if( ligo28ao32_is_locked(pCds->dacDrivers[cur_dac]) ==  0) {
                RTSLOG_ERROR("DAC slot %d, instance %d, type: %d did not sync in the allotted time.\n",
                pCds->dacSlot[cur_dac], pCds->dacInstance[cur_dac], pCds->dac_info[ cur_dac ].card_type);
                return -1;
            }
        }
        ++cur_dac;
    }
    return 0;
}


int lptc_get_temp_c( CDS_HARDWARE* pCds )
{
    uint64_t temp = -1;
    if ( pCds->card_count[ LPTC ] > 0 ) {
        LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE*)pCds->lptc[ 0 ];
        temp = ioread32(&lptcPtr->obf_registers.tips1);
        CONVERT_TEMP_REG_TO_C(temp);
    }
    else {
        RTSLOG_WARN("lptc_get_temp_c() called in system with no LIGO timing card\n");
    }
    return temp;
}