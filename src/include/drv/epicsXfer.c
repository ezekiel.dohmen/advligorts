#include "tRamp.h"

//#define LIGO_INLINE
#include "epicsXfer.h"

/// Perform gain ramping
int
gainRamp( float gainReq, int rampTime, int id, float* gain, int gainRate )
{

    static int   dir[ 40 ];
    static float inc[ 40 ];
    static float gainFinal[ 40 ];
    static float gainOut[ 40 ];

    if ( rampTime <= 0 )
        return 0;

    if ( gainFinal[ id ] != gainReq )
    {
        inc[ id ] = rampTime * gainRate;
        inc[ id ] = ( gainReq - gainOut[ id ] ) / inc[ id ];
        if ( inc[ id ] <= 0.0 )
            dir[ id ] = 0;
        else
            dir[ id ] = 1;
        gainFinal[ id ] = gainReq;
    }
    if ( gainFinal[ id ] == gainOut[ id ] )
    {
        *gain = gainOut[ id ];
        return ( 0 );
    }
    gainOut[ id ] += inc[ id ];
    if ( ( dir[ id ] == 1 ) && ( gainOut[ id ] >= gainFinal[ id ] ) )
        gainOut[ id ] = gainFinal[ id ];
    if ( ( dir[ id ] == 0 ) && ( gainOut[ id ] <= gainFinal[ id ] ) )
        gainOut[ id ] = gainFinal[ id ];
    *gain = gainOut[ id ];
    return ( 1 );
}


