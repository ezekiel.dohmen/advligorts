#ifndef LIGO_28AO32_PRIVATE_H
#define LIGO_28AO32_PRIVATE_H

#include "pg195.h"

#include <linux/types.h>
#include <linux/dma-mapping.h>


#define LIGO_28AO32_NUM_ADCS 32
#define LIGO_28AO32_NUM_DACS 32
#define LIGO_28AO32_BYTES_PER_SAMPLE 4


//Status and Interrupt Control Bit Indices
#define STAT_INT_WATCHDOG_MON 16
#define STAT_INT_ADD_LEAP_SEC_PENDING 20
#define STAT_INT_SUB_LEAP_SEC_PENDING 21
#define STAT_INT_LEAP_SEC_DECODED 22
#define STAT_INT_UTC_TIME_MODEL_ENABLED 23 //Always 0
#define STAT_INT_VCXO_CONTROL_VOLTAGE_OUT_OF_RANGE 24
#define STAT_INT_GPS_LOCKED 25 //Always 0
#define STAT_INT_OCXO_LOCKED 26 //Always 0
#define STAT_INT_LOSS_OF_SIG_ON_UPLINK 27 //Always 0
#define STAT_INT_UPLINK_IS_UP_AND_WORKING 28
#define STAT_INT_SUPPORTS_FANOUT_PORTS 29 //Always 0
#define STAT_INT_IS_ROOT_NODE 30 //Always 0
#define STAT_INT_TIMING_LOCKED 31

//Status and Interrupt Control Bit Masks
#define STAT_INT_CTRL_LEAP_SECS_MASK 0xFF

//
//Sampling Status Bit Indices
#define SAMP_STAT_ADC_DMA_RUNNING             0
#define SAMP_STAT_ADC_CONVERT_RUNNING         1
#define SAMP_STAT_ADC_VALID_SAMP_CONFIG       2
#define SAMP_STAT_TIMING_OK                   7
#define SAMP_STAT_ADC_DMA_ERR_CHAN_0          8
#define SAMP_STAT_ADC_DMA_ERR_CHAN_1          9
#define SAMP_STAT_ADC_DMA_MISSING_DATA_0      10
#define SAMP_STAT_ADC_DMA_MISSING_DATA_1      11
//Bits 15...12: Unused
#define SAMP_STAT_DAC_DMA_RUNNING             16
#define SAMP_STAT_DAC_CONVERT_RUNNING         17
#define SAMP_STAT_DAC_VALID_SAMP_CONFIG       18
#define SAMP_STAT_WATCHDOG_MON                23
#define SAMP_STAT_DAC_DMA_ERR_CHAN_0          24
#define SAMP_STAT_DAC_DMA_ERR_CHAN_1          25
#define SAMP_STAT_DAC_DMA_MISSING_DATA_0      26
#define SAMP_STAT_DAC_DMA_MISSING_DATA_1      27
#define SAMP_STAT_DAC_TIMESTAMP_ERR_CHAN_0    28
#define SAMP_STAT_DAC_TIMESTAMP_ERR_CHAN_1    29






//
//Sampling Configuration Bits 
#define SAMP_CONF_ENABLE_ADC_DMA                0
#define SAMP_CONF_DISABLE_ADC_CONVERSION        1
#define SAMP_CONF_DISABLE_ADC_TIMESTAMP         2
//Bits 7..3 Unused
#define SAMP_CONF_USE_INT_TO_SIGNAL_ADC_DONE    8
//Bits 15 ... 8 Log2 of number of buffers per ADC DMA channel
#define SAMP_CONF_LOG2_NUM_BUF_PER_ADC_DMA_CHAN (0xFF00)
#define SAMP_CONF_ENABLE_DAC_DMA                16
#define SAMP_CONF_DISABLE_DAC_CONVERSION        17
#define SAMP_CONF_DISABLE_DAC_TIMESTAMP         18
#define SAMP_CONF_IGNORE_DAC_TIMESTAMP_ERRORS   19
//Bits 22...20 Unused
#define SAMP_CONF_USE_INT_TO_SIGNAL_DAC_DONE    23
//Bits 31...24 Log2 of number of buffers per DAC DMA channel
#define SAMP_CONF_LOG2_NUM_BUF_PER_DAC_DMA_CHAN (0xFF000000)

//
//On Board Features Registers
#define ON_BOARD_FEATURES_TEMP_MASK 0xFFFF
//Takes the whole temp_and_internal_pow_supply_1 register
//and returns the temperature value in (C)
#define CONVERT_TEMP_REG_TO_C(reg)\
    reg = reg & ON_BOARD_FEATURES_TEMP_MASK;\
    reg *= 503975;\
    reg /= 65536;\
    reg -= 273150;\
    reg /= 1000;\

typedef struct ligo28ao32_ctrl_regs {

    //Timing Registers
    uint32_t time_frac; //In units of 2^32 hz
    uint32_t time_sec;

    //Status and Interrupt Control
    uint32_t status_bits;
    uint32_t firmware_version;

    //Sampling Configuration
    uint32_t samp_status;
    uint32_t samp_config;
    uint32_t samp_adc_error_ctr;
    uint32_t samp_dac_error_ctr;

    //Sampling Setup (in units of 2^-32 sec) - 1
    uint32_t adc_dma_period;
    uint32_t adc_dma_delay;
    uint32_t adc_sample_delay;
    uint32_t adc_sample_period;
    uint32_t dac_dma_period;
    uint32_t dac_dma_delay;
    uint32_t dac_sample_delay;
    uint32_t dac_sample_period;

    //Converter Configuration
    uint32_t max_adc_channels;
    uint32_t max_dac_channels;
    uint32_t num_adc_channels;
    uint32_t num_dac_channels;
    uint32_t max_min_adc_rates;
    uint32_t max_min_dac_rates;
    uint32_t adc_params;
    uint32_t dac_params;
    uint32_t adc_dma_buffer_sz;
    uint32_t dac_dma_buffer_sz;
    uint32_t loopback_and_interface_info;
    uint32_t buffs_per_dma_and_clk;
    uint32_t dma_buffer_widths;
    uint32_t pad0;
    uint32_t axi_clk_rate_hz;
    uint32_t dac_data_delay; //TODO what is clock rate?


    //Filter Setup
    uint32_t filter_config;
    uint32_t pad1[4];
    uint32_t filter_control;

    //Padding 40 bytes
    uint32_t pad2[10];


    struct dma_chan_config_t {

        struct dma_chan_config_entry {
            uint32_t addr_lsb; //Source or dest addr
            uint32_t addr_msb;
            uint32_t length; //Must be multiple of 64 bytes
            uint32_t buffer_offset; //Must be multiple of 64 bytes, moves the DMA location each transfer
        } even_in, odd_in, even_out, odd_out;

    } dma_chan_config;


    //Padding 46 bytes
    uint32_t pad3[11];
    uint8_t  pad4[2];

    //Advanced Timing Features
    uint32_t advanced_timing_features;
    uint32_t node_addr;
    uint32_t advanced_timing_status;
    uint32_t pad5;
    uint32_t board_id_and_rev; 
    uint32_t sw_id_and_rev;

    //Padding 56 bytes
    uint32_t pad6[14];

    //On Board Features
    uint32_t board_config;
    uint32_t xadc_config;
    uint32_t board_power_supply_status;
    uint32_t xadc_status;
    uint32_t temp_and_internal_pow_supply_1;
    uint32_t internal_pow_supply_2;
    uint32_t external_pow_supply_1;
    uint32_t external_pow_supply_2;
    uint32_t external_pow_supply_3;
    uint32_t external_pow_supply_4;
    uint32_t external_pow_supply_5;
    uint32_t external_pow_supply_6;
    uint32_t external_pow_supply_7;
    uint32_t external_pow_supply_8;
    uint32_t pad7[17];
    uint32_t watchdog;


} ligo28ao32_ctrl_regs;




//
//Status Bits 
#define DMA_STAT_ADCS_RUNNING                 0
#define DMA_STAT_ADC_DMA_RUNNING              1
#define DMA_STAT_ADC_DATA_VAILD               2
#define DMA_STAT_TIMING_GOOD                  7 //TODO not sure this works
#define DMA_STAT_ADC_DMA_CHAN_0_NOT_READY     8
#define DMA_STAT_ADC_DMA_CHAN_1_NOT_READY     9
#define DMA_STAT_ADC_DMA_CHAN_0_NO_DATA       10
#define DMA_STAT_ADC_DMA_CHAN_1_NO_DATA       11
#define DMA_STAT_DACS_RUNNING                 16
#define DMA_STAT_DAC_DMA_RUNNING              17
#define DMA_STAT_DAC_DATA_VALID               18
#define DMA_STAT_DAC_WATCHDOG                 23
#define DMA_STAT_DAC_DMA_CHAN_0_NOT_READY     24
#define DMA_STAT_DAC_DMA_CHAN_1_NOT_READY     25
#define DMA_STAT_DAC_DMA_CHAN_0_NO_DATA       26
#define DMA_STAT_DAC_DMA_CHAN_1_NO_DATA       27
#define DMA_STAT_DAC_DMA_CHAN_0_TIME_ERR      28
#define DMA_STAT_DAC_DMA_CHAN_1_TIME_ERR      29

typedef struct ligo_adc_buffer_section_t {
    uint32_t samples[LIGO_28AO32_NUM_ADCS];
    uint32_t cache_buffer[12];
    uint32_t time_232_clk;
    uint32_t time_sec;
    uint32_t status;
    uint32_t overflows;
} ligo_adc_buffer_section_t;

typedef struct ligo_dac_buffer_section_t {
    uint32_t samples[LIGO_28AO32_NUM_DACS];
    uint32_t time_232_clk;
    uint32_t time_sec;
    uint32_t status;
    uint32_t overflows;
} ligo_dac_buffer_section_t;


typedef struct ligo28ao32_dev_t {
    struct pci_dev * pci_dev;
    dma_addr_t adc_dma_bus_addr;
    dma_addr_t dac_dma_bus_addr;
    void* adc_dma_buffer;
    void* dac_dma_buffer;
    ligo28ao32_ctrl_regs * ctrl_mm;
    pg195_dma_ctrl_regs_t * xilinx_dma_mm;

    uint32_t control_regs_len;

    //ADC Context/State
    uint64_t last_locked_ts_ns;
    ligo_adc_buffer_section_t locked_adc_buf;

    //DAC Context/State
    uint32_t cur_dac_tick;
    uint32_t cur_dac_buffer;
    uint32_t dac_num_dma_buffers;
    uint64_t last_dac_tx_ts_ns;
    uint32_t dac_sample_delay;
    uint32_t dac_dma_delay;
    uint32_t dac_write_next_ts;
    uint32_t dac_first_dma;

} ligo28ao32_dev_t;

//Private Util Functions
void ligo28ao32_dma_config_adc(ligo28ao32_dev_t * dev, uint32_t dma_len, uint32_t step);
void ligo28ao32_dma_config_dac(ligo28ao32_dev_t * dev, uint32_t dma_len, uint32_t step);

int ligo28ao32_get_adc_dma_length(ligo28ao32_dev_t * dev);

#endif //LIGO_28AO32_PRIVATE_H
