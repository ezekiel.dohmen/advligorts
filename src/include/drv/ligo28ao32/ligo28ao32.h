#ifndef LIGO_28AO32_H
#define LIGO_28AO32_H

#include <linux/types.h>
#include <linux/dma-mapping.h>

//PCI Subsystem Information
#define L28AO_VENDOR_ID 0x10EE
#define L28AO_DEVICE_ID 0xD8C7 

//DAC Constants
#define L28AO_SAMPLE_LIMIT 134217000
//Don't mask out any of the bit depth,
//while we can only use 28 bits, the DMAed 
//data should be sign extended out to 32 bit.
#define L28AO_SAMPLE_MASK 0xFFFFFFFF 
#define L28AO_NUM_CHANS 32

typedef enum ligo28ao32_error_t
{
    LIGO32AO32_DAC_ERROR = -2,
    LIGO32AO32_TIMEOUT = -1,
    LIGO32AO32_OK = 0,

} ligo28ao32_error_t;


typedef struct ligo28ao32_dev_t ligo28ao32_dev_t;

ligo28ao32_dev_t * ligo28ao32_configureDevice(unsigned instance);
void ligo28ao32_freeDevice(ligo28ao32_dev_t * dev);

uint64_t ligo28ao32_get_time_ns(ligo28ao32_dev_t * dev);
double ligo28ao32_get_time_sec(ligo28ao32_dev_t * dev);
int ligo28ao32_is_locked(ligo28ao32_dev_t * dev);
int ligo28ao32_get_slot_index(ligo28ao32_dev_t * dev);



//ADC Functions
int ligo28ao32_get_num_adc_chans(ligo28ao32_dev_t * dev);
int ligo28ao32_is_adc_config_valid(ligo28ao32_dev_t * dev);
int ligo28ao32_is_adc_dma_running(ligo28ao32_dev_t * dev);
int ligo28ao32_is_adc_converter_running(ligo28ao32_dev_t * dev);
ligo28ao32_error_t ligo28ao32_lock_next_adc_buffer(ligo28ao32_dev_t * dev, uint32_t max_wait_us);
uint64_t ligo28ao32_get_locked_ts_ns(ligo28ao32_dev_t * dev);
uint64_t ligo28ao32_get_locked_ts_raw(ligo28ao32_dev_t * dev);
uint32_t ligo28ao32_get_locked_status(ligo28ao32_dev_t * dev);
uint32_t ligo28ao32_get_locked_overflow(ligo28ao32_dev_t * dev);

//DAC Functions
int ligo28ao32_get_num_dac_chans(ligo28ao32_dev_t * dev);
int ligo28ao32_is_dac_config_valid(ligo28ao32_dev_t * dev);
ligo28ao32_error_t ligo28ao32_queue_next_dac_dma(ligo28ao32_dev_t * dev, uint32_t num_samples, uint32_t samples []);
void * ligo28ao32_get_next_dac_dma_queue(ligo28ao32_dev_t * dev, uint32_t gps_time_s, uint32_t cur_cycle_64kclk);



//
//DMA Functions
void ligo28ao32_dma_config(ligo28ao32_dev_t * dev, unsigned sample_rate_hz, unsigned dma_rate_hz);
void ligo28ao32_dma_start_adcs(ligo28ao32_dev_t * dev);
void ligo28ao32_dma_stop_adcs(ligo28ao32_dev_t * dev);
void ligo28ao32_dma_start_dacs(ligo28ao32_dev_t * dev);
void ligo28ao32_dma_stop_dacs(ligo28ao32_dev_t * dev);

// DMA Error Utils
int ligo28ao32_dma_query_adc_error_ctr(ligo28ao32_dev_t * dev);
void ligo28ao32_dma_clear_adc_error_ctr(ligo28ao32_dev_t * dev);
int ligo28ao32_dma_query_dac_error_ctr(ligo28ao32_dev_t * dev);
void ligo28ao32_dma_clear_dac_error_ctr(ligo28ao32_dev_t * dev);
uint32_t ligo28ao32_dma_read_common_errors(ligo28ao32_dev_t * dev);
int ligo28ao32_dma_query_num_buff_per_adc_chan(ligo28ao32_dev_t * dev);


//Util Functions

void ligo28ao32_reset_watchdog(ligo28ao32_dev_t * dev);
uint32_t ligo28ao32_get_watchdog(ligo28ao32_dev_t * dev);

int ligo28ao32_samp_stat_is_timing_ok(uint32_t samp_status_reg);
int ligo28ao32_get_temp_c(ligo28ao32_dev_t * dev);

int ligo28ao32_get_fw_version(ligo28ao32_dev_t * dev);

void ligo28ao32_print_status_registers(ligo28ao32_dev_t * dev);
void ligo28ao32_print_dma_and_converter_status(ligo28ao32_dev_t * dev);
void ligo28ao32_print_sampling_config(ligo28ao32_dev_t * dev);
void ligo28ao32_print_sampling_setup(ligo28ao32_dev_t * dev);
void ligo28ao32_print_converter_config(ligo28ao32_dev_t * dev);
void ligo28ao32_print_adc_dma_settings(ligo28ao32_dev_t * dev);

void ligo28ao32_dump_ligo_regs(ligo28ao32_dev_t * dev, uint32_t offset, uint32_t num_regs);
bool ligo28ao32_read_reg(ligo28ao32_dev_t * dev, uint32_t offset, uint32_t * value);
bool ligo28ao32_write_reg(ligo28ao32_dev_t * dev, uint32_t offset, uint32_t value);

void ligo28ao32_print_dma_buffer_status(ligo28ao32_dev_t * dev, uint32_t status);

#endif //LIGO_28AO32_H