#ifndef LIGO_DAQLIB_H
#define LIGO_DAQLIB_H

#include "daqmap.h" //DAQ_*

#include "fm10Gen_types.h"//FILT_MOD

//
// Global Variables 
//
extern struct cdsDaqNetGdsTpNum* tpPtr; //Used by fe/controller*.c files
extern unsigned int    curDaqBlockSize;

#ifdef __cplusplus
extern "C" {
#endif

int daqConfig( volatile DAQ_INFO_BLOCK*, volatile DAQ_INFO_BLOCK*, volatile char* );
int loadLocalTable( DAQ_XFER_INFO*, DAQ_LKUP_TABLE[], int, DAQ_INFO_BLOCK*, DAQ_RANGE* );
int daqWrite( int,
              int,
              struct DAQ_RANGE,
              int,
              double*[],
              struct FILT_MOD*,
              int,
              int[],
              double[],
              volatile char* );

#ifdef __cplusplus
}
#endif


#endif //LIGO_DAQLIB_H
