#ifndef LIGO_NO_IOC_TIMING_H
#define LIGO_NO_IOC_TIMING_H

/**
 * These no ioc (IO Chassis) version of these functions are used
 * when we are running the model without an IO chassis. This is 
 * done when the module parameters have virtualIOP=1 or 
 * dolphin_time_rcvr=1. dolphin_time_rcvr is commonly used
 * on large test stands where we have many Front Ends and 
 * few IO chassis.
 *
 * dolphin_time_rcvr=1 Will cause the model to wait for a timing
 * signal over the dolphin network for it to start its next cycle.
 * This is the more common use of these VIO functions.
 *
 * virtualIOP=1 Is used when there is no Dolphin network for timing.
 * CYMACs with NO IO chassis can use this option, but it is rarely 
 * used because CYMACs usually have some hardware (ADCs/DACs).
 *
 */


#include "portableInline.h"
#include "../drv/gpstime/gpstime_kernel.h" //LIGO_TIMESPEC
#include "cds_types.h" //acdInfo_t
#include "controller.h" //cdsPciModules
#include "drv/symmetricomGps.h" //SYMCOM_RCVR
#include "drv/spectracomGPS.h" //TSYNC_RCVR
#include "drv/gsc16ai64.h" //GSAI_DATA_CODE_OFFSET
#include "drv/gsc18ai64.h" //GSA7_DATA_CODE_OFFSET, GSA7_DATA_MASK
#include "drv/gsc16ao16.h" //GSAO_16BIT_CHAN_COUNT
#include "drv/gsc18ao8.h" //GSAO_18BIT_CHAN_COUNT
#include "drv/gsc20ao8.h" //GSAO_20BIT_CHAN_COUNT
#include "../fe/controllerIop.h" //dacTimingError, ioMemCntrDac, dacEnable, pBits
#include "commData3.h" //TIMING_SIGNAL



#ifdef __cplusplus
extern "C" {
#endif

//
// Global Data
//
static int first_adc_read = 1;


LIGO_INLINE int
waitPcieTimingSignal( volatile TIMING_SIGNAL* timePtr, int cycle )
{
    int loop = 0;

    do
    {
        udelay( 1 );
        loop++;
    } while ( timePtr->cycle != cycle && loop < 28 );
    if ( loop >= 25 )
        return ( 1 );
    else
        return ( 0 );
}

static int waitInternalTimingSignal(unsigned long long cpuClock[] )
{
    const double target_cycle_time_ns = 1.0/(MODEL_RATE_HZ/UNDERSAMPLE)
                                        * 1000000000;

    while( timer_tock_ns( &cpuClock[ CPU_TIME_CYCLE_START ] ) < target_cycle_time_ns ) {
        ndelay(100);
    }

    return ( 0 );
}

LIGO_INLINE unsigned int
sync2master( volatile TIMING_SIGNAL* timePtr )
{
    int loop = 0;
    int cycle = 65535;

    do
    {
        udelay( 5 );
        loop++;
    } while ( timePtr->cycle != cycle && loop < 1000000 );
    if ( loop >= 1000000 )
    {
        return ( -1 );
    }
    else
    {
        return ( timePtr->gps_time );
    }
}

LIGO_INLINE unsigned int
sync2cpuclock( void )
{
    LIGO_TIMESPEC t;
    ligo_gpstime_get_ts(&t);
    return t.tv_sec;
}

LIGO_INLINE int
iop_adc_init( adcInfo_t* adcinfo )
{
    int           ii, jj;

    /// \> If IOP,  Initialize the ADC modules
    for ( jj = 0; jj < cdsPciModules.adcCount; jj++ )
    {
        pLocalEpics->epicsOutput.statAdc[ jj ] = 1;
        // Reset Diag Info
        adcinfo->adcRdTimeErr[ jj ] = 0;
        adcinfo->adcChanErr[ jj ] = 0;
        adcinfo->adcOF[ jj ] = 0;
        for ( ii = 0; ii < MAX_ADC_CHN_PER_MOD; ii++ )
            adcinfo->overflowAdc[ jj ][ ii ] = 0;
    }
    adcinfo->adcHoldTime = 0;
    adcinfo->adcHoldTimeMax = 0;
    adcinfo->adcHoldTimeEverMax = 0;
    adcinfo->adcHoldTimeEverMaxWhen = 0;
    adcinfo->adcHoldTimeMin = 0xffff;
    adcinfo->adcHoldTimeAvg = 0;
    adcinfo->adcWait = 0;
    adcinfo->chanHop = 0;

    return 0;
}

LIGO_INLINE void iop_dac_cleanup( void ) 
{
}


static int
iop_adc_read( adcInfo_t* adcinfo, unsigned long long cpuClock[] )
{
    int           kk = 0, ii =0;
    volatile int* packedData;
    int           limit;
    int           mask;
    unsigned int  offset;
    int           num_outs;
    int           ioMemCntr = 0;
    int           adcStat = 0;
    int           card;
    int           chan;
    int           missedCycle;
    int           iocycle = 0;
    int           max_loops = UNDERSAMPLE;  //number of samples typically processed from an A2D per cycle.
    int           loops = max_loops;     //can be less than max_loops on the first cycle when time_shift parameter is used.
    double        val2dec = 0;
    static double iopdHistory[ ( MAX_ADC_MODULES * MAX_ADC_CHN_PER_MOD ) ]
                             [ MAX_HISTRY ];

    // Wait for signal we should start the next cycle
#ifdef USE_DOLPHIN_TIMING
    missedCycle = waitPcieTimingSignal( pcieTimer, cycleNum );
    if( !missedCycle ) timeSec = pcieTimer->gps_time;
#else
    missedCycle = waitInternalTimingSignal( cpuClock );
#endif

    for ( card = 0; card < cdsPciModules.adcCount; card++ )
    {
        /// - ---- ADC DATA RDY is detected when last channel in memory no
        /// longer contains the dummy variable written during initialization and
        /// reset after the read.
        packedData = cdsPciModules.pci_adc[ card ];

        timer_start( &cpuClock[ CPU_TIME_RDY_ADC ] );
        timer_start( &cpuClock[ CPU_TIME_ADC_WAIT ] );
        adcinfo->adcWait = timer_duration_ns( &cpuClock[ CPU_TIME_RDY_ADC ], 
                                              &cpuClock[ CPU_TIME_ADC_WAIT ]) / 1000;

        /// - ---- Added ADC timing diagnostics to verify timing consistent and
        /// all rdy together.
        if ( card == 0 )
        {
            adcinfo->adcRdTime[ card ] = timer_duration_ns( &cpuClock[ CPU_TIME_CYCLE_START ], 
                                                            &cpuClock[ CPU_TIME_ADC_WAIT ]) / 1000;
            if ( adcinfo->adcRdTime[ card ] > 1000 )
                adcStat = ADC_BUS_DELAY;
            if ( adcinfo->adcRdTime[ card ] < 13 )
                adcStat = ADC_SHORT_CYCLE;
        }
        else
        {
            adcinfo->adcRdTime[ card ] = adcinfo->adcWait;
        }

        if ( adcinfo->adcRdTime[ card ] > adcinfo->adcRdTimeMax[ card ] )
            adcinfo->adcRdTimeMax[ card ] = adcinfo->adcRdTime[ card ];

        // if((card==0) && (adcinfo->adcRdTimeMax[card] > MAX_ADC_WAIT_CARD_0))
        if ( ( card == 0 ) && ( adcinfo->adcRdTime[ card ] > 20 ) )
            adcinfo->adcRdTimeErr[ card ]++;

        if ( ( card != 0 ) &&
             ( adcinfo->adcRdTimeMax[ card ] > MAX_ADC_WAIT_CARD_S ) )
            adcinfo->adcRdTimeErr[ card ]++;

        /// - --------- If data not ready in time, abort.
        /// Either the clock is missing or code is running too slow and ADC FIFO
        /// is overflowing.
        if ( adcinfo->adcWait >= MAX_ADC_WAIT )
        {
            pLocalEpics->epicsOutput.stateWord = FE_ERROR_ADC;
            pLocalEpics->epicsOutput.diagWord |= ADC_TIMEOUT_ERR;
            pLocalEpics->epicsOutput.fe_status = ADC_TO_ERROR;
            stop_working_threads = 1;
            vmeDone = 1;
            continue;
        }

        if ( card == 0 )
        {
            // Capture cpu clock for cpu meter diagnostics
            timer_start( &cpuClock[ CPU_TIME_CYCLE_START ] );
            /// \> If first cycle of a new second, capture IRIG-B time. Standard
            /// for aLIGO is TSYNC_RCVR.
            if ( cycleNum == 0 )
            {
                // if SymCom type, just do write to lock current time and read
                // later This save a couple three microseconds here
                if ( cdsPciModules.gpsType == SYMCOM_RCVR )
                    lockGpsTime( &cdsPciModules );
                if ( cdsPciModules.gpsType == TSYNC_RCVR )
                {
                    /// - ---- Reading second info will lock the time register,
                    /// allowing nanoseconds to be read later (on next cycle).
                    /// Two step process used to save CPU time here, as each
                    /// read can take 2usec or more.
                    timeSec = getGpsSecTsync( &cdsPciModules );
                }
            }
        }

        /// \> Read adc data
        packedData = cdsPciModules.pci_adc[ card ];

        // Various ADC models have different number of channels/data bits
        if ( cdsPciModules.adcType[ card ] == GSC_16AI64SSA )
        {
            limit = OVERFLOW_LIMIT_16BIT;
            offset = GSAI_DATA_CODE_OFFSET;
            mask = GSAI_DATA_MASK;
        }
        else
        {
            //Both the GSC_18AI64SSC and GSC_18AI32SSC1M have the same bit resolution
            limit = OVERFLOW_LIMIT_18BIT;
            offset = GSA7_DATA_CODE_OFFSET;
            mask = GSA7_DATA_MASK;
        }
        num_outs = cdsPciModules.adcChannels[ card ];

        // loops and max_loops are usually equal to UNDERSAMPLE, but on the first cycle
        // they loops may be less.
        max_loops = UNDERSAMPLE;
        if(first_adc_read)
        {
            loops = UNDERSAMPLE - cdsPciModules.adcTimeShift[card];
        }
        else
        {
            loops = UNDERSAMPLE;
        }

        // GSC_16AI64SSA can't support rate > 128KS/sec
        // Even if IOP rate is faster, read these ADCs at 65kHz.
        if ( cdsPciModules.adcType[ card ] == GSC_16AI64SSA && UNDERSAMPLE > 4 )
        {
            max_loops = 1;
            loops = 1;
        }

        /// - ---- Determine next ipc memory location to load ADC data
        ioMemCntr = ( cycleNum % IO_MEMORY_SLOTS );
        iocycle = ( cycleNum / ADC_MEMCPY_RATE );

        /// - ----  Read adc data from PCI mapped memory into local variables
        for ( kk = 0; kk < loops; kk++ )
        {
            for ( chan = 0; chan < num_outs; chan++ )
            {
                // adcData is the integer representation of the ADC data
                adcinfo->adcData[ card ][ chan ] = *packedData;

                // dWord is the double representation of the ADC data
                // This is the value used by the rest of the code calculations
                // within this IOP.
                dWord[ card ][ chan ][ kk ] = adcinfo->adcData[ card ][ chan ];

                // If running with fast ADC at 512K and gsc16ai64 at 64K, then:
                // fill in the missing data by doing data copy
                if ( cdsPciModules.adcType[ card ] == GSC_16AI64SSA &&
                     UNDERSAMPLE > 4 )
                {
                    for ( ii = 1; ii < UNDERSAMPLE; ii++ )
                        dWord[ card ][ chan ][ ii ] =
                            adcinfo->adcData[ card ][ chan ];
                }
                /// - ----  Load ADC value into shared memory for control models
                if ( max_loops == 8 )
                {
                    //Filter/decimate fast data
                    val2dec = adcinfo->adcData[ card ][ chan ];
                    ioMemData->inputData[ card ][ ioMemCntr ].data[ chan ] =
                        (int)iir_filter_biquad(
                            val2dec,
                            feCoeff8x,
                            3,
                            &iopdHistory[ chan + card * MAX_ADC_CHN_PER_MOD ]
                                        [ 0 ] );
                }
                else
                {
                    //Just copy non-fast data
                    ioMemData->inputData[ card ][ ioMemCntr ].data[ chan ] =
                        adcinfo->adcData[ card ][ chan ];
                }

                /// - ---- Check for ADC overflows
                if ( ( adcinfo->adcData[ card ][ chan ] > limit ) ||
                     ( adcinfo->adcData[ card ][ chan ] < -limit ) )
                {
                    adcinfo->overflowAdc[ card ][ chan ]++;
                    pLocalEpics->epicsOutput.overflowAdcAcc[ card ][ chan ]++;
                    overflowAcc++;
                    adcinfo->adcOF[ card ] = 1;
                    odcStateWord |= ODC_ADC_OVF;
                }
                packedData++;
            }


            // For normal IOP ADC undersampling ie not a mixed fast 512K adc and
            // normal 64K adc
            if ( UNDERSAMPLE < 5 )
            {
                /// - ---- Write GPS time and cycle count as indicator to
                /// control app that adc data is ready
                ioMemData->gpsSecond = timeSec;
                ioMemData->inputData[ card ][ ioMemCntr ].timeSec = timeSec;
                ioMemData->inputData[ card ][ ioMemCntr ].cycle = iocycle;
                ioMemCntr++;
                iocycle++;
                iocycle %= IOP_IO_RATE;
            }

        } //For every UNDERSAMPLE

        // For ADC undersampling when running with a fast ADC at 512K
        // to limit rate to user apps at 64K
        if ( UNDERSAMPLE > 4 )
        {
            /// - ---- Write GPS time and cycle count as indicator to control
            /// app that adc data is ready
            ioMemData->gpsSecond = timeSec;
            ioMemData->inputData[ card ][ ioMemCntr ].timeSec = timeSec;
            ioMemData->inputData[ card ][ ioMemCntr ].cycle = iocycle;
        }


    } //For each card

    first_adc_read = 0;

    return adcStat;
}

LIGO_INLINE int
iop_dac_init( int errorPend[] )
{
    int ii, jj;

    /// \> Zero out DAC outputs
    for ( ii = 0; ii < MAX_DAC_MODULES; ii++ )
    {
        errorPend[ ii ] = 0;
        for ( jj = 0; jj < 16; jj++ )
        {
            dacOut[ ii ][ jj ] = 0.0;
            dacOutUsed[ ii ][ jj ] = 0;
            dacOutBufSize[ ii ] = 0;
            // Zero out DAC channel map in the shared memory
            // to be used to check on control apps' channel allocation
            ioMemData->dacOutUsed[ ii ][ jj ] = 0;
        }
    }

    for ( jj = 0; jj < cdsPciModules.dacCount; jj++ )
    {
        pLocalEpics->epicsOutput.statDac[ jj ] = DAC_FOUND_BIT;
    }

    return 0;
}

LIGO_INLINE int
iop_dac_write( uint64_t cpuClock[] )
{
    volatile unsigned int* pDacData;
    int           chan, card, mm;
    int           status = 0;
    int           dac_out = 0;

    /// WRITE DAC OUTPUTS ***************************************** \n

    /// Writing of DAC outputs is dependent on code compile option: \n
    /// - -- IOP (IOP_MODEL) reads DAC output values from memory shared with
    /// user apps and writes to DAC hardware. \n
    /// - -- USER APP sends output values to memory shared with IOP.
    /// \n

    /// START OF IOP DAC WRITE ***************************************** \n
    /// \> If DAC FIFO error, always output zero to DAC modules. \n
    /// - -- Code will require restart to clear.
    // COMMENT OUT NEX LINE FOR TEST STAND w/bad DAC cards.
    if ( dacTimingError )
        iopDacEnable = 0;
    // Write out data to DAC modules
    /// \> Loop thru all DAC modules
    for ( card = 0; card < cdsPciModules.dacCount; card++ )
    {
        /// - -- locate the proper DAC memory block
        mm = cdsPciModules.dacConfig[ card ];

        /// - -- Point to DAC memory buffer
        pDacData = (volatile unsigned int*)( cdsPciModules.pci_dac[ card ] );


        /// - -- For each DAC channel
        for ( chan = 0; chan < cdsPciModules.dac_info[ card ].num_chans; chan++ )
        {

            /// - -- Determine if memory block has been set with the correct cycle
            /// count by control app.
            if ( ioMemData->outputData[ mm ][ ioMemCntrDac ].channel_set[chan].cycle_65k == ioClockDac )
            {
                dacEnable |= pBits[ card ];
            }
            else
            {
                dacEnable &= ~( pBits[ card ] );
                dacChanErr[ card ] += 1;
            }


            /// - ---- Read DAC output value from shared memory and reset memory
            /// to zero
            if ( ( !dacChanErr[ card ] ) && ( iopDacEnable ) )
            {
                dac_out = ioMemData->outputData[ mm ][ ioMemCntrDac ].channel_set[chan].data;
                /// - --------- Zero out data in case user app dies by next
                /// cycle when two or more apps share same DAC module.
                ioMemData->outputData[ mm ][ ioMemCntrDac ].channel_set[chan].data = 0;
            }
            else
            {
                dac_out = 0;
                status = 1;
            }
            /// - ---- Check output values are within range of DAC \n
            /// - --------- If overflow, clip at DAC limits and report errors
            if ( dac_out > cdsPciModules.dac_info[ card ].sample_limit || 
                 dac_out < -cdsPciModules.dac_info[ card ].sample_limit )
            {
                overflowDac[ card ][ chan ]++;
                pLocalEpics->epicsOutput.overflowDacAcc[ card ][ chan ]++;
                overflowAcc++;
                dacOF[ card ] = 1;
                odcStateWord |= ODC_DAC_OVF;
                
                if ( dac_out > cdsPciModules.dac_info[ card ].sample_limit )
                    dac_out = cdsPciModules.dac_info[ card ].sample_limit;
                else
                    dac_out = -cdsPciModules.dac_info[ card ].sample_limit;
            }
            /// - ---- If DAQKILL tripped, set output to zero.
            if ( !iopDacEnable )
                dac_out = 0;
            /// - ---- Load last values to EPICS channels for monitoring on
            /// GDS_TP screen.
            dacOutEpics[ card ][ chan ] = dac_out;

            /// - ---- Load DAC testpoints
            floatDacOut[ MAX_DAC_CHN_PER_MOD * card + chan ] = dac_out;

            /// - ---- Write to DAC local memory area, for later xmit to DAC
            /// module
            *pDacData = (unsigned int)( dac_out );
            pDacData++;
        }
        /// - -- Mark cycle count as having been used -1 \n
        /// - --------- Forces control apps to mark this cycle or will not be used
        /// again by Master
        ioMemData->outputData[ mm ][ ioMemCntrDac ].channel_set[chan].cycle_65k = -1;
        /// - -- DMA Write data to DAC module
    }
    /// \> Increment DAC memory block pointers for next cycle
    ioClockDac = ( ioClockDac + 1 ) % IOP_IO_RATE;
    ioMemCntrDac = ( ioMemCntrDac + 1 ) % IO_MEMORY_SLOTS;

    return status;
}

#ifdef __cplusplus
}
#endif


#endif //LIGO_NO_IOC_TIMING_H
