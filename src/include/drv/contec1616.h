#ifndef LGO_CONTEC1616_H 
#define LGO_CONTEC1616_H

#include "drv/cdsHardware.h"

#include <linux/pci.h>

#define C_DIO_1616L_PE  0x8632

#ifdef __cplusplus
extern "C" {
#endif

int contec1616Init( CDS_HARDWARE* , struct pci_dev* );
unsigned int contec1616WriteOutputRegister( CDS_HARDWARE* , int , unsigned int );
unsigned int contec1616ReadOutputRegister( CDS_HARDWARE* , int );
unsigned int contec1616ReadInputRegister( CDS_HARDWARE* , int );
void start_tds_clocks ( int );
void stop_tds_clocks ( int );

#ifdef __cplusplus
}
#endif


#endif // LGO_CONTEC1616_H
