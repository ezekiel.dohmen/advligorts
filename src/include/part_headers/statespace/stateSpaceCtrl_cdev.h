#ifndef LIGO_STATE_SPACE_CTRL_CDEV_H
#define LIGO_STATE_SPACE_CTRL_CDEV_H

#ifdef __cplusplus
extern "C" {
#endif


/**
* @brief Initializes the char device for control over the 
*        statespace parts.
* 
* @param model_name The name of the model, that will be used to 
*                   to create the /dev/<model_name>
*
* @return 0 if the char device was created successfully, -1 if 
*         there was an error.
*/
int initStateSpaceCtrl_cdev( const char * model_name);


/**
* @brief Cleans up the char device, should be
*        called before the model is removed.
* 
* @param none
*
* @return none
*/
void cleanupStateSpaceCtrl_cdev( void );

#ifdef __cplusplus
}
#endif

#endif //LIGO_STATE_SPACE_CTRL_CDEV_H