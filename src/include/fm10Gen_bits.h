#ifndef LIGO_FM10GEN_BITS_H
#define LIGO_FM10GEN_BITS_H

#include "util/macros.h" //COUNT_OF()

/* Switching register bits */

//opSwitch bits that get mapped into SW1S
#define OPSWITCH_COEFF_RESET            (1<<0)
#define OPSWITCH_HISTORY_RESET          (1<<1)
#define OPSWITCH_INPUT_ENABLE           (1<<2)
#define OPSWITCH_OFFSET_ENABLE          (1<<3)
#define OPSWITCH_FM1_STAGE_ENABLE       (1<<4)
#define OPSWITCH_FM2_STAGE_ENABLE       (1<<6)
#define OPSWITCH_FM3_STAGE_ENABLE       (1<<8)
#define OPSWITCH_FM4_STAGE_ENABLE       (1<<10)
#define OPSWITCH_FM5_STAGE_ENABLE       (1<<12)
#define OPSWITCH_FM6_STAGE_ENABLE       (1<<14)

//opSwitch bits that get mapped into SW2S
#define OPSWITCH_FM7_STAGE_ENABLE       (1<<16)
#define OPSWITCH_FM8_STAGE_ENABLE       (1<<18)
#define OPSWITCH_FM9_STAGE_ENABLE       (1<<20)
#define OPSWITCH_FM10_STAGE_ENABLE      (1<<22)

#define OPSWITCH_LIMITER_ENABLE         (1<<24)
#define OPSWITCH_DECIMATE_ENABLE        (1<<25)
#define OPSWITCH_OUTPUT_ENABLE          (1<<26)
#define OPSWITCH_HOLD_ENABLE            (1<<27)

#define OPSWITCH_GAIN_RAMPING           (1<<28)
#define OPSWITCH_LIMITER_RAMPING        (1<<29)

// Default Filter OPSWITCH Values set by skeleton.st
// OPSWITCH is stored in two EPICS channels _SW1S, _SW2S. Our internal
// representation stores it in 1 32 bit variable in the FM_OP_IN struct
// (opSwitchE, opSwitchP). The _SW2S is shifted up into the highest 16
// bits of the opSwitchX vaiables. The opSwitch representation is the
// context of the above constants, so we downshift *_SW2_* channels
// to create a good EPICS channel context.
#define OPSWITCH_SW1_DEFAULT_NEW_FILT_VALUE ( 0x0 )
#define OPSWITCH_SW2_DEFAULT_NEW_FILT_VALUE ((OPSWITCH_DECIMATE_ENABLE ) >> 16)

#define DEFAULT_FILTER_OUTGAIN_VAL 0.0


//Filter Switch Mask Settings/Macros
#define MASK_MONITORED_FM1      (1<<0)
#define MASK_MONITORED_FM2      (1<<1)
#define MASK_MONITORED_FM3      (1<<2)
#define MASK_MONITORED_FM4      (1<<3)
#define MASK_MONITORED_FM5      (1<<4)
#define MASK_MONITORED_FM6      (1<<5)
#define MASK_MONITORED_FM7      (1<<6)
#define MASK_MONITORED_FM8      (1<<7)
#define MASK_MONITORED_FM9      (1<<8)
#define MASK_MONITORED_FM10     (1<<9)
#define MASK_MONITORED_IN       (1<<10)
#define MASK_MONITORED_OFFSET   (1<<11)
#define MASK_MONITORED_OUT      (1<<12)
#define MASK_MONITORED_LIMIT    (1<<13)
#define MASK_MONITORED_UNUSED1  (1<<14)
#define MASK_MONITORED_DECIMATE (1<<15)
#define MASK_MONITORED_HOLD     (1<<16)




#ifdef __cplusplus
extern "C" {
#endif

const static int fltrConst[17] = {OPSWITCH_FM1_STAGE_ENABLE, 
                                  OPSWITCH_FM2_STAGE_ENABLE, 
                                  OPSWITCH_FM3_STAGE_ENABLE,
                                  OPSWITCH_FM4_STAGE_ENABLE,
                                  OPSWITCH_FM5_STAGE_ENABLE,
                                  OPSWITCH_FM6_STAGE_ENABLE,
                                  OPSWITCH_FM7_STAGE_ENABLE,
                                  OPSWITCH_FM8_STAGE_ENABLE,
                                  OPSWITCH_FM9_STAGE_ENABLE,
                                  OPSWITCH_FM10_STAGE_ENABLE,
                                  OPSWITCH_INPUT_ENABLE,
                                  OPSWITCH_OFFSET_ENABLE,
                                  OPSWITCH_OUTPUT_ENABLE,
                                  OPSWITCH_LIMITER_ENABLE,
                                  OPSWITCH_COEFF_RESET,
                                  OPSWITCH_DECIMATE_ENABLE,
                                  OPSWITCH_HOLD_ENABLE};


/** Convert opSwitchE bits into the FiltCtrl2 Ctrl output format
 *  This is mapping SW(1/2)S monitor mask[bit num] -> opSwitch[bit num].
 *  This format can be masked with the snap file's mask for filter
 *  switch channels .
 *
 * @param[in] v opSwitch formatted integer that should be converted
 *
 */
static inline unsigned int filtCtrlBitConvert( unsigned int v )
{
    unsigned int result = 0;
    unsigned          bit;
    for ( bit = 0; bit < COUNT_OF(fltrConst); bit++ )
    {
        if ( v & fltrConst[ bit ] )
            result |= 1 << bit;
    }
    return result;
}

/** Convert a filter module mask to opSwitch format. This is taking
 * all bits that are set in the mask and creating an opSwitch formatted
 * integer with corresponding bits set in the opSwitch locations
 *
 * @param[in] m The mask to convert
 *
 */
static inline unsigned int filtMaskToOpswitchFormat( unsigned int m )
{
    unsigned int result = 0;
    unsigned          bit;
    for ( bit = 0; bit < COUNT_OF(fltrConst); bit++ )
    {
        if ( m & ( 1<<bit ) )
            result |= fltrConst[ bit ];
    }
    return result;
}


#ifdef __cplusplus
}
#endif



#endif //LIGO_FM10GEN_BITS_H
