#ifndef LIGO_FE_H
#define LIGO_FE_H

#include "util/inlineMath.h"

#include "controller.h" //This has most globals externed
#include "controllerko.h" //CDIO* Globals and tdsControl/tdsCount

#include "qnorm.h"
#include "util/timing.h"
#include "util/random_bytes.h"
#include "util/prng_xoroshiroPP.h"
#include "util/unwrapPhase.h"
#include "fm10Gen.h"
#include "tRamp.h"
#include "commData3.h"

#include "drv/inputFilterModule.h" //inputFilterModule()
#include "drv/cdsHardware.h"
#include "drv/epicsXfer.h"

#include "part_headers/statespace/stateSpacePart.h"


#include FE_HEADER
#include "Demodulation.h"

// IOP_IO_RATE is passed as a Makefile parameter, verify power of two here
#if IS_POW_OF_2(IOP_IO_RATE) == 0
#error "Constant failed check for power of 2 compliance on IOP_IO_RATE. ##IOP_IO_RATE"
#endif



#ifdef __KERNEL__
#include <linux/kernel.h>
#include <asm/delay.h>
#include <asm/processor.h>
#else
#include <stdbool.h>
#endif //__KERNEL__

#endif //LIGO_FE_H
