#ifndef LIGO_CRC_H
#define LIGO_CRC_H
/// @file crc.h
/// @brief Contains prototype defs for CRC checking functions 
//         found in drv/crc.c


#ifdef __cplusplus
extern "C" {
#endif


unsigned int crc_ptr( const char* cp, unsigned int bytes, unsigned int crc );
unsigned int crc_len( unsigned int bytes, unsigned int crc );

extern const unsigned int crctab[256];

#ifdef __cplusplus
}
#endif

#endif //LIGO_CRC_H
