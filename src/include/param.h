#ifndef PARAM_H
#define PARAM_H

#include "daqmap.h" //DAQ_INFO_BLOCK, GDS_INFO_BLOCK

typedef struct CHAN_PARAM
{
    int   dcuid;
    int   datarate;
    int   acquire;
    int   ifoid;
    int   rmid;
    int   datatype;
    int   chnnum;
    int   testpoint;
    float gain;
    float slope;
    float offset;
    char  units[ 40 ];
    char  system[ 40 ];
} CHAN_PARAM;

#ifdef __cplusplus
extern "C" {
#endif

// called for each channel as parser reads it in.
typedef int (*daq_channel_callback)(const char *, struct CHAN_PARAM *, void *);

int parseConfigFile( const char*,
                     unsigned long*,
                     daq_channel_callback callback,
                     int,
                     char*,
                     void* );

char *strcat_lower(char *dest, const char *src);

#ifdef __cplusplus
}
#endif


#endif //PARAM_H
