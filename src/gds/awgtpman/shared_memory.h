//
// Created by erik.vonreis on 4/27/21.
// functions for interfacing AWG with shared memory
//

#ifndef DAQD_TRUNK_SHARED_MEMORY_H
#define DAQD_TRUNK_SHARED_MEMORY_H

#include <stdint.h>

#include "shmem_all.h"
#include "drv/cdsHardware.h"

// *** AWG waveform data ***
extern volatile AWG_DATA *shmemAwgData;

/**
 * Open up the awg streaming into a global pointer, shmemAwgData
 * @returns 0 on failuer, nonzero on success
 */
int OpenAwgDataSharedMemory(const char *model_name);


// *** IO data from IOP ***

/**
 * Structure for io data shared memory
 */
extern volatile IO_MEM_DATA *ioMemData;

// these are just convenient pointers into some part of ioMemData.

/**
 * Current cycle count, 1/16th of a second, wraps around each second
 */
extern volatile unsigned int *ioMemDataCycle;

/**
 * Current gps seconds
 */
extern volatile unsigned int *ioMemDataGPS;

/**
 * Data rate from ADCs in samples per second.
 */
extern volatile unsigned int *ioMemDataRate_sps;

/**
 * Open the io shared memory.  Maps ioMemData global to this memory space.
 * @param initialize if true, initialize the structure.  If false, the structure is expected to be initialized
 * by another process (probably the IOP model), and the function will fail if it's not.
 * @return true if successful
 */
int OpenIoMemSharedMemory(int initialize);

// *** Testpoint configuration ***

/**
 * Structure pointing to testpoint config shared memory
 */
extern volatile TESTPOINT_CFG * shmemTestpointCfg;

/**
 * Open shared memory for testpoint configuration.
 * memory is mapped to global shmemTestpointCfg
 * @return true if successful
 */
int OpenTestpointCfgSharedMemory(const char *model_name);

// *** Legacy front end shared memory ***

extern volatile uint32_t *ioFrontEndData;

/**
 * Open the legacy shared memory named after the front end system.
 * Unfortunately the structure of this data is not clearly defined,
 * so it's just a block of 32-bit ints, pointed to to by ioFrontEndData.
 *
 * @brief open the legacy shared memory named after the front end system.
 * @param model_name
 * @return true if successful
 */
int OpenIoFrontEndData(const char *model_name);

/**
 * @brief set a shared memory prefix to use in future shared memory initiliazation.
 * @param the prefix.  Don't include '://', e.g. use 'mbuf', not 'mbuf://'
 * @return true if successful.
 */
int SetSharedMemoryPrefix(const char *prefix);

#endif // DAQD_TRUNK_SHARED_MEMORY_H
