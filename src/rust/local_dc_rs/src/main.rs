use advligorts_rs::{
    DAQD_DEFAULT_SHMEM_BUFFER_SIZE_MB, DAQD_MAX_SHMEM_BUFFER_SIZE_MB, DAQD_MIN_SHMEM_BUFFER_SIZE_MB,
};
use clap::Parser;
use local_dc_rs::{InputType, ModelInfo};

const MIN_SIZE_FOR_RANGE: i64 = DAQD_MIN_SHMEM_BUFFER_SIZE_MB as i64;
const MAX_SIZE_FOR_RANGE: i64 = DAQD_MAX_SHMEM_BUFFER_SIZE_MB as i64 + 1;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Config {
    #[arg(short, long, default_value_t=String::from("local_dc"),help=String::from("Name of the buf to write data to locally"))]
    buffer: String,
    #[arg(short='m', default_value_t=DAQD_DEFAULT_SHMEM_BUFFER_SIZE_MB, help=String::from("Local buffer size in MB"),value_parser=clap::value_parser!(u32).range(MIN_SIZE_FOR_RANGE..MAX_SIZE_FOR_RANGE))]
    buffer_mb: u32,
    #[arg(group="input", short='s', help=String::from("Space separated list of systems (conflicts with --systab and --auto)"))]
    systems: Option<String>,
    #[arg(group="input", long="systab", help=String::from("Read the model list from the given systab file (conflicts with -s and --auto)"))]
    systab: Option<String>,
    #[arg(group="input", long="auto", help=String::from("Autodetect the model list from running modes (conflicts with --systab and -s)"))]
    autodetect: bool,
    #[arg(short='d', help=String::from("Path to the gds tp dir (used to lookup model rates)"))]
    gds_tp_dir: Option<String>,
    #[arg(short='w', help=String::from("Number of ms to wait for models to finish"), default_value_t=1)]
    wait: u32,
    #[arg(short, help=String::from("log file"), default_value_t=String::from("-"))]
    log_file: String,
    #[arg(short, help=String::from("Verbose output"))]
    verbose: bool,
    #[arg(long="period", help=String::from("When autodetecting models, sets the time period between checks for new models in seconds"), default_value_t=10, value_parser = clap::value_parser!(u32).range(2..3600))]
    autodetect_period: u32,
}

impl Config {
    fn parse_args() -> local_dc_rs::Config {
        let args = Self::parse();

        let input_models = if args.autodetect {
            InputType::AutoDetect
        } else {
            let system_names;

            if let Some(input) = args.systab {
                system_names =
                    match local_dc_rs::extract_models_from_file(std::path::Path::new(&input)) {
                        Some(vals) => vals,
                        None => vec![],
                    }
            } else if let Some(input) = args.systems {
                system_names = input.split(" ").map(|s| String::from(s)).collect();
            } else {
                system_names = vec![];
            }
            if system_names.is_empty() {
                panic!("no system names where specified");
            }

            let mut info_list: Vec<ModelInfo> = vec![];

            for model_name in system_names.iter() {
                let mut info = match ModelInfo::new(model_name.as_str()) {
                    Some(val) => val,
                    _ => continue,
                };
                if info.dcuid == 0 || info.rate == 0 {
                    match util_rs::lookup_model_rate_and_dcuid(&info.name, &args.gds_tp_dir) {
                        Some(rate_info) => {
                            info.dcuid = rate_info.dcuid;
                            info.rate = rate_info.rate;
                        }
                        _ => continue,
                    }
                }
                info_list.push(info);
            }
            InputType::StaticList(info_list)
        };

        local_dc_rs::Config {
            output_buffer_name: args.buffer,
            output_buffer_size_mb: args.buffer_mb as usize,
            inputs: input_models,
            logfile_name: args.log_file,
            verbose: args.verbose,
            gds_tp_dir: args.gds_tp_dir,
            wait_ms: args.wait,
            autodetect_period: std::time::Duration::from_secs(args.autodetect_period as u64),
        }
    }
}

fn main() {
    let args = Config::parse_args();
    println!("{:?}", args);

    local_dc_rs::main_loop(args);
}
