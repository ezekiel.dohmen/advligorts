use std::fmt::{Debug, Formatter};
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::iter::Extend;
use std::path::Path;
use std::ptr;
use std::slice::from_raw_parts;
use std::sync::{atomic, Arc};
use std::time::Duration;
use std::{ffi, thread, time};

use advligorts_rs::{
    cdsDaqNetGdsTpNum, CDS_DAQ_NET_DATA_OFFSET, CDS_DAQ_NET_GDS_TP_TABLE_OFFSET,
    CDS_DAQ_NET_IPC_OFFSET, DAQ_GDS_MAX_TP_NUM,
};
use ini_parsing_rs::crc_span;
use util_rs::get_hostname;

mod background;
mod gps;
mod memory;
mod structs;

use structs::{
    get_ptr_at_offset, new_shared_buffer_queue, InputBuffer, ShmemType, RMIPCS_BUF_SIZE_MB,
};

use crate::structs::ShareableQueues;
use memory::DaqMultiCycleBlock;

pub type ModelInfo = structs::ModelInfo;

const MAX_NSYS: usize = 64;

enum WaitStatus {
    Ok,
    TimedOut,
}

pub enum InputType {
    StaticList(Vec<ModelInfo>),
    AutoDetect,
}

impl Debug for InputType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            InputType::StaticList(_inputs) => write!(f, "Static list"),
            InputType::AutoDetect => write!(f, "AutoDetect inputs"),
        }
    }
}

#[derive(Debug)]
pub struct Config {
    pub output_buffer_name: String,
    pub output_buffer_size_mb: usize,
    pub inputs: InputType,
    pub logfile_name: String,
    pub verbose: bool,
    pub gds_tp_dir: Option<String>,
    pub wait_ms: u32,
    pub autodetect_period: std::time::Duration,
}

impl Config {
    fn inputs_bounded(&self) -> bool {
        match &self.inputs {
            InputType::StaticList(lst) => lst.len() <= MAX_NSYS,
            _ => false,
        }
    }

    fn inputs_bounded_and_empty(&self) -> bool {
        match &self.inputs {
            InputType::StaticList(lst) => lst.is_empty(),
            _ => false,
        }
    }
}

fn trunc_data_size(size: u32) -> u32 {
    if size > advligorts_rs::DAQ_DCU_BLOCK_SIZE {
        return advligorts_rs::DAQ_DCU_BLOCK_SIZE;
    }
    size
}

struct Input {
    buffer: Box<InputBuffer>,
    ready: bool,
    prev_cycle: u32,
    idle_counter: u32,
}

impl Input {
    fn from_buffer(buffer: Box<InputBuffer>) -> Self {
        let prev_cycle = buffer.get_cycle();
        Input {
            buffer,
            ready: false,
            prev_cycle,
            idle_counter: 0,
        }
    }
    fn get_rmipcstr(&self) -> &advligorts_rs::rmIpcStr {
        unsafe { &*self.buffer.ipc }
    }

    fn get_tp_table(&self) -> &advligorts_rs::cdsDaqNetGdsTpNum {
        unsafe { &*self.buffer.tp_table }
    }

    fn get_raw_and_tp_data(&self, size: usize, cur_cycle: u32) -> &[ffi::c_uchar] {
        unsafe {
            from_raw_parts(
                self.buffer
                    .data
                    .add(cur_cycle as usize * advligorts_rs::DAQ_DCU_BLOCK_SIZE as usize)
                    as *const ffi::c_uchar,
                size,
            )
        }
    }

    fn may_be_iop(&self) -> bool {
        self.buffer.info.may_be_iop()
    }
}

/// IpcSummary is a collection of all the fields we read from
/// an rmIpcStr structure.  The idea is to read from the rmIpcStr
/// once and cache the result, so that we don't worry about having
/// the fields change under us.
struct IpcSummary<'a> {
    cycle: ffi::c_uint,
    dcuid: ffi::c_uint,
    cfg_crc: ffi::c_uint,
    time_sec: ffi::c_uint,
    time_nsec: ffi::c_uint,
    data_size: ffi::c_uint,
    tp_count: ffi::c_uint,
    tp_size: ffi::c_uint,
    tp_num: [ffi::c_uint; advligorts_rs::DAQ_GDS_MAX_TP_NUM as usize],
    input: &'a Input,
}

impl<'input_lifetime> IpcSummary<'input_lifetime> {
    /// Creates a summary of the RmIpcStr that is only read once and all reads
    /// are done either atomic or volatile so that nothing gets elided.
    fn new(input: &'input_lifetime Input, cycle: usize) -> Self {
        let rmipc = input.get_rmipcstr();
        let tp_table = input.get_tp_table();
        let mut summary = unsafe {
            let tp_count = ptr::addr_of!(tp_table.count).read_volatile() as ffi::c_uint & 0xff;
            Self {
                cycle: rmipc.get_cycle(),
                dcuid: ptr::addr_of!(rmipc.dcuId).read_volatile(),
                cfg_crc: ptr::addr_of!(rmipc.crc).read_volatile(),
                time_sec: ptr::addr_of!(rmipc.bp[cycle].timeSec).read_volatile(),
                time_nsec: ptr::addr_of!(rmipc.bp[cycle].timeNSec).read_volatile(),
                data_size: trunc_data_size(ptr::addr_of!(rmipc.bp[cycle].crc).read_volatile()),
                tp_count,
                tp_size: ((std::mem::size_of::<ffi::c_float>()
                    * input.buffer.info.rate as usize
                    * tp_count as usize)
                    / advligorts_rs::DAQ_NUM_DATA_BLOCKS_PER_SECOND as usize)
                    as ffi::c_uint,
                tp_num: [0; DAQ_GDS_MAX_TP_NUM as usize],
                input,
            }
        };
        for i in 0..summary.tp_count {
            summary.tp_num[i as usize] = tp_table.tpNum[i as usize] as ::std::os::raw::c_uint;
        }
        summary
    }
}

/// Holds the local_dc state
struct State {
    inputs: Vec<Input>,
    output: DaqMultiCycleBlock,
    expected_cycle: u32,
    dynamic: bool,
    wait_time: Option<Duration>,
}

impl State {
    fn new(cfg: &Config) -> Result<State, String> {
        let dummy: Vec<ModelInfo> = vec![];
        let input_list;
        let dynamic = match &cfg.inputs {
            InputType::StaticList(list) => {
                input_list = list;
                false
            }
            _ => {
                input_list = &dummy;
                true
            }
        };
        let out = match cds_shmem_rs::ShMem::new(&cfg.output_buffer_name, cfg.output_buffer_size_mb)
        {
            Ok(shm) => shm,
            _ => return Err(String::from("Unable to open output buffer")),
        };
        let mut inputs: Vec<Input> = vec![];
        inputs.reserve(input_list.len());
        if !dynamic {
            for i in 0..input_list.len() {
                let val = &input_list[i];
                let buffer_box = InputBuffer::new(val)?;
                let prev_cycle_val = buffer_box.get_cycle();

                inputs.push(Input {
                    buffer: buffer_box,
                    ready: false,
                    prev_cycle: prev_cycle_val,
                    idle_counter: 0,
                })
            }
        }

        let output = memory::DaqMultiCycleBlock::new(out)?;

        Ok(State {
            inputs,
            output,
            expected_cycle: advligorts_rs::DAQ_NUM_DATA_BLOCKS - 1,
            dynamic,
            wait_time: if cfg.wait_ms <= 0 {
                None
            } else {
                Some(time::Duration::from_millis(cfg.wait_ms as u64))
            },
        })
    }

    fn load_new_inputs(&mut self, queues: &ShareableQueues) {
        if !self.dynamic || self.inputs.len() == MAX_NSYS {
            return;
        }
        if !self.inputs.is_empty() && self.expected_cycle != 15 {
            return;
        }
        let available_slots = MAX_NSYS - self.inputs.len();
        let len = self.inputs.len();
        self.inputs.extend(
            queues
                .get_upto_n_inputs(available_slots)
                .into_iter()
                .map(|entry| Input::from_buffer(entry)),
        );
        if self.inputs.len() != len {
            if let Some(iop_index) = self.inputs.iter().position(|entry| entry.may_be_iop()) {
                if iop_index > 0 {
                    self.inputs.swap(0, iop_index)
                }
            }
        }
    }

    /// Remove inputs that have not given input for many seconds (5).
    /// Remove old inputs and push them into the to_close queue in
    /// queues.
    ///
    /// Note: This only removes entries when dynamic input lists
    /// are selected.  Entries are only removed on the 15th cycle.
    fn remove_stopped_inputs(&mut self, queues: &ShareableQueues) {
        if !self.dynamic || self.expected_cycle != 15 {
            return;
        }
        let mut old_buffers: Vec<Box<InputBuffer>> = Vec::new();
        let mut i = 0;
        while i < self.inputs.len() {
            if self.inputs[i].idle_counter > 16 * 5 {
                let mut old = self.inputs.remove(i);
                old_buffers.push(old.buffer);
            } else {
                i += 1;
            }
        }
        queues.add_to_close(old_buffers);
    }

    fn next_cycle(&mut self) {
        self.expected_cycle = (self.expected_cycle + 1) % 16
    }

    /// Clear all the ready bits
    fn clear_ready(&mut self) {
        for input in self.inputs.iter_mut() {
            input.ready = false;
        }
    }
}

fn extract_models<I>(inputs: I) -> Option<Vec<String>>
where
    I: Iterator<Item = String>,
{
    let hostname = match get_hostname() {
        Ok(s) => s,
        Err(_) => return None,
    };

    for line in inputs {
        let line = line.trim();
        let parts: Vec<&str> = line
            .split([' ', '\t'])
            .filter(|s| !(*s).is_empty())
            .collect();
        if parts.is_empty() || parts[0] != hostname.as_str() {
            continue;
        }
        return Some(parts[1..].iter().map(|s| String::from(*s)).collect());
    }
    None
}

pub fn extract_models_from_file(path: &Path) -> Option<Vec<String>> {
    match File::open(&path) {
        Err(_) => None,
        Ok(f) => extract_models(
            BufReader::new(f)
                .lines()
                .filter(|val| val.is_ok())
                .map(|val| val.unwrap()),
        ),
    }
}

fn wait_for_data(state: &mut State) -> WaitStatus {
    if state.inputs.is_empty() {
        panic!("wait_for_data called with no inputs");
    }
    let expected_cycle = state.expected_cycle;

    state.clear_ready();
    let mut expected_sec = 0;
    let mut timeout = 0;
    let mut ready = 0;
    while ready == 0 && timeout < 500 {
        timeout += 1;
        thread::sleep(time::Duration::from_millis(2));
        for input in state.inputs.iter_mut() {
            let model = unsafe { &mut *input.buffer.ipc };
            let cur_cycle = model.get_cycle();
            // require a change from the previous state to prove
            // that the iop is alive and we are not looking at
            // a stopped clock that will be right every 1/16 of the
            // time
            if cur_cycle == input.prev_cycle {
                continue;
            }
            input.prev_cycle = cur_cycle;
            if cur_cycle == expected_cycle {
                input.ready = true;
                expected_sec = model.bp[cur_cycle as usize].timeSec;
                ready = 1;
                break;
            }
        }
    }
    if ready > 0 {
        let expected_sec = expected_sec;
        timeout = 0;
        // iop is running at this point
        while timeout < 20 && ready < state.inputs.len() {
            thread::sleep(time::Duration::from_micros(100));
            for input in state.inputs.iter_mut() {
                // if we are already done, don't check again
                if input.ready {
                    continue;
                }
                let model = unsafe { &mut *input.buffer.ipc };
                let cur_sec = model.bp[expected_cycle as usize].timeSec;
                let cur_cycle = model.get_cycle();
                if cur_cycle == expected_cycle && cur_sec == expected_sec {
                    input.ready = true;
                    ready += 1;
                }
            }
            timeout += 1;
        }
    }
    for input in state.inputs.iter_mut() {
        if !input.ready {
            input.idle_counter += 1;
        } else {
            input.idle_counter = 0;
        }
    }
    if ready > 0 {
        WaitStatus::Ok
    } else {
        WaitStatus::TimedOut
    }
}

fn crc_buffer(buffer: &[ffi::c_uchar]) -> ffi::c_uint {
    unsafe { crc_span(buffer.as_ptr() as *const ffi::c_char, buffer.len()) }
}

fn can_data_and_tp_fit(inputs: &Vec<IpcSummary>, max_size: usize) -> bool {
    let mut total_data_size: usize = 0;
    let mut total_tp_size: usize = 0;
    for summary in inputs.iter() {
        total_data_size += summary.data_size as usize;
        total_tp_size += summary.tp_size as usize;
    }
    total_data_size + total_tp_size <= max_size
}

fn copy_available_data(state: &State) {
    match state.wait_time.as_ref() {
        Some(t) => thread::sleep(*t),
        _ => {}
    }
    let cur_cycle = state.expected_cycle;
    let (dcu_headers, mut output_data) = match state.output.cycle_data(cur_cycle) {
        Ok(result) => result,
        Err(_) => {
            return;
        }
    };

    let summaries = state
        .inputs
        .iter()
        .filter_map(|entry| {
            if !entry.ready {
                return None;
            }
            Some(IpcSummary::new(entry, cur_cycle as usize))
        })
        .collect::<Vec<IpcSummary>>();

    dcu_headers.dcuTotalModels = 0;
    dcu_headers.fullDataBlockSize = 0;

    let can_do_tp = can_data_and_tp_fit(&summaries, output_data.len());

    let mut db: usize = 0;

    // let out_start = state.output.buffer.get_ptr_at_offset::<u8>(0).unwrap() as usize;
    // let hdr_ptr = dcu_headers as *const advligorts_rs::daq_multi_dcu_header_t;
    // println!("multi_cycle_header {0}", std::mem::size_of::<structs::daq_multi_cycle_header_t>());
    // println!("multi_dcu_header   {0}", std::mem::size_of::<advligorts_rs::daq_multi_dcu_header_t>());
    // println!("cycle_data_size    {0}", state.output.cycle_data_size);
    // println!("output buffer at   {:p}", state.output.buffer.get_ptr_at_offset::<u8>(0).unwrap());
    // println!("output headers at  {:p}    offset {1}", dcu_headers, hdr_ptr as usize - out_start);
    // println!("output data        {:p}    offset {1}", output_data.as_ptr(), output_data.as_ptr() as usize - out_start);
    //println!("processing cycle {0}, out = {1:p}, multi_cycle_header = {2:p}", cur_cycle, state.output.buffer.get_ptr_at_offset::<i8>(0).unwrap(), state.output.out_header);
    //println!("dcuheaders start {0:p}", dcu_headers);
    for summary in summaries.iter() {
        let out_header = &mut dcu_headers.dcuheader[db];

        //println!("\tdcuid {0}, header {1:p}", summary.dcuid, out_header);

        let data_size = summary.data_size as usize;
        let tp_count: usize;
        let tp_size: usize;
        if can_do_tp {
            tp_count = summary.tp_count as usize;
            tp_size = summary.tp_size as usize;
        } else {
            tp_count = 0;
            tp_size = 0;
        }

        let xfer_size = data_size + tp_size;
        let input_data = summary.input.get_raw_and_tp_data(xfer_size, cur_cycle);
        if input_data.len() > output_data.len() {
            // not enough room
            continue;
        }

        if cur_cycle == 0 {
            //model.reqAck ^= 1;
            summary.input.get_rmipcstr().xor_req_ack(1);
        }
        out_header.dcuId = summary.dcuid;
        out_header.fileCrc = summary.cfg_crc;
        out_header.cycle = summary.cycle;
        out_header.timeSec = summary.time_sec;
        out_header.timeNSec = summary.time_nsec;
        out_header.status = 2;
        out_header.dataBlockSize = data_size as ffi::c_uint;
        out_header.tpCount = tp_count as ffi::c_uint;
        out_header.tpBlockSize = tp_size as ffi::c_uint;

        let tp_dest = &mut out_header.tpNum[0..tp_count];
        tp_dest.copy_from_slice(&summary.tp_num[0..tp_count]);

        let xfer_size = input_data.len();

        out_header.dataCrc = crc_buffer(input_data);

        let dest = &mut output_data[0..xfer_size];
        // println!("dcu {0} - {1:p} {2}", summary.dcuid, dest.as_ptr(), xfer_size);
        // println!("input start {0:p} input data {1:p}", summary.input.buffer_.get_ptr_at_offset::<u8>(0).unwrap() , input_data.as_ptr());
        dest.copy_from_slice(input_data);
        output_data = &mut output_data[xfer_size..];

        dcu_headers.fullDataBlockSize += xfer_size as u32;

        if cur_cycle == 0 {
            //model.reqAck ^= 2;
            summary.input.get_rmipcstr().xor_req_ack(2);
        }
        db += 1;
    }
    dcu_headers.dcuTotalModels = db as u32;
    state.output.header().set_cur_cycle(state.expected_cycle);
}

pub fn main_loop(cfg: Config) {
    if cfg.inputs_bounded_and_empty() {
        panic!("Static config requested with no inputs");
    }
    let queues = new_shared_buffer_queue();
    let stop_flag = Arc::new(atomic::AtomicBool::new(false));
    thread::scope(|s| {
        if !cfg.inputs_bounded() {
            println!("launching background auto configuration loop");
            s.spawn(|| {
                background::detect_models_loop(
                    queues.clone(),
                    cfg.gds_tp_dir.clone(),
                    stop_flag.clone(),
                    cfg.autodetect_period,
                )
            });
        }

        let mut state = match State::new(&cfg) {
            Ok(s) => s,
            Err(s) => panic!("Unable to initialize internal state: {}", s),
        };

        loop {
            state.load_new_inputs(&*queues);
            if state.dynamic && state.inputs.is_empty() {
                std::thread::sleep(cfg.autodetect_period / 2);
                continue;
            }

            state.next_cycle();
            if let WaitStatus::Ok = wait_for_data(&mut state) {
                copy_available_data(&state);
            }
            state.remove_stopped_inputs(&*queues);
        }
        stop_flag.store(true, atomic::Ordering::SeqCst);
    });
}

#[cfg(test)]
mod tests {
    use super::*;
    use advligorts_rs::daq_multi_cycle_header_t;
    use std::char::MAX;

    #[test]
    fn test_extract_models() {
        let hostname = util_rs::get_hostname().unwrap();
        let mut other = hostname.clone();
        other.push_str("_other");

        let mut inputs: Vec<String> = vec![];
        inputs.push(format!("#{} a b c d e f g", &hostname));
        inputs.push(String::from(""));
        inputs.push(format!("{} 1 2\t3", other));
        inputs.push(format!("{}  h i\tj  k\t\tl ", &hostname));
        inputs.push(format!("{} 4 5 6", &hostname));

        assert_eq!(
            extract_models(inputs.iter().map(|s| s.clone())),
            Some(vec![
                String::from("h"),
                String::from("i"),
                String::from("j"),
                String::from("k"),
                String::from("l")
            ])
        );
    }

    fn empty_buf(mb: usize) -> Vec<u8> {
        let mut buf = Vec::<u8>::new();
        buf.resize(mb * 1024 * 1024, 0);
        buf
    }

    unsafe fn populate_input_shmem(
        input: &cds_shmem_rs::ShMem,
        dcuid: u32,
        gps: u32,
        cycle: u8,
        offset: u32,
    ) {
        let cycle = cycle as usize;
        let ipc_ptr =
            get_ptr_at_offset::<advligorts_rs::rmIpcStr>(input, CDS_DAQ_NET_IPC_OFFSET as usize);
        (*ipc_ptr).cycle = cycle as ffi::c_uint;
        (*ipc_ptr).dcuId = dcuid as ffi::c_uint;
        (*ipc_ptr).crc = 42 + dcuid as ffi::c_uint;
        (*ipc_ptr).command = 0;
        (*ipc_ptr).cmdAck = 0;
        (*ipc_ptr).request = 0;
        (*ipc_ptr).reqAck = 0;
        (*ipc_ptr).status = 2;
        (*ipc_ptr).channelCount = 1000;
        (*ipc_ptr).dataBlockSize = (1000 * 4 * 2048) / 16;

        (*ipc_ptr).bp[cycle].status = 2;
        (*ipc_ptr).bp[cycle].timeSec = gps as ffi::c_uint;
        (*ipc_ptr).bp[cycle].timeNSec = (cycle * 62500000) as ffi::c_uint;
        (*ipc_ptr).bp[cycle].run = 0;
        (*ipc_ptr).bp[cycle].cycle = cycle as ffi::c_uint;
        (*ipc_ptr).bp[cycle].crc = (1000 * 4 * 2048) / 16;

        let tp_table_ptr = get_ptr_at_offset::<cdsDaqNetGdsTpNum>(
            &input,
            CDS_DAQ_NET_GDS_TP_TABLE_OFFSET as usize,
        );
        (*tp_table_ptr).count = 0 as ffi::c_int;

        let data_ptr = get_ptr_at_offset::<ffi::c_char>(&input, CDS_DAQ_NET_DATA_OFFSET as usize);
        let data_ptr_orig = data_ptr;
        let data_ptr = data_ptr.add(cycle * advligorts_rs::DAQ_DCU_BLOCK_SIZE as usize);
        let data = std::slice::from_raw_parts_mut(data_ptr as *mut u32, 1000 * 2048 / 16);
        for i in 0..data.len() {
            data[i] = (offset + i as u32);
        }
        let test_ptr = get_ptr_at_offset::<ffi::c_char>(&input, CDS_DAQ_NET_DATA_OFFSET as usize);
        let test_ptr = test_ptr.add(cycle * advligorts_rs::DAQ_DCU_BLOCK_SIZE as usize);
        let test_ptr = test_ptr as *const u32;
        assert_eq!(*test_ptr, offset);
    }

    unsafe fn to_input(input: cds_shmem_rs::ShMem) -> Input {
        let ipc_ptr =
            get_ptr_at_offset::<advligorts_rs::rmIpcStr>(&input, CDS_DAQ_NET_IPC_OFFSET as usize);
        let data_ptr = get_ptr_at_offset::<ffi::c_char>(&input, CDS_DAQ_NET_DATA_OFFSET as usize);
        let tp_table_ptr = get_ptr_at_offset::<cdsDaqNetGdsTpNum>(
            &input,
            CDS_DAQ_NET_GDS_TP_TABLE_OFFSET as usize,
        );
        let dcu_id = (*ipc_ptr).dcuId;
        Input {
            buffer: Box::new(InputBuffer {
                buffer_: input,
                info: ModelInfo {
                    shmem: ShmemType::Mbuf,
                    name: format!("mod{0}", dcu_id),
                    dcuid: dcu_id,
                    rate: 2048,
                },
                ipc: ipc_ptr,
                data: data_ptr,
                tp_table: tp_table_ptr,
            }),
            ready: true,
            prev_cycle: 4,
            idle_counter: 0,
        }
    }

    #[test]
    fn test_copy_available_data() {
        unsafe {
            let mut in1 = empty_buf(64);
            let mut in2 = empty_buf(64);
            let mut out = empty_buf(100);

            let in1_shmem = cds_shmem_rs::ShMem::new_from_slice(in1.as_mut_slice());
            let in2_shmem = cds_shmem_rs::ShMem::new_from_slice(in2.as_mut_slice());
            let out_shmem = cds_shmem_rs::ShMem::new_from_slice(out.as_mut_slice());

            populate_input_shmem(&in1_shmem, 5, 1000000000, 5, 0);
            populate_input_shmem(&in2_shmem, 42, 1000000000, 5, 100000);

            let state = State {
                inputs: vec![to_input(in1_shmem), to_input(in2_shmem)],
                output: memory::DaqMultiCycleBlock::new(out_shmem).unwrap(),
                expected_cycle: 5,
                dynamic: false,
                wait_time: None,
            };
            copy_available_data(&state);
            assert_eq!(state.output.header().get_cur_cycle(), 5);
            let cycle_size: usize = (100 * 1024 * 1024
                - std::mem::size_of::<advligorts_rs::daq_multi_cycle_header_t>())
                / 16;
            let cycle_size = cycle_size - (cycle_size % 8);
            assert_eq!(
                state.output.header().get_cycle_data_size(),
                cycle_size as u32
            );

            let headers = get_ptr_at_offset::<advligorts_rs::daq_multi_dcu_header_t>(
                &state.output.buffer,
                std::mem::size_of::<advligorts_rs::daq_multi_cycle_header_t>() + (5 * cycle_size),
            );
            assert_eq!((*headers).dcuTotalModels, 2);
            assert_eq!((*headers).fullDataBlockSize, 2048 * 1000 * 4 * 2 / 16);
            assert_eq!((*headers).dcuheader[0].dcuId, 5);
            assert_eq!((*headers).dcuheader[0].fileCrc, 42 + 5);
            assert_eq!((*headers).dcuheader[0].cycle, 5);
            assert_eq!((*headers).dcuheader[0].dataBlockSize, 2048 * 1000 * 4 / 16);
            assert_eq!((*headers).dcuheader[0].tpBlockSize, 0);
            assert_eq!((*headers).dcuheader[0].tpCount, 0);
            assert_eq!((*headers).dcuheader[1].dcuId, 42);
            assert_eq!((*headers).dcuheader[1].fileCrc, 42 + 42);
            assert_eq!((*headers).dcuheader[1].cycle, 5);
            assert_eq!((*headers).dcuheader[1].dataBlockSize, 2048 * 1000 * 4 / 16);
            assert_eq!((*headers).dcuheader[1].tpBlockSize, 0);
            assert_eq!((*headers).dcuheader[1].tpCount, 0);

            let data_ptr = get_ptr_at_offset::<u8>(
                &state.output.buffer,
                std::mem::size_of::<advligorts_rs::daq_multi_cycle_header_t>()
                    + (5 * cycle_size)
                    + std::mem::size_of::<advligorts_rs::daq_multi_dcu_header_t>(),
            );
            let data = std::slice::from_raw_parts_mut(data_ptr as *mut u32, 2 * 1000 * 2048 / 16);
            let mut cur = 0usize;
            for offset in [0u32, 100000u32] {
                for i in 0..data.len() / 2 {
                    assert_eq!(data[cur], offset + i as u32);
                    cur += 1;
                }
            }
        }
    }

    fn zero_vec(count_mb: usize) -> Vec<u8> {
        let mut v = Vec::new();
        v.resize(count_mb * 1024 * 1024, 0u8);
        v
    }

    #[test]
    fn test_state_remove() {
        unsafe {
            let in1_shmem = cds_shmem_rs::ShMem::new_from_vec(zero_vec(64));
            let in2_shmem = cds_shmem_rs::ShMem::new_from_vec(zero_vec(64));
            let out_shmem = cds_shmem_rs::ShMem::new_from_vec(zero_vec(100));

            populate_input_shmem(&in1_shmem, 5, 1000000020, 5, 0);
            populate_input_shmem(&in2_shmem, 42, 1000000000, 3, 0);

            let mut state = State {
                inputs: vec![to_input(in1_shmem), to_input(in2_shmem)],
                output: memory::DaqMultiCycleBlock::new(out_shmem).unwrap(),
                expected_cycle: 5,
                dynamic: false,
                wait_time: None,
            };
            state.inputs[0].idle_counter = 0;
            state.inputs[0].ready = true;
            state.inputs[1].idle_counter = 20 * 16 + 2;
            state.inputs[1].ready = false;

            let mut queues = new_shared_buffer_queue();

            let get_length_to_close = || -> usize {
                let q = queues.queues.lock().unwrap();
                q.buffers_to_close.len()
            };
            {
                assert_eq!(get_length_to_close(), 0);

                // test with dynamic = false and expected counter = 5
                state.remove_stopped_inputs(&*queues);
                assert_eq!(get_length_to_close(), 0);
                assert_eq!(state.inputs.len(), 2);

                // test with dynamic = true and expected counter = 5
                state.dynamic = true;
                state.remove_stopped_inputs(&*queues);
                assert_eq!(get_length_to_close(), 0);
                assert_eq!(state.inputs.len(), 2);

                // test with dynamic = false and expected counter = 15
                state.dynamic = false;
                state.expected_cycle = 15;
                state.remove_stopped_inputs(&*queues);
                assert_eq!(get_length_to_close(), 0);
                assert_eq!(state.inputs.len(), 2);

                // test with dynamic = true and expected counter = 15
                state.dynamic = true;
                state.expected_cycle = 15;
                state.remove_stopped_inputs(&*queues);
                assert_eq!(state.inputs.len(), 1);
                assert_eq!(state.inputs[0].get_rmipcstr().dcuId, 5);
                assert_eq!(get_length_to_close(), 1);
            }
        }
    }

    #[test]
    fn test_state_new_inputs() {
        unsafe {
            let mut input_vecs: Vec<Box<InputBuffer>> = Vec::new();

            let out_shmem = cds_shmem_rs::ShMem::new_from_vec(zero_vec(100));
            let mut state = State {
                inputs: Vec::new(),
                output: memory::DaqMultiCycleBlock::new(out_shmem).unwrap(),
                expected_cycle: 5,
                dynamic: false,
                wait_time: None,
            };

            let mut available_inputs: Vec<Box<InputBuffer>> = Vec::new();
            let mut queues = new_shared_buffer_queue();

            let input_len = || -> usize {
                let q = queues.queues.lock().unwrap();
                q.new_inputs.len()
            };

            let clear_output = || -> usize {
                let mut q = queues.queues.lock().unwrap();
                let l = q.buffers_to_close.len();
                available_inputs.append(&mut q.buffers_to_close);
                l
            };

            // everything empty, should be safe
            assert_eq!(input_len(), 0);
            state.dynamic = false;
            state.load_new_inputs(&*queues);
            assert!(state.inputs.is_empty());

            // everything empty, should be safe
            assert_eq!(input_len(), 0);
            state.dynamic = true;
            state.load_new_inputs(&*queues);
            assert!(state.inputs.is_empty());

            available_inputs.reserve(70);
            for i in 1..71 {
                let mut inp = cds_shmem_rs::ShMem::new_from_vec(zero_vec(64));
                let iop_str = if i == 25 { "iop" } else { "" };
                let info = ModelInfo::new(format!("mbuf://mod{1}{0}:{0}:16", i, iop_str).as_str())
                    .unwrap();
                populate_input_shmem(&inp, i as u32, 1000000000, 15, 0);
                available_inputs.push(InputBuffer::from_buffer(&info, inp));
            }

            {
                let mut q = queues.queues.lock().unwrap();
                q.new_inputs.append(&mut available_inputs);
            }
            assert_eq!(input_len(), 70);

            // state empty, inputs full, not dynamic, nothing should happen
            state.dynamic = false;
            state.load_new_inputs(&*queues);
            assert!(state.inputs.is_empty());
            assert_eq!(input_len(), 70);

            // state empty, inputs full, dynamic, should transfer 64
            state.dynamic = true;
            state.expected_cycle = 15;
            state.load_new_inputs(&*queues);
            assert_eq!(state.inputs.len(), MAX_NSYS);
            assert_eq!(input_len(), 70 - MAX_NSYS);
            assert_eq!(state.inputs.first().unwrap().buffer.info.name, "modiop25");

            for mut entry in state.inputs.iter_mut() {
                entry.idle_counter = 0;
                if (*entry.buffer.ipc).dcuId % 2 == 0 {
                    entry.idle_counter = 20 * 16;
                }
            }
            state.remove_stopped_inputs(&*queues);
            println!("inputs remaining {0}", state.inputs.len());

            {
                let mut q = queues.queues.lock().unwrap();
                while q.new_inputs.len() > 3 {
                    available_inputs.push(q.new_inputs.swap_remove(0));
                }
            }

            // state at 32, inputs at 3, dynamic, should transfer 3
            state.dynamic = true;
            state.expected_cycle = 15;
            state.load_new_inputs(&*queues);
            assert_eq!(state.inputs.len(), 35);
            assert_eq!(input_len(), 0);
        }
    }
}
