use advligorts_rs::{
    blockPropT, cdsDaqNetGdsTpNum, rmIpcStr, CDS_DAQ_NET_DATA_OFFSET,
    CDS_DAQ_NET_GDS_TP_TABLE_OFFSET, CDS_DAQ_NET_IPC_OFFSET,
};
use std::ffi;
use std::sync::atomic::{AtomicU32, Ordering};
use std::sync::{Arc, Mutex};

pub const RMIPCS_BUF_SIZE_MB: usize = 64;

#[derive(Debug, PartialEq, Clone)]
pub enum ShmemType {
    Mbuf,
    Posix,
}

impl ShmemType {
    pub fn prefix(&self) -> &'static str {
        match &self {
            ShmemType::Posix => cds_shmem_rs::PosixPrefix,
            ShmemType::Mbuf => cds_shmem_rs::MbufPrefix,
        }
    }
}

#[derive(Debug, Clone)]
pub struct ModelInfo {
    pub shmem: ShmemType,
    pub name: String,
    pub dcuid: u32,
    pub rate: u32,
}

impl ModelInfo {
    /// name  (opt shmem prefix) system name + dcu + rate, extract the dcu + rate.
    /// name The text to parse must be either "model:dcuid:rate" or
    /// "model:dcuid"
    ///
    /// If a dcuid is specified but a rate is not, the rate defaults to 16
    /// If a dcuid or rate is invalid it is set to 0
    /// If no dcuid (and thus also no rate) is specified then both are set to 0
    ///
    pub fn new(input: &str) -> Option<Self> {
        let dcuid;
        let rate;
        let name;
        let shmem_type;
        let input_name;

        match input.strip_prefix(cds_shmem_rs::PosixPrefix) {
            Some(s) => {
                shmem_type = ShmemType::Posix;
                input_name = s;
            }
            None => {
                shmem_type = ShmemType::Mbuf;
                match input.strip_prefix(cds_shmem_rs::MbufPrefix) {
                    Some(s) => {
                        input_name = s;
                    }
                    None => {
                        if input.contains("://") {
                            return None;
                        }
                        input_name = input;
                    }
                }
            }
        }

        let parts: Vec<&str> = input_name.splitn(3, ':').collect();
        if parts.len() == 3 {
            name = parts[0].to_string();
            dcuid = match parts[1].parse::<u32>() {
                Ok(i) => i,
                Err(_) => 0,
            };
            if dcuid == 0 {
                rate = 0
            } else {
                if parts[2] == "" {
                    rate = 16;
                } else {
                    rate = match parts[2].parse::<u32>() {
                        Ok(i) => i,
                        Err(_) => 0,
                    };
                }
            }
        } else if parts.len() == 2 {
            name = parts[0].to_string();
            dcuid = match parts[1].parse::<u32>() {
                Ok(i) => i,
                Err(_) => 0,
            };
            rate = 16;
        } else {
            name = input_name.to_string();
            dcuid = 0;
            rate = 0;
        }
        if name.is_empty() {
            None
        } else {
            Some(ModelInfo {
                name: name,
                shmem: shmem_type,
                dcuid: dcuid,
                rate: rate,
            })
        }
    }

    pub fn may_be_iop(&self) -> bool {
        self.name.contains("iop")
    }
}

impl PartialEq for ModelInfo {
    fn eq(&self, other: &Self) -> bool {
        self.shmem == other.shmem
            && self.name == other.name
            && self.dcuid == other.dcuid
            && self.rate == other.rate
    }
}
impl Eq for ModelInfo {}

pub struct InputBuffer {
    pub buffer_: cds_shmem_rs::ShMem,
    pub info: ModelInfo,
    pub ipc: *mut rmIpcStr,
    pub data: *mut ffi::c_char,
    pub tp_table: *const cdsDaqNetGdsTpNum,
}

impl InputBuffer {
    pub fn from_buffer(val: &ModelInfo, shmem: cds_shmem_rs::ShMem) -> Box<InputBuffer> {
        let ipc_ptr = get_ptr_at_offset::<rmIpcStr>(&shmem, CDS_DAQ_NET_IPC_OFFSET as usize);
        let data_ptr = get_ptr_at_offset::<ffi::c_char>(&shmem, CDS_DAQ_NET_DATA_OFFSET as usize);
        let tp_table_ptr = get_ptr_at_offset::<cdsDaqNetGdsTpNum>(
            &shmem,
            CDS_DAQ_NET_GDS_TP_TABLE_OFFSET as usize,
        );
        Box::new(InputBuffer {
            buffer_: shmem,
            info: val.clone(),
            ipc: ipc_ptr,
            data: data_ptr,
            tp_table: tp_table_ptr,
        })
    }
    pub fn new(val: &ModelInfo) -> std::result::Result<Box<Self>, String> {
        let mut full_name = String::from(val.shmem.prefix());
        full_name.push_str(&val.name);
        full_name.push_str("_daq");
        let shmem = match cds_shmem_rs::ShMem::new(&full_name, RMIPCS_BUF_SIZE_MB) {
            Ok(mem) => mem,
            Err(_) => return Err(format!("Unable to open input {}", full_name)),
        };
        Ok(Self::from_buffer(val, shmem))
    }

    pub fn get_cycle(&self) -> u32 {
        unsafe { (*self.ipc).get_cycle() }
    }
}

/// BufferQueues are used to communicate between the main process and the background
/// buffer checking code.
pub struct BufferQueues {
    /// new buffers to add to inputs, generated from
    /// the background thread
    pub new_inputs: Vec<Box<InputBuffer>>,
    /// old buffers to close, generated by the
    /// main thread
    pub buffers_to_close: Vec<Box<InputBuffer>>,
}

impl BufferQueues {
    fn new() -> Self {
        BufferQueues {
            new_inputs: Vec::new(),
            buffers_to_close: Vec::new(),
        }
    }
}

pub struct ShareableQueues {
    pub queues: Mutex<BufferQueues>,
}

impl ShareableQueues {
    fn new() -> Self {
        ShareableQueues {
            queues: Mutex::new(BufferQueues::new()),
        }
    }

    pub fn add_inputs(&self, mut inputs: Vec<Box<InputBuffer>>) {
        let mut l = self.queues.lock().unwrap();
        l.new_inputs.append(&mut inputs);
    }

    pub fn get_upto_n_inputs(&self, n: usize) -> Vec<Box<InputBuffer>> {
        let mut results = Vec::new();
        if n == 0 {
            return results;
        }
        let mut l = self.queues.lock().unwrap();
        if l.new_inputs.len() <= n {
            results.append(&mut l.new_inputs);
        } else {
            let start = l.new_inputs.len() - n;
            results = l.new_inputs.drain(start..).collect();
        }
        results
    }

    pub fn add_to_close(&self, mut to_close: Vec<Box<InputBuffer>>) {
        let mut l = self.queues.lock().unwrap();
        l.buffers_to_close.append(&mut to_close);
    }

    pub fn get_to_close(&self) -> Vec<Box<InputBuffer>> {
        let mut results = Vec::new();
        let mut l = self.queues.lock().unwrap();
        results.append(&mut l.buffers_to_close);
        results
    }
}

// Justification for safety - this type is guarded by a mutex
unsafe impl Send for ShareableQueues {}
// Justification for safety - this type is guarded by a mutex
unsafe impl Sync for ShareableQueues {}

pub type SharedBufferQueue = Arc<ShareableQueues>;

pub fn new_shared_buffer_queue() -> SharedBufferQueue {
    return Arc::new(ShareableQueues::new());
}

pub fn get_ptr_at_offset<T>(shmem: &cds_shmem_rs::ShMem, offset: usize) -> *mut T {
    unsafe {
        let raw = shmem.mapping_as::<ffi::c_char>();
        return raw.add(offset) as *mut T;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::mem::size_of;
    use std::ops::Add;

    fn to_string(a: &str, b: &str) -> String {
        let s = String::from(a);
        s.add(b)
    }

    #[test]
    fn test_model_info_new() {
        let test_sequence = [
            ("", ShmemType::Mbuf),
            ("mbuf://", ShmemType::Mbuf),
            ("shm://", ShmemType::Posix),
        ];
        for sequence in test_sequence {
            let (prefix, mem_type) = sequence;
            assert_eq!(
                ModelInfo::new(to_string(prefix, "model").as_str()),
                Some(ModelInfo {
                    shmem: mem_type.clone(),
                    name: String::from("model"),
                    dcuid: 0,
                    rate: 0
                })
            );
            assert_eq!(
                ModelInfo::new(to_string(prefix, "model:5:256").as_str()),
                Some(ModelInfo {
                    shmem: mem_type.clone(),
                    name: String::from("model"),
                    dcuid: 5,
                    rate: 256
                })
            );
            assert_eq!(
                ModelInfo::new(to_string(prefix, "model:5").as_str()),
                Some(ModelInfo {
                    shmem: mem_type.clone(),
                    name: String::from("model"),
                    dcuid: 5,
                    rate: 16
                })
            );
            assert_eq!(
                ModelInfo::new(to_string(prefix, "model").as_str()),
                Some(ModelInfo {
                    shmem: mem_type.clone(),
                    name: String::from("model"),
                    dcuid: 0,
                    rate: 0
                })
            );
            assert_eq!(
                ModelInfo::new(to_string(prefix, "model:52").as_str()),
                Some(ModelInfo {
                    shmem: mem_type.clone(),
                    name: String::from("model"),
                    dcuid: 52,
                    rate: 16
                })
            );
            assert!(ModelInfo::new(to_string(prefix, ":52").as_str()).is_none());
            assert!(ModelInfo::new(to_string(prefix, ":52:100").as_str()).is_none());
            assert_eq!(
                ModelInfo::new(to_string(prefix, "model:52:100").as_str()),
                Some(ModelInfo {
                    shmem: mem_type.clone(),
                    name: String::from("model"),
                    dcuid: 52,
                    rate: 100
                })
            );
            assert!(ModelInfo::new(to_string(prefix, ":blah:100").as_str()).is_none());
            assert_eq!(
                ModelInfo::new(to_string(prefix, "model:blah:blah").as_str()),
                Some(ModelInfo {
                    shmem: mem_type.clone(),
                    name: String::from("model"),
                    dcuid: 0,
                    rate: 0
                })
            );
            assert!(ModelInfo::new(to_string(prefix, ":blah:blah").as_str()).is_none());
            assert!(ModelInfo::new(to_string(prefix, ":42:blah").as_str()).is_none());
            assert!(ModelInfo::new(to_string(prefix, ":42a:blah").as_str()).is_none());
            assert!(ModelInfo::new(to_string(prefix, ":42a:128b").as_str()).is_none());
            assert_eq!(
                ModelInfo::new(to_string(prefix, "model:blah:128").as_str()),
                Some(ModelInfo {
                    shmem: mem_type.clone(),
                    name: String::from("model"),
                    dcuid: 0,
                    rate: 0
                })
            );
            assert_eq!(
                ModelInfo::new(to_string(prefix, "model::128").as_str()),
                Some(ModelInfo {
                    shmem: mem_type.clone(),
                    name: String::from("model"),
                    dcuid: 0,
                    rate: 0
                })
            );
            assert!(ModelInfo::new(to_string(prefix, ":").as_str()).is_none());
            assert!(ModelInfo::new(to_string(prefix, "::").as_str()).is_none());
        }
    }
}
