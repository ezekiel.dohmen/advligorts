use advligorts_rs::{
    daq_multi_cycle_header_t, daq_multi_dcu_header_t, DAQ_NUM_DATA_BLOCKS,
    DAQ_NUM_DATA_BLOCKS_PER_SECOND,
};
use advligorts_rs::{DAQD_MAX_SHMEM_BUFFER_SIZE_MB, DAQD_MIN_SHMEM_BUFFER_SIZE_MB};
use cds_shmem_rs::ShMem;
use std::ffi;
use std::slice::from_raw_parts_mut;

pub struct DaqMultiCycleBlock {
    pub buffer: ShMem,
    pub out_header: *mut daq_multi_cycle_header_t,
    pub out_data: *mut ffi::c_uchar,
    pub cycle_data_size: usize,
}

impl DaqMultiCycleBlock {
    pub fn new(buffer: ShMem) -> Result<Self, String> {
        if buffer.size_mb() < DAQD_MIN_SHMEM_BUFFER_SIZE_MB as usize {
            return Err("The buffer is too small".to_string());
        }
        if buffer.size_mb() > DAQD_MAX_SHMEM_BUFFER_SIZE_MB as usize {
            return Err("The buffer is too large".to_string());
        }
        let out_header = buffer.get_ptr_at_offset::<daq_multi_cycle_header_t>(0)?;
        let out_data = buffer
            .get_ptr_at_offset::<ffi::c_uchar>(std::mem::size_of::<daq_multi_cycle_header_t>())?;
        let max_out_size = buffer.size_mb() * 1024 * 1024;
        let cycle_data_size = (max_out_size - std::mem::size_of::<daq_multi_cycle_header_t>())
            / DAQ_NUM_DATA_BLOCKS as usize;
        let cycle_data_size = cycle_data_size - (cycle_data_size % 8);
        let out = DaqMultiCycleBlock {
            buffer,
            out_header,
            out_data,
            cycle_data_size,
        };
        out.header().set_cur_cycle(50);
        out.header().set_max_cycle(DAQ_NUM_DATA_BLOCKS_PER_SECOND);
        out.header().set_cycle_data_size(out.cycle_data_size as u32);
        out.header().set_msg_crc(0);
        Ok(out)
    }

    pub fn header(&self) -> &mut daq_multi_cycle_header_t {
        unsafe { &mut (*self.out_header) }
    }

    pub fn cycle_data<'a>(
        &self,
        index: u32,
    ) -> Result<(&'a mut daq_multi_dcu_header_t, &'a mut [ffi::c_uchar]), String> {
        if index >= DAQ_NUM_DATA_BLOCKS_PER_SECOND as u32 {
            return Err("Index out of range".to_string());
        }
        let header_size = std::mem::size_of::<daq_multi_dcu_header_t>();
        let data_size = self.cycle_data_size - header_size;
        unsafe {
            let block_ptr = self.out_data.add(self.cycle_data_size * index as usize);
            let header_ptr = block_ptr as *mut daq_multi_dcu_header_t;
            let data_ptr = block_ptr.add(header_size);
            Ok((&mut *header_ptr, from_raw_parts_mut(data_ptr, data_size)))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn empty_buf(mb: usize) -> Vec<u8> {
        let mut buf = Vec::<u8>::new();
        buf.resize(mb * 1024 * 1024, 0);
        buf
    }

    #[test]
    fn test_daq_multi_cycle_data_sizes() {
        let mut out = empty_buf(advligorts_rs::DAQD_MAX_SHMEM_BUFFER_SIZE_MB as usize + 2);
        for i in 1..advligorts_rs::DAQD_MAX_SHMEM_BUFFER_SIZE_MB as usize + 2 {
            let data = out.as_mut_slice();
            let data = &mut data[0..i * 1024 * 1024];
            let shmem = unsafe { cds_shmem_rs::ShMem::new_from_slice(data) };
            let result = DaqMultiCycleBlock::new(shmem);
            if i < advligorts_rs::DAQD_MIN_SHMEM_BUFFER_SIZE_MB as usize
                || i > advligorts_rs::DAQD_MAX_SHMEM_BUFFER_SIZE_MB as usize
            {
                assert!(result.is_err());
            } else {
                assert!(result.is_ok());
            }
        }
    }

    #[test]
    fn test_cycle_data_offsets() {
        let mut out = empty_buf(100);
        let out_shmem = unsafe { cds_shmem_rs::ShMem::new_from_slice(out.as_mut_slice()) };
        let multi_cycle = DaqMultiCycleBlock::new(out_shmem).unwrap();
        for cycle in 0..16u32 {
            let (header, data) = multi_cycle.cycle_data(cycle).unwrap();
            let h = (header as *mut daq_multi_dcu_header_t) as usize;
            let d = data.as_ptr() as usize;
            assert_eq!(
                h + std::mem::size_of::<advligorts_rs::daq_multi_dcu_header_t>(),
                d
            );
        }
        assert!(multi_cycle.cycle_data(16).is_err());
    }
}
