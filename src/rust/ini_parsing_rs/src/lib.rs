#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

use std::ffi;

pub unsafe fn crc_span(input: *const ffi::c_char, length: usize) -> u32 {
    unsafe {
        let crc = crc_ptr(input, length as u32, 0);
        return crc_len(length as u32, crc) as u32;
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_crcs_match() {
        let test_cases = [
            (String::from("abcdefghijklmnopqrstuvwxyz"), 0xa1b937a8),
            (
                String::from("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"),
                0x7073ed5a,
            ),
            (
                String::from("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890"),
                0x207fc642,
            ),
        ];
        for entry in test_cases {
            let bytes = entry.0.into_bytes();
            let expected = entry.1;

            let val_ligo = unsafe { crc_span(bytes.as_ptr() as *const i8, bytes.len()) };

            assert_eq!(expected, val_ligo);
        }
    }
}
