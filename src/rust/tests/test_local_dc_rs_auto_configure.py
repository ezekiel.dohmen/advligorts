
import json
import os
import os.path
import time
import urllib.request
import subprocess


import integration

integration.Executable(name="local_dc_rs", hints=["./target/debug/"], description="local_dc rust version")
integration.Executable(name="fe_stream_check", hints=["../fe_stream_test",], description="verification program")
integration.Executable(name="fe_simulated_streams", hints=["../fe_stream_test/fe_simulation",], description="data stream generator")
integration.Executable(name="mbuf_probe", hints=["../drv/mbuf/mbuf_probe"], description="mbuf probe/info tool")

ini_dir = integration.state.temp_dir('ini_files')
master_file = os.path.join(ini_dir, "master")


def ini(dcuid: int) -> str:
    global ini_dir
    return os.path.join(ini_dir, "mod{0}.ini".format(dcuid))


def par(dcuid: int) -> str:
    global ini_dir
    return os.path.join(ini_dir, "tpchn_mod{0}.par".format(dcuid))


fe_simulated_streams = integration.Process("fe_simulated_streams",
                                           ["-S",
                                            "-i", ini_dir,
                                            "-M", master_file,
                                            "--admin", "127.0.0.1:10000",
                                            "-k", "300",
                                            "-D", "5,6,7",
                                            "--close-daq-on-stop"])

local_dc = integration.Process("local_dc_rs",
                               ["-m", "100",
                                "--auto",
                                "-d", ini_dir,
                                "-b", "local_dc",
                                "--period", "4"])


def stop_model(dcuid:int):

    def do_stop_model():
        integration.log("attempting to stop dcu {0}".format(dcuid))
        payload = json.dumps({
            'dcuid': dcuid,
        }).encode('utf-8')
        req = urllib.request.Request("http://127.0.0.1:10000/api/v1/dcu/{0}/stop/".format(dcuid),
                                     data=payload,
                                     headers={
                                         'Content-Type': 'application/json',
                                     },
                                     method='POST')
        with urllib.request.urlopen(req) as f:
            obj = json.load(f)
            if obj['status'] != 'ok':
                raise RuntimeError('Unexpected response from fe_simulation')

    return do_stop_model


def start_model(dcuid:int):

    def do_start_model():
        integration.log("attempting to start dcu {0}".format(dcuid))
        payload = json.dumps({
            'dcuid': dcuid,
        }).encode('utf-8')
        req = urllib.request.Request("http://127.0.0.1:10000/api/v1/dcu/{0}/start/".format(dcuid),
                                     data=payload,
                                     headers={
                                         'Content-Type': 'application/json',
                                     },
                                     method='POST')
        with urllib.request.urlopen(req) as f:
            obj = json.load(f)
            if obj['status'] != 'ok':
                raise RuntimeError('Unexpected response from fe_simulation')

    return do_start_model


def check_ok(timeout:float):
    end_time = time.time() + timeout

    def do_check():
        global fe_stream_check
        while True:
            state = fe_stream_check.state()
            if state == integration.Process.STOPPED:
                fe_stream_check.ignore()
                return
            elif state == integration.Process.FAILED:
                raise RuntimeError('Data check failed')
            print("state is {0}".format(state))

            if time.time() > end_time:
                raise RuntimeError('Timed out while waiting for check to complete')
            time.sleep(0.5)

    return do_check


def get_cur_dcus() -> set[int]:
    mbuf_probe_cmd = integration.state.get_executable("mbuf_probe").path()
    try:
        results = subprocess.run(args=[mbuf_probe_cmd, "list_dcus", "-m", "100", "-b", "local_dc", "--struct", "daq_multi_cycle"], capture_output=True, encoding="utf-8")
        if results.returncode != 0:
            return set()
        return set(int(x) for x in results.stdout.split(' '))
    except:
        return set()


def wait_for_dcus(dcus:set[int], timeout:int):
    end_time = time.time() + timeout

    def do_check():
        while True:
            actual_dcus = get_cur_dcus()
            print('looking for dcus {0} got dcus {1}'.format(dcus, actual_dcus))
            if actual_dcus == dcus:
                break
            if time.time() > end_time:
                raise RuntimeError('Timed out while waiting for the following dcus to be available {0}'.format(dcus))
            time.sleep(0.5)

    return do_check


def wait_for_models_start(dcuids: [int], timeout:int):
    end_time = time.time() + timeout
    model_names = ["mod{0}_daq".format(dcuid) for dcuid in dcuids]

    def do_check():
        while True:
            try:
                with open('/sys/kernel/mbuf/status', 'rt') as f:
                    data = f.read()
                    matches = 0
                    for model_name in model_names:
                        if data.find(model_name) >= 0:
                            matches += 1
                    if matches == len(model_names):
                        integration.log("{0} have all started".format(model_names))
                        return
            except:
                pass
            if time.time() > end_time:
                raise RuntimeError("Timed out while waiting for {0} to appear", model_names)
            time.sleep(0.1)

    return do_check


def wait_for_model_stop(dcuid: int, timeout:int):
    end_time = time.time() + timeout
    model_name = "mod{0}_daq".format(dcuid)

    def do_check():
        while True:
            try:
                with open('/sys/kernel/mbuf/status', 'rt') as f:
                    data = f.read()
                    if data.find(model_name) < 0:
                        integration.log("{0} has stopped ({1})".format(model_name, data.find(model_name)))
                        integration.log("status file\n'{0}'".format(data))
                        return
                    else:
                        integration.log("{0} has not stopped ({1}) '{2}'".format(model_name, data.find(model_name), data))
            except:
                pass
            if time.time() > end_time:
                raise RuntimeError("Timed out while waiting for {0} to disappear", model_name)
            time.sleep(0.1)

    return do_check


def log_mbufs():
    with open('/sys/kernel/mbuf/status', 'rt') as f:
        integration.log(f.read())


integration.Sequence(
    [
        # integration.state.preserve_files,
        integration.require_readable(files=["/dev/gpstime", "/dev/mbuf"], description="required device files"),
        fe_simulated_streams.run,
        integration.wait(),
        wait_for_models_start([5,6,7], 5),
        stop_model(5),
        wait_for_model_stop(5, 5),
        stop_model(6),
        wait_for_model_stop(6, 5),
        stop_model(7),
        wait_for_model_stop(7, 5),
        log_mbufs,
        local_dc.run,
        integration.wait,
        start_model(5),
        wait_for_dcus({5}, timeout=15),
        start_model(6),
        wait_for_dcus({5, 6}, timeout=15),
        start_model(7),
        wait_for_dcus({5, 6, 7}, timeout=15),
        stop_model(5),
        wait_for_dcus({6,7}, timeout=15),
        wait_for_model_stop(5, 25),
        fe_simulated_streams.ignore,
        local_dc.ignore,
    ]
)