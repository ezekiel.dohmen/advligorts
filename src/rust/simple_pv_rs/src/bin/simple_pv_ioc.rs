use simple_pv_rs::*;
use std::sync::{atomic, Arc, Mutex};
use std::thread::sleep;
use std::time::Duration;

fn main() {
    let prefix = "X1:";
    let val_a = Arc::new(atomic::AtomicI32::new(0));
    let val_b = Arc::new(atomic::AtomicU16::new(0));
    let val_c = Arc::new(AtomicFloat32::new(1.1f32));
    let val_d = Arc::new(AtomicFloat64::new(1.2f64));
    let val_e: Arc<StringPVData> = Arc::new(StringPVData {
        data: Mutex::new("".to_string()),
    });
    let mut server = Server::new(prefix).expect("Could not create server");
    server
        .add_pv(PV::Int32(Int32PV {
            name: "VAL_A".to_string(),
            data: val_a.clone(),
            access_mode: ReadWrite::ReadWrite,
            alarms: AlarmBounds {
                alarm_high: 100,
                alarm_low: -100,
                warn_high: 100,
                warn_low: -100,
            },
        }))
        .expect("Could not add val_a");
    server
        .add_pv(PV::UInt16(UInt16PV {
            name: "VAL_B".to_string(),
            data: val_b.clone(),
            access_mode: ReadWrite::ReadWrite,
            alarms: AlarmBounds {
                alarm_high: 100,
                alarm_low: 0,
                warn_high: 100,
                warn_low: 0,
            },
        }))
        .expect("Could not add val_b");
    server
        .add_pv(PV::Float32(Float32PV {
            name: "VAL_C".to_string(),
            data: val_c.clone(),
            access_mode: ReadWrite::ReadWrite,
            alarms: AlarmBounds {
                alarm_high: 100.0,
                alarm_low: 0.0,
                warn_high: -100.0,
                warn_low: 0.0,
            },
        }))
        .expect("Could not add val_c");
    server
        .add_pv(PV::Float64(Float64PV {
            name: "VAL_D".to_string(),
            data: val_d.clone(),
            access_mode: ReadWrite::ReadWrite,
            alarms: AlarmBounds {
                alarm_high: 100.0,
                alarm_low: 0.0,
                warn_high: -100.0,
                warn_low: 0.0,
            },
        }))
        .expect("Could not add val_d");
    server
        .add_pv(PV::String(StringPV {
            name: "VAL_E".to_string(),
            data: val_e.clone(),
            access_mode: ReadWrite::ReadWrite,
        }))
        .expect("Could not add val_e");
    loop {
        println!(
            "{0}VAL_A={1}\n{0}VAL_B={2}\n{0}VAL_C={3}\n{0}VAL_D={4}\n{0}VAL_E=\"{5}\"\n--\n",
            &prefix,
            val_a.load(atomic::Ordering::SeqCst),
            val_b.load(atomic::Ordering::SeqCst),
            val_c.load(atomic::Ordering::SeqCst),
            val_d.load(atomic::Ordering::SeqCst),
            val_e.data.lock().unwrap(),
        );
        for _i in 0..10 {
            sleep(Duration::from_millis(100));
            server.update();
        }
    }
}
