use std::error::Error;
use std::ffi::{CStr, CString};
use std::fmt::{Debug, Display, Formatter};
use std::os::raw::{c_char, c_int, c_void};
use std::sync::{atomic, Arc, Mutex};

use simple_pv_rs_sys::{
    simple_pv_server_add_float32, simple_pv_server_add_float64, simple_pv_server_add_int32,
    simple_pv_server_add_string, simple_pv_server_add_uint16, simple_pv_server_create,
    simple_pv_server_destroy, simple_pv_server_update, string_value_create, SimpleFloat32PV,
    SimpleFloat64PV, SimpleInt32PV, SimpleStringPV, SimpleUInt16PV, StringPVIO, StringValue,
};

pub struct AtomicFloat64 {
    data: atomic::AtomicU64,
}

impl AtomicFloat64 {
    pub fn new(val: f64) -> AtomicFloat64 {
        AtomicFloat64 {
            data: atomic::AtomicU64::new(f64::to_bits(val)),
        }
    }

    pub fn load(&self, ordering: atomic::Ordering) -> f64 {
        f64::from_bits(self.data.load(ordering))
    }

    pub fn store(&self, val: f64, ordering: atomic::Ordering) {
        self.data.store(f64::to_bits(val), ordering);
    }

    pub fn as_ptr(&self) -> *mut f64 {
        unsafe { std::mem::transmute::<*mut u64, *mut f64>(self.data.as_ptr()) }
    }
}

pub struct AtomicFloat32 {
    data: atomic::AtomicU32,
}

impl AtomicFloat32 {
    pub fn new(val: f32) -> AtomicFloat32 {
        AtomicFloat32 {
            data: atomic::AtomicU32::new(f32::to_bits(val)),
        }
    }

    pub fn load(&self, ordering: atomic::Ordering) -> f32 {
        f32::from_bits(self.data.load(ordering))
    }

    pub fn store(&self, val: f32, ordering: atomic::Ordering) {
        self.data.store(f32::to_bits(val), ordering);
    }

    pub fn as_ptr(&self) -> *mut f32 {
        unsafe { std::mem::transmute::<*mut u32, *mut f32>(self.data.as_ptr()) }
    }
}

pub struct StringPVData {
    pub data: Mutex<String>,
}

extern "C" fn read_stringpv_data(handle: *mut c_void) -> StringValue {
    let pv_data: &mut StringPVData =
        unsafe { &mut *std::mem::transmute::<*mut c_void, *mut StringPVData>(handle) };
    let data = pv_data.data.lock().unwrap();
    let c_data = match CString::new(data.as_str()) {
        Ok(s) => s,
        Err(_) => unsafe {
            return string_value_create(std::ptr::null());
        },
    };
    unsafe { string_value_create(c_data.as_ptr()) }
}

extern "C" fn write_stringpv_data(handle: *mut c_void, c_data: *const c_char) -> c_int {
    // https://stackoverflow.com/questions/24145823/how-do-i-convert-a-c-string-into-a-rust-string-and-back-via-ffi
    let c_str_data = unsafe { CStr::from_ptr(c_data) };
    let source = match c_str_data.to_str() {
        Ok(s) => String::from(s),
        Err(_) => return 0,
    };
    let pv_data: &mut StringPVData =
        unsafe { &mut *std::mem::transmute::<*mut c_void, *mut StringPVData>(handle) };
    let mut data = pv_data.data.lock().unwrap();
    *data = source;
    1
}

pub enum PVError {
    PVAddError,
}

impl Debug for PVError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            PVError::PVAddError => f.write_str("Error adding a PV"),
        }
    }
}

impl Display for PVError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            PVError::PVAddError => f.write_str("<pvadd>"),
        }
    }
}

impl std::error::Error for PVError {}

pub enum ReadWrite {
    ReadOnly,
    ReadWrite,
}

impl ReadWrite {
    fn to_cint(&self) -> c_int {
        match self {
            ReadWrite::ReadOnly => 0,
            ReadWrite::ReadWrite => 1,
        }
    }
}

pub struct AlarmBounds<T> {
    pub alarm_high: T,
    pub alarm_low: T,
    pub warn_high: T,
    pub warn_low: T,
}

pub struct UInt16PV {
    pub name: String,
    pub data: Arc<atomic::AtomicU16>,
    pub access_mode: ReadWrite,
    pub alarms: AlarmBounds<u16>,
}

pub struct Int32PV {
    pub name: String,
    pub data: Arc<atomic::AtomicI32>,
    pub access_mode: ReadWrite,
    pub alarms: AlarmBounds<i32>,
}

pub struct Float32PV {
    pub name: String,
    pub data: Arc<AtomicFloat32>,
    pub access_mode: ReadWrite,
    pub alarms: AlarmBounds<f32>,
}

pub struct Float64PV {
    pub name: String,
    pub data: Arc<AtomicFloat64>,
    pub access_mode: ReadWrite,
    pub alarms: AlarmBounds<f64>,
}

pub struct StringPV {
    pub name: String,
    pub data: Arc<StringPVData>,
    pub access_mode: ReadWrite,
}

pub enum PV {
    UInt16(UInt16PV),
    Int32(Int32PV),
    Float32(Float32PV),
    Float64(Float64PV),
    String(StringPV),
}

enum PVArc {
    UInt16(Arc<atomic::AtomicU16>),
    Int32(Arc<atomic::AtomicI32>),
    Float32(Arc<AtomicFloat32>),
    Float64(Arc<AtomicFloat64>),
    String(Arc<StringPVData>),
}

pub struct Server {
    handle: *mut c_void,
    pv_ptrs: Vec<PVArc>,
}

impl Server {
    pub fn new(prefix: &str) -> Option<Self> {
        let c_prefix = CString::new(prefix).ok()?;
        let handle =
            unsafe { simple_pv_server_create(c_prefix.as_ptr(), std::ptr::null_mut(), 0 as c_int) };
        println!("Created server");
        return Some(Server {
            handle,
            pv_ptrs: Vec::new(),
        });
    }

    pub fn add_pv(&mut self, pv: PV) -> Result<bool, Box<dyn Error>> {
        let lifetime_extension: PVArc;
        let ok = match pv {
            PV::UInt16(p) => {
                let cname = CString::new(p.name.as_str())?;
                lifetime_extension = PVArc::UInt16(p.data.clone());
                unsafe {
                    simple_pv_server_add_uint16(
                        self.handle,
                        SimpleUInt16PV {
                            name: cname.as_ptr(),
                            data: p.data.as_ptr() as *mut c_void,
                            read_write: p.access_mode.to_cint(),
                            alarm_high: p.alarms.alarm_high,
                            alarm_low: p.alarms.alarm_low,
                            warn_high: p.alarms.warn_high,
                            warn_low: p.alarms.warn_low,
                        },
                    )
                }
            }
            PV::Int32(p) => {
                let cname = CString::new(p.name.as_str())?;
                lifetime_extension = PVArc::Int32(p.data.clone());
                unsafe {
                    simple_pv_server_add_int32(
                        self.handle,
                        SimpleInt32PV {
                            name: cname.as_ptr(),
                            data: p.data.as_ptr() as *mut c_void,
                            read_write: p.access_mode.to_cint(),
                            alarm_high: p.alarms.alarm_high,
                            alarm_low: p.alarms.alarm_low,
                            warn_high: p.alarms.warn_high,
                            warn_low: p.alarms.warn_low,
                        },
                    )
                }
            }
            PV::Float32(p) => {
                let cname = CString::new(p.name.as_str())?;
                lifetime_extension = PVArc::Float32(p.data.clone());
                unsafe {
                    simple_pv_server_add_float32(
                        self.handle,
                        SimpleFloat32PV {
                            name: cname.as_ptr(),
                            data: p.data.as_ptr() as *mut c_void,
                            read_write: p.access_mode.to_cint(),
                            alarm_high: p.alarms.alarm_high,
                            alarm_low: p.alarms.alarm_low,
                            warn_high: p.alarms.warn_high,
                            warn_low: p.alarms.warn_low,
                        },
                    )
                }
            }
            PV::Float64(p) => {
                let cname = CString::new(p.name.as_str())?;
                lifetime_extension = PVArc::Float64(p.data.clone());
                unsafe {
                    simple_pv_server_add_float64(
                        self.handle,
                        SimpleFloat64PV {
                            name: cname.as_ptr(),
                            data: p.data.as_ptr() as *mut c_void,
                            read_write: p.access_mode.to_cint(),
                            alarm_high: p.alarms.alarm_high,
                            alarm_low: p.alarms.alarm_low,
                            warn_high: p.alarms.warn_high,
                            warn_low: p.alarms.warn_low,
                        },
                    )
                }
            }
            PV::String(p) => {
                let cname = CString::new(p.name.as_str())?;
                lifetime_extension = PVArc::String(p.data.clone());
                unsafe {
                    let io = StringPVIO {
                        user_handle: Arc::as_ptr(&p.data) as *mut c_void,
                        read_fn: Some(read_stringpv_data),
                        write_fn: Some(write_stringpv_data),
                    };
                    simple_pv_server_add_string(
                        self.handle,
                        SimpleStringPV {
                            name: cname.as_ptr(),
                            io,
                            read_write: p.access_mode.to_cint(),
                        },
                    )
                }
            }
        };
        if ok != 1 as c_int {
            return Err(Box::new(PVError::PVAddError));
        }
        self.pv_ptrs.push(lifetime_extension);
        Ok(true)
    }

    pub fn update(&self) {
        unsafe {
            simple_pv_server_update(self.handle);
        }
    }
}

impl Drop for Server {
    fn drop(&mut self) {
        unsafe {
            simple_pv_server_destroy(&mut self.handle);
        }
        println!("Dropped server");
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_sizes() {
        assert_eq!(
            std::mem::size_of::<f32>(),
            std::mem::size_of::<AtomicFloat32>()
        );
        assert_eq!(
            std::mem::size_of::<f64>(),
            std::mem::size_of::<AtomicFloat64>()
        );
    }
}
