use std::env;
use std::path::PathBuf;

fn main() {
    let mut simple_pv = PathBuf::from(env::var("OUT_DIR").unwrap());
    for _i in 0..6 {
        if !simple_pv.pop() {
            panic!("cannot create the path to simple_pv")
        }
    }
    simple_pv.push("simple_pv");
    println!("cargo:rustc-link-search={}", simple_pv.to_str().unwrap());
    println!("cargo:rustc-link-lib=dylib=simple_pv_so");
    println!("cargo:rerun-if-change=wrapper.h");

    let bindings = bindgen::Builder::default()
        .clang_args(["-I../../"])
        .header("wrapper.h")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings");
}
