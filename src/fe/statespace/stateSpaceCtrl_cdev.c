
#include "part_headers/statespace/stateSpacePart.h"
#include "part_headers/statespace/stateSpaceCtrl_ioctl.h"

//rts-logger
#include "drv/rts-logger.h" 

#include <linux/kernel.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include<linux/uaccess.h>              //copy_to/from_user()
#include <linux/ioctl.h>
#include <linux/err.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/mutex.h>

//256 Hz model could take ~3907 us
#define NUM_SLEEPS_TO_WAIT_FOR_PART_LOAD 1000
#define MIN_SLEEP_TIME_US 5

//Global kernel-space buffer for config load request
static uint8_t g_new_cfg_ptr[STATE_SPACE_MAX_BUFFER] = {0,};

//Mutex to only allow one request at a time
static struct mutex g_ioctl_mutex;

//Global char dev data
dev_t g_dev = 0;
static struct class *g_dev_class;
static struct cdev g_cdev;

static int      stateSpaceCtrl_open(struct inode *inode, struct file *file);
static int      stateSpaceCtrl_release(struct inode *inode, struct file *file);
static ssize_t  stateSpaceCtrl_read(struct file *filp, char __user *buf, size_t len, loff_t * off);
static ssize_t  stateSpaceCtrl_write(struct file *filp, const char *buf, size_t len, loff_t * off);
static long     stateSpaceCtrl_ioctl(struct file *file, unsigned int cmd, unsigned long arg);

/*
static struct proc_ops sstateSpaceCtrl_file_ops = {
	.proc_open    = stateSpaceCtrl_open,
	.proc_release = stateSpaceCtrl_release,
	.proc_read    = stateSpaceCtrl_read,
    .proc_write   = stateSpaceCtrl_write,
    .proc_ioctl   = stateSpaceCtrl_ioctl
};*/

static struct file_operations fops =
{
        .owner          = THIS_MODULE,
        .read           = stateSpaceCtrl_read,
        .write          = stateSpaceCtrl_write,
        .open           = stateSpaceCtrl_open,
        .unlocked_ioctl = stateSpaceCtrl_ioctl,
        .release        = stateSpaceCtrl_release,
};


static int      stateSpaceCtrl_open(struct inode *inode, struct file *file)
{
    return 0;
}

static int      stateSpaceCtrl_release(struct inode *inode, struct file *file)
{
    return 0;
}

static ssize_t  stateSpaceCtrl_read(struct file *filp, char __user *buf, size_t len, loff_t * off)
{
    return 0;
}

static ssize_t  stateSpaceCtrl_write(struct file *filp, const char *buf, size_t len, loff_t * off)
{
    return 0;
}

static long     stateSpaceCtrl_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    unsigned num_parts = getNumPartsStateSpace();
    unsigned req_part_index, data_block_sz, wait_count=0;
    char * part_name_ptr;
    struct stateSpace_get_name_req_t * get_name_req_usr_ptr = (struct stateSpace_get_name_req_t *) arg;
    struct stateSpace_part_config_t * config_req_usr_ptr = (struct stateSpace_part_config_t * ) arg;
    struct stateSpace_part_config_t config_hold_kern;
    int ret_val = -EINVAL;

    if ( num_parts == 0) {
        RTSLOG_ERROR("stateSpaceCtrl_cdev - There are no stacespace parts for this model.\n");
        return -EINVAL;
    }

    mutex_lock(&g_ioctl_mutex);

    switch(cmd) 
    {
        case STATE_SPACE_GET_NUM_PARTS:
            if ( copy_to_user( (void*) arg, &num_parts, sizeof(unsigned) ) != 0) {
                ret_val = -EFAULT;
                goto exit_ioctl;
            }
            ret_val = 0;
        break;

        case STATE_SPACE_GET_PART_NAME:
            

            //Check pointer is ok
            if ( !access_ok( (void*) get_name_req_usr_ptr, sizeof(struct stateSpace_get_name_req_t) ) ) {
                ret_val = -EFAULT;
                goto exit_ioctl;
            }

            //Copy and check requested index
            if ( copy_from_user( &req_part_index, (void*) get_name_req_usr_ptr, sizeof(unsigned) ) != 0)
            {
                RTSLOG_ERROR("stateSpaceCtrl_cdev - copy_from_user() failed\n");
                ret_val = -EINVAL;
                goto exit_ioctl;
            } 
            if( req_part_index >= num_parts )
            {
                RTSLOG_ERROR("stateSpaceCtrl_cdev - The requested part index of %d is too high.\n", req_part_index);
                ret_val = -EINVAL;
                goto exit_ioctl;
            }

            getPartNamePtrStateSpace(req_part_index, &part_name_ptr);
            if( copy_to_user(&get_name_req_usr_ptr->part_name, part_name_ptr, STATE_SPACE_ALLOC_NAME_LEN) != 0)
            {
                RTSLOG_ERROR("stateSpaceCtrl_cdev - copy_to_user() failed\n");
                ret_val = -EINVAL;
                goto exit_ioctl;
            }
            ret_val = 0;

        break;

        case STATE_SPACE_CONFIGURE_PART:

            //We only read, we we don't need access_ok checks

            //Copy and check block header
            if ( copy_from_user( &config_hold_kern, (void*) config_req_usr_ptr, sizeof(config_hold_kern) ) != 0)
            {
                RTSLOG_ERROR("stateSpaceCtrl_cdev - copy_from_user() failed\n");
                ret_val = -EINVAL;
                goto exit_ioctl;
            } 

            if( config_hold_kern.part_index >= num_parts ) //Make sure index is good
            {
                RTSLOG_ERROR("stateSpaceCtrl_cdev - The requested part index of %d is too high.\n", req_part_index);
                ret_val = -EINVAL;
                goto exit_ioctl;
            }

            //Now we know the size of the data block, copy everything over
            data_block_sz = calcDataBlockSzStateSpace(
                                config_hold_kern.input_vec_len,
                                config_hold_kern.output_vec_len, 
                                config_hold_kern.state_vec_len);

            //Make sure the request is not too big for our buffer
            if( data_block_sz + sizeof(struct stateSpace_part_config_t) > STATE_SPACE_MAX_BUFFER) { 
                RTSLOG_ERROR("stateSpaceCtrl_cdev - A total part buffer size of %d bytes is too large.\n", data_block_sz);
                ret_val = -EINVAL;
                goto exit_ioctl;
            }

            if ( copy_from_user( g_new_cfg_ptr, (void*) config_req_usr_ptr, 
                                 data_block_sz + sizeof(struct stateSpace_part_config_t) ) != 0)
            {
                RTSLOG_ERROR("stateSpaceCtrl_cdev - copy_from_user() failed, rest of stateSpace_part_config could not be copied.\n");
                ret_val = -EINVAL;
                goto exit_ioctl;
            } 




            struct stateSpace_part_config_t * kern_config_ptr = (struct stateSpace_part_config_t *) g_new_cfg_ptr;

            unsigned state_matrix_offset = kern_config_ptr->state_vec_len;
            unsigned in_matrix_offset = state_matrix_offset + kern_config_ptr->state_vec_len * kern_config_ptr->state_vec_len;
            unsigned out_matrix_offset = in_matrix_offset + kern_config_ptr->state_vec_len * kern_config_ptr->input_vec_len;
            unsigned feedthrough_matrix_offset = out_matrix_offset + kern_config_ptr->output_vec_len * kern_config_ptr->state_vec_len;

            //RTSLOG_INFO("Total buffer size %u\n", data_block_sz + sizeof(struct stateSpace_part_config_t) );
            //RTSLOG_INFO("out_matrix_offset %u, addr: %lx\n", out_matrix_offset,  &kern_config_ptr->data_block[out_matrix_offset]);

            
            //Call the function to update the parameters in the model
            if( matrixConfigureStateSpace(
                kern_config_ptr->part_index, 
                kern_config_ptr->input_vec_len,
                kern_config_ptr->output_vec_len, 
                kern_config_ptr->state_vec_len, 
                &kern_config_ptr->data_block[0], //init state vec
                &kern_config_ptr->data_block[state_matrix_offset], //state matrix
                &kern_config_ptr->data_block[in_matrix_offset], //input matrix
                &kern_config_ptr->data_block[out_matrix_offset], //output matrix
                &kern_config_ptr->data_block[feedthrough_matrix_offset]) //feedthrough matrix
                != 0 )
            {
                RTSLOG_ERROR("stateSpaceCtrl_cdev - matrixConfigureStateSpace() returned an error.\n");
                ret_val = -EINVAL;
                goto exit_ioctl;
            }

            //Wait until the RT model loads the new config into the part
            while( check_pending_config() != -1 && 
                   wait_count < NUM_SLEEPS_TO_WAIT_FOR_PART_LOAD) {
                usleep_range(MIN_SLEEP_TIME_US, MIN_SLEEP_TIME_US + 5);
                ++wait_count;
            }
            //Make sure we didn't time out
            if( wait_count ==  NUM_SLEEPS_TO_WAIT_FOR_PART_LOAD) {
                RTSLOG_ERROR("stateSpaceCtrl_cdev - Part %d was not loaded by the RT model before the timeout\n", 
                kern_config_ptr->part_index);
                ret_val = -ETIMEDOUT;
                goto exit_ioctl;
            }
            ret_val = 0;

        break;

        case STATE_SPACE_GET_PART_CONFIG:

            //Check pointer is ok to read data
            if ( !access_ok( (void*) config_req_usr_ptr, sizeof(struct stateSpace_part_config_t) ) ) {
                ret_val = -EFAULT;
                goto exit_ioctl;
            }

            //Copy and check requested index
            if ( copy_from_user( &req_part_index, (void*) &config_req_usr_ptr->part_index, sizeof(unsigned) ) != 0)
            {
                RTSLOG_ERROR("stateSpaceCtrl_cdev - copy_from_user() failed\n");
                ret_val = -EINVAL;
                goto exit_ioctl;
            } 
            if( req_part_index >= num_parts )
            {
                RTSLOG_ERROR("stateSpaceCtrl_cdev - The requested part index of %d is too high.\n", req_part_index);
                ret_val = -EINVAL;
                goto exit_ioctl;
            }


            config_hold_kern.ioctl_version = STATE_SPACE_IOCTL_V0;
            getConfigOfPartStateSpace(req_part_index, &config_hold_kern);
            if( copy_to_user(config_req_usr_ptr, &config_hold_kern, sizeof(struct stateSpace_part_config_t)) != 0)
            {
                RTSLOG_ERROR("stateSpaceCtrl_cdev - copy_to_user() failed\n");
                ret_val = -EINVAL;
                goto exit_ioctl;
            }

            ret_val = 0;
        break;

        case STATE_SPACE_GET_PART_DATA:

            //Copy and check block header
            if ( copy_from_user( &config_hold_kern, (void*) config_req_usr_ptr, sizeof(config_hold_kern) ) != 0)
            {
                RTSLOG_ERROR("stateSpaceCtrl_cdev - copy_from_user() failed\n");
                ret_val = -EINVAL;
                goto exit_ioctl;
            } 

            req_part_index = config_hold_kern.part_index;
            if( req_part_index >= num_parts ) //Make sure index is good
            {
                RTSLOG_ERROR("stateSpaceCtrl_cdev - The requested part index of %d is too high.\n", req_part_index);
                ret_val = -EINVAL;
                goto exit_ioctl;
            }


            //Get the config of the part
            getConfigOfPartStateSpace(req_part_index, &config_hold_kern);


            //Calc size of matrix data
            data_block_sz = calcDataBlockSzStateSpace(
                                config_hold_kern.input_vec_len,
                                config_hold_kern.output_vec_len, 
                                config_hold_kern.state_vec_len);

            //Make sure the request is not too big for our buffer
            if( data_block_sz + sizeof(struct stateSpace_part_config_t) > STATE_SPACE_MAX_BUFFER) { 
                RTSLOG_ERROR("stateSpaceCtrl_cdev - The configured part buffer size of %d bytes is too large.\n", data_block_sz);
                ret_val = -EINVAL;
                goto exit_ioctl;
            }

            //Check pointer is ok to write data
            if ( !access_ok( (void*) config_req_usr_ptr, sizeof(struct stateSpace_part_config_t) + data_block_sz ) )
            {
                RTSLOG_ERROR("stateSpaceCtrl_cdev - The ioctl caller did not allocate enough space fore the return.\n");
                ret_val = -EFAULT;
                goto exit_ioctl;
            }

            
            ((struct stateSpace_part_config_t *)g_new_cfg_ptr)->ioctl_version = STATE_SPACE_IOCTL_V0;

            if ( getConfigAndDataOfPartStateSpace(req_part_index, data_block_sz, (void*)g_new_cfg_ptr) != 0 ) {
                RTSLOG_ERROR("stateSpaceCtrl_cdev - getConfigAndDataOfPartStateSpace(%d) failed, has the part been configured?\n", req_part_index);
                ret_val = -EINVAL;
                goto exit_ioctl;
            }
            if( copy_to_user(config_req_usr_ptr, g_new_cfg_ptr, sizeof(struct stateSpace_part_config_t) + data_block_sz ) != 0)
            {
                RTSLOG_ERROR("stateSpaceCtrl_cdev - copy_to_user() failed\n");
                ret_val = -EINVAL;
                goto exit_ioctl;
            }
            ret_val = 0;
        break;
            
    }

exit_ioctl:
    mutex_unlock(&g_ioctl_mutex);
    return ret_val;

}

int initStateSpaceCtrl_cdev( const char * model_name)
{
    mutex_init(&g_ioctl_mutex);

    // Allocating Major number
    if((alloc_chrdev_region(&g_dev, 0, 1, model_name)) <0){
        RTSLOG_ERROR("Cannot allocate cdev_name: %s major number\n", model_name);
        return -1;
    }
    RTSLOG_INFO("initStateSpaceCtrl_cdev() - Major = %d Minor = %d \n",MAJOR(g_dev), MINOR(g_dev));

    // Creating cdev structure
    cdev_init(&g_cdev, &fops);

    // Adding character device to the system
    if((cdev_add(&g_cdev, g_dev, 1)) < 0){
        RTSLOG_ERROR("Cannot add the device to the system\n");
        goto r_class;
    }

    // Creating struct class
    if(IS_ERR(g_dev_class = class_create(THIS_MODULE, model_name ))){
        RTSLOG_ERROR("Cannot create the struct class\n");
        goto r_class;
    }

    // Creating device
    if(IS_ERR(device_create(g_dev_class, NULL, g_dev, NULL, model_name))){
        RTSLOG_ERROR("Cannot create the Device 1\n");
        goto r_device;
    }
    RTSLOG_INFO("initStateSpaceCtrl_cdev() - cdev %s creation complete.\n", model_name);
    return 0;
 
r_device:
    class_destroy(g_dev_class);
r_class:
    unregister_chrdev_region(g_dev,1);
    return -1;
}

void cleanupStateSpaceCtrl_cdev( void )
{
    device_destroy(g_dev_class, g_dev);
    class_destroy(g_dev_class);
    cdev_del(&g_cdev);
    unregister_chrdev_region(g_dev, 1);
}