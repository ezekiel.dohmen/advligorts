/// @file verify_card_count.c
/// @brief Helper file for moduleLoad.c to check prior IO card counts/types.

#include "verify_card_count.h"
#include "controller.h" //CDS_CARD_TYPES, IO_CONFIG_ERROR, 
#include "drv/rts-logger.h"


void
initialize_card_counts( CDS_HARDWARE* pCds, int cards_per_model[] )
{
    int ii;

    pCds->adcCount = 0;
    pCds->dacCount = 0;
    pCds->dioCount = 0;
    pCds->doCount = 0;

    for ( ii = 0; ii < MAX_IO_MODULES; ii++ )
    {
        cards_per_model[ ii ] = 0;
        pCds->card_count[ ii ] = 0;
    }
    // Determine total number of cards model is looking for by type
    for ( ii = 0; ii < pCds->cards; ii++ )
    {
        cards_per_model[ pCds->cards_used[ ii ].type ]++;
        // Contec6464 DIO are split into 2 cards, so add another
        if ( pCds->cards_used[ ii ].type == CON_6464DIO )
            cards_per_model[ CON_6464DIO ]++;
    }
}

int
verify_card_count( CDS_HARDWARE* pCds, int model_cards[], const char* sysname )
{
    int  ii;
    int  errStat = 0;

    for ( ii = 0; ii < CDS_CARD_TYPES; ii++ )
    {
        if ( pCds->card_count[ ii ] < model_cards[ ii ] && 
		ii != CON_1616DIO )
        {
            errStat = IO_CONFIG_ERROR;
#ifdef REQUIRE_IO_CNT
            RTSLOG_ERROR( "Did not find correct number of %s cards \n",
                          _cdscardtypename[ ii ] );
            RTSLOG_ERROR( "Expected %d and found %d - exiting\n",
                    model_cards[ ii ],
                    pCds->card_count[ ii ] );
#else
            RTSLOG_WARN( "Did not find correct number of %s cards \n",
                    _cdscardtypename[ ii ] );
            RTSLOG_WARN( "Expected %d and found %d \n",
                    model_cards[ ii ],
                    pCds->card_count[ ii ] );
#endif
        }
    }

    return errStat;
}
