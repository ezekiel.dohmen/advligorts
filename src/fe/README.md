# Front End (FE)

Software in this directory is used as the core common logic, to build the FE computer real time models.

## Overview 

The RCG supports 2 basic real time model configurations, a brief over overview of the relevant (to this directory) functions is below. For more general information take a look at the [Model Class Types page](https://git.ligo.org/cds/software/advligorts/-/wikis/Model-Class-Types)

### IOP (Input/Output) Model
- FE synchronization to Timing Distribution System (TDS)
- Read data from ADC modules 
- Write data to DAC modules
- Provide pointers to digital I/O and real-time IPC (Dolphin) network

### User/Control Models 
- Provide the actual control algorithms

## CDS Parameter Block
There are various configuration options to both of these model types. The options are set using the Parameter Block settings in the Matlab `.mdl` file. Pictured is an example parameter block showing the options selected for the h1ioplsc0 IOP model.

![image.png](./image.png)

### Required Common Parameters 
Both IOP and User model require a set of common parameters, they are as follows. 

- `ifo=IFO_DESIGNATOR` 
    - e.g. L1,H1,X2
    - NOTE: 'site' was the previous designator for this, but erroneously named. While
      following old site parameter is still recognized, in lieu of ifo, it
      will soon be deprecated.
- `rate=MODEL_CYCLE_RATE`
    - standard `64K` for production systems
- `dcuid=DCUID`
    - A system unique number for data acquisition
    - NOTE: 1 thru 6 and 14,15 are reserved for DAQ and may not be used
- `host=COMPUTER_HOST_NAME`
    - Hostname of computer on which this code is to run


## IOP Kernel Module Software 

### Code where core IOP functions are implemented

- `fe/controllerIop.c`
    -  Primary infinite control loop; controls I/O and timing
- `fe/moduleLoad.c`
    - Handles kernel module loading and unloading
- `fe/dolphin.c`
    - Provides setup and mapping of Dolphin network for IPCs
- `fe/map.c`
    - Used to map PCIe I/O modules
- `include/drv/iop_adc_functions.c`
    - Handles ADC I/O
- `include/drv/iop_dac_functions.c`
    - Handles DAC I/O
- `include/drv/dac_info.c`
    - Provides DAC card status info
- `include/drv/adc_info.c`
    - Provides ADC card status info

### Common IOP Configuration Block Settings

The RCG uses model Parameter block entries to obtain compile options.

#### Required Model Parameter Block Entries for all IOP configurations

Besides the required common parameters, IOP models must also have the following:
 
- `iop_model=1`
    - Indicates this model is to be compiled as an IOP
    
### Parameter Block Entries for Specific Use Cases

#### Standard LIGO Configuration (SLC) 
This is defined as a FE computer that has an I/O chassis (IOC) connected,
via a PCIe or short range fiber optic cable. 

##### In this configuration the IOC contains all of the following

1. LIGO timing receiver module connected to the LIGO Timing Distribution System (TDS).
2. Contec1616 binary I/O (BIO) to control the timing receiver
3. IRIG-B time code receiver connected to TDS IRIG timing 
4. At least one ADC module 
5. No more than combined total of 24 ADC/DAC modules

###### There are a number of compile options for the SLC:

- `pciRfm=1` Used in most LIGO systems, indicates connection to the Dolphin network for real-time comms between FE computers. FE code with use Dolphin Gen2 drivers. This option is set if Dolphin Gen1 is desired. TODO: Is this deprecated?
- `dolphin_time_xmit=1` This IOP will send GPS time (seconds) and  cycle count (0-65535). This is used to trigger IOP timing for FE computers that do not have IOC connected.
> :warning: There can only be one time master on the dolphin network. i.e. One IOP model configured with `dolphin_time_xmit=1`

#### Standard configuration EXCEPT no LIGO TDS 
In this configuration, an ADC/DAC clocking signal is provided via a non-LIGO TDS ie the clock for I/O modules is provided by some other method than via a LIGO timing receiver. Options here are:

1. A 64K clock is provided to clock the I/O module and a 1PPS sync signal is provided on the last channel of the first ADC. No special Parameter block setting is required. On IOP startup, if it does not find a Contec1616 module to control the timing signal, it will fail over to trying to sync to a 1PPS signal.
2. A 64K clock is provided without 1PPS sync. This is the configuration commonly referred to as a Cymac. Any 65536Hz TTL clock can be used. For this configuration, set the  `no_sync=1` parameter.
    > :warning: If there are multiple ADC modules in the system, the IOP may have issues getting all of them to start on the same clock cycle. This would be noticed by long IOP cycle times. An IOP restart may, or may not, correct this.

#### FE computer without an IOC or I/O modules connected
There are 2 options for running an IOP on a FE without I/O modules:

1. As an FE connected to a Dolphin network on a distributed system.
    > In LIGO production systems, this is used on FE that use IPC inputs from other FE exclusively in performing their calculations and only use IPCs to output control signals.
    - Parameter block entries required for this use case:
        1. `pciRfm=1`
        2. `dolphin_time_rime_rcvr=1`
2. As a "virtual" machine for testing on a standalone computer.
    > In this mode, the IOP will use the CPU time to control its timing.
    1. Parameter block entries required for this use case:
        1.  `virtualIOP=1`
    2. Additional notes
        - ADC input signals will be static. The value for each ADC card input will equal its channel number eg chan 0 = 0, chan 1 = 1, etc. Ability to change ADC input values dynamically is on the TODO list.
        - Any IPCs defined in the IOP model will be set to use shared memory (SHM) type, regardless of type setting in the .mdl file. Since there are no I/O cards, this is the only option.


#### Long Range IOP Configuration

When the IO chassis is connected over a long range fiber, such as mid station PEM:, set the following in the Parameter block:
1. `rate=64K`
2. `clock_div=2`

In this configuration, the IOP waits for 2 adc samples/chan to begin a processing cycle, instead of the usual one sample. The code will run thru 2 complete cycles every time it is triggered and continue to pass ADC data to user models at the standard 64K rate.

#### High Speed ADC Configuration (512KHz Clock)

> :warning: TESTING OF THIS CONFIGURATION IS NOT COMPLETE

For a specific A+ application, a 1MS/sec ADC is clocked at 512KHz. The IOP gets triggered every 8 samples, processes data at 512K, and passes data to user models at the standard 64K rate (no downsample filtering).

##### Necessary Parameter Block settings for this configuration
1. `rate=512K`
2. `clock_div=8`

> LIGO standard 16bit ADC modules can coexist with the high speed ADC cards. However, they must still be clocked at the LIGO standard 64KHz. Therefore, the IOC must be provided with both a 512KHz clock for fast ADCs and 64K clocks for standard ADCs.

#### IOP running with a 128KHz ADC clock

The RCG now supports running an IOP with an ADC clock at 128KHz with the General Standards 16ai64 ADC module. 

Required Parameter Block Settings:
1. `rate=128K`
2. `adcclock=128`

> The IOP will now pass data to/from user control models at this rate, so those models require a new Parameter setting to run with this IOP.

#### Addional Optional IOP Parameter Block Settings
- `optimizeIO=1`
    - When set, the IOP will not preload the DAC FIFOs with the usual 2 samples at startup. This can provide for lower phase delay for 2 clock cycles, or 30 usec at the typical 64K IOP rate.
- `requireIOcnt=1`
    - When set, IOP will not run unless it finds all of the I/O cards specified in the control model. This is provided as a safety factor for code running on production systems.
- `rfm_delay=<VAL>`
    - Due to the long transmission delays between LIGO corner and end station computers, it may be desirable in some cases to allow extra IPC transmission time. The transmitting model will use this setting to provide `VAL` extra code cycles delay for IPC receivers.
    - WARNING: IPCs normally have a 1 cycle delay to the receiver, but using this option will add an additional `VAL` cycles of delay to RFM IPCs. 
    - The delay in cycles in at the sender model rate.
    - This setting is valid in the IPC sender model, and ***only*** effects RFM IPCs.
    - ***Senders running faster that 16K***
        - RFM IPCs can only be copied by the `cdsrfmswitch` at a max rate of 16384 Hz, normally IPCs can be sent from a faster model to a slower one, but the invisible decimation done by the `cdsrfmswitch` can lead to unexpected results.
        - When RFM IPC senders are sending faster than 16384 Hz, lets say, 65536 Hz to models running at 16K (Note: the highest RFM IPC receiver can run is 16K ) the reader will think the sender rate is 65536 Hz, but because the receiver is running at 16K it does not read faster than the 16K decimated data of RFM IPCs. 
        - In older versions of the RTS, when `rfm_delay` could only be disabled (0) or enabled (1), the assumption was that the sender was 16K so the added delay would be 1/16384 Hz ~= 61 us this was more than enough time to make up for the ~25 us propagation delay of the 4km interferometer arms. This version attempts to support faster senders, so `rfm_delay` is now given a value [0-1028] in terms of the sender's rate. So a fast model (few us processing time) running at 64K would need to set rfm_delay to at least 2 ( (1/65536Hz)*2 ~= 30.5 us ) to make time for the signal propagation.
- `no_cpu_shutdown=1`
    - At runtime, code will be assigned to the CPU core specified in the Matlab model but the core will not be locked ie code installed as a typical kernel module. This is useful when testing a model on a computer that does not have the LIGO CDS realtime patch installed in the kernel.
- `ipc_rate=x`, where x is 2048,4096,8192,16384, or 32768 and <= 1/2 model rate.
    - By default, a control model transmits its IPC signals at the same rate as defined in the model rate parameter. If desired, the IPC send rate can be reduced by setting this parameter. This may be useful in reducing Dolphin network traffic where the receiver model does not require the signal at full rate. An example is the SUS IOP models sending watchdog signals to SEI IOP models at 64K, but signal is not acted on at anywhere near this rate.
    -  If set, all IPCs will be sent at this rate. i.e. is not a per IPC channel setting.

### User Model (Control Model) Configurations

This is the standard production configuration for non-IOP models 

### Code where core User Model functions are implemented

- `fe/controllerApp.c`
    - Primary infinite control loop; controls I/O and timing
- `fe/moduleLoad.c`
    - Handles kernel module loading and unloading
- `fe/mapApp.c`
    - Used to map PCIe I/O modules
- `drv/app_adc_read.c`
    - Handles ADC I/O
- `drv/app_dac_functions.c`
    - Handles DAC I/O
- `drv/app_dio_routines.c`
    - Provides DIO card I/O

### User Model Rate Considerations
Most user models, of various rates, will automatically handle data from their IOP model. However when IOP's are configured to pass data faster than 64K, user models need an additional parameter of `adcclock=128`.

#### Required User Model Parameter Block Entries

Besides the required common parameters, some User models must also have the following:

- For use with ***only*** an IOP passing data at 128KS/sec, add the following option:
    - `adcclock=128`
    

#### Additional compile options intended for CDS code test systems
- `no_zero_pad=1`
    - In performing upsampling of DAC outputs to match IOP rate, the user app code using zero padding in its upsample filtering.
    - If this option is set, zero padding is disabled.
- `no_cpu_shutdown=1`
    - At runtime, code will be assigned to the CPU core specified in the Matlab model but the core will not be locked ie code installed as a typical kernel module. This is useful when testing a model on a computer that does not have the LIGO CDS realtime patch installed in the kernel.
- `use_shm_ipc=1`
    - Regardless of IPC type definition in the Matlab model, the RCG will force all IPCs to be defined as SHMEM type. 
    - This can be useful in running a production system model, most of which use PCIe type IPC, in a standalone system for testing. 
    - IPC receivers in the model still need to find the channels in your IFO.ipc file. A quick way to to this is:
        1. Copy over the production system IPC file 
        2. Use sed, or similar, to change all ipcType=SHMEM
        3. Use sed, or similar, to change all ipcHost="Your computer name"
