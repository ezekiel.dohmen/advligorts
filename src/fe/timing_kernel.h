#ifndef LIGO_TIMING_KERNEL_H
#define LIGO_TIMING_KERNEL_H

#include "../drv/gpstime/gpstime_kernel.h"
#include "portableInline.h"

/// \file timing_kernel.h
/// \brief File contains some timing diagnostics previously imbedded
///<        into the controller.c code.
//***********************************************************************
/// \brief Get current kernel time (in GPS)
/// @return Current time in form of GPS Seconds.
//***********************************************************************
LIGO_INLINE unsigned long
current_time_fe( void )
{

    LIGO_TIMESPEC t;
    ligo_gpstime_get_ts(&t);
    return  t.tv_sec;
}
LIGO_INLINE unsigned long
current_nanosecond( void )
{
    LIGO_TIMESPEC t;
    ligo_gpstime_get_ts(&t);
    return t.tv_nsec;
}

#endif

