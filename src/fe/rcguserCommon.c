/// @file rcguserCommon.c
/// @brief File contains routines for user app startup

#include "print_io_info.h"
#include "controller.h"
#include "../cds-shmem/cds-shmem.h"
#include "drv/rts-logger.h"

#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

// **********************************************************************************************
// Capture SIGHALT from ControlC
void
intHandler( int signal)
{
    (void) signal;
    pLocalEpics->epicsInput.vmeReset = 1;
    RTSLOG_INFO( "Received exit signal, exiting...\n" );
}

// **********************************************************************************************

int
attach_shared_memory( const char* sysname )
{

    char * s;
    char         shm_name[ 64 ];
    char         sysname_lower[ 64 ];

    //Build lower case version of the system (model) name
    strcpy(sysname_lower, sysname);
    for(s = sysname_lower; *s; s++) *s = tolower(*s);


    // epics shm used to communicate with model's EPICS process
    g_epics_shm_handle = shmem_open(sysname_lower, SHMEM_EPICS_SIZE_MB);
    if ( g_epics_shm_handle == NULL )
    {
        RTSLOG_ERROR( "shmem_open(%s, %d) failed; ret = %p\n",
                sysname, SHMEM_EPICS_SIZE_MB, g_epics_shm_handle );
        return -1;
    }
    _epics_shm = shmem_mapping(g_epics_shm_handle);
    RTSLOG_INFO( "EPICSM at %p\n", _epics_shm );

    // testpoint config shm used to control testpoints from awgtpman
    sprintf( shm_name, "%s%s", sysname_lower, SHMEM_TESTPOINT_SUFFIX );
    g_tp_shm_handle = shmem_open(shm_name, SHMEM_TESTPOINT_SIZE_MB);
    if ( g_tp_shm_handle == NULL )
    {
        RTSLOG_ERROR( "shmem_open(%s, %d) failed; ret = %p\n",
                shm_name, SHMEM_TESTPOINT_SIZE_MB, g_tp_shm_handle );
        return -1;
    }
    _tp_shm = shmem_mapping(g_tp_shm_handle);
    RTSLOG_INFO( "TPSM at %p\n", _tp_shm );
    
    // awg data shm used to stream data from awgtpman
    sprintf( shm_name, "%s%s", sysname_lower, SHMEM_AWG_SUFFIX );
    g_awg_shm_handle = shmem_open(shm_name, SHMEM_AWG_SIZE_MB);
    if ( g_awg_shm_handle == NULL )
    {
        RTSLOG_ERROR( "shmem_open(%s, %d) failed; ret = %p\n", 
                shm_name, SHMEM_AWG_SIZE_MB, g_awg_shm_handle );
        return -1;
    }
    _awg_shm = shmem_mapping(g_awg_shm_handle);
    RTSLOG_INFO( "AWGSM at %p\n", _awg_shm );
    
    // ipc_shm used to communicate with IOP
    g_ipc_shm_handle = shmem_open( SHMEM_IOMEM_NAME, SHMEM_IOMEM_SIZE_MB);
    if ( g_ipc_shm_handle == NULL )
    {
        RTSLOG_ERROR( "shmem_open(%s, %d) failed; ret = %p\n",
                SHMEM_IOMEM_NAME, SHMEM_IOMEM_SIZE_MB, g_ipc_shm_handle );
        return -1;
    }
    _ipc_shm = shmem_mapping(g_ipc_shm_handle);
    RTSLOG_INFO( "IPC    at %p\n", _ipc_shm );
    ioMemData = (volatile IO_MEM_DATA*)( ( (char*)_ipc_shm ) + 0x4000 );
    RTSLOG_INFO("IOMEM  at %p size 0x%lx\n", ioMemData, sizeof( IO_MEM_DATA ) );
    RTSLOG_INFO( "%d PCI cards found\n", ioMemData->totalCards );

    // DAQ is via shared memory
    sprintf( shm_name, "%s_daq", sysname_lower );
    g_daq_shm_handle = shmem_open(shm_name, DEFAULT_SHMEM_ALLOC_SIZE_MB);
    if ( g_daq_shm_handle == NULL )
    {
        RTSLOG_ERROR( "shmem_open(%s, %d) failed; ret = %p\n",
                shm_name, DEFAULT_SHMEM_ALLOC_SIZE_MB, g_daq_shm_handle );
        return -1;
    }
    _daq_shm = shmem_mapping(g_daq_shm_handle);
    RTSLOG_INFO( "DAQSM at %p\n", _daq_shm );
    daqPtr = (struct rmIpcStr*)_daq_shm;

    // shmipc is used to send SHMEM IPC data between processes on same computer
    g_shmipc_shm_handle = shmem_open(SHMEM_IPCCOMMS_NAME, SHMEM_IPCCOMMS_SIZE_MB); 
    if ( g_shmipc_shm_handle == NULL )
    {
        RTSLOG_ERROR( "shmem_open(%s, %d) failed; ret = %p\n",
                SHMEM_IPCCOMMS_NAME, SHMEM_IPCCOMMS_SIZE_MB, g_shmipc_shm_handle );
        return -1;
    }
    _shmipc_shm = shmem_mapping(g_shmipc_shm_handle);

    // Open new IO shared memory in support of no hardware I/O
    g_io_shm_handle = shmem_open("virtual_io_space", DEFAULT_SHMEM_ALLOC_SIZE_MB);
    if ( g_io_shm_handle == NULL )
    {
        RTSLOG_ERROR( "shmem_open(%s, %d) failed; ret = %p\n",
                "virtual_io_space", DEFAULT_SHMEM_ALLOC_SIZE_MB, g_io_shm_handle );
        return -1;
    }
    _io_shm = shmem_mapping(g_io_shm_handle);
    ioMemDataIop = (volatile IO_MEM_DATA_IOP*)( ( (char*)_io_shm ) );

    return 0;
}

void detach_shared_memory()
{
    shmem_close(g_epics_shm_handle);
    shmem_close(g_tp_shm_handle);
    shmem_close(g_awg_shm_handle);
    shmem_close(g_ipc_shm_handle);
    shmem_close(g_daq_shm_handle);
    shmem_close(g_shmipc_shm_handle);
    shmem_close(g_io_shm_handle);
}
