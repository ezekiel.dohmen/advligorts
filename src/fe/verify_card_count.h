/// @file verify_card_count.h
/// @brief Helper file for moduleLoad.c to check prior IO card counts/types.
#ifndef LIGO_VERIFY_CARD_COUNT_H
#define LIGO_VERIFY_CARD_COUNT_H

#include "drv/cdsHardware.h"

#ifdef __cplusplus
extern "C" {
#endif

void initialize_card_counts( CDS_HARDWARE*, int[] );
int  verify_card_count( CDS_HARDWARE*, int[], const char* sysname );

#ifdef __cplusplus
}
#endif


#endif //LIGO_VERIFY_CARD_COUNT_H
