#ifndef LIGO_RCG_COMMON_H
#define LIGO_RCG_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

void intHandler( int dummy );
int attach_shared_memory( const char* sysname );
void detach_shared_memory();

#ifdef __cplusplus
}
#endif


#endif //LIGO_RCG_COMMON_H
