#ifndef LIGO_MAP_CARDS_2_SLOTS_H
#define LIGO_MAP_CARDS_2_SLOTS_H

#include "drv/cdsHardware.h"

#ifdef __cplusplus
extern "C" {
#endif

int map_cards_2_slots (CDS_HARDWARE* pCds );

#ifdef __cplusplus
}
#endif


#endif //LIGO_MAP_CARDS_2_SLOTS_H
