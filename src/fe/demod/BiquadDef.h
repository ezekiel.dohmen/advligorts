#pragma once

#ifndef USE_AVX512
#if defined(__AVX512F__) && defined(__AVX512CD__) && defined(__AVX512DQ__) && defined(__AVX512BW__) && !defined (NO_AVX512)
#define USE_AVX512 1
#else
#define USE_AVX512 0
#endif
#endif

#ifndef USE_AVX2
#if defined(__AVX2__) && defined(__FMA__) && !defined (NO_AVX2)
#define USE_AVX2 1
#else
#define USE_AVX2 0
#endif
#endif

#ifndef USE_SSE3
#if defined(_MSC_VER)
#if defined(__AVX2__) && !defined (NO_SSE3)
#define USE_SSE3 1
#else
#define USE_SSE3 0
#endif
#else
#if defined(__SSE3__) && defined(__FMA__) && !defined (NO_SSE3)
#define USE_SSE3 1
#else
#define USE_SSE3 0
#endif
#endif
#endif

#ifndef NO_LIGO_BIQUAD
#define LIGO_BIQUAD
#endif // !NO_LIGO_BIQUAD

#include "util/fixed_width_types.h"
