#pragma once

/** @file Biquad.h
	@brief Contains implementations of the biquad filter using AVX2 and AVX512
 *****************************************************************************/

/** @defgroup biquadavx Vectorized biquad functions
	These functions implement IIR filters using cascaded biquad sections
	using vector arithmetics based on AVX2 and AVX512.
 *****************************************************************************/
 /** @{ */

#include "BiquadDef.h"

#ifdef __cplusplus 
extern "C" {
#endif

	/// iir_filter_biquad
	/// 
	/// Biquad form IIR (from fm10Gen.c)
	/// @brief Perform IIR filtering sample by sample on doubles.
	/// Implements Biquad form calculations.
	///	@param[in] input New input sample
	///	@param[in] *coef Pointer to filter coefficient data with size 4*n + 1 (gain)
	///	@param[in] sections Number of second order sections in filter definition
	///	@param[in,out] *history Pointer to filter history data of size 2*n
	///	@return Result of filter calculation
	double iir_filter_biquad(double input, const double* coef, size_t sections, double* history);


	/// biquad2_sse3
	/// 
	/// Calculates 2 sets of cascaded second-order sections to form an IIR filter
	/// using SSE2 and FMA operations. Filter runs once.
	/// 
	/// The input and output data arrays as well as the history buffer need to be
	/// aligned by 16 bytes (2 doubles).
	/// 
	/// @param inp Input values arranged 2 wide
	/// @param out Output values arranged 2 wide
	/// @param coeff Filter coefficienst arranged in LIGO format and 2 wide each
	/// @param hist History values arranged 4 wide
	/// @param sections Number of second-order sections in the filter
	void biquad2_sse3(const double* inp, double* out, const double* coeff, double* hist, size_t sections);



	/// biquad4_avx2
	/// 
	/// Calculates 4 sets of cascaded second-order sections to form an IIR filter
	/// using AVX2 operations. Filter runs once.
	/// 
	/// The input and output data arrays as well as the history buffer need to be
	/// aligned by 32 bytes (4 doubles).
	/// 
	/// @param inp Input values arranged 4 wide
	/// @param out Output values arranged 4 wide
	/// @param coeff Filter coefficienst arranged in LIGO format and 4 wide each
	/// @param hist History values arranged 8 wide
	/// @param sections Number of second-order sections in the filter
	void biquad4_avx2(const double* inp, double* out, const double* coeff,
		double* hist, size_t sections);



	/// biquad8_avx512
	/// 
	/// Calculates 8 sets of cascaded second-order sections to form an IIR filter
	/// using AVX-512 operations. Filter runs once.
	/// 
	/// The input and output data arrays as well as the history buffer need to be
	/// aligned by 64 bytes (8 doubles).
	/// 
	/// @param inp Input values arranged 8 wide
	/// @param out Output values arranged 8 wide
	/// @param coeff Filter coefficienst arranged in LIGO format and 8 wide each
	/// @param hist History values arranged 16 wide
	/// @param sections Number of second-order sections in the filter
	void biquad8_avx512(const double* inp, double* out, const double* coeff,
		double* hist, size_t sections);


	/// biquad_stride2_std
	/// 
	/// Calculates a set of cascaded second-order sections to form an IIR filter.
	/// This function has a minimum stride size of 2. 
	/// 
	/// This function works on multiple strides of data in sequence. The number 
	/// of data points per stride is denoted by the stride paramter, whereas the 
	/// number of strides is denoted by the samples parameter. If strides are 
	/// not stored in continguous memory, the stride_offset parameters can be 
	/// used to denote the offset between strides in number of data samples. 
	/// If no offset is specified, continuous strides are assumed.
	/// 
	/// The input and output data arrays as well as the history buffer need to be
	/// aligned by 8 bytes (1 double).
	/// 
	/// Testing only. Use functions in Biquad.h for production code.
	/// 
	/// @param inp Input values arranged stride wide and samples deep
	/// @param out Output values arranged stride wide
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History values arranged double stride wide
	/// @param sections Number of second-order sections in the filter
	/// @param stride Width of data array
	/// @param samples Number of samples in input array which are decimated down
	/// @param stride_ofs Stride offset  (0 indicates stride)
	void biquad_stride2_std(const double* inp, double* out, const double* coeff,
		double* hist, size_t sections, size_t stride, size_t samples, size_t stride_ofs);


	/// biquad_stride2_section3_sse3
	/// 
	/// Calculates a set of cascaded second-order sections to form an IIR filter.
	/// This function requires the stride to be a multiple of 2, and supports 
	/// a filter of order 6th (3 second order sections). 
	/// 
	/// This function works on multiple strides of data in sequence. The number 
	/// of data points per stride is denoted by the stride paramter, whereas the 
	/// number of strides is denoted by the samples parameter. If strides are 
	/// not stored in continguous memory, the stride_offset parameters can be 
	/// used to denote the offset between strides in number of data samples. 
	/// If no offset is specified, continuous strides are assumed.
	/// 
	/// The input and output data arrays as well as the history buffer need to be
	/// aligned by 16 bytes (2 doubles).
	/// 
	/// SSE2/FMA version.
	/// 
	/// @param inp Input values arranged stride wide and samples deep
	/// @param out Output values arranged stride wide
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History values arranged double stride wide
	/// @param sections Number of second-order sections in the filter
	/// @param stride Width of data array
	/// @param samples Number of samples in input array which are decimated down
	/// @param stride_ofs Stride offset  (0 indicates stride)
	void biquad_stride2_section3_sse3(const double* inp, double* out,
		const double* coeff, double* hist, size_t stride, size_t samples, size_t stride_ofs);

	/// biquad_stride8_section3_sse3
	/// 
	/// Calculates a set of cascaded second-order sections to form an IIR filter.
	/// This function requires the stride to be a multiple of 8, and supports
	/// a filter of order 6th (3 second order sections). This function is faster than 
	/// biquad_stride2_sse3 in case of -O optimization on gcc. It is slower than the 
	/// biquad_stride2_sse3 version when using -O3 on gcc without AVX512. If AVX512
	/// is available the number of xmm registers is doubled, and it is faster again.
	/// 
	/// This function works on multiple strides of data in sequence. The number 
	/// of data points per stride is denoted by the stride paramter, whereas the 
	/// number of strides is denoted by the samples parameter. If strides are 
	/// not stored in continguous memory, the stride_offset parameters can be 
	/// used to denote the offset between strides in number of data samples. 
	/// If no offset is specified, continuous strides are assumed.
	/// 
	/// The input and output data arrays as well as the history buffer need to be
	/// aligned by 16 bytes (2 doubles).
	/// 
	/// SSE2/FMA version.
	/// 
	/// @param inp Input values arranged stride wide and samples deep
	/// @param out Output values arranged stride wide
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History values arranged double stride wide
	/// @param sections Number of second-order sections in the filter
	/// @param stride Width of data array
	/// @param samples Number of samples in input array which are decimated down
	/// @param stride_ofs Stride offset  (0 indicates stride)
	void biquad_stride8_section3_sse3(const double* inp, double* out,
		const double* coeff, double* hist, size_t stride, size_t samples, size_t stride_ofs);



	/// biquad_stride4_avx2
	/// 
	/// Calculates a set of cascaded second-order sections to form an IIR filter.
	/// This function requires the stride to be a multiple of 4, and supports 
	/// a filter of order 6th (3 second order sections). 
	/// 
	/// This function works on multiple strides of data in sequence. The number 
	/// of data points per stride is denoted by the stride paramter, whereas the 
	/// number of strides is denoted by the samples parameter. If strides are 
	/// not stored in continguous memory, the stride_offset parameters can be 
	/// used to denote the offset between strides in number of data samples. 
	/// If no offset is specified, continuous strides are assumed.
	/// 
	/// The input and output data arrays as well as the history buffer need to be
	/// aligned by 32 bytes (4 doubles).
	/// 
	/// AVX2 version.
	/// 
	/// @param inp Input values arranged stride wide and samples deep
	/// @param out Output values arranged stride wide
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History values arranged double stride wide
	/// @param sections Number of second-order sections in the filter
	/// @param stride Width of data array
	/// @param samples Number of samples in input array which are decimated down
	/// @param stride_ofs Stride offset  (0 indicates stride)
	void biquad_stride4_section3_avx2(const double* inp, double* out,
		const double* coeff, double* hist, size_t stride, size_t samples, size_t stride_ofs);

	/// biquad_stride16_avx2
	/// 
	/// Calculates a set of cascaded second-order sections to form an IIR filter.
	/// This function requires the stride to be a multiple of 16, and supports
	/// a filter of order 6th (3 second order sections). This function is faster than 
	/// biquad_stride4_avx2 in case of -O optimization on gcc. It is slower than the 
	/// biquad_stride4_avx2 version when using -O3 on gcc.
	/// 
	/// This function works on multiple strides of data in sequence. The number 
	/// of data points per stride is denoted by the stride paramter, whereas the 
	/// number of strides is denoted by the samples parameter. If strides are 
	/// not stored in continguous memory, the stride_offset parameters can be 
	/// used to denote the offset between strides in number of data samples. 
	/// If no offset is specified, continuous strides are assumed.
	/// 
	/// The input and output data arrays as well as the history buffer need to be
	/// aligned by 32 bytes (4 doubles).
	/// 
	/// AVX2 version.
	/// 
	/// @param inp Input values arranged stride wide and samples deep
	/// @param out Output values arranged stride wide
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History values arranged double stride wide
	/// @param sections Number of second-order sections in the filter
	/// @param stride Width of data array
	/// @param samples Number of samples in input array which are decimated down
	/// @param stride_ofs Stride offset  (0 indicates stride)
	void biquad_stride16_section3_avx2(const double* inp, double* out,
		const double* coeff, double* hist, size_t stride, size_t samples, size_t stride_ofs);



	/// biquad_stride16_section3_avx512
	/// 
	/// Calculates a set of cascaded second-order sections to form an IIR filter.
	/// This function requires the stride to be a multiple of 16, and supports
	/// a filter of order 6th (3 second order sections).
	/// 
	/// This function works on multiple strides of data in sequence. The number 
	/// of data points per stride is denoted by the stride paramter, whereas the 
	/// number of strides is denoted by the samples parameter. If strides are 
	/// not stored in continguous memory, the stride_offset parameters can be 
	/// used to denote the offset between strides in number of data samples. 
	/// If no offset is specified, continuous strides are assumed.
	/// 
	/// The input and output data arrays as well as the history buffer need to be
	/// aligned by 64 bytes (8 doubles).
	/// 
	/// AVX512 version.
	/// 
	/// @param inp Input values arranged stride wide and samples deep
	/// @param out Output values arranged stride wide
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History values arranged double stride wide
	/// @param sections Number of second-order sections in the filter
	/// @param stride Width of data array
	/// @param samples Number of samples in input array which are decimated down
	/// @param stride_ofs Stride offset  (0 indicates stride)
	void biquad_stride16_section3_avx512(const double* inp, double* out,
		const double* coeff, double* hist, size_t stride, size_t samples, size_t stride_ofs);


	/// biquad_stride4_section3
	/// 
	/// Calculates a set of cascaded second-order sections to form an IIR filter.
	/// This function requires the stride to be a multiple of 4, and supports
	/// a filter of order 6th (3 second order sections). For strides that are
	/// a multiple of 16, use biquad_stride16_section3.
	/// 
	/// This function works on multiple strides of data in sequence. The number 
	/// of data points per stride is denoted by the stride paramter, whereas the 
	/// number of strides is denoted by the samples parameter. If strides are 
	/// not stored in continguous memory, the stride_offset parameters can be 
	/// used to denote the offset between strides in number of data samples. 
	/// If no offset is specified, continuous strides are assumed.
	/// 
	/// The input and output data arrays as well as the history buffer need to be
	/// aligned by 32 bytes (4 doubles).
	/// 
	/// Selects AVX2 when available. If not, uses the non-vectorized version.
	/// 
	/// @param inp Input values arranged stride wide and samples deep
	/// @param out Output values arranged stride wide
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History values arranged double stride wide
	/// @param sections Number of second-order sections in the filter
	/// @param stride Width of data array
	/// @param samples Number of samples in input array which are decimated down
	/// @param stride_ofs Stride offset  (0 indicates stride)
	void biquad_stride4_section3(const double* inp, double* out,
		const double* coeff, double* hist, size_t stride, size_t samples, size_t stride_ofs);


	/// biquad_stride16_section3
	/// 
	/// Calculates a set of cascaded second-order sections to form an IIR filter.
	/// This function requires the stride to be a multiple of 16, and supports
	/// a filter of order 6th (3 second order sections).
	/// 
	/// This function works on multiple strides of data in sequence. The number 
	/// of data points per stride is denoted by the stride paramter, whereas the 
	/// number of strides is denoted by the samples parameter. If strides are 
	/// not stored in continguous memory, the stride_offset parameters can be 
	/// used to denote the offset between strides in number of data samples. 
	/// If no offset is specified, continuous strides are assumed.
	/// 
	/// The input and output data arrays as well as the history buffer need to be
	/// aligned by 64 bytes (8 doubles).
	/// 
	/// Selects AVX512 when available. If not, checks for AVX2. If neither, 
	/// uses the non-vectorized version.
	/// 
	/// @param inp Input values arranged stride wide and samples deep
	/// @param out Output values arranged stride wide
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History values arranged double stride wide
	/// @param sections Number of second-order sections in the filter
	/// @param stride Width of data array
	/// @param samples Number of samples in input array which are decimated down
	/// @param stride_ofs Stride offset  (0 indicates stride)
	void biquad_stride16_section3(const double* inp, double* out,
		const double* coeff, double* hist, size_t stride, size_t samples, size_t stride_ofs);


#ifdef __cplusplus 
}
#endif
/** @} */

