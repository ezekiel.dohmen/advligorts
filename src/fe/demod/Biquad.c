#include "Biquad.h"

#ifdef __KERNEL__
// defining _MM_MALLOC_H_INCLUDED here blocks header
// files included in immintrin.h that require malloc
// and can't be used when building kernel modules
#define _MM_MALLOC_H_INCLUDED
#endif

#if USE_SSE3 > 0 || USE_AVX2 > 0 || USE_AVX512 > 0
#include <immintrin.h>
#endif

///* iir_filter_biquad
// ************************************************************************/
//double iir_filter_biquad_std(double input, const double* coef, size_t sections, double* history)
//{
//
//	const double* coef_ptr = coef; /* coefficient pointer */
//
//	double* hist1_ptr = history; /* first history */
//	double* hist2_ptr = hist1_ptr + 1; /* next history */
//
//	double output = input * (*coef_ptr++); /* overall input scale factor */
//
//        size_t i;
//	for (i = 0; i < sections; i++)
//	{
//		double new_w, new_u, w, u, a11, a12, c1, c2;
//
//		w = *hist1_ptr;
//		u = *hist2_ptr;
//
//		a11 = *coef_ptr++;
//		a12 = *coef_ptr++;
//		c1 = *coef_ptr++;
//		c2 = *coef_ptr++;
//
//		new_w = output + a11 * w + a12 * u;
//		output = output + w * c1 + u * c2;
//		new_u = w + u;
//
//		*hist1_ptr++ = new_w;
//		*hist2_ptr++ = new_u;
//		hist1_ptr++;
//		hist2_ptr++;
//	}
//
//	return output;
//}

/* biquad2_sse3
 ************************************************************************/

#if USE_SSE3
void biquad2_sse3(const double* inp, double* out, const double* coeff, double* hist, size_t sections)
{
	// typecast double pointers into __m256d pointers
	const __m128d* const minp = (__m128d*)inp;
	__m128d* const mout = (__m128d*)out;
	const __m128d* const mcoeff = (__m128d*)coeff;
	__m128d* const mhist = (__m128d*)hist;

	// get first input value and mulitply it with filter gain

	__m128d y = _mm_mul_pd(mcoeff[0], *minp);

	// Loop over sections
        size_t j;
	for (j = 0; j < sections; ++j)
	{
		// Fetch coefficients
		// Fetch previous history
		const __m128d w = mhist[2 * j + 0];
		const __m128d u = mhist[2 * j + 1];

#ifdef LIGO_BIQUAD
		// Calculate and save new history
		mhist[2 * j + 0] = _mm_fmadd_pd(mcoeff[4 * j + 2], u, _mm_fmadd_pd(mcoeff[4 * j + 1], w, y));
		// Difference equations for LIGO biquad
		y = _mm_fmadd_pd(mcoeff[4 * j + 4], u, _mm_fmadd_pd(mcoeff[4 * j + 3], w, y));
		// Calculate and save new history
		mhist[2 * j + 1] = _mm_add_pd(u, w);
#else
		// Difference equations for transposed direct form II biquad
		const __m128d x = y;
		y = _mm_add_pd(x, w);
		// Calculate and save new history
		mhist[2 * j + 0] = _mm_fnmadd_pd(mcoeff[4 * j + 1], y, _mm_fmadd_pd(mcoeff[4 * j + 3], x, u));
		mhist[2 * j + 1] = _mm_fnmadd_pd(mcoeff[4 * j + 2], y, _mm_mul_pd(mcoeff[4 * j + 4], x));
#endif
	}

	// Store output
	*mout = y;
}
#endif // USE_SSE3


#if USE_AVX2
/* biquad4_avx2
 ************************************************************************/

void biquad4_avx2(const double* inp, double* out, const double* coeff, double* hist, size_t sections)
{
	// typecast double pointers into __m256d pointers
	const __m256d* const minp = (__m256d*)inp;
	__m256d* const mout = (__m256d*)out;
	const __m256d* const mcoeff = (__m256d*)coeff;
	__m256d* const mhist = (__m256d*)hist;

	// get first input value and mulitply it with filter gain

	__m256d y = _mm256_mul_pd(mcoeff[0], *minp);

	// Loop over sections
        size_t j;
	for (j = 0; j < sections; ++j)
	{
		// Fetch coefficients
		// Fetch previous history
		const __m256d w = mhist[2 * j + 0];
		const __m256d u = mhist[2 * j + 1];

		// Calculate and save new history
		mhist[2 * j + 0] = _mm256_fmadd_pd(mcoeff[4 * j + 2], u, _mm256_fmadd_pd(mcoeff[4 * j + 1], w, y));
		// Difference equations for LIGO biquad
		y = _mm256_fmadd_pd(mcoeff[4 * j + 4], u, _mm256_fmadd_pd(mcoeff[4 * j + 3], w, y));
		// Calculate and save new history
		mhist[2 * j + 1] = _mm256_add_pd(u, w);
	}

	// Store output
	*mout = y;
}
#endif //USE_AVX2


#if USE_AVX512
/* biquad8_avx512
 ************************************************************************/

void biquad8_avx512(const double* inp, double* out, const double* coeff, double* hist, size_t sections)
{
	// typecast double pointers into __m512d pointers
	const __m512d* const minp = (__m512d*)inp;
	__m512d* const mout = (__m512d*)out;
	const __m512d* const mcoeff = (__m512d*)coeff;
	__m512d* const mhist = (__m512d*)hist;

	// get first input value and mulitply it with filter gain
	__m512d y = _mm512_mul_pd(mcoeff[0], *minp);

	// Loop over sections
        size_t j;
	for (j = 0; j < sections; ++j)
	{
		// Fetch coefficients
		// Fetch previous history
		const __m512d w = mhist[2 * j + 0];
		const __m512d u = mhist[2 * j + 1];

		// Calculate and save new history
		mhist[2 * j + 0] = _mm512_fmadd_pd(mcoeff[4 * j + 2], u, _mm512_fmadd_pd(mcoeff[4 * j + 1], w, y));
		// Difference equations for LIGO biquad
		y = _mm512_fmadd_pd(mcoeff[4 * j + 4], u, _mm512_fmadd_pd(mcoeff[4 * j + 3], w, y));
		// Calculate and save new history
		mhist[2 * j + 1] = _mm512_add_pd(u, w);
	}

	// Store output
	*mout = y;
}
#endif // USE_AVX512


/* biquad_stride2_std
 ************************************************************************/
void biquad_stride2_std(const double* inp, double* out, const double* coeff,
	double* hist, size_t sections, size_t stride, size_t samples, size_t stride_ofs)
{
	// intrinsic stride width is 2
#define stride2_intrinsic 2
	if (samples < 1) return;
	if ((stride < stride2_intrinsic) || (stride % stride2_intrinsic != 0)) return;
	if (stride_ofs <= 0) stride_ofs = stride;
	// loop over stride
        size_t i, n, j;
	for (i = 0; i + stride2_intrinsic <= stride; i = i + stride2_intrinsic)
	{
		double y1, y2;
		// loop over number of samples
		for (n = 0; n < samples; ++n)
		{
			// get first input value and mulitply it with filter gain
			y1 = coeff[0] * inp[i + n * stride_ofs];
			y2 = coeff[0] * inp[i + n * stride_ofs + 1];
			// Loop over sections
			for (j = 0; j < sections; ++j)
			{
				// Fetch previous history
				const double w1 = hist[i + (2 * j + 0) * stride];
				const double w2 = hist[i + (2 * j + 0) * stride + 1];
				const double u1 = hist[i + (2 * j + 1) * stride];
				const double u2 = hist[i + (2 * j + 1) * stride + 1];

				// Calculate and save new history
				hist[i + (2 * j + 0) * stride + 0] = y1 + coeff[4 * j + 1] * w1 + coeff[4 * j + 2] * u1;
				hist[i + (2 * j + 0) * stride + 1] = y2 + coeff[4 * j + 1] * w2 + coeff[4 * j + 2] * u2;
				// Difference equations for LIGO biquad
				y1 = y1 + coeff[4 * j + 3] * w1 + coeff[4 * j + 4] * u1;
				y2 = y2 + coeff[4 * j + 3] * w2 + coeff[4 * j + 4] * u2;
				// Calculate and save new history
				hist[i + (2 * j + 1) * stride + 0] = u1 + w1;
				hist[i + (2 * j + 1) * stride + 1] = u2 + w2;
			}
		}
		// Store output
		out[i + 0] = y1;
		out[i + 1] = y2;
	}
}


#if USE_SSE3
/* biquad_stride2_section3_sse3
 ************************************************************************/

void biquad_stride2_section3_sse3(const double* inp, double* out,
	const double* coeff, double* hist, size_t stride, size_t samples, size_t stride_ofs)
{
	// intrinsic stride width is 4
#define stride_mm128 2
#define stride_mm128_q(i) (	 (i == 0) ? 0 * stride_mm128: \
							 (i == 1) ? 1 * stride_mm128 : \
							 (i == 2) ? 2 * stride_mm128 : \
							 (i == 3) ? 3 * stride_mm128 : \
							 (i == 4) ? 4 * stride_mm128 : \
							 (i == 5) ? 5 * stride_mm128 : \
							 (i == 6) ? 6 * stride_mm128 : \
									    7 * stride_mm128)
#define stride2_mm128 2
#define stride2_mm128_num (stride2_mm128 / stride_mm128)

	// number of sections are 3
#define sections3_intrinsic 3

	if (samples < 1) return;
	if ((stride < stride2_mm128) || (stride % stride2_mm128 != 0)) return;
	if (stride_ofs <= 0) stride_ofs = stride;

	// load coefficients into AVX registers
	__m128d fcoeff[sections3_intrinsic * 4 + 1];
        size_t k;
	for (k = 0; k < sections3_intrinsic * 4 + 1; ++k)
	{
		fcoeff[k] = _mm_set1_pd(coeff[k]);
	}
	// loop over stride
        size_t i, n ,j;
	for (i = 0; i <= stride - stride2_mm128; i = i + stride2_mm128)
	{
		__m128d y;
		// loop over number of samples
		for ( n = 0; n < samples; ++n)
		{
			// get first input value and mulitply it with filter gain
			y = _mm_mul_pd(fcoeff[0], *(__m128d*)(inp + i + n * stride_ofs));
			// Loop over sections
			for (j = 0; j < sections3_intrinsic; ++j)
			{
				// Fetch previous history
				const __m128d w = *(__m128d*)(hist + i + (2 * j + 0) * stride);
				const __m128d u = *(__m128d*)(hist + i + (2 * j + 1) * stride);

				// Calculate and save new history
				* (__m128d*)(hist + i + (2 * j + 0) * stride) = _mm_fmadd_pd(fcoeff[4 * j + 2], u, _mm_fmadd_pd(fcoeff[4 * j + 1], w, y));
				// Difference equations for LIGO biquad
				y = _mm_fmadd_pd(fcoeff[4 * j + 4], u, _mm_fmadd_pd(fcoeff[4 * j + 3], w, y));
				// Calculate and save new history
				*(__m128d*)(hist + i + (2 * j + 1) * stride) = _mm_add_pd(u, w);
			}
		}
		// Store output
		*((__m128d*)(out + i)) = y;
	}
}


/* biquad_stride8_section3_sse3
 ************************************************************************/
void biquad_stride8_section3_sse3(const double* inp, double* out, const double* coeff,
	double* hist, size_t stride, size_t samples, size_t stride_ofs)
{
	// intrinsic stride width is 16
#define stride_mm128 2
#define stride_mm128_q(i) (	 (i == 0) ? 0 * stride_mm128: \
							 (i == 1) ? 1 * stride_mm128 : \
							 (i == 2) ? 2 * stride_mm128 : \
							 (i == 3) ? 3 * stride_mm128 : \
							 (i == 4) ? 4 * stride_mm128 : \
							 (i == 5) ? 5 * stride_mm128 : \
							 (i == 6) ? 6 * stride_mm128 : \
									    7 * stride_mm128)
#define stride8_mm128 8
#define stride8_mm128_num (stride8_mm128 / stride_mm128)

	// number of sections are 3
#define sections3_intrinsic 3

	if (samples < 1) return;
	if ((stride < stride8_mm128) || (stride % stride8_mm128 != 0)) return;
	if (stride_ofs <= 0) stride_ofs = stride;

	// load coefficients into AVX registers
	__m128d fcoeff[sections3_intrinsic * 4 + 1];
        size_t k;
	for (k = 0; k < sections3_intrinsic * 4 + 1; ++k)
	{
		fcoeff[k] = _mm_set1_pd(coeff[k]);
	}
	// loop over stride
        size_t i,n,j;
	for (i = 0; i <= stride - stride8_mm128; i = i + stride8_mm128)
	{
		__m128d y1, y2, y3, y4;
		// loop over number of samples
		for (n = 0; n < samples; ++n)
		{
			// get first input value and mulitply it with filter gain
			y1 = _mm_mul_pd(fcoeff[0], *(__m128d*)(inp + i + n * stride_ofs + stride_mm128_q(0)));
			y2 = _mm_mul_pd(fcoeff[0], *(__m128d*)(inp + i + n * stride_ofs + stride_mm128_q(1)));
			y3 = _mm_mul_pd(fcoeff[0], *(__m128d*)(inp + i + n * stride_ofs + stride_mm128_q(2)));
			y4 = _mm_mul_pd(fcoeff[0], *(__m128d*)(inp + i + n * stride_ofs + stride_mm128_q(3)));
			// Loop over sections
			for (j = 0; j < sections3_intrinsic; ++j)
			{
				// Fetch previous history
				const __m128d w1 = *(__m128d*)(hist + i + (2 * j + 0) * stride + stride_mm128_q(0));
				const __m128d w2 = *(__m128d*)(hist + i + (2 * j + 0) * stride + stride_mm128_q(1));
				const __m128d w3 = *(__m128d*)(hist + i + (2 * j + 0) * stride + stride_mm128_q(2));
				const __m128d w4 = *(__m128d*)(hist + i + (2 * j + 0) * stride + stride_mm128_q(3));
				const __m128d u1 = *(__m128d*)(hist + i + (2 * j + 1) * stride + stride_mm128_q(0));
				const __m128d u2 = *(__m128d*)(hist + i + (2 * j + 1) * stride + stride_mm128_q(1));
				const __m128d u3 = *(__m128d*)(hist + i + (2 * j + 1) * stride + stride_mm128_q(2));
				const __m128d u4 = *(__m128d*)(hist + i + (2 * j + 1) * stride + stride_mm128_q(3));

				// Calculate and save new history
				*(__m128d*)(hist + i + (2 * j + 0) * stride + stride_mm128_q(0)) = _mm_fmadd_pd(fcoeff[4 * j + 2], u1, _mm_fmadd_pd(fcoeff[4 * j + 1], w1, y1));
				*(__m128d*)(hist + i + (2 * j + 0) * stride + stride_mm128_q(1)) = _mm_fmadd_pd(fcoeff[4 * j + 2], u2, _mm_fmadd_pd(fcoeff[4 * j + 1], w2, y2));
				*(__m128d*)(hist + i + (2 * j + 0) * stride + stride_mm128_q(2)) = _mm_fmadd_pd(fcoeff[4 * j + 2], u3, _mm_fmadd_pd(fcoeff[4 * j + 1], w3, y3));
				*(__m128d*)(hist + i + (2 * j + 0) * stride + stride_mm128_q(3)) = _mm_fmadd_pd(fcoeff[4 * j + 2], u4, _mm_fmadd_pd(fcoeff[4 * j + 1], w4, y4));
				// Difference equations for LIGO biquad
				y1 = _mm_fmadd_pd(fcoeff[4 * j + 4], u1, _mm_fmadd_pd(fcoeff[4 * j + 3], w1, y1));
				y2 = _mm_fmadd_pd(fcoeff[4 * j + 4], u2, _mm_fmadd_pd(fcoeff[4 * j + 3], w2, y2));
				y3 = _mm_fmadd_pd(fcoeff[4 * j + 4], u3, _mm_fmadd_pd(fcoeff[4 * j + 3], w3, y3));
				y4 = _mm_fmadd_pd(fcoeff[4 * j + 4], u4, _mm_fmadd_pd(fcoeff[4 * j + 3], w4, y4));
				// Calculate and save new history
				*(__m128d*)(hist + i + (2 * j + 1) * stride + stride_mm128_q(0)) = _mm_add_pd(u1, w1);
				*(__m128d*)(hist + i + (2 * j + 1) * stride + stride_mm128_q(1)) = _mm_add_pd(u2, w2);
				*(__m128d*)(hist + i + (2 * j + 1) * stride + stride_mm128_q(2)) = _mm_add_pd(u3, w3);
				*(__m128d*)(hist + i + (2 * j + 1) * stride + stride_mm128_q(3)) = _mm_add_pd(u4, w4);
			}
		}
		// Store output
		*((__m128d*)(out + i + stride_mm128_q(0))) = y1;
		*((__m128d*)(out + i + stride_mm128_q(1))) = y2;
		*((__m128d*)(out + i + stride_mm128_q(2))) = y3;
		*((__m128d*)(out + i + stride_mm128_q(3))) = y4;
	}
}
#endif // USE_SSE3

#if (USE_AVX2)
/* biquad_stride4_section3_avx2
 ************************************************************************/

void biquad_stride4_section3_avx2(const double* inp, double* out, const double* coeff, 
	double* hist, size_t stride, size_t samples, size_t stride_ofs)
{
	// intrinsic stride width is 4
#define stride_mm256 4
#define stride_mm256_q(i) (	 (i == 0) ? 0 * stride_mm256: \
							 (i == 1) ? 1 * stride_mm256 : \
							 (i == 2) ? 2 * stride_mm256 : \
							 (i == 3) ? 3 * stride_mm256 : \
							 (i == 4) ? 4 * stride_mm256 : \
							 (i == 5) ? 5 * stride_mm256 : \
							 (i == 6) ? 6 * stride_mm256 : \
									    7 * stride_mm256)
#define stride4_mm256 4
#define stride4_mm256_num (stride4_mm256 / stride_mm256)

	// number of sections are 3
#define sections3_intrinsic 3

	if (samples < 1) return;
	if ((stride < stride4_mm256) || (stride % stride4_mm256 != 0)) return;
	if (stride_ofs <= 0) stride_ofs = stride;

	// load coefficients into AVX registers
	__m256d fcoeff[sections3_intrinsic * 4 + 1];
        size_t k;
	for (k = 0; k < sections3_intrinsic * 4 + 1; ++k)
	{
		fcoeff[k] = _mm256_set1_pd(coeff[k]);
	}
	// loop over stride
        size_t i,j,n;
	for (i = 0; i <= stride - stride4_mm256; i = i + stride4_mm256)
	{
		__m256d y;
		// loop over number of samples
		for (n = 0; n < samples; ++n)
		{
			// get first input value and mulitply it with filter gain
			y = _mm256_mul_pd(fcoeff[0], *(__m256d*)(inp + i + n * stride_ofs));
			// Loop over sections
			for (j = 0; j < sections3_intrinsic; ++j)
			{
				// Fetch previous history
				const __m256d w = *(__m256d*)(hist + i + (2 * j + 0) * stride);
				const __m256d u = *(__m256d*)(hist + i + (2 * j + 1) * stride);


				// Calculate and save new history
				* (__m256d*)(hist + i + (2 * j + 0) * stride) = _mm256_fmadd_pd(fcoeff[4 * j + 2], u, _mm256_fmadd_pd(fcoeff[4 * j + 1], w, y));
				// Difference equations for LIGO biquad
				y = _mm256_fmadd_pd(fcoeff[4 * j + 4], u, _mm256_fmadd_pd(fcoeff[4 * j + 3], w, y));
				// Calculate and save new history
				*(__m256d*)(hist + i + (2 * j + 1) * stride) = _mm256_add_pd(u, w);
			}
		}
		// Store output
		*((__m256d*)(out + i)) = y;
	}
}


/* biquad_stride16_section3_avx2
 ************************************************************************/
void biquad_stride16_section3_avx2(const double* inp, double* out, const double* coeff, 
	double* hist, size_t stride, size_t samples, size_t stride_ofs)
{
	// intrinsic stride width is 16
#define stride_mm256 4
#define stride_mm256_q(i) (	 (i == 0) ? 0 * stride_mm256: \
							 (i == 1) ? 1 * stride_mm256 : \
							 (i == 2) ? 2 * stride_mm256 : \
							 (i == 3) ? 3 * stride_mm256 : \
							 (i == 4) ? 4 * stride_mm256 : \
							 (i == 5) ? 5 * stride_mm256 : \
							 (i == 6) ? 6 * stride_mm256 : \
									    7 * stride_mm256)
#define stride16_mm256 16
#define stride16_mm256_num (stride16_mm256 / stride_mm256)

	// number of sections are 3
#define sections3_intrinsic 3

	if (samples < 1) return;
	if ((stride < stride16_mm256) || (stride % stride16_mm256 != 0)) return;
	if (stride_ofs <= 0) stride_ofs = stride;

	// load coefficients into AVX registers
	__m256d fcoeff[sections3_intrinsic * 4 + 1];
        size_t k;
	for (k = 0; k < sections3_intrinsic * 4 + 1; ++k)
	{
		fcoeff[k] = _mm256_set1_pd(coeff[k]);
	}
	// loop over stride
        size_t i,n,j;
	for (i = 0; i <= stride - stride16_mm256; i = i + stride16_mm256)
	{
		__m256d y1, y2, y3, y4;
		// loop over number of samples
		for (n = 0; n < samples; ++n)
		{
			// get first input value and mulitply it with filter gain
			y1 = _mm256_mul_pd(fcoeff[0], *(__m256d*)(inp + i + n * stride_ofs + stride_mm256_q(0)));
			y2 = _mm256_mul_pd(fcoeff[0], *(__m256d*)(inp + i + n * stride_ofs + stride_mm256_q(1)));
			y3 = _mm256_mul_pd(fcoeff[0], *(__m256d*)(inp + i + n * stride_ofs + stride_mm256_q(2)));
			y4 = _mm256_mul_pd(fcoeff[0], *(__m256d*)(inp + i + n * stride_ofs + stride_mm256_q(3)));
			// Loop over sections
			for (j = 0; j < sections3_intrinsic; ++j)
			{
				// Fetch previous history
				const __m256d w1 = *(__m256d*)(hist + i + (2 * j + 0) * stride + stride_mm256_q(0));
				const __m256d w2 = *(__m256d*)(hist + i + (2 * j + 0) * stride + stride_mm256_q(1));
				const __m256d w3 = *(__m256d*)(hist + i + (2 * j + 0) * stride + stride_mm256_q(2));
				const __m256d w4 = *(__m256d*)(hist + i + (2 * j + 0) * stride + stride_mm256_q(3));
				const __m256d u1 = *(__m256d*)(hist + i + (2 * j + 1) * stride + stride_mm256_q(0));
				const __m256d u2 = *(__m256d*)(hist + i + (2 * j + 1) * stride + stride_mm256_q(1));
				const __m256d u3 = *(__m256d*)(hist + i + (2 * j + 1) * stride + stride_mm256_q(2));
				const __m256d u4 = *(__m256d*)(hist + i + (2 * j + 1) * stride + stride_mm256_q(3));

				// Calculate and save new history
				*(__m256d*)(hist + i + (2 * j + 0) * stride + stride_mm256_q(0)) = _mm256_fmadd_pd(fcoeff[4 * j + 2], u1, _mm256_fmadd_pd(fcoeff[4 * j + 1], w1, y1));
				*(__m256d*)(hist + i + (2 * j + 0) * stride + stride_mm256_q(1)) = _mm256_fmadd_pd(fcoeff[4 * j + 2], u2, _mm256_fmadd_pd(fcoeff[4 * j + 1], w2, y2));
				*(__m256d*)(hist + i + (2 * j + 0) * stride + stride_mm256_q(2)) = _mm256_fmadd_pd(fcoeff[4 * j + 2], u3, _mm256_fmadd_pd(fcoeff[4 * j + 1], w3, y3));
				*(__m256d*)(hist + i + (2 * j + 0) * stride + stride_mm256_q(3)) = _mm256_fmadd_pd(fcoeff[4 * j + 2], u4, _mm256_fmadd_pd(fcoeff[4 * j + 1], w4, y4));
				// Difference equations for LIGO biquad
				y1 = _mm256_fmadd_pd(fcoeff[4 * j + 4], u1, _mm256_fmadd_pd(fcoeff[4 * j + 3], w1, y1));
				y2 = _mm256_fmadd_pd(fcoeff[4 * j + 4], u2, _mm256_fmadd_pd(fcoeff[4 * j + 3], w2, y2));
				y3 = _mm256_fmadd_pd(fcoeff[4 * j + 4], u3, _mm256_fmadd_pd(fcoeff[4 * j + 3], w3, y3));
				y4 = _mm256_fmadd_pd(fcoeff[4 * j + 4], u4, _mm256_fmadd_pd(fcoeff[4 * j + 3], w4, y4));
				// Calculate and save new history
				*(__m256d*)(hist + i + (2 * j + 1) * stride + stride_mm256_q(0)) = _mm256_add_pd(u1, w1);
				*(__m256d*)(hist + i + (2 * j + 1) * stride + stride_mm256_q(1)) = _mm256_add_pd(u2, w2);
				*(__m256d*)(hist + i + (2 * j + 1) * stride + stride_mm256_q(2)) = _mm256_add_pd(u3, w3);
				*(__m256d*)(hist + i + (2 * j + 1) * stride + stride_mm256_q(3)) = _mm256_add_pd(u4, w4);
			}
		}
		// Store output
		*((__m256d*)(out + i + stride_mm256_q(0))) = y1;
		*((__m256d*)(out + i + stride_mm256_q(1))) = y2;
		*((__m256d*)(out + i + stride_mm256_q(2))) = y3;
		*((__m256d*)(out + i + stride_mm256_q(3))) = y4;
	}
}
#endif // USE_AVX2

#if USE_AVX512
/* biquad_stride16_section3_avx512
 ************************************************************************/

void biquad_stride16_section3_avx512(const double* inp, double* out, const double* coeff, 
	double* hist, size_t stride, size_t samples, size_t stride_ofs)
{
	// intrinsic stride width is 16
#define stride_mm512 8
#define stride_mm512_q(i) (	 (i == 0) ? 0 * stride_mm512: \
							 (i == 1) ? 1 * stride_mm512 : \
							 (i == 2) ? 2 * stride_mm512 : \
							 (i == 3) ? 3 * stride_mm512 : \
							 (i == 4) ? 4 * stride_mm512 : \
							 (i == 5) ? 5 * stride_mm512 : \
							 (i == 6) ? 6 * stride_mm512 : \
									    7 * stride_mm512)
#define stride16_mm512 16
#define stride16_mm152_num (stride16_mm512 / stride_mm512)

	// number of sections are 3
#define sections3_intrinsic 3

	if (samples < 1) return;
	if ((stride < stride16_mm512) || (stride % stride16_mm512 != 0)) return;
	if (stride_ofs <= 0) stride_ofs = stride;

	// load coefficients into AVX registers
	__m512d fcoeff[sections3_intrinsic * 4 + 1];
        size_t k;
	for (k = 0; k < sections3_intrinsic * 4 + 1; ++k)
	{
		fcoeff[k] = _mm512_set1_pd(coeff[k]);;
	}
	// loop over stride
        size_t i,n,j;
        for (i = 0; i + stride16_mm512 <= stride; i = i + stride16_mm512)
	{
		__m512d y1, y2;	// filter value
		// loop over number of samples
		for (n = 0; n < samples; ++n)
		{
			// get first input value and mulitply it with filter gain
			y1 = _mm512_mul_pd(fcoeff[0], *(__m512d*)(inp + i + n * stride_ofs + stride_mm512_q(0)));
			y2 = _mm512_mul_pd(fcoeff[0], *(__m512d*)(inp + i + n * stride_ofs + stride_mm512_q(1)));
			// Loop over sections
			for (j = 0; j < sections3_intrinsic; ++j)
			{
				// Fetch previous history
				const __m512d w1 = *(__m512d*)(hist + i + (2 * j + 0) * stride + stride_mm512_q(0));
				const __m512d w2 = *(__m512d*)(hist + i + (2 * j + 0) * stride + stride_mm512_q(1));
				const __m512d u1 = *(__m512d*)(hist + i + (2 * j + 1) * stride + stride_mm512_q(0));
				const __m512d u2 = *(__m512d*)(hist + i + (2 * j + 1) * stride + stride_mm512_q(1));

				// Calculate and save new history
				*(__m512d*)(hist + i + (2 * j + 0) * stride + stride_mm512_q(0)) = _mm512_fmadd_pd(fcoeff[4 * j + 2], u1, _mm512_fmadd_pd(fcoeff[4 * j + 1], w1, y1));
				*(__m512d*)(hist + i + (2 * j + 0) * stride + stride_mm512_q(1)) = _mm512_fmadd_pd(fcoeff[4 * j + 2], u2, _mm512_fmadd_pd(fcoeff[4 * j + 1], w2, y2));
				// Difference equations for LIGO biquad
				y1 = _mm512_fmadd_pd(fcoeff[4 * j + 4], u1, _mm512_fmadd_pd(fcoeff[4 * j + 3], w1, y1));
				y2 = _mm512_fmadd_pd(fcoeff[4 * j + 4], u2, _mm512_fmadd_pd(fcoeff[4 * j + 3], w2, y2));
				// Calculate and save new history
				*(__m512d*)(hist + i + (2 * j + 1) * stride + stride_mm512_q(0)) = _mm512_add_pd(u1, w1);
				*(__m512d*)(hist + i + (2 * j + 1) * stride + stride_mm512_q(1)) = _mm512_add_pd(u2, w2);
			}
		}
		// Store output
		*((__m512d*)(out + i + stride_mm512_q(0))) = y1;
		*((__m512d*)(out + i + stride_mm512_q(1))) = y2;
	}
}
#endif // USE_AVX512


void biquad_stride4_section3(const double* inp, double* out,
                         const double* coeff, double* hist, size_t stride, size_t samples, size_t stride_ofs)
{
#if USE_AVX2
    biquad_stride4_section3_avx2(inp, out, coeff, hist, stride, samples, stride_ofs);
#else
    biquad_stride2_std(inp, out, coeff, hist, 3, stride, samples, stride_ofs);
#endif
}

void biquad_stride16_section3(const double* inp, double* out,
                          const double* coeff, double* hist, size_t stride, size_t samples, size_t stride_ofs)
{
#if USE_AVX512
    biquad_stride16_section3_avx512(inp, out, coeff, hist, stride, samples, stride_ofs);
#elif USE_AVX2
#if	defined(__AVX512F__) && defined(__AVX512CD__) && defined(__AVX512DQ__) && defined(__AVX512BW__)
    // There is a difference of using AVX2 on a machine that supports AVX512 and one that
    // doesn't. The former has 32 internal ymm registers, whereas the later only has 16.
    biquad_stride16_section3_avx2(inp, out, coeff, hist, stride, samples, stride_ofs);
#else
    // A stride of 16 isn't efficient if only 16 registers are present, so we use a stride of 4.
    biquad_stride4_section3_avx2(inp, out, coeff, hist, stride, samples, stride_ofs);
#endif
#elif USE_SSE3
#if	defined(__AVX512F__) && defined(__AVX512CD__) && defined(__AVX512DQ__) && defined(__AVX512BW__)
    // There is a difference of using AVX2 on a machine that supports AVX512 and one that
    // doesn't. The former has 32 internal xmm registers, whereas the later only has 16.
    biquad_stride8_section3_sse3(inp, out, coeff, hist, stride, samples, stride_ofs);
#else
    // A stride of 16 isn't efficient if only 16 registers are present, so we use a stride of 4.
    biquad_stride2_section3_sse3(inp, out, coeff, hist, stride, samples, stride_ofs);
#endif
#else
    biquad_stride2_std(inp, out, coeff, hist, 3, stride, samples, stride_ofs);
#endif
}

