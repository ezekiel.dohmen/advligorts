#ifndef LIGO_MAPAPP_H
#define LIGO_MAPAPP_H

#include "controllerko.h" //ioMemData
#include "drv/cdsHardware.h" //CDS_HARDWARE

#ifdef __cplusplus
extern "C" {
#endif

int mapPciModules( CDS_HARDWARE* );
void initmap(CDS_HARDWARE* );
void unmapPciModules(CDS_HARDWARE* pCds); /* Cleans up any card areas that we have allocated*/


#ifdef __cplusplus
}
#endif


#endif //LIGO_MAPAPP_H
