#ifndef LIGO_VERIFY_SLOTS_H
#define LIGO_VERIFY_SLOTS_H

#include "drv/cdsHardware.h" //CDS_HARDWARE


#ifdef __cplusplus
extern "C" {
#endif

int verify_cards2slots( CDS_HARDWARE* );
int adc_cards2slots( CDS_HARDWARE*, int );
int dac_cards2slots( CDS_HARDWARE*, int );


#ifdef __cplusplus
}
#endif


#endif //LIGO_VERIFY_SLOTS_H
