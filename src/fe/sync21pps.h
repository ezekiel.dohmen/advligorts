#ifndef LIGO_SYNC21PPS_H
#define LIGO_SYNC21PPS_H

#include "cds_types.h"
#include "controller.h"

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_CYCLE_FOR_1PPS_TO_BE_HIGH (2)
#define MAX_CYCLES_TO_WAIT_FOR_1PPS_HIGH (MODEL_RATE_HZ * 4) //4 seconds
#define INVALID_CYCLE_1PPS -1

typedef enum 
{
    SYNC2PPS_ERROR_OK = 0,
    SYNC2PPS_ERROR_SYSTIME = -1,
    SYNC2PPS_ERROR_CHAN_HOP = CHAN_HOP_ERROR,  //-4
    SYNC2PPS_ERROR_ADC_TO = ADC_TO_ERROR, //-7
} SYNC2PPS_ERROR_T;

typedef struct one_pps_sync_t
{
    float value; /// @param onePps Value of 1PPS signal, if used, for diagnostics
    int last_cycle_high; /// @param last_cycle_high One PPS diagnostic check
    int transition_high_cycle;
} one_pps_sync_t;


void print_sync2pps_error(SYNC2PPS_ERROR_T error_no);

SYNC2PPS_ERROR_T sync2pps_signal( one_pps_sync_t* p1pps, adcInfo_t* padcinfo, uint64_t cpuClock[] );

int sync21pps_check( one_pps_sync_t* p1pps, adcInfo_t* padcinfo, int hk_cycle );


#ifdef __cplusplus
}
#endif


#endif // LIGO_SYNC21PPS_H
