package gps

/*
#include <stdint.h>
#include <sys/ioctl.h>

#define IOCTL_GPSTIME_TIME 1
#define IOCTL_GPSTIME_PAUSE _IO('x', 2)
#define IOCTL_GPSTIME_RESUME _IO('x', 3)
#define IOCTL_GPSTIME_RESET _IO('x', 4)
#define IOCTL_GPSTIME_STEP _IO('x', 5)

static void gps_now(int fd, int64_t *dest_sec, int32_t *dest_nano) {
	unsigned long t[3];
	ioctl( fd, IOCTL_GPSTIME_TIME, &t);
	t[1] *= 1000;
	t[1] += t[2];
	*dest_sec = t[0];
	*dest_nano = t[1];
}

static void gps_action(int fd, int action) {
	ioctl( fd, action );
}

static void gps_pause(int fd) {
	gps_action(fd, IOCTL_GPSTIME_PAUSE);
}

static void gps_resume(int fd) {
	gps_action(fd, IOCTL_GPSTIME_RESUME);
}

static void gps_step(int fd) {
	gps_action(fd, IOCTL_GPSTIME_STEP);
}

*/
import "C"
import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

const ioctlStatus = 0
const ioctlTime = 1

// found via
// a := time.Date(1900, 1, 1, 0, 0, 0, 0, time.UTC)
// fmt.Println(a)
// fmt.Println(a.Unix())
const unixToNtpOffset = 2208988800

// found via
// a := time.Date(1980, 1, 6, 0, 0, 0, 0, time.UTC)
// fmt.Println(a)
// fmt.Println(a.Unix())
const gpsToUtcOffset = -315964800

// read from table, number of leapseconds on Jan 1 1980
const leapSecAtGPSEpoch = 19

type Clock interface {
	io.Closer
	Now() time.Time
	String() string
	Paused() bool
	Pause()
	Resume()
	Step()
}

type GpsClock struct {
	gpsFile *os.File
	gpsFd   uintptr
	offset  int64
}

func split(input string, pattern string) []string {
	out := make([]string, 0)
	for _, line := range strings.Split(input, pattern) {
		if len(line) != 0 {
			out = append(out, line)
		}
	}
	return out
}

func systemClockGps() (Clock, error) {
	var curOffset int64 = 0
	nowNtp := time.Now().Unix() + unixToNtpOffset
	leapFile, err := os.Open("/usr/share/zoneinfo/leap-seconds.list")
	if err != nil {
		return nil, err
	}
	defer leapFile.Close()
	scanner := bufio.NewScanner(leapFile)
	for scanner.Scan() {
		line := scanner.Text()
		splitIndex := strings.Index(line, "#")
		if splitIndex >= 0 {
			line = line[0:splitIndex]
		}
		line = strings.Trim(line, " \t\r\n\v")
		if len(line) == 0 {
			continue
		}
		parts := split(line, " ")
		if len(parts) != 2 {
			continue
		}
		ntp, err := strconv.ParseInt(parts[0], 10, 64)
		if err != nil {
			continue
		}
		offset, err := strconv.ParseInt(parts[1], 10, 64)
		if err != nil {
			continue
		}
		if nowNtp >= ntp {
			curOffset = offset
		}
	}
	return &GpsClock{
		gpsFile: nil,
		gpsFd:   0,
		offset:  curOffset,
	}, nil
}

func NewClock() (Clock, error) {
	gpsFile, err := os.Open("/dev/gpstime")
	if err != nil {
		return NewSystemGpsClock()
	}
	return &GpsClock{
		gpsFile: gpsFile,
		gpsFd:   gpsFile.Fd(),
		offset:  0,
	}, nil
}

func (c *GpsClock) Close() error {
	if c != nil {
		c.gpsFile.Close()
	}
	return nil
}

func (c *GpsClock) Now() time.Time {
	var gpsSec C.int64_t
	var gpsNano C.int32_t

	if c.gpsFile != nil {
		C.gps_now(C.int(c.gpsFd), &gpsSec, &gpsNano)
		return time.Unix(int64(gpsSec+C.int64_t(c.offset)), int64(gpsNano))
	} else {
		now := time.Now()
		return time.Unix(int64(now.Unix()+unixToNtpOffset), int64(now.Nanosecond()))
	}
}

func (c *GpsClock) String() string {
	t := c.Now()
	return fmt.Sprintf("%d:%d", t.Unix(), t.Nanosecond())
}

func (c *GpsClock) Paused() bool {
	if c.gpsFile == nil {
		return false
	}
	f, err := os.Open("/sys/kernel/gpstime/debug_pause_status")
	if err != nil {
		return false
	}
	defer f.Close()
	var buffer [1]byte
	n, err := f.Read(buffer[:])
	if n == 0 || err != nil {
		return false
	}
	return buffer[0] == '1'
}

func (c *GpsClock) Pause() {
	if c.gpsFile != nil {
		C.gps_pause(C.int(c.gpsFd))
	}
}

func (c *GpsClock) Resume() {
	if c.gpsFile != nil {
		C.gps_resume(C.int(c.gpsFd))
	}
}

func (c *GpsClock) Step() {
	if c.gpsFile != nil {
		C.gps_step(C.int(c.gpsFd))
	}
}

func getLeapSecs() int64 {
	const defaultLeapSecDuration = 37 - 19
	var curOffset int64 = 0
	nowNtp := time.Now().Unix() + unixToNtpOffset
	leapFile, err := os.Open("/usr/share/zoneinfo/leap-seconds.list")
	if err != nil {
		return defaultLeapSecDuration
	}
	defer leapFile.Close()
	scanner := bufio.NewScanner(leapFile)
	for scanner.Scan() {
		line := scanner.Text()
		splitIndex := strings.Index(line, "#")
		if splitIndex >= 0 {
			line = line[0:splitIndex]
		}
		line = strings.Trim(line, " \t\r\n\v")
		if len(line) == 0 {
			continue
		}
		parts := split(line, " ")
		if len(parts) != 2 {
			continue
		}
		ntp, err := strconv.ParseInt(parts[0], 10, 64)
		if err != nil {
			continue
		}
		offset, err := strconv.ParseInt(parts[1], 10, 64)
		if err != nil {
			continue
		}
		if nowNtp >= ntp {
			curOffset = offset
		}
	}
	return curOffset
}

type SystemGpsClock struct {
	gpsOffset     int64
	currentOffset time.Duration
	paused        bool
	pausedAt      time.Time
	lock          sync.Mutex
}

func NewSystemGpsClock() (*SystemGpsClock, error) {
	return &SystemGpsClock{
		gpsOffset:     gpsToUtcOffset + getLeapSecs() - leapSecAtGPSEpoch,
		currentOffset: time.Duration(0),
		paused:        false,
		pausedAt:      time.Time{},
		lock:          sync.Mutex{},
	}, nil
}

func (s *SystemGpsClock) Close() error {
	return nil
}

func (s *SystemGpsClock) Now() time.Time {
	s.lock.Lock()
	defer s.lock.Unlock()

	t0 := time.Now()
	if s.paused {
		t0 = s.pausedAt
	}
	t0 = t0.Add(s.currentOffset)
	return time.Unix(t0.Unix()+s.gpsOffset, int64(t0.Nanosecond()))
}

func (s *SystemGpsClock) String() string {
	t := s.Now()
	return fmt.Sprintf("%d:%d", t.Unix(), t.Nanosecond())
}

func (s *SystemGpsClock) Paused() bool {
	s.lock.Lock()
	defer s.lock.Unlock()
	return s.paused
}

func (s *SystemGpsClock) Pause() {
	s.lock.Lock()
	defer s.lock.Unlock()

	if !s.paused {
		s.pausedAt = time.Now()
		s.paused = true
	}
}

func (s *SystemGpsClock) Resume() {
	s.lock.Lock()
	defer s.lock.Unlock()

	if s.paused {
		delta := s.pausedAt.Sub(time.Now())
		s.currentOffset = s.currentOffset + delta
		s.paused = false
	}
}

func (s *SystemGpsClock) Step() {
	s.lock.Lock()
	defer s.lock.Unlock()

	if s.paused {
		s.pausedAt = s.pausedAt.Add(time.Second / 16)
	} else {
		s.currentOffset += time.Duration(time.Second / 16)
	}
}
