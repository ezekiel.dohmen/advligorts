package gps

import (
	"testing"
	"time"
)

func TestNewSystemGpsClock(t *testing.T) {
	clock, err := NewSystemGpsClock()
	if err != nil {
		t.Errorf("NewSystemGpsClock returned an error %v", err)
	}
	t0 := clock.Now()
	time.Sleep(time.Millisecond * 25)
	clock.Pause()
	t1 := clock.Now()
	time.Sleep(time.Millisecond * 25)
	t2 := clock.Now()

	if t0 == t1 {
		t.Errorf("time did not progress as expected, cannot show a pause")
	}
	if t1 != t2 {
		t.Errorf("The pause call did nothing t1 != t2")
	}

	clock.Step()
	t3 := clock.Now()
	actualDur := t3.Sub(t2)
	expectedDur := time.Second / 16
	if expectedDur != actualDur {
		t.Errorf("The Step call did not step time as expected")
	}

}
