add_library(fe_stream_generator STATIC
        fe_stream_generator.cc
        fe_stream_generator.hh
        str_split.cc
        str_split.hh
        fe_generator_support.hh
        fe_generator_support.cc)
target_include_directories(fe_stream_generator PUBLIC ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/../include)

add_executable(fe_stream_check
        fe_stream_check.cc
        )
target_link_libraries(fe_stream_check
        PRIVATE
        fe_stream_generator
        rt
        driver::shmem
        Threads::Threads)

add_executable(fe_check fe_check.cc)
target_link_libraries(fe_check
        PRIVATE
        rt
        driver::shmem)

install(TARGETS fe_stream_check
        DESTINATION bin)
add_subdirectory(fe_simulation)


if (libNDS2Client_FOUND)

add_executable(fe_stream_check_nds fe_stream_check_nds.cc)
target_link_libraries(fe_stream_check_nds
        PRIVATE
        fe_stream_generator
        nds2client::cxx
        driver::shmem
        Threads::Threads
        Boost::filesystem
        Boost::system)

add_executable(fe_stream_check_edcu_nds fe_stream_check_edcu_nds.cc)
target_include_directories(fe_stream_check_edcu_nds PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../include)
target_link_libraries(fe_stream_check_edcu_nds
        PRIVATE
        fe_stream_generator
        nds2client::cxx
        driver::shmem)

install(TARGETS fe_stream_check_nds DESTINATION bin)

endif (libNDS2Client_FOUND)