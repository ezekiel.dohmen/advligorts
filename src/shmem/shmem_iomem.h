//
// Created by erik.vonreis on 5/5/21.
//

#ifndef DAQD_TRUNK_SHMEM_IOMEM_H
#define DAQD_TRUNK_SHMEM_IOMEM_H

#include "drv/cdsHardware.h"

// The below logic rounds up to the nearest multiple of 1MB
#define SHMEM_IOMEM_SIZE_BYTES ( ROUND_UP(sizeof(IO_MEM_DATA) + 0x4000, (1024 * 1024)) )

#define SHMEM_IOMEM_NAME "ipc"
// Divides by 2^20 bytes to get size in MB
#define SHMEM_IOMEM_SIZE_MB ( SHMEM_IOMEM_SIZE_BYTES / (1024 * 1024) )
#define SHMEM_IOMEM_SIZE (SHMEM_IOMEM_SIZE_BYTES)
#define SHMEM_IOMEM_STRUCT IO_MEM_DATA

#endif // DAQD_TRUNK_SHMEM_IOMEM_H
