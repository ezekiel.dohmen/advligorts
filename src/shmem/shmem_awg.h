#ifndef SHMEM_AWG_H
#define SHMEM_AWG_H

#include "awg_data.h"

#define SHMEM_AWG_SUFFIX "_awg"
#define SHMEM_AWG_SIZE_MB 64
#define SHMEM_AWG_SIZE ( SHMEM_AWG_SIZE_MB * ( 1024 ) * ( 1024 ) )

#define SHMEM_AWG_STRUCT AWG_DATA;

#endif // SHMEM_AWG_H
