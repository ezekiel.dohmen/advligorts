#include "../../include/commData3.h"
#include "../../include/util/timing.h"
#include "../drv/gpstime/gpstime_kernel.h"
#include "../drv/rts-cpu-isolator/rts-cpu-isolator.h"

//LIGO Netlink Message Definitions
#include "../dolphin_daemon/include/daemon_messages.h"

#define RTS_LOG_PREFIX "cdsrfm"
#include "drv/rts-logger.h"

#include <linux/module.h>       /* Needed by all modules */
#include <linux/kernel.h>       /* Needed for KERN_INFO */
#include <linux/kthread.h>       /* Needed for KERN_INFO */
#include <asm/cacheflush.h>
#include <asm/uaccess.h>
#include <asm/delay.h>
#include <linux/proc_fs.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/string.h>
#include <linux/timer.h>
#include <linux/ctype.h>

//Netlink Socket Headers
#include <net/sock.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>




#define RFMX_NUM_DOLPHIN_CARDS  3
#define RFMX_TOTAL_SEGMENTS     4
#define RFMX_NUM_COPY_CPUS      4
#define RFMX_MAX_CHANS_PER_RFM  100
#define RFMX_MAX_XFER_CNT   100000000

//Segment ID 1 is used by models for the RFM IPCs and 
//these are the IPCs that get routed to the legs(X/Y) and 
//corner station
#define RFMX_SEGMENT_ID 1

//These map you into the g_dolphin_(read/write)_addrs, RFMX_TOTAL_SEGMENTS
//need to match the number of defines in this list
#define RFM0_EX_ADDR_INDEX 0
#define RFM0_CS_ADDR_INDEX 1
#define RFM1_CS_ADDR_INDEX 2
#define RFM1_EY_ADDR_INDEX 3


//Time to wait for a response from the dolphin daemon before
//timing out and signaling an error
#define DAEMON_RESP_TIMEOUT_MS 2000
#define DAEMON_RESP_CHECK_RATE_MS 30


//These define what dolphin adapters are connected
//to the three dolphin networks 
static int CORNER_ADAPTER_NUM = 2;
module_param(CORNER_ADAPTER_NUM, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(CORNER_ADAPTER_NUM, "The adapter index where the cdsrfm is connected to the corner station.");

static int RFM0_EX_ADAPTER_NUM = 1;
module_param(RFM0_EX_ADAPTER_NUM, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(RFM0_EX_ADAPTER_NUM, "The adapter index where the cdsrfm is connected to the RFM0 (The X leg).");

static int RFM1_EY_ADAPTER_NUM = 0;
module_param(RFM1_EY_ADAPTER_NUM, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(RFM1_EY_ADAPTER_NUM, "The adapter index where the cdsrfm is connected to the RFM1 (The Y leg).");

static int MON_THREAD_CORE = 0;
module_param(MON_THREAD_CORE, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(MON_THREAD_CORE, "What core should the monitor thread be run on");


//Dolphin adapter lookup, this is used to always order the indexes in a known order 
static unsigned g_adapter_map [RFMX_NUM_DOLPHIN_CARDS] = {0,}; //gets configured in module load
static CDS_IPC_COMMS *g_dolphin_read_addrs[RFMX_TOTAL_SEGMENTS];
static CDS_IPC_COMMS *g_dolphin_write_addrs[RFMX_TOTAL_SEGMENTS];

//Dolphin Daemon Messaging Globals
static atomic_t g_dolphin_init_success = ATOMIC_INIT(0);
static struct sock * g_nl_sock = NULL;
static int g_dolphin_bad_init = 1;

// Structs for kernel threads
static struct task_struct *g_monitor_thread;
struct monitor_thread_params_t
{
    char name[100];
    int delay;
    int idx;
    int netFrom;
    int netTo;
};
struct monitor_thread_params_t g_monitor_thread_params;

static unsigned long mycounter[10];

// IPC block arrays for monitoring and switching
// IPC_BLOCKS = 64 and MAX_IPC = 512
static unsigned long syncArray[RFMX_TOTAL_SEGMENTS][IPC_BLOCKS][MAX_IPC];
static unsigned      expected_ipc_datapoint [RFMX_TOTAL_SEGMENTS][MAX_IPC] = {{0,},{0,},{0,},{0,}}; //TODO: We don't really need MAX_IPC on some of these dimensions (we only do 64)
static unsigned long lastSyncWord[RFMX_TOTAL_SEGMENTS][RFMX_MAX_CHANS_PER_RFM];
static unsigned int myactive[RFMX_TOTAL_SEGMENTS][10];
static unsigned int mytraffic[RFMX_TOTAL_SEGMENTS];

static uint64_t     start_times_tsc[RFMX_NUM_COPY_CPUS];
volatile static uint64_t     max_copy_times_ns[RFMX_NUM_COPY_CPUS] = {0,};
volatile static uint64_t     total_copies[RFMX_NUM_COPY_CPUS] = {1,};

static int ipcActive[RFMX_TOTAL_SEGMENTS][RFMX_MAX_CHANS_PER_RFM];
static volatile int stop_working_threads;
static int mysysstatus;
static int g_copy_cpu_allocated [RFMX_NUM_COPY_CPUS];
atomic_t g_atom_has_exited[] = {ATOMIC_INIT(0), ATOMIC_INIT(0), ATOMIC_INIT(0), ATOMIC_INIT(0)};


// Function prototypes for the /sys/kernel/ (sysfs)
static ssize_t cdsrfm_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf);
static ssize_t cdsrfm_store(struct kobject *kobj, struct kobj_attribute *attr,
                            const char *buf, size_t count);

// Data for using /sys/kernel/
#define PERM 0644
static struct kobject *cdsrfm_kobject;
static struct kobj_attribute cdsrfm_attribute =__ATTR(stats, PERM, cdsrfm_show,
                                                   cdsrfm_store);
// End of globals *********************************************************



//
// This function gets the current time from the GPS driver
inline unsigned long my_current_time(void) {
    LIGO_TIMESPEC t;
    ligo_gpstime_get_ts(&t);
    return t.tv_sec;
}

// ************************************************************************
// Code thread monitors all switch ports for activity.
// Marks channels by network as active or not for use by data xfer threads.
int monitorActiveConnections(void *data) 
{
    int indx = ((struct monitor_thread_params_t*)data)->idx;
    int delay = ((struct monitor_thread_params_t*)data)->delay;

    unsigned long syncWord;
    int ii,jj,kk,mm;
    static int traffic[RFMX_TOTAL_SEGMENTS];
    int swstatus;

    if(g_dolphin_read_addrs[RFM0_EX_ADDR_INDEX] != NULL &&
       g_dolphin_read_addrs[RFM0_CS_ADDR_INDEX] != NULL && 
       g_dolphin_read_addrs[RFM1_CS_ADDR_INDEX] != NULL && 
       g_dolphin_read_addrs[RFM1_EY_ADDR_INDEX] != NULL)
    {
        while(!kthread_should_stop()) {
            // Check port activity once per second
            msleep(delay);
            for (jj=0;jj<RFMX_TOTAL_SEGMENTS;jj++) {

                mycounter[jj] = 0;
                for(ii=0;ii<indx;ii++) {
                    // Will send port activity info to EPICS via 32 bit ints
                    kk = ii/ 32;
                    mm = ii % 32;
                    // Determine active channels by change in data timestamp
                    // Only checking first data block of each channel, as should have
                    // changed in the last second if active.
                    syncWord = g_dolphin_read_addrs[jj]->dBlock[0][ii].timestamp;
                    if(syncWord != lastSyncWord[jj][ii]) {
                        // Indicate channel active for data xfer threads
                        ipcActive[jj][ii] = 1;
                        lastSyncWord[jj][ii] = syncWord;
                        // Increment the active channel counter for this network
                        mycounter[jj] += 1;
                        // Set the active port info for EPICS
                        myactive[jj][kk] |= (1<<mm);
                    } else {
                        // Indicate channel NOT active for data xfer threads
                        ipcActive[jj][ii] = 0;
                        // Unset active port info for EPICS
                        myactive[jj][kk] &= ~(1<<mm);
                    }
                }
            }
            // Get current GPS time
            // Sent to EPICS as indicator switch code running
            mycounter[9] = my_current_time();
            // Check traffic for all data xfer threads
            // and set thread status bits.
            swstatus = 0;
            for (jj=0;jj<RFMX_TOTAL_SEGMENTS;jj++) {
                if(traffic[jj] != mytraffic[jj]) {
                    traffic[jj] = mytraffic[jj];
                    swstatus |= (1 << jj);
                }
            }
            // Copy data xfer thread status to be sent to EPICS
            mysysstatus = swstatus;
        }
        RTSLOG_INFO("%s thread has terminated %d\n",((struct monitor_thread_params_t*)data)->name, indx);
        return 0;
    } else {
        RTSLOG_ERROR("Do not have pointers to Dolphin read - %s exiting \n", ((struct monitor_thread_params_t*)data)->name);
        return -1;
    }
}


// ************************************************************************
// Common code to xfer data between 2 RFM links
inline int copyIpcData (int start_indx, int end_indx, int netFrom, int netTo)
{
    unsigned long syncWord;
    int ii,jj, start_ipc_datapoint;
    int cblock,dblock,ttcache;
    int eor = end_indx;
    CDS_IPC_XMIT tmp_block;
    int xfers = 0;

    // Reset cache flushing indices and flag
    cblock = 0;
    dblock = 0;
    ttcache = 0;
    // Scan thru switching ports for new data
    for(ii=start_indx;ii<=eor;ii++) {
        // Copy data only if port is active, as marked by switch monitor task
        if(ipcActive[netFrom][ii]) {

            start_ipc_datapoint = expected_ipc_datapoint[netTo][ii];

            // Scan thru 64 data blocks associated with a channel for new data
            // Only check every 4th block, as max RFM channel rate is 16K
            for(jj=0; jj<IPC_BLOCKS; jj+=4) {
                // Determine new data by comparing present timestamp with last xfer timestamp
                syncWord = g_dolphin_read_addrs[netFrom]->dBlock[start_ipc_datapoint][ii].timestamp;
                if(syncWord != syncArray[netFrom][start_ipc_datapoint][ii]) {


                    xfers ++;
                    // Copy data and time stamp to next Dolphin switch
                    tmp_block.data = g_dolphin_read_addrs[netFrom]->dBlock[start_ipc_datapoint][ii].data;
                    tmp_block.timestamp = syncWord;
                    memcpy( &g_dolphin_write_addrs[netTo]->dBlock[start_ipc_datapoint][ii], &tmp_block, sizeof(tmp_block));
                    syncArray[netFrom][start_ipc_datapoint][ii] = syncWord;
                    expected_ipc_datapoint[netTo][ii] = (start_ipc_datapoint + 4 ) % IPC_BLOCKS;
                    cblock = start_ipc_datapoint;
                    dblock = ii;
                    ttcache += 1;



                    break;
                }
                start_ipc_datapoint = (start_ipc_datapoint + 4) % IPC_BLOCKS;
                
            }
        }
    }

    // If anything was copied, we need to flush the buffer
    if (ttcache > 0) clflush_cache_range (&(g_dolphin_write_addrs[netTo]->dBlock[cblock][dblock].data), 16);
    //if(!ttcache) udelay(1);
    // return total xfers, which is used as code running diagnostic
    return xfers;
}


// DATA XFER Threads  *****************************************************
// Following 4 code modules are the switch data xfer threads. Each thread:
//  - Is locked to a CPU core (3-6)
//  - Transfers 32 channels of data between EX and CS or CS and EY.
//
//  Note: The (total_copies[function_indx] & 2097151ULL) is a fast modulus
//        that clears the mat every 2097152 loops
//
// ************************************************************************

#define COPY_WORKER_FUNC(function_name, function_indx, copy_start_index, copy_end_index, buffer_index_0, buffer_index_1) \
    void function_name (void) { \
        static int totalxfers; \
                                     \
        if( g_dolphin_read_addrs[buffer_index_0] != NULL && g_dolphin_write_addrs[buffer_index_1] != NULL) \
        { \
            while(!stop_working_threads) { \
                timer_start(&start_times_tsc[function_indx]); \
                totalxfers += copyIpcData (copy_start_index, copy_end_index, buffer_index_0, buffer_index_1); \
                totalxfers %= RFMX_MAX_XFER_CNT; \
                mytraffic[ function_indx ] = totalxfers; \
                timer_end_ns(&start_times_tsc[function_indx]); \
                if( start_times_tsc[function_indx] >  max_copy_times_ns[function_indx] ) \
                { \
                    max_copy_times_ns[function_indx] = start_times_tsc[function_indx]; \
                } \
                ++total_copies[function_indx]; \
                if( (total_copies[function_indx] & 2097151ULL) == 0 ) \
                { \
                    max_copy_times_ns[function_indx] = 0; \
                } \
            } \
            RTSLOG_INFO("%s thread has terminated.\n", #function_name); \
            atomic_set(&g_atom_has_exited[ function_indx ], 1); \
            return; \
        } else { \
            RTSLOG_ERROR("Do not have pointers to Dolphin read - %s exiting \n", #function_name); \
            atomic_set(&g_atom_has_exited[ function_indx ], 1); \
            return; \
        } \
} \


// Thread to copy data chans 0-31 between EX and CS (RFM0)
COPY_WORKER_FUNC(copyRfmDataEX2CS1, 0, 0, 64,  RFM0_CS_ADDR_INDEX, RFM0_EX_ADDR_INDEX);
COPY_WORKER_FUNC(copyRfmDataEX2CS0, 1, 0, 64, RFM0_EX_ADDR_INDEX, RFM0_CS_ADDR_INDEX);
// Thread to copy data chans 0-31 between EY and CS (RFM1)
COPY_WORKER_FUNC(copyRfmDataEY2CS0, 2, 0, 64, RFM1_CS_ADDR_INDEX, RFM1_EY_ADDR_INDEX);
COPY_WORKER_FUNC(copyRfmDataEY2CS1, 3, 0, 64,  RFM1_EY_ADDR_INDEX, RFM1_CS_ADDR_INDEX);



// ************************************************************************
// Dolphin NIC initialization functions.
// ************************************************************************

//
// Callback for the netlink unicast response message
//
static void netlink_recv_resp(struct sk_buff *skb)
{
    struct nlmsghdr *nlh;
    int pid;
    all_dolphin_daemon_msgs_union any_msg;

    //Store info and pointer to message
    nlh = (struct nlmsghdr *)skb->data;
    pid = nlh->nlmsg_pid; // pid of sending process
    any_msg.as_hdr = (dolphin_mc_header *) nlmsg_data(nlh);


    //Make sure we got (at least) a good header, before we dive into message
    if ( !check_mc_header_valid(any_msg.as_hdr, nlmsg_len(nlh)) )
    {
        RTSLOG_ERROR("netlink_test_recv_msg() - Malformed message, too short or bad preamble\n");
        atomic_set(&g_dolphin_init_success, -1);
        return;
    }

    if(any_msg.as_hdr->interface_id != RT_KM_INTERFACE_ID)
    {
        RTSLOG_ERROR("netlink_test_recv_msg() - Got a message that had an interface_id for somthing else.\n");
        atomic_set(&g_dolphin_init_success, -1);
        return; //Msg not ment for us
    }

    switch(any_msg.as_hdr->msg_id)
    {
        case DOLPHIN_DAEMON_ALLOC_RESP:
            if ( !check_alloc_resp_valid(any_msg.as_alloc_resp, nlmsg_len(nlh)) )
            {
                RTSLOG_ERROR("netlink_test_recv_msg() - Got a DOLPHIN_DAEMON_ALLOC_RESP, but message was not valid, discarding. "
                        "Size was %u\n", nlmsg_len(nlh));
                atomic_set(&g_dolphin_init_success, -1);
                return;
            }

            if ( any_msg.as_alloc_resp->status != DOLPHIN_ERROR_OK )
            {
                RTSLOG_ERROR("netlink_test_recv_msg() - An error (%d) was returned by the dolphin daemon. Failed to init dolphin.\n", any_msg.as_alloc_resp->status);
                atomic_set(&g_dolphin_init_success, -1);
                return;
            }

            if( any_msg.as_alloc_resp->num_addrs != RFMX_NUM_DOLPHIN_CARDS )
            {
                RTSLOG_ERROR("netlink_test_recv_msg() - The number of returned addrs was %u, "
                             "but we requested %u segments.\n",
                             any_msg.as_alloc_resp->num_addrs,
                             RFMX_NUM_DOLPHIN_CARDS);
                atomic_set(&g_dolphin_init_success, -1);
                return;
            }

            //Loop over response and store pointers 
            //TODO: We ASSUME the order returned is the same as the order requested, this is correct, but we might want to return the adapter num in the future
            char * read_addr_tmp;
            char * write_addr_tmp;
            for(int i=0; i < any_msg.as_alloc_resp->num_addrs; ++i)
            {
                read_addr_tmp = (char*)any_msg.as_alloc_resp->addrs[ i ].read_addr;
                write_addr_tmp = (char*)any_msg.as_alloc_resp->addrs[ i ].write_addr;

                //No matter the adapter numbers, g_adapter_map orders them as expected below
                if(i == 0)
                {
                    g_dolphin_read_addrs[RFM0_CS_ADDR_INDEX] = (CDS_IPC_COMMS *)(read_addr_tmp + IPC_PCIE_BASE_OFFSET + RFM0_OFFSET);
                    g_dolphin_write_addrs[RFM0_CS_ADDR_INDEX] = (CDS_IPC_COMMS *)(write_addr_tmp + IPC_PCIE_BASE_OFFSET + RFM0_OFFSET);
                    g_dolphin_read_addrs[RFM1_CS_ADDR_INDEX] = (CDS_IPC_COMMS *)(read_addr_tmp + IPC_PCIE_BASE_OFFSET + RFM1_OFFSET);
                    g_dolphin_write_addrs[RFM1_CS_ADDR_INDEX] = (CDS_IPC_COMMS *)(write_addr_tmp + IPC_PCIE_BASE_OFFSET + RFM1_OFFSET);
                }
                else if (i == 1)
                {
                    g_dolphin_read_addrs[RFM0_EX_ADDR_INDEX] = (CDS_IPC_COMMS *)(read_addr_tmp + IPC_PCIE_BASE_OFFSET + RFM0_OFFSET);
                    g_dolphin_write_addrs[RFM0_EX_ADDR_INDEX] = (CDS_IPC_COMMS *)(write_addr_tmp + IPC_PCIE_BASE_OFFSET + RFM0_OFFSET);
                }
                else if (i == 2)
                {
                    g_dolphin_read_addrs[RFM1_EY_ADDR_INDEX] = (CDS_IPC_COMMS *)(read_addr_tmp + IPC_PCIE_BASE_OFFSET + RFM1_OFFSET);
                    g_dolphin_write_addrs[RFM1_EY_ADDR_INDEX] = (CDS_IPC_COMMS *)(write_addr_tmp + IPC_PCIE_BASE_OFFSET + RFM1_OFFSET);
                }
                else           
                    RTSLOG_WARN("netlink_test_recv_msg() - Unsupported index %d in response\n", i);

            }

        break;

        default:
            RTSLOG_ERROR("netlink_test_recv_msg() - Got a message with ID %u, which we don't support, discarding.\n", any_msg.as_hdr->msg_id);
            atomic_set(&g_dolphin_init_success, -1);
            return;
        break;

    }
    atomic_set(&g_dolphin_init_success, 1);//Signal init was successful

}


int init_dolphin( void ) 
{
    struct sk_buff *skb_out;
    struct nlmsghdr *nlh;
    int res;
    dolphin_mc_alloc_req * alloc_req;

    struct netlink_kernel_cfg cfg = {
        .input = netlink_recv_resp,
    };

    g_nl_sock = netlink_kernel_create(&init_net, DOLPHIN_DAEMON_REQ_LINK_ID, &cfg);
    if (!g_nl_sock) {
        RTSLOG_ERROR("init_dolphin() - Error creating netlink socket.\n");
        return -1;
    }


    //Start message creation
    unsigned total_payload_sz = GET_ALLOC_REQ_SZ(RFMX_NUM_DOLPHIN_CARDS);

    ///Allocate the request's msg space, OS does free once it is sent
    skb_out = nlmsg_new( total_payload_sz, 0);
    if (!skb_out) {
        RTSLOG_ERROR("init_dolphin() - Failed to allocate skb for allocation request. Not sending...\n");
        return -1;
    }

    //Fill allocation request
    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, total_payload_sz, 0);
    alloc_req = (dolphin_mc_alloc_req*) nlmsg_data(nlh);
    alloc_req->header.msg_id = DOLPHIN_DAEMON_ALLOC_REQ;
    alloc_req->header.preamble_seq = DOLPHIN_DAEMON_MSG_PREAMBLE;
    alloc_req->num_segments = RFMX_NUM_DOLPHIN_CARDS;
    for(int i =0; i < RFMX_NUM_DOLPHIN_CARDS; ++i)
    {
        //The RFMX uses the same segment ID across networks for RFM IPCs
        alloc_req->segments[i].segment_id = RFMX_SEGMENT_ID;
        //Each connection uses a diffrent dolphin adapter, and we bridge the network across them
        alloc_req->segments[i].adapter_num = g_adapter_map[i];
        alloc_req->segments[i].segment_sz_bytes = IPC_TOTAL_ALLOC_SIZE;
    }

    res = nlmsg_multicast(g_nl_sock, skb_out, 0, DOLPHIN_DAEMON_REQ_GRP_ID, GFP_KERNEL);
    if( res != 0 )
    {
        if( res == -ESRCH)
            RTSLOG_ERROR("init_dolphin() - nlmsg_multicast() failed... Is the dolphin_daemon running?\n");
        else
            RTSLOG_ERROR("init_dolphin() - nlmsg_multicast() returned an error: %d\n", res);
        return -1;
    }

    //We wait for response to be handled by netlink_recv_resp()
    int time_waited_ms = 0;
    while( atomic_read(&g_dolphin_init_success) == 0 && time_waited_ms < DAEMON_RESP_TIMEOUT_MS)
    {
        msleep( DAEMON_RESP_CHECK_RATE_MS );
        time_waited_ms += DAEMON_RESP_CHECK_RATE_MS;
    }

    //Timeout case
    if(atomic_read(&g_dolphin_init_success) == 0)
    {
        RTSLOG_ERROR("init_dolphin() - Did not get a response from the dolphin_daemon in a timely manner.");
        return -1;
    }

    //If there was a returned error
    if(atomic_read(&g_dolphin_init_success) < 0)
    {
        RTSLOG_ERROR("init_dolphin() - dolphin_proxy_km returned an error, check dmesg output for error message\n");
        return -1;
    }

    //Otherwise globals are set and we should be good to go
    g_dolphin_bad_init = 0;
    return 0;


}

// ************************************************************************
void finish_dolphin( void ) 
{
    struct sk_buff *skb_out;
    struct nlmsghdr *nlh;
    int res;
    dolphin_mc_free_all_req * free_req;

    unsigned total_payload_sz = sizeof( dolphin_mc_free_all_req );

    if( g_nl_sock == NULL) return; //If we never set up the socket, just return

    //When this is set the dolphin_init failed, so we don't send the cleanup message
    if( g_dolphin_bad_init)
    {
        netlink_kernel_release(g_nl_sock);
        return;
    }


    //Allocate the request's msg space, OS does free once it's sent
    skb_out = nlmsg_new( total_payload_sz, 0);
    if (!skb_out) {
        RTSLOG_ERROR("finish_dolphin() - Failed to allocate skb for free request. Not sending...\n");
        return;
    }

    //Allocate and send message
    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, total_payload_sz, 0);


    free_req = (dolphin_mc_free_all_req*) nlmsg_data(nlh);
    free_req->header.msg_id = DOLPHIN_DAEMON_FREE_REQ;
    free_req->header.preamble_seq = DOLPHIN_DAEMON_MSG_PREAMBLE;

    res = nlmsg_multicast(g_nl_sock, skb_out, 0, DOLPHIN_DAEMON_REQ_GRP_ID, GFP_KERNEL);
    if( res != 0 )
        RTSLOG_ERROR("finish_dolphin() - nlmsg_multicast() returned an error: %d\n", res);

    netlink_kernel_release(g_nl_sock);
}

// ************************************************************************
// ************************************************************************
// Switching code communicates info with user space via a /sys/kernel file.
// The following routines provide the necessary file functions.
// ************************************************************************

static ssize_t cdsrfm_show(struct kobject *kobj, struct kobj_attribute *attr,
                      char *buf)
{
    return sprintf(buf,
                   "%ld %ld %ld %ld %ld %d %d %d %d %d %d %d %d %d %d %d %d %d"
                   " %llu %llu %llu %llu\n",
                   mycounter[0], mycounter[1], mycounter[2], mycounter[3], mycounter[9],
                   myactive[0][0],myactive[0][1],myactive[0][2],
                   myactive[1][0],myactive[1][1],myactive[1][2],
                   myactive[2][0],myactive[2][1],myactive[2][2],
                   myactive[3][0],myactive[3][1],myactive[3][2], 
                   mysysstatus,
                   max_copy_times_ns[0], max_copy_times_ns[1],
                   max_copy_times_ns[2], max_copy_times_ns[3]
                   );
}

static ssize_t cdsrfm_store(struct kobject *kobj, struct kobj_attribute *attr,
                      const char *buf, size_t count)
{
        return 0;
}


static int __init lr_switch_init(void)
{
    int error;
    ISOLATOR_ERROR ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN]={0,};

    RTSLOG_INFO("Starting CDS RFM SWITCH with %d dolphin adapters.\n", RFMX_NUM_DOLPHIN_CARDS);
    
    //Configure the g_adapter_map, inputs are not const, so we need to do this here
    g_adapter_map[0] = CORNER_ADAPTER_NUM;
    g_adapter_map[1] = RFM0_EX_ADAPTER_NUM;
    g_adapter_map[2] = RFM1_EY_ADAPTER_NUM; 
#if (RFMX_NUM_DOLPHIN_CARDS - 1) != 2
#error The number of cards/adapters does not match what we are configuring
#endif

    // Setup /sys/kernel file to move diag info out to user space for EPICS
    cdsrfm_kobject = kobject_create_and_add("cdsrfm",
                                             kernel_kobj);
    if(!cdsrfm_kobject)
    {
        RTSLOG_ERROR("kobject_create_and_add failed.\n");
        return -ENOMEM;
    }

    error = sysfs_create_file(cdsrfm_kobject, &cdsrfm_attribute.attr);
    if (error) {
        RTSLOG_ERROR("failed to create the stats file in /sys/kernel/cdsrfm/ \n");
        return error;
    }


    // Initialize Dolphin NICs and get data pointers.
    if ( init_dolphin() != 0 )
    {
        RTSLOG_ERROR("lr_switch_init() - Dolphin init failed, km exiting...\n");
        kobject_put(cdsrfm_kobject);
        finish_dolphin();
        return -ENXIO;
    }
    // Reset variable used to stop data xfer threads on rmmod.
    stop_working_threads = 0;


    // Set thread parameters for IPC channel monitoring thread
    strncpy(g_monitor_thread_params.name, "cdsrfmnetmon", 100);
    // Set thread delays
    g_monitor_thread_params.delay = 1000;
    // Set thread number of IPC channels to monitor per RFM network
    g_monitor_thread_params.idx = RFMX_MAX_CHANS_PER_RFM;
    // Set thread netFrom (NOT USED)
    g_monitor_thread_params.netFrom = 0;
    // Set thread netto (NOT USED)
    g_monitor_thread_params.netTo = 0;
    //
    // Create thread that monitors switch port activity.
    g_monitor_thread = kthread_create(monitorActiveConnections, (void *)&g_monitor_thread_params, "cdsrfmnetmon");
    if(IS_ERR(g_monitor_thread)) {
        RTSLOG_ERROR("ERROR! kthread_run\n");
        finish_dolphin();
        return PTR_ERR(g_monitor_thread);
    }
    // Bind thread to CPU MON_THREAD_CORE
    kthread_bind(g_monitor_thread, MON_THREAD_CORE);
    // Start thread
    wake_up_process(g_monitor_thread);



    //Aray of function pointers for isolated cores 
    realtime_thread_fp_t func_ptrs[ RFMX_NUM_COPY_CPUS ] = {copyRfmDataEX2CS0,  copyRfmDataEX2CS1, 
                                                              copyRfmDataEY2CS0, copyRfmDataEY2CS1};

    for(int i=0; i < RFMX_NUM_COPY_CPUS; ++i)
    {
        RTSLOG_INFO("Shutting down next available CPU, at gps_time %ld\n",  my_current_time());

        ret = rts_isolator_run( func_ptrs[i], -1, &g_copy_cpu_allocated[i]);
        if( ret != ISOLATOR_OK)
        {
            isolator_lookup_error_msg(ret, error_msg);
            RTSLOG_ERROR(": Requesting core :  %s\n",  error_msg);
            finish_dolphin();
            return -1;
        }

    }


    return 0;
}

static void __exit lr_switch_exit(void)
{
    int ret;
    ISOLATOR_ERROR isolation_ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN]={0,};
    RTSLOG_INFO("Goodbye, cdsrfmswitch is shutting down\n");

    // Stop the Active Channel monitor thread
    ret = kthread_stop(g_monitor_thread);
    if (ret != -EINTR)
        RTSLOG_INFO("RFM thread has stopped %ld\n", mycounter[1]);
    ssleep(2);

    //Clean up the /sys/kernel/cdsrfm directory
    kobject_put(cdsrfm_kobject);


    stop_working_threads = 1;

    for(int i=0; i < RFMX_NUM_COPY_CPUS; ++i) 
    {
        while (atomic_read(&g_atom_has_exited[i]) == 0)
        {
            msleep( 1 );
        }
        RTSLOG_INFO("Copy thread %d exited, bringing CPU back up...\n", g_copy_cpu_allocated[i]);

        isolation_ret = rts_isolator_cleanup( g_copy_cpu_allocated[i] );
        if( isolation_ret != ISOLATOR_OK)
        {
            isolator_lookup_error_msg(ret, error_msg);
            RTSLOG_ERROR("There was an erro when calling rts_isolator_cleanup(): %s\n", error_msg);
        }
        else
            RTSLOG_INFO("CPU %d is back up.\n", g_copy_cpu_allocated[i]);
    }

    // Cleanup the dolphin NIC connections
    finish_dolphin();
}

module_init(lr_switch_init);
module_exit(lr_switch_exit);

MODULE_AUTHOR("R.Bork");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_DESCRIPTION("Long Range PCIe Switch");
MODULE_SUPPORTED_DEVICE("Long Range PCIe Switch");
