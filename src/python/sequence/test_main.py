import os
import re
import pytest
import subprocess
from py import path
import sys

seqerr_re = re.compile(".*_sequenceErrors.txt$")

test_dir = path.local("test_files")

@pytest.fixture
def wipeout():
    seqfiles = [x for x in os.listdir(test_dir) if seqerr_re.match(x)]
    for f in seqfiles:
        os.remove(test_dir / f)

def file_compare(fname1, fname2):
    with open(fname1,"r") as f1:
        with open(fname2,"r") as f2:
            data1 = f1.read()
            data2 = f2.read()

            if len(data1) != len(data2):
                return False

            for i in range(len(data1)):
                if data1[i] != data2[i]:
                    return False

            return True

def run_check(model):
    result = subprocess.run(["./main.py",test_dir,model], stdout=sys.stdout, stderr=sys.stderr)
    assert file_compare(test_dir / f"{model}_sequenceErrors.txt", test_dir / f"{model}_sequenceErrors.expected")
    return result

def test_lsc_bad(wipeout):
    result = run_check("x2lscbad")
    assert result.returncode == 3

def test_lsc_good(wipeout):
    result = run_check("x2lsc")
    assert result.returncode == 0