add_library(simple_pv STATIC simple_pv.cc
        simple_epics.cc
        simple_pv_internal.cc)
target_include_directories(simple_pv PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories(simple_pv PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/private)
target_link_libraries(simple_pv PUBLIC epics::cas epics::gdd /usr/lib/epics/lib/linux-x86_64/libCom.so epics::ca)
target_requires_cpp11(simple_pv PUBLIC)
install(TARGETS simple_pv DESTINATION lib)
install(FILES simple_pv.h simple_epics.hh DESTINATION include/advligorts)
add_library(pv::simple_pv ALIAS simple_pv)

add_library(simple_pv_so SHARED simple_pv.cc
        simple_epics.cc
        simple_pv_internal.cc)
target_include_directories(simple_pv_so PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories(simple_pv_so PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/private)
target_link_libraries(simple_pv_so PUBLIC epics::cas epics::gdd /usr/lib/epics/lib/linux-x86_64/libCom.so epics::ca)
target_requires_cpp11(simple_pv_so PUBLIC)

add_executable(test_simple_pv tests/test_simple_pv.cc)
target_link_libraries(test_simple_pv PUBLIC simple_pv)
target_include_directories(test_simple_pv PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})

add_executable(test_rw_ioc tests/test_rw_cas.cc)
target_link_libraries(test_rw_ioc PUBLIC simple_pv)