//
// Created by jonathan.hanks on 12/20/19.
//
#include "simple_epics.hh"
#include "simple_pv_internal.hh"

#include <stdexcept>
#include <algorithm>

namespace
{
    StringValue
    default_string_pv_read_fn( void* user )
    {
        if ( !user )
        {
            return string_value_create( nullptr );
        }
        auto* backing = reinterpret_cast< char* >( user );
        return string_value_create( backing );
    }

    int
    default_string_pv_write_fn( void* user, const char* input )
    {
        return 0;
    }

    StringPVIO
    default_ro_string_io( char* data )
    {
        return StringPVIO{ data,
                           default_string_pv_read_fn,
                           default_string_pv_write_fn };
    }
} // namespace

namespace simple_epics
{
    pvStringAttributes::pvStringAttributes( std::string pv_name, char* data )
        : name_{ std::move( pv_name ) }, mode_{ PVMode::ReadOnly }, io_{
              default_ro_string_io( data )
          }
    {
    }

    Server::~Server( ) = default;

    void
    Server::addPV( pvUShortAttributes attr )
    {
        std::lock_guard< std::mutex > l_( m_ );

        auto it = pvs_.find( attr.name( ) );
        if ( it != pvs_.end( ) )
        {
            throw std::runtime_error(
                "Duplicate key insertion to the epics db" );
        }
        std::string name{ attr.name( ) };
        pvs_.insert(
            std::make_pair( std::move( name ),
                            detail::make_unique_ptr< detail::simpleUShortPV >(
                                *this, std::move( attr ) ) ) );
    }

    void
    Server::addPV( pvIntAttributes attr )
    {
        std::lock_guard< std::mutex > l_( m_ );

        auto it = pvs_.find( attr.name( ) );
        if ( it != pvs_.end( ) )
        {
            throw std::runtime_error(
                "Duplicate key insertion to the epics db" );
        }
        std::string name{ attr.name( ) };
        pvs_.insert(
            std::make_pair( std::move( name ),
                            detail::make_unique_ptr< detail::simpleIntPV >(
                                *this, std::move( attr ) ) ) );
    }

    void
    Server::addPV( pvUIntAttributes attr )
    {
        std::lock_guard< std::mutex > l_( m_ );

        auto it = pvs_.find( attr.name( ) );
        if ( it != pvs_.end( ) )
        {
            throw std::runtime_error(
                "Duplicate key insertion to the epics db" );
        }
        std::string name{ attr.name( ) };
        pvs_.insert(
            std::make_pair( std::move( name ),
                            detail::make_unique_ptr< detail::simpleUIntPV >(
                                *this, std::move( attr ) ) ) );
    }

    void
    Server::addPV( pvStringAttributes attr )
    {
        std::lock_guard< std::mutex > l_( m_ );

        auto it = pvs_.find( attr.name( ) );
        if ( it != pvs_.end( ) )
        {
            throw std::runtime_error(
                "Duplicate key insertion to the epics db" );
        }
        std::string name{ attr.name( ) };
        pvs_.insert(
            std::make_pair( std::move( name ),
                            detail::make_unique_ptr< detail::simpleStringPV >(
                                *this, std::move( attr ) ) ) );
    }

    void
    Server::addPV( pvFloatAttributes attr )
    {
        std::lock_guard< std::mutex > l_( m_ );

        auto it = pvs_.find( attr.name( ) );
        if ( it != pvs_.end( ) )
        {
            throw std::runtime_error(
                "Duplicate key insertion to the epics db" );
        }
        std::string name{ attr.name( ) };
        pvs_.insert(
            std::make_pair( std::move( name ),
                            detail::make_unique_ptr< detail::simpleFloatPV >(
                                *this, std::move( attr ) ) ) );
    }

    void
    Server::addPV( pvDoubleAttributes attr )
    {
        std::lock_guard< std::mutex > l_( m_ );

        auto it = pvs_.find( attr.name( ) );
        if ( it != pvs_.end( ) )
        {
            throw std::runtime_error(
                "Duplicate key insertion to the epics db" );
        }
        std::string name{ attr.name( ) };
        pvs_.insert(
            std::make_pair( std::move( name ),
                            detail::make_unique_ptr< detail::simpleDoublePV >(
                                *this, std::move( attr ) ) ) );
    }

    void
    Server::update( )
    {
        for ( auto& entry : pvs_ )
        {
            entry.second->update( );
        }
    }

    pvAttachReturn
    Server::pvAttach( const casCtx& ctx, const char* pPVAliasName )
    {
        std::lock_guard< std::mutex > l_( m_ );
        auto                          it = pvs_.find( pPVAliasName );
        if ( it == pvs_.end( ) )
        {
            return S_casApp_pvNotFound;
        }
        return pvCreateReturn( *( it->second ) );
    }

    pvExistReturn
    Server::pvExistTest( const casCtx&    ctx,
                         const caNetAddr& clientAddress,
                         const char*      pPVAliasName )
    {
        std::lock_guard< std::mutex > l_( m_ );
        auto                          it = pvs_.find( pPVAliasName );
        if ( it == pvs_.end( ) )
        {
            return pverDoesNotExistHere;
        }
        return pverExistsHere;
    }

} // namespace simple_epics
