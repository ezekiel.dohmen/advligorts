//
// Created by jonathan.hanks on 1/6/20.
//
#include "simple_pv_internal.hh"
#include <cstring>

namespace
{
    class StringValueCleanup
    {
    public:
        explicit StringValueCleanup( ::StringValue& sv ) : sv_{ sv }
        {
        }
        StringValueCleanup( const StringValueCleanup& ) = delete;
        StringValueCleanup( StringValueCleanup&& ) = delete;
        StringValueCleanup& operator=( const StringValueCleanup& ) = delete;
        StringValueCleanup& operator=( StringValueCleanup&& ) = delete;

        ~StringValueCleanup( )
        {
            if ( sv_.str )
            {
                ::string_value_destroy( &sv_ );
            }
        }

    private:
        ::StringValue sv_;
    };
} // namespace

namespace simple_epics
{
    namespace detail
    {
        /*
         * Start of string PV
         */

        gddAppFuncTable< simpleStringPV >&
        simpleStringPV::get_func_table( )
        {
            static gddAppFuncTable< simpleStringPV > func_table;
            return func_table;
        }

        simpleStringPV::~simpleStringPV( ) = default;

        gddAppFuncTableStatus
        simpleStringPV::read_severity( gdd& g )
        {
            g.putConvert( val_->getSevr( ) );
            return S_casApp_success;
        }

        gddAppFuncTableStatus
        simpleStringPV::read_status( gdd& g )
        {
            g.putConvert( val_->getStat( ) );
            return S_casApp_success;
        }

        gddAppFuncTableStatus
        simpleStringPV::read_precision( gdd& g )
        {
            g.putConvert( 0 );
            return S_casApp_success;
        }

        gddAppFuncTableStatus
        simpleStringPV::read_value( gdd& g )
        {
            auto status =
                gddApplicationTypeTable::app_table.smartCopy( &g, val_.get( ) );
            return ( status ? S_cas_noConvert : S_casApp_success );
        }

        void
        simpleStringPV::setup_func_table( )
        {
            auto install = []( const char* name,
                               gddAppFuncTableStatus (
                                   simpleStringPV::*handler )( gdd& ) ) {
                gddAppFuncTableStatus status;

                //           char error_string[100];

                status = get_func_table( ).installReadFunc( name, handler );
                if ( status != S_gddAppFuncTable_Success )
                {
                    //                errSymLookup(status, error_string,
                    //                sizeof(error_string));
                    //               throw std::runtime_error(error_string);
                    throw std::runtime_error(
                        "Unable to initialize pv lookup table" );
                }
            };

            install( "units", &simpleStringPV::read_attr_not_handled );
            install( "status", &simpleStringPV::read_status );
            install( "severity", &simpleStringPV::read_severity );
            install( "maxElements", &simpleStringPV::read_attr_not_handled );
            install( "precision", &simpleStringPV::read_precision );
            install( "alarmHigh", &simpleStringPV::read_attr_not_handled );
            install( "alarmLow", &simpleStringPV::read_attr_not_handled );
            install( "alarmHighWarning",
                     &simpleStringPV::read_attr_not_handled );
            install( "alarmLowWarning",
                     &simpleStringPV::read_attr_not_handled );
            install( "maxElements", &simpleStringPV::read_attr_not_handled );
            install( "graphicHigh", &simpleStringPV::read_attr_not_handled );
            install( "graphicLow", &simpleStringPV::read_attr_not_handled );
            install( "controlHigh", &simpleStringPV::read_attr_not_handled );
            install( "controlLow", &simpleStringPV::read_attr_not_handled );
            install( "enums", &simpleStringPV::read_attr_not_handled );
            install( "menuitem", &simpleStringPV::read_attr_not_handled );
            install( "timestamp", &simpleStringPV::read_attr_not_handled );
            install( "value", &simpleStringPV::read_value );
        }

        caStatus
        simpleStringPV::read( const casCtx& ctx, gdd& prototype )
        {
            return get_func_table( ).read( *this, prototype );
        }
        caStatus
        simpleStringPV::write( const casCtx& ctx, const gdd& value )
        {
            if ( attr_.mode( ) != PVMode::ReadWrite )
            {
                return S_casApp_noSupport;
            }
            aitString newValue;
            value.get( &newValue, aitEnumString );
            if ( newValue.string( ) == nullptr )
            {
                return S_casApp_noSupport;
            }
            set_value( newValue.string( ) );
            auto& io = attr_.io( );
            io.write_fn( io.user_handle, newValue.string( ) );
            return S_casApp_success;
        }

        aitEnum
        simpleStringPV::bestExternalType( ) const
        {
            return val_->primitiveType( );
        }

        caStatus
        simpleStringPV::interestRegister( )
        {
            monitored_ = true;
            return S_casApp_success;
        }

        void
        simpleStringPV::interestDelete( )
        {
            monitored_ = false;
        }

        void
        simpleStringPV::update( )
        {
            auto& io = attr_.io( );
            if ( io.write_fn )
            {
                StringValue        sv = io.read_fn( io.user_handle );
                StringValueCleanup sv_{ sv };
                if ( sv.str )
                {
                    set_value( sv.str );
                }
            }
        }

        void
        simpleStringPV::set_value( const char* value )
        {
            aitString current_value;

            val_->getConvert( current_value );
            if ( std::strcmp( current_value, value ) == 0 )
            {
                return;
            }

            val_->putConvert( value );
            aitTimeStamp ts = aitTimeStamp( epicsTime::getCurrent( ) );
            val_->setTimeStamp( &ts );
            val_->setSevr( epicsSevNone );
            val_->setStat( epicsAlarmNone );

            if ( monitored_ )
            {
                casEventMask mask = casEventMask( server_.valueEventMask( ) );
                postEvent( mask, *val_ );
            }
        }

    } // namespace detail

} // namespace simple_epics