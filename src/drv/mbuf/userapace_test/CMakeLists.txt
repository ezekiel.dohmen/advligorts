set(this_target mbuf_userspace_utest)

add_executable(${this_target}
        test.cc)

target_include_directories(${this_target} PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/..
        ${CMAKE_CURRENT_SOURCE_DIR}/../../include/
        )

target_link_libraries(${this_target} PUBLIC
            driver::shmem pthread)


add_test(NAME "mbuf_userspace_utest"
            COMMAND mbuf_userspace_utest
            WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")


#target_requires_cpp11(mbuf_probe PRIVATE)

#install(TARGETS mbuf_probe DESTINATION bin)
