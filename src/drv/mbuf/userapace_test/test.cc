#include "../mbuf.h"

#include "cds-shmem.h"


#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>

#include <chrono>
#include <thread>
#include <memory>
#include <random>

#define MBUFS_TO_CREATE 25

void typical_use( )
{
    int fd, i;
    size_t len = 64 * 1024 * 1024;
    long buf_len;
    volatile void* mapped_buf;
    struct mbuf_request_struct req = {len, "c_test_buffer"};
    struct mbuf_request_struct bad_req = {len, ""};

    if ((fd = open ("/dev/mbuf", O_RDWR | O_SYNC)) < 0) {
        perror ("open");
        exit(-1);
    }

    //First do alloc req with bad name
    for(i=0; i<MBUF_NAME_LEN+1; ++i)
        bad_req.name[i] = 'a';
    buf_len = ioctl (fd, IOCTL_MBUF_ALLOCATE, &bad_req);
    if(buf_len == len) exit(-1);


    buf_len = ioctl (fd, IOCTL_MBUF_ALLOCATE, &req);
    if(buf_len < len) exit(-1);

    mapped_buf = (volatile void*)mmap( 0,
                                     buf_len,
                                     PROT_READ | PROT_WRITE,
                                     MAP_SHARED,
                                     fd,
                                     0 );

    if ( mapped_buf == MAP_FAILED ) exit(-1);


    volatile unsigned char* mem = (volatile unsigned char*) mapped_buf;
    unsigned char val = 0;
    for(i=0; i<len; ++i) //Fill up buffer
    {
        mem[i] = val++;
    }

    //Verify nothing has changed
    for(i=0; i<len-1; ++i)
    {
        if ( (mem[i+1] != mem[i] + 1) && mem[i+1] != 0)
            exit(-1);
    }


    //Clean up
    if (munmap( (void*)( mapped_buf ), buf_len ) != 0) exit(-1);
    close(fd);

}

void fill_all_areas()
{
    int fds[MBUFS_TO_CREATE+1];
    size_t len = 64 * 1024 * 1024;
    long buf_len;
    int i;
    volatile void* mapped_buffers[MBUFS_TO_CREATE+1];

    struct mbuf_request_struct req = {len, "" };
    



    //Fill all the areas
    for(i=0; i<MBUFS_TO_CREATE; ++i)
    {
        if ((fds[i] = open ("/dev/mbuf", O_RDWR | O_SYNC)) < 0) {
            perror ("open");
            exit(-1);
        }

        snprintf(req.name, MBUF_NAME_LEN+1, "shmem_%d", i);
        buf_len = ioctl (fds[i], IOCTL_MBUF_ALLOCATE, &req);
        //printf("%d, name: %s, ret: %d\n",i, req.name, buf_len);
        if(buf_len < (long)len)
        {
            printf("Error: Could not allocate all the buffers, failed at buffer %s\n", req.name);
            exit(-1);
        }

        mapped_buffers[i] = (volatile void*)mmap( 0,
                                     buf_len,
                                     PROT_READ | PROT_WRITE,
                                     MAP_SHARED,
                                     fds[i],
                                     0 );

        //printf("mapped_buffers[%d] : %p\n", i, mapped_buffers[i]);
        if ( mapped_buffers[i] == MAP_FAILED )
        { 
            printf("Error: i: %d - Could not map all the buffers.\n", i);
            exit(-1);
        }
    }


    //Clean up
    for(i=0; i<MBUFS_TO_CREATE; ++i)
    {
        //Clean up
        if (munmap( (void*)( mapped_buffers[i] ), len ) != 0) exit(-1);
        close(fds[i]);
    }

}

#define NUM_RUNS 1024
#define NUM_THREADS 25
#define MIN_SLEEP_MS 10
#define MAX_SLEEP_MS 100


void mthread_worker(std::string name)
{
    std::random_device                  rand_dev;
    std::mt19937                        generator(rand_dev());
    std::uniform_int_distribution<int>  distr(MIN_SLEEP_MS, MAX_SLEEP_MS);
    
    for(int i = 0; i < NUM_RUNS; ++i)
    {
        std::unique_ptr<shmem::shmem> buffer_ptr( new shmem::shmem(name, 1) ); //1 MB
        char * hold = const_cast< char* >( buffer_ptr->mapping< char >( ) );

        //std::this_thread::sleep_for(std::chrono::milliseconds( distr(generator) ));

        buffer_ptr.reset(); //Release buffer 
    }

}

void shmem_mthread_class_test()
{
    char name[1024];
    std::thread threads[NUM_THREADS];

    for(int i=0; i < NUM_THREADS; ++i)
    {
        snprintf(name, MBUF_NAME_LEN+1, "shmem_%d", i);
        threads[i] = std::thread(mthread_worker, std::string(name));
    }

    for(int i=0; i < NUM_THREADS; ++i)
        threads[i].join();

}


// Returns 0 if all tests pass
//
//
int main()
{
    typical_use();
    fill_all_areas();
    shmem_mthread_class_test();

    return 0;
}









