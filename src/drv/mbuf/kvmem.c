/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <linux/slab.h>

//#include <rtai_shm.h>
#define KMALLOC_LIMIT (128*1024)
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,10)
 #define mm_remap_page_range(vma,from,to,size,prot) remap_page_range(vma,from,to,size,prot)
#else
#define mm_remap_page_range(vma,from,to,size,prot) remap_pfn_range(vma,from,to>>PAGE_SHIFT,size,prot)
#endif


static __inline__ int vm_remap_page_range(struct vm_area_struct *vma, unsigned long from, unsigned long to)
{
#if LINUX_VERSION_CODE < KERNEL_VERSION(3,7,0)
    vma->vm_flags |= VM_RESERVED;
#else
    vma->vm_flags |= (VM_DONTEXPAND | VM_DONTDUMP);
#endif
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,14)
    return vm_insert_page(vma, from, vmalloc_to_page((void *)to));
#else
    return mm_remap_page_range(vma, from, kvirt_to_pa(to), PAGE_SIZE, PAGE_SHARED);
#endif
}

static __inline__ int km_remap_page_range(struct vm_area_struct *vma, unsigned long from, unsigned long to, unsigned long size)
{
#if LINUX_VERSION_CODE < KERNEL_VERSION(3,7,0)
    vma->vm_flags |= VM_RESERVED;
#else
    vma->vm_flags |= (VM_DONTEXPAND | VM_DONTDUMP);
#endif
    return mm_remap_page_range(vma, from, virt_to_phys((void *)to), size, PAGE_SHARED);
}

/**
 * The signedness of the size parameter to rvmalloc and rvfree is
 * important. We have an issue if size is ever not a PAGE_SIZE multiple.
 * When we subtract PAGE_SIZE from it in the page reservation section, 
 * we would expect a negative result that should get us out of the loop. 
 * But if size is unsigned that can't happen. 
 */


/** @brief allocate user space mmapable block of memory in kernel space
 *
 *  @param size The size of the buffer that should be allocated
 *              Must be signed, see note above
 *
 *  @return A pointer to the allocated buffer if successful, 0 otherwise
 */
void *rvmalloc(long size)
{
    void* mem;
    char* adr;
    
    if (size < 1) return 0;    

    if ((mem = vmalloc(size))) {
        adr = (char*)mem;
        while (size > 0) {
            SetPageReserved(vmalloc_to_page((void *)adr));
            adr  += PAGE_SIZE;
            size -= PAGE_SIZE;
        }
    }
    return mem;
}

/** @brief Frees the block of memory allocated with rvmalloc
 *
 *  @param mem A pointer to the block of memory you would 
 *             like to free
 *
 *  @param size The size of the buffer that was allocated
 *              Must be signed, see note above. We need this 
 *              value so that we can free all the reserved pages
 *              from rvmalloc(). vmalloc() will align the size to 
 *              a page boundary, but if a non-aligned size is used
 *              the page res/free logic will still wor.
 *
 *  @return Void
 */
void rvfree(void *mem, long size)
{
    char* adr;
    
    if (size < 1) return;    

    if ((adr = (char*)mem)) {
        while (size > 0) {
            ClearPageReserved(vmalloc_to_page((void *)adr));
            adr  += PAGE_SIZE;
            size -= PAGE_SIZE;
        }
        vfree(mem);
    }
}

/* this function will map (fragment of) rvmalloc'ed memory area to user space */
int rvmmap(void *mem, unsigned long memsize, struct vm_area_struct *vma)
{
    unsigned long pos, offset;
    long size;
    unsigned long start  = vma->vm_start;

    /* this is not time critical code, so we check the arguments */
    /* vma->vm_offset HAS to be checked (and is checked)*/
    if (vma->vm_pgoff > (~0UL >> PAGE_SHIFT)) {
        return -EFAULT;
    }
    offset = vma->vm_pgoff << PAGE_SHIFT;
    size = vma->vm_end - start;
    if ((size + offset) > memsize) {
        return -EFAULT;
    }
    pos = (unsigned long)mem + offset;
    if (pos%PAGE_SIZE || start%PAGE_SIZE || size%PAGE_SIZE) {
        return -EFAULT;
    }
    while (size > 0) {
//      if (mm_remap_page_range(vma, start, kvirt_to_pa(pos), PAGE_SIZE, PAGE_SHARED)) {
        if (vm_remap_page_range(vma, start, pos)) {
            return -EAGAIN;
        }
        start += PAGE_SIZE;
        pos   += PAGE_SIZE;
        size  -= PAGE_SIZE;
    }
    return 0;
}

/* allocate user space mmapable block of memory in kernel space */
void *rkmalloc(int *msize, int suprt)
{
    unsigned long mem, adr;
    long size;
        
    if (*msize <= KMALLOC_LIMIT) {
        mem = (unsigned long)kmalloc(*msize, suprt);
    } else {
        mem = (unsigned long)__get_free_pages(suprt, get_order(*msize));
    }
    if (mem) {
        adr  = PAGE_ALIGN(mem);
        size = *msize -= (adr - mem);
        while (size > 0) {
//          mem_map_reserve(virt_to_page(adr));
            SetPageReserved(virt_to_page(adr));
            adr  += PAGE_SIZE;
            size -= PAGE_SIZE;
        }
    }
    return (void *)mem;
}

void rkfree(void *mem, long size)
{
        unsigned long adr;
        
    if ((adr = (unsigned long)mem)) {
        long sz = size;
        adr  = PAGE_ALIGN((unsigned long)mem);
        while (size > 0) {
//          mem_map_unreserve(virt_to_page(adr));
            ClearPageReserved(virt_to_page(adr));
            adr  += PAGE_SIZE;
            size -= PAGE_SIZE;
        }
        if (sz <= KMALLOC_LIMIT) {
            kfree(mem);
        } else {
            free_pages((unsigned long)mem, get_order(sz));
        }
    }
}

/* this function will map an rkmalloc'ed memory area to user space */
int rkmmap(void *mem, unsigned long memsize, struct vm_area_struct *vma)
{
    unsigned long pos, size, offset;
    unsigned long start  = vma->vm_start;

    /* this is not time critical code, so we check the arguments */
    /* vma->vm_offset HAS to be checked (and is checked)*/
    if (vma->vm_pgoff > (~0UL >> PAGE_SHIFT)) {
        return -EFAULT;
    }
    offset = vma->vm_pgoff << PAGE_SHIFT;
    size = vma->vm_end - start;
    if ((size + offset) > memsize) {
        return -EFAULT;
    }
    pos = (unsigned long)mem + offset;
    if (pos%PAGE_SIZE || start%PAGE_SIZE || size%PAGE_SIZE) {
        return -EFAULT;
    }
//  if (mm_remap_page_range(vma, start, virt_to_phys((void *)pos), size, PAGE_SHARED)) {
    if (km_remap_page_range(vma, start, pos, size)) {
        return -EAGAIN;
    }
    return 0;
}
