# kernel_interface_test
This is a small kernel module that will test the mbuf kernel space interfaces. All you need to do to run it, is load and unload this module while `mbuf` is loaded.

## Building 
If you are building in the source tree, you need to first build the mbuf module in the directory above. You then need to define the `RCG_SRC` environmental variable so it points to your `advligorts` repository.

```sh
~/git/advligorts/src/drv/mbuf$ export RCG_SRC=~/git/advligorts/
~/git/advligorts/src/drv/mbuf$ make #Build the mbuf module
...
~/git/advligorts/src/drv/mbuf$ cd kernel_interface_test/
~/git/advligorts/src/drv/mbuf/kernel_interface_test$ make #Build the test module
...
~/git/advligorts/src/drv/mbuf/kernel_interface_test$ sudo insmod mbuf_test.ko; sudo rmmod mbuf_test.ko
```

If you don't want to build the `mbuf` module in the source tree you need to define `KBUILD_EXTRA_SYMBOLS` so that is points to your installed `mbuf` `Module.symvers` file.
```sh
~/git/advligorts/src/drv/mbuf/kernel_interface_test$ KBUILD_EXTRA_SYMBOLS=/path/to/Module.symvers make
```


### Getting Test Results
You can view the test results with `dmesg`
```sh
sudo dmesg
```
#### Example Test Output
```sh
[44248.587289] mbuf-test: module starting tests.
[44248.691329] Test typical_use: Passed
[44248.700367] Test too_small_alloc: Passed
[44248.709542] Test alloc_and_lookup: Passed
[44249.267501] Test fill_all_areas: Passed
[44249.267503] mbuf-test Return code messages:
[44249.267504] mbuf-test : MBUF_KERNEL_CODE_OK :
[44249.267505] mbuf-test : MBUF_KERNEL_CODE_NO_BUFFERS : The mbuf module does not have any more free areas for the requested buffer
[44249.267506] mbuf-test : MBUF_KERNEL_CODE_ALLOC_FAILED : The buffer allocation failed
[44249.267506] mbuf-test : MBUF_KERNEL_CODE_SZ_ERROR : A buffer with the name already allocated, and the requested size does not match
[44249.267507] mbuf-test : MBUF_KERNEL_CODE_NAME_NOT_FOUND : A buffer with the requested name could not be found
[44249.267508] mbuf-test : MBUF_KERNEL_CODE_LAST : Unsupported Error Code
[44249.267509] Test error_lookup: Passed
[44249.267510] mbuf-test: module unloaded from 0x0000000023f8b040
```

### A Note On Failure
Because mbuf is a stateful module, if one test case fails it can leave the module in a bad state and successive test might fail even though there is nothing wrong with them. Find out why the first test is failing, fix that, and it might resolve all successive tests.
