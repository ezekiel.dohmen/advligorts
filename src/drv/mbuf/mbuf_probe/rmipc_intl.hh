//
// Created by jonathan.hanks on 5/19/21.
//

#ifndef DAQD_TRUNK_RMIPC_INTL_HH
#define DAQD_TRUNK_RMIPC_INTL_HH

#include <daqmap.h>
#include <daq_core_defs.h>

namespace rmipc
{
    /*!
     * @brief a grouping of commonly needed offsets and structures in a
     * rmIpcStr style buffer
     */
    struct memory_layout
    {
        explicit memory_layout( volatile char* buf )
            : ipc( reinterpret_cast< volatile rmIpcStr* >(
                  buf + CDS_DAQ_NET_IPC_OFFSET ) ),
              data( buf + CDS_DAQ_NET_DATA_OFFSET ),
              tp_num( reinterpret_cast< volatile cdsDaqNetGdsTpNum* >(
                  buf + CDS_DAQ_NET_GDS_TP_TABLE_OFFSET ) )
        {
        }

        volatile rmIpcStr*          ipc;
        volatile char*              data;
        volatile cdsDaqNetGdsTpNum* tp_num;
    };
} // namespace rmipc

#endif // DAQD_TRUNK_RMIPC_INTL_HH
