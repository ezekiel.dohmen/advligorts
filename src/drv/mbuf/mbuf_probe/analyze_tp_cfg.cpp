//
// Created by erik.vonreis on 5/20/21.
//

#include <iostream>
#include "analyze_tp_cfg.hh"
#include "shmem_testpoint.h"
#include "analyze_header.hh"

using namespace std;

namespace analyze
{
    #define PRINTVAL(X) printf(#X " = %d\n", X);

    static void dump_tp_cfg(volatile TESTPOINT_CFG *tp_cfg)
    {
        auto detected_type = analyze_header(tp_cfg);
        if(detected_type != MBUF_TP_CFG)
        {
            cout << "WARNING: attempting to analyze AWG_DATA structure, but structure was of type " << detected_type << endl;
        }

        PRINTVAL(sizeof(TESTPOINT_CFG));

        for(;;)
        {
            cout << "ex:";
            for(int i=0; i<4; ++i)
            {
                cout << " " << tp_cfg->excitations[i];
            }
            cout << endl;

            cout << "tp:";
            for(int i=0; i<4; ++i)
            {
                cout << " " << tp_cfg->testpoints[i];
            }
            cout << endl;

            cout << "ew:";
            for(int i=0; i<4; ++i)
            {
                cout << " " << tp_cfg->excitations_writeback[i];
            }
            cout << endl;

            cout << "tw:";
            for(int i=0; i<4; ++i)
            {
                cout << " " << tp_cfg->testpoints_writeback[i];
            }
            cout << endl;

            usleep(2000);
        }
    }

    void analyze_tp_cfg( volatile void*    buffer,
                           std::size_t       size,
                           const ConfigOpts& options )
    {

        dump_tp_cfg(
            reinterpret_cast<volatile TESTPOINT_CFG *>(buffer)
        );
    }
}