//
// Created by jonathan.hanks on 9/14/23.
//

#ifndef DAQD_TRUNK_LIST_DCUS_HH
#define DAQD_TRUNK_LIST_DCUS_HH

#include <iostream>
#include "mbuf_probe.hh"

namespace list_dcus
{
    extern int list_dcus( std::ostream&    out,
                          volatile void*   buffer,
                          std::size_t      size,
                          ::MBufStructures buffer_type );
}

#endif // DAQD_TRUNK_LIST_DCUS_HH
