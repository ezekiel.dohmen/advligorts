//
// Created by erik.vonreis on 4/29/21.
//

#ifndef DAQD_TRUNK_ANALYZE_AWG_DATA_HH
#define DAQD_TRUNK_ANALYZE_AWG_DATA_HH

#include <cstddef>
#include "mbuf_probe.hh"

namespace analyze
{
    void analyze_awg_data( volatile void*    buffer,
        std::size_t       size,
        const ConfigOpts& options );
}

#endif // DAQD_TRUNK_ANALYZE_AWG_DATA_HH
