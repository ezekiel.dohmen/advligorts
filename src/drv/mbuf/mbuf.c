//#include <linux/config.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <linux/mm.h>
#include <linux/pci.h>

#include <linux/types.h>
#include <linux/errno.h>
#include <linux/fcntl.h>
#include <linux/mm.h>
#include <linux/miscdevice.h>
#include <linux/proc_fs.h>
#include <linux/uaccess.h>
#include <linux/vmalloc.h>
#include <linux/kernel.h>
#include <linux/kobject.h>
#include <linux/sysfs.h>

#ifdef MODVERSIONS
#  include <linux/modversions.h>
#endif
#include <asm/io.h>
#include "mbuf.h"
#include "mbuf_kernel.h"

#include "kvmem.c"

#if LINUX_VERSION_CODE < KERNEL_VERSION(3, 0, 0)
#error "This kernel version is no longer supported"
#endif

/* Set if the allocated memory filled in with one-bits */
static short int one_fill = 0;

module_param(one_fill, short, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(on_fill, "Set to 1 want to fill newly allocated memory with one bits");

/* character device structures */
static dev_t mbuf_dev = MKDEV( 0, 0 ) ;
static struct class* mbuf_class = NULL;
static struct device* mbuf_device = NULL;
static struct cdev mbuf_cdev;

#define CDS_LOCAL_MBUF_SETTINGS_BUFFER_SIZE 10

atomic_t mbuf_verbosity = ATOMIC_INIT(0);


/* methods of the character device */
static int mbuf_open(struct inode *inode, struct file *filp);
static int mbuf_release(struct inode *inode, struct file *filp);
static int mbuf_mmap(struct file *filp, struct vm_area_struct *vma);
static long mbuf_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);

/* the file operations, i.e. all character device methods */
static struct file_operations mbuf_fops = {
        .open = mbuf_open,
        .release = mbuf_release,
        .mmap = mbuf_mmap,
        .unlocked_ioctl = mbuf_ioctl,
        .owner = THIS_MODULE,
};

// To be used by the IOP to store Dolphin memory state
int iop_rfm_valid;
EXPORT_SYMBOL(iop_rfm_valid);



//
// Internal data
//

// Defines the max memory areas we will support
#define MAX_AREAS 1024

// pointer to the kmalloc'd area, rounded up to a page boundary
static void *kmalloc_area[MAX_AREAS];

// Memory area tags (OM1, OM2, etc)
static char mtag[MAX_AREAS][MBUF_NAME_LEN + 1];

// Memory usage counters
static unsigned int usage_cnt[MAX_AREAS];

// Memory area sizes
static unsigned int kmalloc_area_size[MAX_AREAS];

//This global mutex protects critical sections of this module
struct mutex g_mutex;


/// @brief Stores the error message mapping for each MBUF_KERNEL_CODE.
///        The number of entries here MUST match the number of MBUF_KERNEL_CODE
///        enums defined.
static const char error_msgs[][MBUF_ERROR_MSG_ALLOC_LEN] =
{
    {""},
    {"The mbuf module does not have any more free areas for the requested buffer"},
    {"The buffer allocation failed"},
    {"A buffer with the name already allocated, and the requested size does not match"},
    {"A buffer with the requested name could not be found"},
    {"Unsupported Error Code"}
};



// Attempts to lookup an allocated buffer's index by name
// Returns the index in kmalloc_area where the buffer is located if found,
// and -1 if no buffer with the requested name was found.
// This is an internal helper function, the caller MUST
// acquire lock before calling this function
static int find_mbuf_index(const char * name)
{
    int i =0;
    for (i = 0; i < MAX_AREAS; i++) {
        if (0 == strcmp (mtag[i], name)) {
            return i;
        }
    }
    return -1;
}


void mbuf_lookup_error_msg( enum MBUF_KERNEL_CODE code, char* error_msg)
{
    int err_indx;
    if(code < MBUF_KERNEL_CODE_OK || code >= MBUF_KERNEL_CODE_LAST)
        err_indx = MBUF_KERNEL_CODE_LAST;

    strncpy(error_msg, error_msgs[code], MBUF_ERROR_MSG_MAX_LEN);

    return;
}
EXPORT_SYMBOL(mbuf_lookup_error_msg);


enum MBUF_KERNEL_CODE mbuf_release_area_file(const char* name, struct file *file) 
{
    //Attempt to release the buffer
    enum MBUF_KERNEL_CODE ret = mbuf_release_area(name);

    //If the release is good, clear out the file private data
    if(ret == MBUF_KERNEL_CODE_OK){
        if (file) {
            file->private_data = 0;
        }
    }
    return ret;
}

enum MBUF_KERNEL_CODE mbuf_release_area(const char* name) 
{
    int i, verbose;

    verbose = (int)atomic_read(&mbuf_verbosity);

    //name is cleared out by mtag[i][0] = 0; below
    if(verbose) printk(KERN_INFO "mbuf_release_area - releasing buffer with name %s\n", name);

    mutex_lock(&g_mutex);
    // See if allocated
    for (i = 0; i < MAX_AREAS; i++) {
        if (0 == strcmp (mtag[i], name)) {
            // Found the area
            usage_cnt[i]--;
            if (usage_cnt[i] <= 0 ){
                mtag[i][0] = 0;
                usage_cnt[i] = 0;
                rvfree(kmalloc_area[i], kmalloc_area_size[i]);
                kmalloc_area[i] = 0;
                kmalloc_area_size[i] = 0;
            }
            mutex_unlock(&g_mutex);
            return MBUF_KERNEL_CODE_OK;
        }
    }

    //We only get here if we don't find the buffer
    mutex_unlock(&g_mutex);
    if(verbose) printk(KERN_INFO "mbuf_release_area - %s could not be found\n", name);
    return MBUF_KERNEL_CODE_NAME_NOT_FOUND;
}
EXPORT_SYMBOL(mbuf_release_area);


int mbuf_allocate_area_file(const char* name, unsigned int size, struct file *file)
{
    enum MBUF_KERNEL_CODE ret;
    volatile void * buffer_ptr;
    int buffer_index;

    //Try to allocate buffer
    ret = mbuf_allocate_area(name, size, &buffer_ptr);
    if(ret != MBUF_KERNEL_CODE_OK) return -1;

    //Get the index of the buffer we just created
    mutex_lock(&g_mutex);
    buffer_index = find_mbuf_index(name);
    mutex_unlock(&g_mutex);
    if( buffer_index < 0) return -1;

    if (file) file->private_data = mtag [buffer_index];

    return buffer_index;
}


// Returns pointer to buffer allocated, or 0 (NULL) if there was an error.
// 0x0 if no slots
enum MBUF_KERNEL_CODE mbuf_allocate_area(const char *name, unsigned int size, volatile void ** buffer_ptr) {
    int i, verbose;
    int mod;

    verbose = (int)atomic_read(&mbuf_verbosity);

    mutex_lock(&g_mutex);

    //Round up the requested size to the closest page size
    mod = size % PAGE_SIZE;
    if (mod != 0) {
        size += PAGE_SIZE - mod;
    }


    // See if we already have a buffer with the name
    i = find_mbuf_index(name);
    if (i >= 0)
    {
        if (kmalloc_area_size[i] != size) {
            *buffer_ptr = NULL;
            mutex_unlock(&g_mutex); 
            return MBUF_KERNEL_CODE_SZ_ERROR;
        }

        ++usage_cnt[i];
        *buffer_ptr = kmalloc_area[i];
        mutex_unlock(&g_mutex);
        if(verbose) printk(KERN_INFO "mbuf_allocate_area - increased usage count of %s\n", name);
        return MBUF_KERNEL_CODE_OK;
    }

    
    // Find first free slot
    for (i = 0; i < MAX_AREAS; i++) {
        if (kmalloc_area[i] == 0) break;
    }
    
    // Out of slots
    if (i >= MAX_AREAS) {
        *buffer_ptr = NULL;
        mutex_unlock(&g_mutex);
        if(verbose){
            printk(KERN_INFO "mbuf_allocate_area - Could not create "
                             "buffer : %s, out of free slots\n", name);
        }
        return MBUF_KERNEL_CODE_NO_BUFFERS;
    }

    kmalloc_area[i] = 0;
    kmalloc_area[i] = rvmalloc (size); 

    if (kmalloc_area[i] == 0) {
        *buffer_ptr = NULL;
        mutex_unlock(&g_mutex);
        printk("malloc() failed\n");
        return MBUF_KERNEL_CODE_ALLOC_FAILED;
    }

    //1 fill if the allocation was good, and we are configured to do so
    if (one_fill) memset(kmalloc_area[i], 0xff, size);

    kmalloc_area_size[i] = size;
    strncpy(mtag[i], name, MBUF_NAME_LEN);
    mtag[i][MBUF_NAME_LEN] = 0;
    usage_cnt[i] = 1;
    *buffer_ptr = kmalloc_area[i];
    mutex_unlock(&g_mutex);
    if(verbose) printk(KERN_INFO "mbuf_allocate_area - Created new buffer %s, sz: %u\n", name, size);
    return MBUF_KERNEL_CODE_OK;
}

EXPORT_SYMBOL(mbuf_allocate_area);

/* character device open method */
static int mbuf_open(struct inode *inode, struct file *filp)
{
        return 0;
}

/* character device last close method */
static int mbuf_release(struct inode *inode, struct file *filp)
{
    char *name;
    if (filp -> private_data == 0) return 0;
    name = (char *) filp -> private_data;
    mbuf_release_area_file(name, filp);
    return 0;
}

// helper function, mmap's the kmalloc'd area which is physically contiguous
int mmap_kmem(unsigned int i, struct vm_area_struct *vma)
{
    long length = vma->vm_end - vma->vm_start;
    int ret = -EINVAL;

    if (kmalloc_area_size[i] < length) {
        printk("mbuf_mmap() request to map %lu bytes; but only allocated %u\n", 
               length, kmalloc_area_size[i]);
        return -EINVAL;
    }

    mutex_lock(&g_mutex);
    ret = rvmmap(kmalloc_area[i], length, vma);
    mutex_unlock(&g_mutex);

    return ret;
}

/* character device mmap method */
static int mbuf_mmap(struct file *file, struct vm_area_struct *vma)
{
    int buffer_index;
    char *name;

    if (file->private_data == 0)
    {
        printk(KERN_ERR "buf_mmap - No private_data, allocation error.\n");
        return -EINVAL;
    } 
    name = (char *) file->private_data;

    // Find our memory area index by name
    mutex_lock(&g_mutex);
    buffer_index = find_mbuf_index(name);
    mutex_unlock(&g_mutex);
    if( buffer_index < 0)
    {
        printk(KERN_ERR "mbuf_mmap - Did not find the buffer by the name %s, for mmap.\n", name);
        return -EINVAL;
    }

    return  mmap_kmem (buffer_index, vma);

}


static long mbuf_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    int res;
    int verbose = 0;
    struct mbuf_request_struct req;
    void __user *argp = (void __user *)arg;

    verbose = (int)atomic_read(&mbuf_verbosity);


    switch(cmd){
    case IOCTL_MBUF_ALLOCATE:
    {
        if (copy_from_user (&req, (void *) argp, sizeof (req))) {
            return -EFAULT;
        }
        if( strnlen(req.name, MBUF_NAME_LEN + 1) > MBUF_NAME_LEN )
        {
            printk(KERN_ERR "mbuf_ioctl() - Buffer name in request is too long or malformed.\n");
            return -EINVAL; //Catch no null termination
        }
        if (verbose > 0) {
            printk(KERN_INFO
            "mbuf_ioctl: name:%.32s, size:%d, cmd:%d, file:%p\n", req.name, (int) req.size, cmd, file);
        }
        if( file->private_data != 0 )
        {
            printk(KERN_ERR "mbuf_ioctl() - Got request to allocatei buffer: %s, but fd already used for buffer.\n", req.name);
            return -EBUSY;
        }

        res = mbuf_allocate_area_file(req.name, req.size, file);
        if (res < 0) return -EINVAL; //If there was an issue
        return kmalloc_area_size[res];
    }
    break;
    case IOCTL_MBUF_DEALLOCATE:
    {
        if (copy_from_user (&req, (void *) argp, sizeof (req))) {
            return -EFAULT;
        }

        if (verbose > 0) printk(KERN_INFO "mbuf_ioctl() - Got ioctl to deallocate, name: %s\n", req.name);
        res = mbuf_release_area_file(req.name, file);
        if (res == MBUF_KERNEL_CODE_OK) {
            return  0;
        } else {
            return -EINVAL;
        }
    } 
    break;

    default:        
        return -EINVAL;
    }
    return -EINVAL;
}

static ssize_t mbuf_sysfs_status(struct kobject *kobj, struct kobj_attribute *attr, char *buf) {
    size_t remaining = PAGE_SIZE;
    size_t count = 0;
    size_t tmp = 0;
    char *cur = buf;
    int i = 0;

    mutex_lock(&g_mutex);

    for (i = 0; i < MAX_AREAS; i++) {
        if (kmalloc_area[i] == 0) continue;
        tmp = snprintf(cur, remaining, "%s: %d %d\n", mtag[i], kmalloc_area_size[i], usage_cnt[i]);
        if (tmp > remaining)
            break;
        cur += tmp;
        remaining -= tmp;
        count += tmp;
    }

    mutex_unlock(&g_mutex);

    return count;
}

static ssize_t mbuf_sysfs_verbosity_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count) {
    int new_verbosity = 0;
    char localbuf[CDS_LOCAL_MBUF_SETTINGS_BUFFER_SIZE+1];
    int conv_ret = 0;

    memset(localbuf, 0, CDS_LOCAL_MBUF_SETTINGS_BUFFER_SIZE+1);
    if (count >= CDS_LOCAL_MBUF_SETTINGS_BUFFER_SIZE) {
        printk("mbuf_sysfs_verbosity_store called with too much input, ignoring the value");
        return -EFAULT;
    }
    memcpy(localbuf, buf, count);
    localbuf[count] ='\0';


    if ((conv_ret = kstrtoint(localbuf, 10, &new_verbosity)) != 0) {
        printk("mbuf_sysfs_verbosity_store error converting verbosity into an integer - %s %d", (conv_ret == -ERANGE ? "range" : "overflow"), conv_ret);
        return -EFAULT;
    }
    atomic_set(&mbuf_verbosity, new_verbosity);
    return (ssize_t) count;
}

static ssize_t mbuf_sysfs_verbosity_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf) {
    return sprintf(buf, "%d\n", (int)atomic_read(&mbuf_verbosity));
}


/* sysfs related structures */
static struct kobject *mbuf_sysfs_dir = NULL;

/* individual sysfs debug attributes */
static struct kobj_attribute sysfs_mbuf_status_attr = __ATTR(status, 0444, mbuf_sysfs_status, NULL);
static struct kobj_attribute sysfs_mbuf_verbosity_attr = __ATTR(verbosity, 0644, mbuf_sysfs_verbosity_show, mbuf_sysfs_verbosity_store);

/* group attributes together for bulk operations */
static struct attribute *mbuf_fields[] = {
        &sysfs_mbuf_status_attr.attr,
        &sysfs_mbuf_verbosity_attr.attr,
        NULL,
};

static struct attribute_group mbuf_attr_group = {
        .attrs = mbuf_fields,
};

/* module initialization - called at module load time */
static int __init mbuf_init(void)
{
    int i;
    int ret = -EINVAL;

    mutex_init(&g_mutex);

    /* get the major number of the character device */
    if ((ret = alloc_chrdev_region(&mbuf_dev, 0, 1, "mbuf")) < 0) {
            printk(KERN_ERR "could not allocate major number for mbuf\n");
            goto out;
    }

    if (IS_ERR(mbuf_class = class_create( THIS_MODULE, "ligo_mbuf" )))
    {
            printk(KERN_ERR "could not allocate device class for mbuf\n");
            goto out_unalloc_region;
    };

    if (IS_ERR(mbuf_device = device_create( mbuf_class, NULL, mbuf_dev, NULL, "mbuf" )))
    {
            printk(KERN_ERR "could not create device file for mbuf\n");
            goto out_cleanup_class;
    }

    /* initialize the device structure and register the device with the kernel */
    cdev_init(&mbuf_cdev, &mbuf_fops);
    if ((ret = cdev_add(&mbuf_cdev, mbuf_dev, 1)) < 0) {
            printk(KERN_ERR "could not allocate chrdev for buf\n");
            goto out_cleanup_device;
    }

    mbuf_sysfs_dir = kobject_create_and_add("mbuf", kernel_kobj);
    if (mbuf_sysfs_dir == NULL) {
        printk(KERN_ERR "Could not create /sys/kernel/mbuf directory!\n");
        goto out_unalloc_region;
    }

    if (sysfs_create_group(mbuf_sysfs_dir, &mbuf_attr_group) != 0) {
        printk(KERN_ERR "Could not create /sys/kernel/mbuf/... fields!\n");
        goto out_remove_sysfs;
    }

    // Init local data structs
    for ( i = 0; i < MAX_AREAS; i++) {
        kmalloc_area[i] = 0;
        mtag[i][0] = 0;
        usage_cnt[i] = 0;
    }
    ret = 0;
    return ret;

  out_remove_sysfs:
    kobject_del(mbuf_sysfs_dir);
  out_cleanup_device:
        device_destroy( mbuf_class, mbuf_dev );
  out_cleanup_class:
        class_destroy( mbuf_class );
  out_unalloc_region:
        unregister_chrdev_region(mbuf_dev, 1);
  out:
        return ret;
}

/* module unload */
static void __exit mbuf_exit(void)
{
        device_destroy( mbuf_class, mbuf_dev );
        class_destroy( mbuf_class );
        /* remove the character deivce */
        cdev_del(&mbuf_cdev);
        unregister_chrdev_region(mbuf_dev, 1);
    if (mbuf_sysfs_dir != NULL) {
        kobject_del(mbuf_sysfs_dir);
    }
}

module_init(mbuf_init);
module_exit(mbuf_exit);
MODULE_DESCRIPTION("Kernel memory buffer driver");
MODULE_AUTHOR("Alex Ivanov aivanov@ligo.caltech.edu");
MODULE_LICENSE("Dual BSD/GPL");

