/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/

#include <pybind11/pybind11.h>

#include "cds-shmem.h"

namespace py = pybind11;

namespace
{
    // clang-format off
    // clang-format does not handle C++ raw strings well, so don't format them.
    const char* doc_string_shmem_obj=R"docstring(
        The shmem objects represent shared memory buffers.
        Creating a shmem object corresponds to opening a shared memory
        buffer.

        The slicing operators can be used to get a copy of the data in the
        shared memory buffer.  For access without the overhead of a copy the
        Shmem objects implement the python buffer protocol, so they may be
        efficiently used with systems like numpy.

        To specify the type of buffer to create (LIGO mbuf or
        posix shared memory) add a prefix to the name 'mbuf://' for
        a LIGO mbuf or 'shm://' for a posix shared memory buffer.  The
        default if no prefix is given is to use a LIGO mbuf.

        For example to get a live updating view of the data, you can create a
        shmem object then create a numpy array from it with copy = false.

        import numpy
        import ligo_shmem

        buffer = ligo_shmem.shmem('local_dc', 100)
        # make a copy of 100 bytes of data
        data = buffer[100:200]
        # set the first four bytes of the buffer to the
        # string '1234'
        data[0:4] = b'1234'

        # create a 'live' view into the data by associating a
        # numpy array with the buffer as the backing store
        live_data = numpy.array(buffer, copy=False)
)docstring";

    const char* doc_string_shmem_init_ = R"docstring(
        Open a shared memory buffer.

        Parameters:
        -----------
        name - the buffer name, may be prefixed with mbuf:// or shm:// to specify the type
               defaults to mbuf if not specified.
        size - the size in MB of the shared memory window.
)docstring";

    const char* doc_string_shmem_getitem_ = R"docstring(
        Read from the the shared memory buffer.  Indexing is slice based.

        Please note:
         * Custom step values are not allowed Data is returned as a bytes object.
)docstring";

    const char* doc_string_shmem_setitem_ = R"docstring(
        Set bytes in the shared memory buffer.Indexing is slice based.

        Please note:
         * Custom step values are not allowed
         * This copies a bytes object( byte string ) into the buffer.
         * The size of the bytes object must equal the size of the slice,
           this does not expand or shrink the shared buffer,
           only replaces some of the data.
)docstring";

    // clang-format on
} // namespace

PYBIND11_MODULE( ligo_shmem, m )
{
    m.doc( ) = "Interface into LIGO shared memory buffers";

    py::class_< shmem::shmem >(
        m, "shmem", py::buffer_protocol( ), doc_string_shmem_obj )
        .def( py::init< std::string, int >( ), doc_string_shmem_init_ )
        .def_buffer( []( shmem::shmem& s ) -> py::buffer_info {
            return py::buffer_info(
                const_cast< void* >( s.mapping< void >( ) ),
                1,
                py::format_descriptor< unsigned char >::format( ),
                1,
                { s.size( ) },
                { 1 } );
        } )
        .def( "__len__", &shmem::shmem::size )
        .def( "__getitem__",
              []( shmem::shmem& s, const py::slice& slice ) -> py::bytes {
                  size_t start, stop, step, slicelength;
                  if ( !slice.compute(
                           s.size( ), &start, &stop, &step, &slicelength ) )
                  {
                      throw py::error_already_set( );
                  }
                  if ( step != 1 )
                  {
                      throw py::index_error( );
                  }
                  auto data = const_cast< char* >( s.mapping< char >( ) );
                  return py::bytes( data + start, slicelength );
              },
              doc_string_shmem_getitem_ )
        .def( "__setitem__",
              []( shmem::shmem& s, py::slice& slice, py::bytes& value ) {
                  size_t start, stop, step, slicelength;
                  if ( !slice.compute(
                           s.size( ), &start, &stop, &step, &slicelength ) )
                  {
                      throw py::error_already_set( );
                  }
                  if ( step != 1 )
                  {
                      throw py::index_error( );
                  }
                  // drop down to the python api to get the data from the bytes
                  // object. Otherwise with the pybind11 api we need to cast it
                  // to a string and do another copy.
                  char*      buffer = nullptr;
                  Py_ssize_t buffer_len = 0;
                  if ( PyBytes_AsStringAndSize(
                           value.ptr( ), &buffer, &buffer_len ) == -1 )
                  {
                      throw std::runtime_error(
                          "Unable to get the input value bytes" );
                  }
                  if ( static_cast< Py_ssize_t >( slicelength ) != buffer_len )
                  {
                      throw std::runtime_error(
                          "left and right side of slice assignment have "
                          "different sizes" );
                  }
                  auto data = const_cast< char* >( s.mapping< char >( ) );
                  std::copy( buffer, buffer + buffer_len, data + start );
              },
              doc_string_shmem_setitem_ );
}