#ifndef LIGO_GPSTIME_KERNEL_H
#define LIGO_GPSTIME_KERNEL_H

#include <linux/version.h>
#include <linux/time.h>

// linux/timekeeping.h does not include all the headers it uses
// so we must include linux/ktime.h before using it
//
#include <linux/ktime.h>
#include <linux/timekeeping.h> 

#if LINUX_VERSION_CODE < KERNEL_VERSION(3, 0, 0)
#error "This kernel version is no longer supported by the gpstime module"
#endif

enum GPSTIME_CARD_TYPE
{
    GPSTIME_NO_CARD          = -1,
    GPSTIME_SYMMETRICOM_CARD = 0,
    GPSTIME_SPECTRACOM_CARD  = 1,
    GPSTIME_LIGO_PCI_CARD    = 2
};


#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 18, 0)
#define LIGO_TIMESPEC struct timespec

static inline void ligo_get_current_kernel_time(LIGO_TIMESPEC *t)
{
    *t = current_kernel_time();
}
#else
#define LIGO_TIMESPEC struct timespec64

static inline void ligo_get_current_kernel_time(LIGO_TIMESPEC* t)
{
    ktime_get_coarse_real_ts64(t);
}
#endif

//
// Exported From the gpstime kernel module, for use by other
// kernel space modules
//
long ligo_get_gps_driver_offset( void );
int ligo_gpstime_get_ts(LIGO_TIMESPEC * ts);


#endif
