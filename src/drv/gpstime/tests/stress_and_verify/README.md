# gpstime_stress_and_verify
This is a module that stress tests and does some verification of the ligo_gpstime_get_ts() function provided by the gpstime module. The verification it does is a check to make sure the times returned are monotonic. This can catch issues were the wrong time is returned, or second rollover issues.

## Running
Just load and unload the module with the `gpstime` module running.

```
sudo insmod gpstime_stress_and_verify.ko && sudo rmmod gpstime_stress_and_verify.ko
```

## Example Output
Output is printed to dmesg, tail with `sudo dmesg -w`

### Run With Errors Detected
```
[130876.840680] gpstime_stress_and_verify: module loaded at 0x000000005573f216
[130876.840690] gpstime_stress_and_verify: Error time queried after a later time, run index 0, last_ts : {1328822227, 346717000}, new_ts: {1328822227, 175438000}
[130878.665299] gpstime_stress_and_verify: Error time queried after a later time, run index 244647, last_ts : {1328822229, 999999000}, new_ts: {1328822229, 3000}
[130884.297727] gpstime_stress_and_verify: Test Complete
[130884.297728] gpstime_stress_and_verify: module unloaded from 0x0000000025dcef09
```

### Run With No Errors Detected
```
[135009.635944] gpstime_stress_and_verify: module loaded at 0x0000000083050067
[135017.095052] gpstime_stress_and_verify: Test Complete
[135017.095054] gpstime_stress_and_verify: module unloaded from 0x000000001aeac45b
```
