//
// Created by jonathan.hanks on 2/28/22.
// Note, this function explicitly does not include defines for LIGO_TIMESPEC
// so that it can be overidden to test from userspace.
#ifndef DAQD_TRUNK_TIMESPEC_UTILS_H
#define DAQD_TRUNK_TIMESPEC_UTILS_H

#ifdef __KERNEL__

/**
 * @brief ligo_duration represents a time span
 */
typedef struct ligo_duration {
    s64 tv_sec;
    s64 tv_nsec;
} ligo_duration;

#else

#include <stdint.h>

typedef struct ligo_duration {
    int64_t tv_sec;
    int64_t tv_nsec;
} ligo_duration;

#endif

/**
 * @brief Find the difference between two timespecs
 * @param t1 input time 1
 * @param t2 input time 2
 * @return t1 - t2
 */
static inline ligo_duration
ltimespec_difference(LIGO_TIMESPEC t1, LIGO_TIMESPEC t2)
{
    ligo_duration results;
    if (t2.tv_nsec > t1.tv_nsec)
    {
        t1.tv_nsec += 1000000000;
        t1.tv_sec--;
    }
    results.tv_sec = t1.tv_sec - t2.tv_sec;
    results.tv_nsec = t1.tv_nsec - t2.tv_nsec;
    return results;
}

/**
 * @brief Subtract a duration from a time returning the new time
 * @param t0 input time
 * @param duration amount to subtract
 * @return t0 - duration
 */
static inline LIGO_TIMESPEC
ltimespec_sub(LIGO_TIMESPEC t0, ligo_duration duration)
{
    LIGO_TIMESPEC results;
    if (duration.tv_nsec > t0.tv_nsec)
    {
        t0.tv_nsec += 1000000000;
        t0.tv_sec--;
    }
    results.tv_sec = t0.tv_sec - duration.tv_sec;
    results.tv_nsec = t0.tv_nsec - duration.tv_nsec;
    return results;
}

/**
 * @brief Add two timespecs, returning the sum
 * @param t0 input time
 * @param duration duration to add to the time t0
 * @return t1 + t2
 */
static inline LIGO_TIMESPEC
ltimespec_add(LIGO_TIMESPEC t0, ligo_duration duration)
{
    LIGO_TIMESPEC results;
    results.tv_sec = t0.tv_sec + duration.tv_sec;
    results.tv_nsec = t0.tv_nsec + duration.tv_nsec;
    if (results.tv_nsec >= 1000000000)
    {
        results.tv_nsec -= 1000000000;
        results.tv_sec++;
    }
    return results;
}

#endif // DAQD_TRUNK_TIMESPEC_UTILS_H
