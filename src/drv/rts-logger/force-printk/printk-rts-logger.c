#include "drv/rts-logger.h"

#include <linux/printk.h>
#include <linux/atomic.h>

static atomic_t g_log_level = ATOMIC_INIT( RTSLOG_LOG_LEVEL_INFO );


void rtslog_print(int level, const char * fmt, ...)
{
    va_list args;

    if(level < rtslog_get_log_level() ) return;

    va_start(args, fmt);

    vprintk(fmt, args);

    va_end(args);

}

int rtslog_set_log_level(int new_level)
{
    if(new_level < RTSLOG_LOG_LEVEL_DEBUG || new_level > RTSLOG_LOG_LEVEL_ERROR) return 0;

    atomic_set(&g_log_level, new_level);
    return 1;

}

int rtslog_get_log_level( void )
{
    return atomic_read(&g_log_level);
}


int rtslog_get_num_dropped_no_space ( void )
{
    return 0;
}

int rtslog_get_num_ready_to_print( void )
{
    return 0;
}

