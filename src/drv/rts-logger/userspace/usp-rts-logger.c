#include "drv/rts-logger.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdatomic.h>

atomic_int g_usp_rtslog_level = ATOMIC_VAR_INIT( RTSLOG_LOG_LEVEL_INFO );


void rtslog_print(int level, const char * fmt, ...)
{
    va_list args;

    if(level < g_usp_rtslog_level) return;

    va_start(args, fmt);

    vprintf(fmt, args);

    va_end(args);

}

int rtslog_set_log_level(int new_level)
{
    if(new_level < RTSLOG_LOG_LEVEL_DEBUG || new_level > RTSLOG_LOG_LEVEL_ERROR) return 0;

    atomic_store(&g_usp_rtslog_level, new_level);
    return 1;

}

int rtslog_get_log_level( void )
{
    return atomic_load(&g_usp_rtslog_level);
}


int rtslog_get_num_dropped_no_space ( void )
{
    return 0;
}

int rtslog_get_num_ready_to_print( void )
{
    return 0;
}

