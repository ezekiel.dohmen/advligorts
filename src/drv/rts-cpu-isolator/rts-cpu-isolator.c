#include <linux/init.h>
#include <linux/module.h>
#include <linux/cpu.h>
#include <linux/version.h>
#include <linux/cpumask.h>
#include <linux/sched/isolation.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>

#include "rts-cpu-isolator.h"

#define MODULE_NAME "rts_cpu_isolator" 

// 5.18 changed hk_flags to hk_types in linux/sched/isolation.h
#if LINUX_VERSION_CODE < KERNEL_VERSION(5,18,0)
#define HOUSEKEEPING_DOMAIN HK_FLAG_DOMAIN
#else
#define HOUSEKEEPING_DOMAIN HK_TYPE_DOMAIN
#endif

//
// Module globals for handling kernel space isolation
//
static realtime_thread_fp_t original_play_dead_handler = NULL;
static DEFINE_PER_CPU( realtime_thread_fp_t , fe_code );
static struct mutex g_module_mutex;

#define MAX_CPUS_SUPPORTED 64
static int g_isolated_cpus[MAX_CPUS_SUPPORTED] = {0,};
static int g_running_cpus[MAX_CPUS_SUPPORTED] = {0,};

/// @brief Stores the error message mapping for each ISOLATOR_ERROR.
///        The number of entries here MUST match the number of ISOLATOR_ERROR
///        enums defined.
static const char error_msgs[][ISOLATOR_ERROR_MSG_ALLOC_LEN] =
{
    {""},
    {"No free CPU cores. Either all cores are being used or no cores are isolated (isolcpus=.. cmd line parameter)."},
    {"The core you are requesting is larger than the number of isolated cpus available."},
    {"The cpu you requested is already in use."},
    {"Cleanup cannot be called with cpu that has not been allocated with run."},
    {"Internel rts-cpu-isolator module error. dmesg should have more info."}
};


//
// Userspace interface device globals
//
static int      isolator_open(struct inode *inode, struct file *file);
static int      isolator_release(struct inode *inode, struct file *file);
static long     isolator_ioctl(struct file *file, unsigned int cmd, long unsigned int data_ptr);

static struct file_operations fops =
{
        .owner          = THIS_MODULE,
        .open           = isolator_open,
        .unlocked_ioctl = isolator_ioctl,
        .release        = isolator_release,
};

static dev_t isolator_dev = 0;
static struct cdev isolator_cdev;
static struct class *dev_class;

//This allows us to set IOCTL dev permissions
static char *mydevnode(struct device *dev, umode_t *mode)
{
    if (mode)
        *mode = 0666;
    return NULL;
}


//
// Internal Utility Functions
//

#if LINUX_VERSION_CODE <= KERNEL_VERSION(5,6,0)
static inline int start_rt_func( unsigned int CPU_ID ){ return cpu_down(CPU_ID); }
static inline int stop_rt_func( unsigned int CPU_ID ){ return cpu_up(CPU_ID); }
#else //This is 5.10.0 and newer, caller interface is the same between versions
static inline int start_rt_func( unsigned int CPU_ID ){ return remove_cpu(CPU_ID); }
static inline int stop_rt_func( unsigned int CPU_ID ){ return add_cpu(CPU_ID); }
#endif //LINUX_VERSION_CODE <= KERNEL_VERSION(5,6,0)


static int store_isolated_cpus( void )
{
    cpumask_var_t isolated;
    int num_isolated =0;

    if(nr_cpu_ids > MAX_CPUS_SUPPORTED)
    {
        printk( KERN_ALERT MODULE_NAME ": More CPUs present than supported by this module.\n");
        return 0;
    }

    if (!alloc_cpumask_var(&isolated, GFP_KERNEL))
    {
        printk( KERN_ALERT MODULE_NAME ": Could not allocate space for cpumask.\n");
        return 0;
    }

    cpumask_andnot(isolated, cpu_possible_mask,
               housekeeping_cpumask(HOUSEKEEPING_DOMAIN));


    for(unsigned i=0; i<MAX_CPUS_SUPPORTED; ++i)
    {
        if( (1ull<<(i)) & isolated->bits[0]  )
        {
            g_isolated_cpus[i] = 1;
            ++num_isolated;
        }
    }

    free_cpumask_var(isolated);

    printk( KERN_INFO MODULE_NAME ": Found %d isolated cores we can run real time functions on.\n", num_isolated);
    return 1;
}

static int is_cpu_isolated( unsigned int cpu_id )
{
    if( cpu_id > MAX_CPUS_SUPPORTED)
        return 0;

    return g_isolated_cpus[cpu_id];
}



static void
ligo_play_dead( void )
{
    realtime_thread_fp_t handler = per_cpu( fe_code, smp_processor_id( ) );
    printk( KERN_INFO MODULE_NAME ": entering ligo_play_dead, cpu handler = %p", handler );

    if ( handler )
    {
        printk( KERN_INFO MODULE_NAME ": calling LIGO code" );
        local_irq_disable( );
        ( *handler )( );
        printk( KERN_INFO MODULE_NAME ": LIGO code is done, calling regular shutdown code" );
        ( *original_play_dead_handler )( );
    }
    else
    {
        ( *original_play_dead_handler )( );
    }
}


static ISOLATOR_ERROR reserve_free_core( int requested_cpu, int * allocated_cpu )
{

    mutex_lock(&g_module_mutex);

    if ( requested_cpu < 0 ) //We will pick the core
    {

        //We will just ignore the cpu
        for(int i=0; i < nr_cpu_ids; ++i)
        {
            //If the core is isolated, and we don't already have a rt thread on it
            if(is_cpu_isolated(i) && g_running_cpus[i] == 0)
            {
                //printk( KERN_INFO MODULE_NAME ": fe_code: %p\n", per_cpu( fe_code, i ));
                requested_cpu = i;
                break;
            }
        }
    }

    if( requested_cpu < 0 ) //If it's still negative, we didn't find a free core
    {
        printk( KERN_ALERT MODULE_NAME ": reserve_free_core() Don't have any free cores left.\n");
        mutex_unlock(&g_module_mutex);
        return ISOLATOR_NO_FREE_CORES;
    }

    //nr_cpu_ids is the number of CPUs. Provided by linux/cpumask.h
    if( requested_cpu >= nr_cpu_ids)
    {
        printk( KERN_ALERT MODULE_NAME ": reserve_free_core() CPU index %d requested, but only have %u cpus.\n",
                requested_cpu, nr_cpu_ids);
        mutex_unlock(&g_module_mutex);
        return ISOLATOR_INVALID_CPU_ID;
    }


    // Make sure the requested core is isolated
    if( !is_cpu_isolated(requested_cpu) )
    {
        printk( KERN_ALERT MODULE_NAME ": reserve_free_core() CPU index %d requested, but that core is not in the isolcpu list\n",
                requested_cpu);
        mutex_unlock(&g_module_mutex);
        return ISOLATOR_INVALID_CPU_ID;
    }


    if( g_running_cpus[requested_cpu] != 0)
    {
        printk( KERN_ALERT MODULE_NAME ": reserve_free_core() called with CPUID %d, "
                "but that core is already running another real time function.\n", requested_cpu);
        mutex_unlock(&g_module_mutex);
        return ISOLATOR_CPU_ALREADY_TAKEN;
    }

    //Mark core as used and return
    g_running_cpus[requested_cpu] = 1;
    *allocated_cpu = requested_cpu;
    mutex_unlock(&g_module_mutex);
    return ISOLATOR_OK;

}

//
// Exported Symbols
//

ISOLATOR_ERROR rts_isolator_cleanup( unsigned int CPU_ID )
{

    if(CPU_ID > nr_cpu_ids)
    {
        printk( KERN_ALERT MODULE_NAME ": rts_isolator_cleanup() called with CPU id: %d, but only have %u CPUS\n",
                CPU_ID, nr_cpu_ids);
        return ISOLATOR_INVALID_CPU_ID;
    }

    mutex_lock(&g_module_mutex);

    if( g_running_cpus[CPU_ID] != 1)
    {
         printk( KERN_ALERT MODULE_NAME ": rts_isolator_cleanup() called with CPU id: %d, "
                 "but that CPU was never executed with rts_isolator_run(). Doing nothing.\n", CPU_ID);
         mutex_unlock(&g_module_mutex);
         return ISOLATOR_NOTHING_TO_CLEANUP;
    }

    per_cpu( fe_code, CPU_ID ) = 0;

    stop_rt_func(CPU_ID);
    g_running_cpus[CPU_ID] = 0;
    mutex_unlock(&g_module_mutex);
    return ISOLATOR_OK;
}
EXPORT_SYMBOL( rts_isolator_cleanup );


ISOLATOR_ERROR rts_isolator_run( realtime_thread_fp_t rt_runner_func, int requested_cpu, int * allocated_cpu )
{
    ISOLATOR_ERROR ret;
    int temp_cpu;

    if (rt_runner_func == 0 )
    {
        printk( KERN_ALERT MODULE_NAME ": rts_isolator_run() called, but real time function passed is null.\n");
        return ISOLATOR_INTERNAL_ERROR;
    }

    ret = reserve_free_core(requested_cpu, &temp_cpu);
    if( ret != ISOLATOR_OK) return ret;

    //Set real time function pointer and start execution
    per_cpu( fe_code, temp_cpu ) = rt_runner_func;
    start_rt_func(temp_cpu);


    *allocated_cpu = temp_cpu;
    return ret;
}
EXPORT_SYMBOL( rts_isolator_run );


void isolator_lookup_error_msg( ISOLATOR_ERROR code, char* error_msg)
{
    int err_indx;
    if(code < ISOLATOR_OK || code >= ISOLATOR_CODE_LAST)
        err_indx = ISOLATOR_CODE_LAST;

    strncpy(error_msg, error_msgs[code], ISOLATOR_ERROR_MSG_MAX_LEN);
    return;
}
EXPORT_SYMBOL( isolator_lookup_error_msg );


//
// Start Userspace IOCTL interface
//

static int isolator_open(struct inode *inode, struct file *file)
{
    file->private_data = (void*) -1;
    return 0;
}

static int isolator_release(struct inode *inode, struct file *file)
{
    //If this user allocated a core we free it when the last FD is closed
    if( (uint64_t) file->private_data >= 0 )
    {
        if ( rts_isolator_cleanup((uint64_t) file->private_data) != ISOLATOR_OK)
            printk( KERN_ALERT MODULE_NAME ": isolator_release() Failed to release CPU %llu\n", (uint64_t) file->private_data );
    }

    return 0;
}

static long isolator_ioctl(struct file *file, unsigned int cmd, long unsigned int data_ptr)
{
    ISOLATOR_ERROR ret;
    int32_t cpu_to_request;
    int32_t allocated_cpu;

    switch(cmd) {
        case GET_CORE_IOCLT:

            if (copy_from_user(&cpu_to_request, (int32_t*)data_ptr, sizeof(cpu_to_request)))
            {
                printk( KERN_ERR MODULE_NAME " : isolator_ioctl() : Failed to copy argument from userspace.\n");
                return ISOLATOR_INTERNAL_ERROR;
            }

            ret = reserve_free_core(cpu_to_request, &allocated_cpu);
            if( ret != ISOLATOR_OK) return ret;

            file->private_data = (void*)((uint64_t)allocated_cpu);

            if( copy_to_user((int32_t*) data_ptr, &allocated_cpu, sizeof(allocated_cpu)) )
            {
                printk( KERN_ERR MODULE_NAME ": isolator_ioctl() : Failed to copy result to userspace\n");
            }

        break;
        default:
            printk( KERN_ERR MODULE_NAME " : isolator_ioctl() : Unhandled IOCTL cmd type. Ignoring...\n");
            return ISOLATOR_INTERNAL_ERROR;
        break;
    }
    return ISOLATOR_OK;
}


//
// Module init and exit
//

static int
rts_cpu_isolator_init( void )
{

    //
    // Set up handler for redirection to LIGO handler
    //
    mutex_init(&g_module_mutex);
    if( !store_isolated_cpus() ) return -ENOMEM;
    original_play_dead_handler = smp_ops.play_dead;
    printk( KERN_INFO MODULE_NAME ": rts_cpu_isolator_init.  Handler was %p\n", smp_ops.play_dead );
    smp_ops.play_dead = ligo_play_dead;
    printk( KERN_INFO MODULE_NAME ": Handler is now %p", smp_ops.play_dead );

    //
    // Set up dev/class for IOCTL
    //
    if((alloc_chrdev_region(&isolator_dev, 0, 1, "dev_rts_cpu_isolator")) < 0) {
        pr_err("Cannot allocate major number\n");
        return -1;
    }
    printk( KERN_ERR MODULE_NAME " : Major = %d Minor = %d \n", MAJOR(isolator_dev), MINOR(isolator_dev));

    cdev_init(&isolator_cdev, &fops);

    if((cdev_add(&isolator_cdev, isolator_dev, 1)) < 0) {
        printk( KERN_ERR MODULE_NAME ": Cannot add the device to the system\n");
        goto clean_device;
    }

    if(IS_ERR(dev_class = class_create(THIS_MODULE, "cpu_isolator_class"))){
        pr_err("Cannot create the struct class\n");
        goto clean_cdev;
    }
    dev_class->devnode = mydevnode;

    if(IS_ERR(device_create(dev_class, NULL, isolator_dev, NULL, "rts_cpu_isolator"))){
        pr_err("Cannot create the Device 1\n");
        goto clean_class;
    }


    return 0; //Normal exit

clean_class:
    class_destroy(dev_class);
clean_cdev:
    cdev_del(&isolator_cdev);
clean_device:
    unregister_chrdev_region(isolator_dev,1);
    
    return -1;
}

static void
rts_cpu_isolator_exit( void )
{
    //Clean up idle handler
    smp_ops.play_dead = original_play_dead_handler;
    printk( KERN_INFO MODULE_NAME ": rts_cpu_isolator_exit.  Handler restored to %p\n",
            smp_ops.play_dead );

    //Clean up char dev/class
    device_destroy(dev_class, isolator_dev);
    class_destroy(dev_class);
    cdev_del(&isolator_cdev);
    unregister_chrdev_region(isolator_dev, 1);
}

MODULE_LICENSE( "Dual BSD/GPL" );
module_init( rts_cpu_isolator_init );
module_exit( rts_cpu_isolator_exit );
