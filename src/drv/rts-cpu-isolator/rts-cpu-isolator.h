#ifndef RTS_CPU_ISOLATOR_H
#define RTS_CPU_ISOLATOR_H

//
// This is the function pointer type of the kernel's play_dead()
// function that we overwrite in order to inject LIGO's "real-time" 
// module code. It should match the play_dead() prototype in:
// https://elixir.bootlin.com/linux/v5.10/source/arch/x86/include/asm/smp.h
//
typedef void (*realtime_thread_fp_t)( void );

typedef enum 
{
    ISOLATOR_OK = 0,
    ISOLATOR_NO_FREE_CORES = 1,
    ISOLATOR_INVALID_CPU_ID = 2,
    ISOLATOR_CPU_ALREADY_TAKEN = 3,
    ISOLATOR_NOTHING_TO_CLEANUP = 4,
    ISOLATOR_INTERNAL_ERROR = 5,
    ISOLATOR_CODE_LAST

} ISOLATOR_ERROR;

#define ISOLATOR_ERROR_MSG_MAX_LEN 128
#define ISOLATOR_ERROR_MSG_ALLOC_LEN (ISOLATOR_ERROR_MSG_MAX_LEN+1)


///
/// IOCTL Interface
///
#define GET_CORE_IOCLT _IOWR('a', 0, int32_t*)


///
/// @brief Instead of calling CPU up/down directly, new real-time module
///        implementers should use the rts_isolator_run() and 
///        rts_isolator_cleanup() functions defined here.
///
///        All cpu_ids are 0 indexed.
///


///
/// @brief This function combines set_rt_callback()/rts_isolator_exec()
///        functionality to start the rt_runner_func on an isolated core.
///
///
/// @param rt_runner_func     A function pointer to the "real-time" code you would
///                           like to run
/// @param requested_cpu      The CPU number to isolate/run the function on (0 indexed)
///                           NOTE: If you don't care what core your code runs on, pass
///                           -1 and the module will pick an available core for your code
///@param allocated_cpu [out] This is filled with the CPU the isolator allocated for 
///                           your real time function, and shall be passed into 
///                           rts_isolator_cleanup() for cleanup.
///
/// @return ISOLATOR_OK if a core was successfully allocated, another error if not
///
ISOLATOR_ERROR rts_isolator_run( realtime_thread_fp_t rt_runner_func, int requested_cpu, int * allocated_cpu  );

///
/// @brief This function should be called once the real time function
///        that was started with rts_isolator_run() has exited. It cleans
///        up internal data structures and frees the core so it can be
///        reused.
///
///
/// @param cpu The CPU number to un-isolate/free. WARNING: You need to 
///            pass the actual core used, the allocated_cpu returned from 
///            rts_isolator_run()
///
///
ISOLATOR_ERROR rts_isolator_cleanup( unsigned int cpu_id );

///
/// @brief This function can be used to lookup a string explanation for
///        a returned error code. The buffer that error_msg points to
///        should be ISOLATOR_ERROR_MSG_ALLOC_LEN in size
///
///
/// @param code      The error code returned by another function.
/// @param error_msg A buffer for the function to fill with the error
///                  code's message. Must be ISOLATOR_ERROR_MSG_ALLOC_LEN 
///                  bytes long.
///
void isolator_lookup_error_msg( ISOLATOR_ERROR code, char* error_msg);




#endif // RTS_CPU_ISOLATOR_H

