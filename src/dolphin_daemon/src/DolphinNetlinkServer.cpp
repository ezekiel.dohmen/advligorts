#include "DolphinNetlinkServer.hpp"

#include "Dolphin_SISCI_Resource.hpp"

//libspdlog-dev
#include "spdlog/spdlog.h"
#include "spdlog/cfg/env.h"

#include <vector>

//Netlink Defines
#define NETLINK_GROUP_NUM 17
#define MAX_PAYLOAD 4096

std::unique_ptr<DolphinNetlinkServer> DolphinNetlinkServer::create_instance()
{
    std::unique_ptr<DolphinNetlinkServer> me_ptr (new DolphinNetlinkServer());

    if ( !me_ptr->init_sockets() ) return nullptr;


    return me_ptr;
}

DolphinNetlinkServer::~DolphinNetlinkServer()
{
}

DolphinNetlinkServer::DolphinNetlinkServer()
{
}

void DolphinNetlinkServer::thread_body()
{
    struct sockaddr_nl nladdr;
    struct msghdr msg;
    struct iovec iov;
    char buffer[MAX_PAYLOAD] = {0};
    int ret;

    //Set up receive structures 
    iov.iov_base = (void *) buffer;
    iov.iov_len = sizeof(buffer);
    msg.msg_name = (void *) &(nladdr);
    msg.msg_namelen = sizeof(nladdr);
    msg.msg_control = nullptr;
    msg.msg_controllen = 0;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    while( !should_stop() )
    {
        ret = recvmsg(_sock_fd, &msg, 0);

        if(ret == -1) continue; //Timout

        if (ret < 0)
        {
            spdlog::warn("DolphinNetlinkServer - Error {}, when receiving netlink multicast message from group {}", 
                         ret, NETLINK_GROUP_NUM);
            continue;
        }


        unsigned payload_sz =  ret - NLMSG_HDRLEN ;
        spdlog::info("Received {} bytes, payload_sz : {}", ret, payload_sz);

        all_dolphin_daemon_msgs_union any_msg;
        any_msg.as_hdr = (dolphin_mc_header *) NLMSG_DATA((struct nlmsghdr *) &buffer);;
        

        switch ( any_msg.as_hdr->msg_id )
        {
            case DOLPHIN_DAEMON_ALLOC_REQ:

                //Verify message length
                if ( check_alloc_req_valid(any_msg.as_alloc_req, payload_sz) == 0)
                {
                    spdlog::error("DolphinNetlinkServer - Invalid message size {}, for num_segments {}", 
                                  payload_sz, any_msg.as_alloc_req->num_segments);
                    build_and_send_alloc_resp_error(DOLPHIN_ERROR_MALFORMED_MSG);
                    break;
                }
                handle_alloc_req( any_msg.as_alloc_req );

            break;
            case DOLPHIN_DAEMON_FREE_REQ:


                handle_free_all_req( any_msg.as_free_req );
            break;
            default:
                spdlog::error("DolphinNetlinkServer - Invalid message id of {}, discarding message.", any_msg.as_hdr->msg_id);
                build_and_send_alloc_resp_error(DOLPHIN_ERROR_MALFORMED_MSG);
                continue;
            break;

        }

        spdlog::info("DolphinNetlinkServer - Received kernel multicast message.");

    }



}

void DolphinNetlinkServer::handle_free_all_req( dolphin_mc_free_all_req * req_ptr )
{
    _dolphin_resources.clear(); //Free all dolphin resources

    spdlog::info("DolphinNetlinkServer::handle_free_all_req() - Dolphin resources released.");

    return;
}


void DolphinNetlinkServer::handle_alloc_req( dolphin_mc_alloc_req * req_ptr )
{
    DOLPHIN_ERROR_CODES status;

    //We leave new segments in this vector until we have processed the whole message
    //if we fail along the way we return, and these resources get cleaned up
    std::vector< std::unique_ptr< Dolphin_SISCI_Resource > > new_segments;

    spdlog::info("dolphin_mc_alloc_req message received.");
    spdlog::info("There are {} num_segments requested.", req_ptr->num_segments);

    for(int i =0; i < req_ptr->num_segments; ++i)
    {
        spdlog::info("Segment {}", req_ptr->segment_ids[i]);


        //It does not look like dolphin supports two conntections to the same multicast segment
        //from a single node, so we only are going to support one segment id per front end / daemon
        auto it = _dolphin_resources.find(req_ptr->segment_ids[i]);
        if (it != _dolphin_resources.end() )
        {
            spdlog::error("DolphinNetlinkServer::handle_alloc_req() - "
                          "Sgment ID {} was requested, but segment is already allocated.",
                          req_ptr->segment_ids[i]);
            return build_and_send_alloc_resp_error(DOLPHIN_ERROR_SEGMENT_ALREADY_ALLOCATED); 
        }

        //Create a new dolphin resource manager for the segment
        new_segments.push_back( Dolphin_SISCI_Resource::create_instance() );
        if( !new_segments.back() ) return build_and_send_alloc_resp_error(DOLPHIN_ERROR_SEGMENT_SETUP_ERROR);

        //Setup the multicast segment
        status = new_segments.back()->connect_mc_segment(req_ptr->segment_ids[i], req_ptr->segment_sz_bytes);
        if( status != DOLPHIN_ERROR_OK ) return build_and_send_alloc_resp_error(status);
    }



    //If we got here, each of the requested segments have been accepted, add all to map
    for(int i=0; i < new_segments.size(); ++i)
    {
        _dolphin_resources[ new_segments[i]->get_segment_id() ] = std::move(new_segments[i]);//todo: can we use for index and move?
    }

    //Time to send the accepted respose
    unsigned msg_size = GET_ALLOC_RESP_SZ(num_addrs);
    dolphin_mc_alloc_resp * resp_ptr = (dolphin_mc_alloc_resp *) malloc( msg_size );
    resp_ptr->header.msg_id = DOLPHIN_DAEMON_ALLOC_RESP;
    resp_ptr->status = DOLPHIN_ERROR_OK;
    resp_ptr->num_addrs = req_ptr->num_segments;

    for(int i=0; i < req_ptr->num_segments; ++i)
    {
        resp_ptr->addrs[i].read_addr = _dolphin_resources[ req_ptr->segment_ids[i] ]->kernel_read_addr();
        resp_ptr->addrs[i].write_addr = _dolphin_resources[ req_ptr->segment_ids[i] ]->kernel_write_addr();
    }

    send_netlink_message(resp_ptr, msg_size);

    free(resp_ptr);

    return;
    
}


void DolphinNetlinkServer::build_and_send_alloc_resp_error(DOLPHIN_ERROR_CODES status)
{
    //Build error response message
    dolphin_mc_alloc_resp resp;
    memset(&resp, 0, sizeof(resp));
    resp.header.msg_id = DOLPHIN_DAEMON_ALLOC_RESP;
    resp.status = status;
    resp.num_addrs = 0;

    //No returned segments on error, so msg is just struct size
    send_netlink_message(&resp, sizeof(resp));
}

void DolphinNetlinkServer::send_netlink_message(void * msg_ptr, unsigned sz_bytes)
{
    struct sockaddr_nl dest_addr;
    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.nl_family = AF_NETLINK;
    dest_addr.nl_pid = 0;   // For Linux Kernel
    dest_addr.nl_groups = 0; // unicast

    struct nlmsghdr *nlh;
    nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE( sz_bytes ));
    memset(nlh, 0, NLMSG_SPACE( sz_bytes ));
    nlh->nlmsg_len = NLMSG_SPACE(sz_bytes);
    nlh->nlmsg_pid = getpid();  // self pid
    nlh->nlmsg_flags = 0;

    memcpy(NLMSG_DATA(nlh), msg_ptr, sz_bytes);

    struct iovec iov;
    memset(&iov, 0, sizeof(iovec));
    iov.iov_base = (void *)nlh;
    iov.iov_len = nlh->nlmsg_len;

    struct msghdr msg;
    memset(&msg, 0, sizeof(msg));
    msg.msg_name = (void *)&dest_addr;
    msg.msg_namelen = sizeof(dest_addr);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    int rc = sendmsg(_sock_fd, &msg, 0);
    if (rc < 0) {
        spdlog::error("sendmsg(): {}", strerror(errno));
    }
    spdlog::info("Sent message ret {}, requested size {}", rc, sz_bytes);

    free(nlh);
}


bool DolphinNetlinkServer::init_sockets()
{
    //
    // Attempt to setup netlink socket
    //
    _sock_fd = socket(PF_NETLINK, SOCK_RAW, NETLINK_USERSOCK);
    if (_sock_fd < 0) {
        spdlog::error("DolphinNetlinkServer::create_instance() - Cannot create raw socket for Netlink comms: {}",
                      strerror(errno));
        return false;
    }

    memset(&_src_addr, 0, sizeof(_src_addr));
    _src_addr.nl_family = AF_NETLINK;
    _src_addr.nl_pid = 0; //from kernel
    bind(_sock_fd, (struct sockaddr*)&_src_addr, sizeof(_src_addr));


    int group = NETLINK_GROUP_NUM;
    if (setsockopt(_sock_fd, SOL_NETLINK, NETLINK_ADD_MEMBERSHIP, &group, sizeof(group)) < 0) {
        spdlog::error("DolphinNetlinkServer::create_instance() - Cannot add netlink membership for group: {}, error: {}",
                      group, strerror(errno));
        close(_sock_fd);
        return false;
    }

    // Set up timeout, so we return from the recv call and can exit when signaled
    struct timeval timeout;
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;
    if (setsockopt (_sock_fd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof timeout) < 0)
    {
        spdlog::error("DolphinNetlinkServer::create_instance() - Cannot set timeout for NETLINK socket, error: {}",
                      strerror(errno));
        close(_sock_fd);
        return false;
    }

    return true;

}


