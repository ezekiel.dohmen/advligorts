#include <net/sock.h>
#include <linux/module.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>
#include <linux/kthread.h>  // for threads
#include <linux/delay.h>
#include <linux/slab.h>

#include "daemon_messages.h"

#define MODULE_NAME "dolphin_proxy_km"

//Dolphin Includes
#include "genif.h" //sci_* calls
#include "id.h" //MULTICAST_NODEID
//#include "irm_types.h" //Sci_p global
//#include "priv_io.h" //for chip_type_t, so we can include query.h
//#include "query.h" //query_mcast_group_size

//Dolphin helper defines
#define NO_CALLBACK         NULL
#define NO_FLAGS            0
#define MAX_MCAST_GROUPS 4 //TODO: Can we query this?
#define MAX_ADAPTERS 4
#define MODULE_ID   0

//
// Dolphin globals
//
static volatile void *         g_read_addrs [ MAX_ADAPTERS ][ MAX_MCAST_GROUPS ] = {0};
static volatile void *         g_write_addrs [ MAX_ADAPTERS ][ MAX_MCAST_GROUPS ] = {0};
static sci_l_segment_handle_t  g_segments [ MAX_ADAPTERS ][ MAX_MCAST_GROUPS ];
static sci_map_handle_t        g_client_map_handles [ MAX_ADAPTERS ][ MAX_MCAST_GROUPS ];
static sci_r_segment_handle_t  g_remote_segment_handles [ MAX_ADAPTERS ][ MAX_MCAST_GROUPS ];
// State tracking for cleanup
static int g_local_segment_created [ MAX_ADAPTERS ][ MAX_MCAST_GROUPS ];
static int g_local_segment_exported [ MAX_ADAPTERS ][ MAX_MCAST_GROUPS ];
static int g_remote_segment_mapped [ MAX_ADAPTERS ][ MAX_MCAST_GROUPS ];
static int g_remote_segment_connected [ MAX_ADAPTERS ][ MAX_MCAST_GROUPS ];
static int g_callback_registered [ MAX_ADAPTERS ]; //One callback per adapter


// Netlink Globals
struct sock *nl_sock = NULL;
static DEFINE_MUTEX(g_msg_mutex);



void send_netlink_response(void * msg, unsigned sz_bytes, unsigned pid)
{
    struct sk_buff *skb_out;
    struct nlmsghdr *nlh;

    skb_out = nlmsg_new(sz_bytes, 0);
    if (!skb_out) {
      printk(KERN_ERR MODULE_NAME ": Failed to allocate new skb for response message.\n");
      return;
    }

    // put the response into reply
    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, sz_bytes, 0);
    NETLINK_CB(skb_out).dst_group = 0; // not in mcast group 
    memcpy(nlmsg_data(nlh), msg, sz_bytes);

    //Send response
    int res = nlmsg_unicast(nl_sock, skb_out, pid);
    if (res < 0)
      printk(KERN_ERR MODULE_NAME ": Error while sending response message.\n");

}

void build_and_send_alloc_resp_error(DOLPHIN_ERROR_CODES status, unsigned pid)
{
    //Build error response message
    dolphin_mc_alloc_resp resp;
    memset(&resp, 0, sizeof(resp));
    resp.header.msg_id = DOLPHIN_DAEMON_ALLOC_RESP;
    resp.header.preamble_seq = DOLPHIN_DAEMON_MSG_PREAMBLE;
    resp.status = status;
    resp.num_addrs = 0;

    //No returned segments on error, so msg is just struct size
    send_netlink_response(&resp, sizeof(resp), pid);
}

/*

R_OK,
       SR_DISABLED,
       SR_WAITING,
       SR_CHECKING,
       SR_CHECK_TIMEOUT,
       SR_LOST,
       SR_OPEN_TIMEOUT,
       SR_HEARTBEAT_RECEIVED
      } session_cb_reason_t;
*/
static int32_t
session_callback( session_cb_arg_t IN arg,
                  session_cb_reason_t IN reason,
                  session_cb_status_t IN status,
                  uint32_t IN target_node,
                  uint32_t IN local_adapter_number )
{
    /// @brief This function contains the required Dolphin callback routine. \n
    printk(KERN_INFO MODULE_NAME ": Session callback reason=0x%x status=0x%x target_node=%d\n", reason,
    status, target_node); 
    return 0;
}

/// Function for Dolphin connection callback
static int32_t
connect_callback( void IN*               arg,
                  sci_r_segment_handle_t IN remote_segment_handle,
                  uint32_t IN reason,
                  uint32_t IN status )
{
    printk(KERN_INFO MODULE_NAME ": Connect callback reason=0x%x status=0x%x\n", reason, status);
    return 0;
}

static int32_t
create_segment_callback( void IN*               arg,
                         sci_l_segment_handle_t IN local_segment_handle,
                         uint32_t IN reason,
                         uint32_t IN source_node,
                         uint32_t IN local_adapter_number )
{
    printk(KERN_INFO MODULE_NAME ": Create segment callback reason=0x%x source_node=%d\n", reason, source_node);
    return 0;
}

void free_dolphin_segment( unsigned segment_id, unsigned adapter_num )
{

    //MAX_ADAPTERS ][ MAX_MCAST_GROUPS
    if(segment_id > MAX_ADAPTERS || adapter_num > MAX_MCAST_GROUPS)
    {
        printk(KERN_ERR MODULE_NAME ":free_dolphin_segment() called but segment_id: %u or adapter_num: %u invalid. \n", segment_id, adapter_num);
        return;
    }

    if( g_remote_segment_mapped[ adapter_num ][segment_id ] == 1)
    {
        sci_unmap_segment( &g_client_map_handles[ adapter_num ][ segment_id ], 0 );
        g_remote_segment_mapped[ adapter_num ][segment_id ] = 0;
    }

    if( g_remote_segment_connected[ adapter_num ][ segment_id ] == 1)
    {
        sci_disconnect_segment( &g_remote_segment_handles[ adapter_num ][ segment_id ], NO_FLAGS );
        g_remote_segment_connected[ adapter_num ][ segment_id ] = 0;
    }

    if( g_callback_registered[ adapter_num ] == 1)
    {
        sci_cancel_session_cb( adapter_num, NO_FLAGS );
        g_callback_registered[ adapter_num ] = 0;
    }

    if( g_local_segment_exported[ adapter_num ][ segment_id ] == 1 )
    {
        sci_unexport_segment( g_segments[ adapter_num ][ segment_id ], adapter_num, NO_FLAGS );
        g_local_segment_exported[ adapter_num ][ segment_id ] = 0;
    }

    if( g_local_segment_created[ adapter_num ][ segment_id ] == 1)
    {
        sci_remove_segment( &g_segments[ adapter_num ][ segment_id ], NO_FLAGS );
        g_local_segment_created[ adapter_num ][ segment_id ] = 0;
    }

    g_read_addrs[ adapter_num ][segment_id] = NULL;
    g_write_addrs[ adapter_num ][segment_id] = NULL;

}


DOLPHIN_ERROR_CODES allocate_segment(unsigned segment_id, unsigned adapter_num, unsigned size)
{
    scierror_t err;

    err = sci_create_segment( NO_BINDING,
                              adapter_num, //The genif.h header says a module ID, but old cdsrfm is treating like this is the adapter num, so thats how we use it
                              segment_id,
                              DIS_BROADCAST,
                              size,
                              create_segment_callback,
                              0,
                              &g_segments[ adapter_num ][ segment_id ] );
    printk(KERN_INFO MODULE_NAME ": DIS segment alloc status 0x%x\n", err);
    if ( err )
    {
        if (err == 0x20000b08)
        {
            printk(KERN_INFO MODULE_NAME ": Error : May be caused by the maximum segment size being too small.\n");
        }

        return DOLPHIN_ERROR_SEGMENT_SETUP_ERROR;
    }
    g_local_segment_created[ adapter_num ][ segment_id ] = 1;

    err = sci_set_local_segment_available( g_segments[ adapter_num ][ segment_id ], adapter_num );
    printk(KERN_INFO MODULE_NAME ": DIS segment making available status 0x%x\n", err);
    if ( err )
    {
        free_dolphin_segment(adapter_num, segment_id);
        return DOLPHIN_ERROR_SEGMENT_SETUP_ERROR;
    }



    err = sci_export_segment( g_segments[ adapter_num ][ segment_id ], adapter_num, DIS_BROADCAST );
    printk(KERN_INFO MODULE_NAME ": DIS segment export status 0x%x\n", err);
    if ( err )
    {
        free_dolphin_segment(adapter_num, segment_id);
        return DOLPHIN_ERROR_SEGMENT_SETUP_ERROR;
    }
    g_local_segment_exported[ adapter_num ][ segment_id ] = 1;

    g_read_addrs[ adapter_num ][segment_id] = sci_local_kernel_virtual_address( g_segments[ adapter_num ][ segment_id ] );
    if ( g_read_addrs[ adapter_num ][segment_id] == 0 )
    {
        printk(KERN_ERR MODULE_NAME ": DIS sci_local_kernel_virtual_address returned 0\n");
        free_dolphin_segment(adapter_num, segment_id);
        return DOLPHIN_ERROR_SEGMENT_SETUP_ERROR;
    }
    else
    {
        printk(KERN_INFO MODULE_NAME ": Dolphin memory read at 0x%p\n", g_read_addrs[adapter_num][segment_id]);

    }
    mdelay( 40 );

    err = sci_connect_segment( NO_BINDING,
                               MULTICAST_NODEID, //broadcast node - should not match any
                               adapter_num,
                               MODULE_ID,
                               segment_id,
                               DIS_BROADCAST,
                               connect_callback,
                               0,
                               &g_remote_segment_handles[ adapter_num ][ segment_id ] );
    printk(KERN_INFO MODULE_NAME ": DIS connect segment status 0x%x\n", err);
    if ( err )
    {
        free_dolphin_segment(adapter_num, segment_id);
        return DOLPHIN_ERROR_SEGMENT_SETUP_ERROR;
    }
    g_remote_segment_connected[ adapter_num ][ segment_id ] = 1;

    mdelay( 40 );
    err = sci_map_segment( g_remote_segment_handles[ adapter_num ][ segment_id ],
                           DIS_BROADCAST,
                           adapter_num,
                           size,
                           &g_client_map_handles[ adapter_num ][ segment_id ] );
    printk(KERN_INFO MODULE_NAME ": DIS segment mapping status 0x%x\n", err);
    if ( err )
    {
        free_dolphin_segment(adapter_num, segment_id);
        return DOLPHIN_ERROR_SEGMENT_SETUP_ERROR;
    }
    g_remote_segment_mapped[ adapter_num ][segment_id ] = 1;

    g_write_addrs[ adapter_num ][ segment_id ] = sci_kernel_virtual_address_of_mapping( g_client_map_handles[ adapter_num ][segment_id ] );
    if ( g_write_addrs[ adapter_num ][ segment_id ]  == 0 )
    {
        free_dolphin_segment(adapter_num, segment_id);
        return DOLPHIN_ERROR_SEGMENT_SETUP_ERROR;
    }
    else
    {
         printk(KERN_INFO MODULE_NAME ": Dolphin memory write at 0x%p\n", g_write_addrs[ adapter_num ][ segment_id ]);
    }

    sci_register_session_cb( adapter_num, NO_FLAGS, session_callback, NULL );
    g_callback_registered[ adapter_num ] = 1;

    return DOLPHIN_ERROR_OK;
}



void handle_alloc_req( dolphin_mc_alloc_req * req_ptr , unsigned pid)
{
    DOLPHIN_ERROR_CODES status;

    //Verify the segments are free
    for(int i =0; i < req_ptr->num_segments; ++i)
    {

        if( req_ptr->segments[i].adapter_num > MAX_ADAPTERS )
        {
            printk(KERN_ERR MODULE_NAME ": Request to allocate segment on adapter %d, but that is too high.\n",  req_ptr->segments[i].adapter_num);
            return build_and_send_alloc_resp_error(DOLPHIN_ERROR_ADAPTER_NOT_SUPPORTED, pid);
        }


        if( g_read_addrs[req_ptr->segments[i].adapter_num][ req_ptr->segments[i].segment_id ] != NULL )
        {
            printk(KERN_ERR MODULE_NAME ": Request to allocate segment ID: %u, but ID is already allocated.\n", req_ptr->segments[i].segment_id);
            return build_and_send_alloc_resp_error(DOLPHIN_ERROR_SEGMENT_ALREADY_ALLOCATED, pid);
        }

        printk(KERN_INFO MODULE_NAME ": Req element %d Adapter %u, Segment: %u, Size (bytes) %u\n", 
               i,
               req_ptr->segments[i].adapter_num, 
               req_ptr->segments[i].segment_id, 
               req_ptr->segments[i].segment_sz_bytes);


    }

    //Allocate requested segments
    //
    for(int i=0; i < req_ptr->num_segments; ++i)
    {
        status = allocate_segment(req_ptr->segments[i].segment_id, req_ptr->segments[i].adapter_num, req_ptr->segments[i].segment_sz_bytes);

        if ( status != DOLPHIN_ERROR_OK)
        {
            printk(KERN_ERR MODULE_NAME ": Failed to allocate segment ID %u in request.\n", req_ptr->segments[i].segment_id);
            //Found an error, clean up all allocations 
            for(int j=0; j < i; ++j)
            {
                printk(KERN_ERR MODULE_NAME ": Freeing segment %u to clean up partial request.\n", req_ptr->segments[j].segment_id);
                free_dolphin_segment( req_ptr->segments[j].segment_id, req_ptr->segments[j].adapter_num );
            }
            build_and_send_alloc_resp_error(DOLPHIN_ERROR_SEGMENT_SETUP_ERROR, pid);
            return;
                
        }

    }

    //Time to send the accepted respose
    unsigned msg_size = GET_ALLOC_RESP_SZ(req_ptr->num_segments);

    dolphin_mc_alloc_resp * resp_ptr = (dolphin_mc_alloc_resp *) kmalloc( msg_size, GFP_KERNEL );
    resp_ptr->header.msg_id = DOLPHIN_DAEMON_ALLOC_RESP;
    resp_ptr->status = DOLPHIN_ERROR_OK;
    resp_ptr->header.preamble_seq = DOLPHIN_DAEMON_MSG_PREAMBLE;
    resp_ptr->num_addrs = req_ptr->num_segments;

    //Fill the requested segments back in the resp
    for(int i=0; i < req_ptr->num_segments; ++i)
    {
        printk(KERN_INFO MODULE_NAME ": Allocated read: %p, write: %p\n", 
			g_read_addrs[ req_ptr->segments[i].adapter_num ][ req_ptr->segments[i].segment_id ], 
			g_write_addrs[ req_ptr->segments[i].adapter_num ][ req_ptr->segments[i].segment_id ]);
        resp_ptr->addrs[ i ].read_addr = g_read_addrs[ req_ptr->segments[i].adapter_num ][ req_ptr->segments[i].segment_id ];
        resp_ptr->addrs[ i ].write_addr = g_write_addrs[ req_ptr->segments[i].adapter_num ][ req_ptr->segments[i].segment_id ];
    }

    send_netlink_response(resp_ptr, msg_size, pid);
    kfree(resp_ptr);
    printk(KERN_INFO MODULE_NAME ": handle_alloc_req() Finished handling request.\n");
}

void handle_free_all_req( dolphin_mc_free_all_req * req_ptr )
{
    printk(KERN_INFO MODULE_NAME ": Got a dolphin_mc_free_all_req request\n");
    for(int adap_num=0; adap_num < MAX_ADAPTERS; ++adap_num)
    { 
        for(int i =0; i < MAX_MCAST_GROUPS; ++i)
        {
            if ( g_read_addrs[adap_num][i] != NULL ) //If allocated
            {
                free_dolphin_segment( i, adap_num );
            }
        }
    }
}

static void netlink_recv_unicast_msg(struct sk_buff *skb)
{
    struct nlmsghdr *nlh;
    all_dolphin_daemon_msgs_union any_msg;
    int pid;

    // We are only going to process one message at a time
    mutex_lock(&g_msg_mutex);


    nlh = (struct nlmsghdr *)skb->data;
    pid = nlh->nlmsg_pid; // pid of sending process 

    //printk(KERN_INFO MODULE_NAME ": Got a message, size : %u\n", nlmsg_len(nlh));

    any_msg.as_hdr = (dolphin_mc_header *) nlmsg_data(nlh);

    //printk(KERN_INFO MODULE_NAME ": preamble_seq: %llu, from pid: %d\n", any_msg.as_hdr->preamble_seq, pid);

    if ( !check_mc_header_valid(any_msg.as_hdr, nlmsg_len(nlh)) )
    {
        mutex_unlock(&g_msg_mutex);
        printk(KERN_ERR MODULE_NAME ": Malformed message, too short or bad preamble\n");
        return;
    }

    if(any_msg.as_hdr->interface_id != DOLPHIN_KM_INTERFACE_ID)
    {
        mutex_unlock(&g_msg_mutex);
        return; //Msg not ment for us
    }


    //Switch on message ID and handle accordingly
    switch ( any_msg.as_hdr->msg_id )
    {
        case DOLPHIN_DAEMON_ALLOC_REQ:

            if ( check_alloc_req_valid(any_msg.as_alloc_req, nlmsg_len(nlh)) == 0)
            {
                printk(KERN_INFO MODULE_NAME ": netlink_recv_unicast_msg() Malformed DOLPHIN_DAEMON_ALLOC_REQ\n");
                break;
            }
            handle_alloc_req( any_msg.as_alloc_req, pid );

        break;
        case DOLPHIN_DAEMON_FREE_REQ:
            handle_free_all_req( any_msg.as_free_req );
        break;
        default:
            printk(KERN_INFO MODULE_NAME ": netlink_recv_unicast_msg() - Invalid message id of %u, discarding message.", any_msg.as_hdr->msg_id);
        break;

    }
    mutex_unlock(&g_msg_mutex);

}





static int __init rts_netlink_dolphin_init(void)
{
    printk(KERN_INFO MODULE_NAME ": Init module\n");

    //scierror_t error;
    //query_mcast_group_size(Sci_p, &g_mcast_group_sz); //Where do we get Sci_p from

    struct netlink_kernel_cfg cfg = {
        .input = netlink_recv_unicast_msg,
    };

    nl_sock = netlink_kernel_create(&init_net, DOLPHIN_DAEMON_DPROXY_LINK_ID, &cfg);
    if (!nl_sock) {
        printk(KERN_ALERT MODULE_NAME ": Error creating netlink socket.\n");
        return -10;
    }

    return 0;
}

static void __exit rts_netlink_dolphin_exit(void)
{
    printk(KERN_INFO MODULE_NAME ": Exit module\n");

    netlink_kernel_release(nl_sock);
}

module_init(rts_netlink_dolphin_init);
module_exit(rts_netlink_dolphin_exit);

MODULE_LICENSE("Dual MIT/GPL");
