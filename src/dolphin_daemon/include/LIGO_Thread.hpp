#ifndef LIGO_THREAD_HPP
#define LIGO_THREAD_HPP

#include <thread>
#include <memory>

class LIGO_Thread
{
    public:

        /**
         * @brief This is the method that LIGO_Thread inheritors must implement
         *
         */
        virtual void thread_body() = 0;

        /**
         * @brief When this method is called, the thread is spawned and started.
         *        The caller will not be blocked, and can continue doing other work.
         *
         */
        void nonblocking_start()
        {
            _thread = std::thread(&LIGO_Thread::thread_body, this);
        }
        
        /**
         * @brief When this method is called, the calling thread is blocked until
         *        the thread_body() implementation exits, usually signaled to do
         *        so by signal_stop()
         *
         */
        void blocking_start() 
        {
            nonblocking_start();
            if(_joined == false)
            {
                join();
                _joined = true;
            }
        }

        /**
         * @brief When this method is called, the _should_stop variable is set to stop.
         *        The thread_body() implementation should be checking should_stop() in
         *        its main loop and respond by exiting.
         *
         */
        void signal_stop()
        {
            _should_stop = true;
        }

        /**
         * @brief Joins with an exited thread.
         *
         */
        void join() 
        {
            if(_joined == false)
            {
                _thread.join();
                _joined = true;
            }
        }

        /**
         * @brief This method signals the thread to stop and waits for the thread
         * join.
         *
         */
        void signal_stop_and_join()
        {
            signal_stop();
            if(_joined == false)
            {
                join();
                _joined = true;
            }
        }

        virtual ~LIGO_Thread() {}


    protected:

        LIGO_Thread() : _should_stop(false), _joined(false)
        {
        }

        /**
         * @brief The thread_body() implementation should be checking should_stop() in
         *        its main loop and respond by exiting when true.
         *
         */
        bool should_stop()
        {
            return _should_stop;
        }

    private:

        volatile bool _should_stop;
        bool _joined;

        std::thread _thread;

};



#endif //LIGO_THREAD_HPP
