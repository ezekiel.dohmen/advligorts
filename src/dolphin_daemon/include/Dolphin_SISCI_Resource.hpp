#ifndef DOLPHIN_SISCI_RESOURCE_HPP
#define DOLPHIN_SISCI_RESOURCE_HPP

#include "daemon_messages.h"

//ligo-dolphin-srcdis
#include "sisci_api.h"

#include <memory>
#include <mutex>


class Dolphin_SISCI_Resource
{
    public:

    static std::unique_ptr< Dolphin_SISCI_Resource > create_instance();
    virtual ~Dolphin_SISCI_Resource();


    DOLPHIN_ERROR_CODES connect_mc_segment(unsigned segment_id, unsigned segment_size);

    int get_segment_id(); 

    volatile void * kernel_read_addr();
    volatile void * kernel_write_addr();

    private:

    //Static members
    static unsigned _usage_count;
    static std::mutex _data_mutex;
    static const unsigned _local_adapter_num;
    static unsigned   _local_node_id; //Filled when we init the lib

    //Instance members
    sci_desc_t     _v_dev;
    unsigned                _segment_id;
    sci_local_segment_t     _local_segment;
    sci_remote_segment_t    _remote_segment;
    sci_map_t               _local_map;
    sci_map_t               _remote_map;
    volatile void *         _read_addr = 0;
    volatile void *         _write_addr = 0;

    bool mc_segment_initialized = false;

    //Private member functions
    bool supports_multicast();

    volatile void * get_kernel_read_addr();

    void clean_up_segment();
};



#endif //DOLPHIN_SISCI_RESOURCE_HPP
