#ifndef LIGO_NETLINK_SOCKET_HPP
#define LIGO_NETLINK_SOCKET_HPP

//libspdlog-dev
#include "spdlog/spdlog.h"


#include <sys/socket.h>
#include <linux/netlink.h>

#include <memory>


class NetlinkSocket
{
    public:
    
        static std::unique_ptr< NetlinkSocket > create_instance( int netlnk_link_id, int netlink_groups = 0, double timeout_s = 0.0 )
        {
            std::unique_ptr< NetlinkSocket > me_ptr (new NetlinkSocket());

            me_ptr->_sock_fd = socket(PF_NETLINK, SOCK_RAW, netlnk_link_id);
            if ( me_ptr->_sock_fd < 0) {
                spdlog::error("socket error: {}", strerror(errno));
                return nullptr;
            }

            me_ptr->_my_pid = getpid() + me_ptr->_id;  // Let OS assign PID
            ++me_ptr->_id;
            me_ptr->_netlink_groups = netlink_groups;

            memset(& me_ptr->_bind_addr, 0, sizeof( me_ptr->_bind_addr));
            me_ptr->_bind_addr.nl_family = AF_NETLINK;
            me_ptr->_bind_addr.nl_pid =  me_ptr->_my_pid;
            //me_ptr->_bind_addr.nl_groups = me_ptr->_netlink_groups;  // does not work use setsockopt
            bind( me_ptr->_sock_fd, (struct sockaddr*)& me_ptr->_bind_addr, sizeof( me_ptr->_bind_addr));


            //If netlink_group is set 
            if( netlink_groups != 0 )
            {
                if (setsockopt(me_ptr->_sock_fd, SOL_NETLINK, NETLINK_ADD_MEMBERSHIP, &netlink_groups, sizeof(netlink_groups)) < 0) {
                    spdlog::error("NetlinkSocket::create_instance() - Cannot add netlink membership for group: {}, error: {}",
                                  netlink_groups, strerror(errno));
                    close(me_ptr->_sock_fd);
                    return nullptr;
                }
            }

            //If timeout is set
            struct timeval timeout;
            timeout.tv_sec = (int) timeout_s;
            timeout.tv_usec = (timeout_s - ((int) timeout_s)) * 1e6;
            if (setsockopt (me_ptr->_sock_fd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof timeout) < 0)
            {
                spdlog::error("NetlinkSocket::create_instance() - Cannot set timeout for NETLINK socket, error: {}",
                              strerror(errno));
                close(me_ptr->_sock_fd);
                return nullptr;
            }


            memset(& me_ptr->_dest_addr, 0, sizeof( me_ptr->_dest_addr));
            me_ptr->_dest_addr.nl_family = AF_NETLINK;
            me_ptr->_dest_addr.nl_pid = 0;   // for kernel 

            return me_ptr;
        }

        ssize_t sendmsg(const void * data, size_t bytes)
        {
            struct nlmsghdr *nlh;
            struct iovec iov;
            struct msghdr msg;

            //get_msg_buf() stores a buffer for use and reallocates if we need a bigger one
            nlh = (struct nlmsghdr *) get_msg_buf(NLMSG_SPACE(bytes));
            nlh->nlmsg_len = NLMSG_SPACE(bytes);
            nlh->nlmsg_pid = _my_pid;  
            nlh->nlmsg_flags = 0;

            memcpy(NLMSG_DATA(nlh), data, bytes);

            memset(&iov, 0, sizeof(iov));
            iov.iov_base = (void *)nlh;
            iov.iov_len = nlh->nlmsg_len;

            _bind_addr.nl_groups =  1<<_netlink_groups;

            memset(&msg, 0, sizeof(msg));
            msg.msg_name = (void *)&_dest_addr;
            msg.msg_namelen = sizeof(_dest_addr);
            msg.msg_iov = &iov;
            msg.msg_iovlen = 1;

            return ::sendmsg(_sock_fd, &msg, 0);
        }

        ssize_t recvmsg(void * data, size_t bytes)
        {
            struct iovec iov;
            struct msghdr msg;
            struct nlmsghdr *nlh;

            nlh = (struct nlmsghdr *) get_msg_buf(NLMSG_SPACE(bytes));
            memset(nlh, 0, NLMSG_SPACE(bytes));

            memset(&iov, 0, sizeof(iov));
            iov.iov_base = (void *)nlh;
            iov.iov_len = NLMSG_SPACE(bytes);

            memset(&msg, 0, sizeof(msg));
            msg.msg_name = (void *)&_dest_addr;
            msg.msg_namelen = sizeof(_dest_addr);
            msg.msg_iov = &iov;
            msg.msg_iovlen = 1;

            ssize_t ret = ::recvmsg(_sock_fd, &msg, 0);
            if( ret > 0)
            {
                memcpy(data, NLMSG_DATA(nlh), ret - NLMSG_HDRLEN);
                return ret - NLMSG_HDRLEN; //Return size of data
            }

            return ret;//Return error code
        }


        virtual ~NetlinkSocket()
        {
            close(_sock_fd);
            if( _msg_buffer ) delete [] _msg_buffer;
        }

    private:

        static inline unsigned _id;

        NetlinkSocket() = default;

        int                   _sock_fd;
        struct sockaddr_nl   _bind_addr;
        struct sockaddr_nl   _dest_addr;
        int               _netlink_groups;

        int _my_pid;
        char * _msg_buffer = 0;
        size_t _msg_buffer_sz = 0;

        char * get_msg_buf(size_t bytes)
        {
            if ( _msg_buffer_sz < bytes)
            {
                if( _msg_buffer ) delete [] _msg_buffer;
                _msg_buffer = new char[bytes];
                memset(_msg_buffer, 0, sizeof(char) * bytes);
            }
            return _msg_buffer;
        }

};



#endif //NETLINK_UNICAST_SOCKET_HPP
