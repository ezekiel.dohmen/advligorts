#ifndef LIGO_DOLPHIN_KERNEL_PROXY_HPP
#define LIGO_DOLPHIN_KERNEL_PROXY_HPP

#include "LIGO_Thread.hpp"
#include "daemon_messages.h"
#include "NetlinkSocket.hpp"

#include <linux/netlink.h>
#include <sys/socket.h>
#include <memory>
#include <map>


class DolphinKernelProxy : public LIGO_Thread
{
    public:

        static std::unique_ptr< DolphinKernelProxy > create_instance();

        virtual ~DolphinKernelProxy();

        void thread_body();

        //Initialization Helpers
        bool init_sockets();

        //Message Handlers 
        void handle_free_all_req( dolphin_mc_free_all_req * , unsigned bytes );
        void handle_alloc_req( dolphin_mc_alloc_req * , unsigned bytes);


    private:

        DolphinKernelProxy();
        void build_and_send_alloc_resp_error(DOLPHIN_ERROR_CODES status);
        void send_netlink_request_response(void * msg_ptr, unsigned sz_bytes);

        //Netlink socket for requests
        std::unique_ptr< NetlinkSocket > _requests_socket;


        //Netlink to dolphin proxy kernel module
        std::unique_ptr< NetlinkSocket > _dolphin_proxy_socket;

};


#endif //LIGO_DOLPHIN_KERNEL_PROXY_HPP
