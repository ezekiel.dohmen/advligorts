#ifndef LIGO_DOLPHIN_DAEMON_MESSAGES
#define LIGO_DOLPHIN_DAEMON_MESSAGES

#include "util/fixed_width_types.h"

//Common preamble for all messages
#define DOLPHIN_DAEMON_MSG_PREAMBLE 0xA5B1600EC2DDDBF9ull

// Groups for kernel initiated messages
#define DOLPHIN_DAEMON_REQ_GRP_ID 17

//We have to use NETLINK_USERSOCK for the DOLPHIN_DAEMON_REQ_LINK_ID because
//it behaves slightly differently than undefined LINK IDs in <linux/netlink.h>.
//NETLINK_USERSOCK allows user space to bind to the socket BEFORE the kernel 
//module registers the link, this is not the case with other free link IDs.
#define DOLPHIN_DAEMON_REQ_LINK_ID NETLINK_USERSOCK
#define DOLPHIN_DAEMON_DPROXY_LINK_ID 31

// IDs for filtering user space sent messages
#define RT_KM_INTERFACE_ID 0
#define DOLPHIN_KM_INTERFACE_ID 1

//Maximum msg size, should be used to allocate recv buffers
#define DOLPHIN_DAEMON_MAX_PAYLOAD 4096

typedef enum DOLPHIN_DAEMON_MSGS 
{
    DOLPHIN_DAEMON_ALLOC_REQ = 1,
    DOLPHIN_DAEMON_ALLOC_RESP = 2,
    DOLPHIN_DAEMON_FREE_REQ = 3
} DOLPHIN_DAEMON_MSGS;

typedef enum DOLPHIN_ERROR_CODES
{
    DOLPHIN_ERROR_OK = 0,
    DOLPHIN_ERROR_MALFORMED_MSG = 1,
    DOLPHIN_ERROR_NO_LIBRARY_SUPPORT = 2,
    DOLPHIN_ERROR_NO_MCAST_SUPPORT = 3,
    DOLPHIN_ERROR_SEGMENT_SETUP_ERROR = 4,
    DOLPHIN_ERROR_SEGMENT_ALREADY_ALLOCATED = 5,
    DOLPHIN_ERROR_ADAPTER_NOT_SUPPORTED = 6,
    DOLPHIN_ERROR_KERNEL_MODULE_TIMOUT = 7
} DOLPHIN_ERROR_CODES;


//
// Common header on all messages
//
typedef struct dolphin_mc_header
{
    uint64_t preamble_seq;
    uint32_t msg_id;
    uint32_t interface_id;
} dolphin_mc_header;

static inline int check_mc_header_valid(const dolphin_mc_header * header, unsigned payload_len)
{
    if(payload_len < sizeof(dolphin_mc_header)) return 0;

    if(header->preamble_seq != DOLPHIN_DAEMON_MSG_PREAMBLE) return 0;

    return 1;
}

//
// Allocation Request 
//

typedef struct dolphin_mc_alloc_req_info
{
    //Padding to 64 bit align this struct
    uint32_t align_64_pad_1;
    //Size of the dolphin segment to be allocated
    uint32_t segment_sz_bytes;
    //Dolphin adpater num (module) to create the segment on
    uint32_t adapter_num;
    //Segment ID for the requested segment
    uint32_t segment_id;
} dolphin_mc_alloc_req_info;

typedef struct dolphin_mc_alloc_req
{
    dolphin_mc_header header;

    uint32_t align_64_pad_1;
    uint32_t num_segments;
    dolphin_mc_alloc_req_info segments[];
} dolphin_mc_alloc_req;

#define GET_ALLOC_REQ_SZ(num_segments) ( (num_segments * sizeof(dolphin_mc_alloc_req_info)) + sizeof(dolphin_mc_alloc_req) )

static inline int check_alloc_req_valid(dolphin_mc_alloc_req * msg, unsigned payload_len)
{
    if(msg->header.preamble_seq != DOLPHIN_DAEMON_MSG_PREAMBLE) return 0;

    // Correct msg ID
    if(msg->header.msg_id != DOLPHIN_DAEMON_ALLOC_REQ) return 0;

    // All required fields are present
    if(payload_len < sizeof(dolphin_mc_alloc_req)) return 0;

    //Msg is long enough for all requested segments
    if (payload_len < GET_ALLOC_REQ_SZ(msg->num_segments)) 
        return 0;

    return 1;
}


//
// Allocation response 
//
typedef struct dolphin_mc_alloc_resp_info
{
    volatile void * read_addr;
    volatile void * write_addr;
} dolphin_mc_alloc_resp_info;

typedef struct dolphin_mc_alloc_resp
{
    dolphin_mc_header header;

    enum DOLPHIN_ERROR_CODES status;

    uint32_t num_addrs;
    dolphin_mc_alloc_resp_info addrs[];
} dolphin_mc_alloc_resp;

#define GET_ALLOC_RESP_SZ(num_addrs) ( (num_addrs * sizeof(dolphin_mc_alloc_resp_info)) + sizeof(dolphin_mc_alloc_resp))

static inline int check_alloc_resp_valid(dolphin_mc_alloc_resp * msg, unsigned payload_len)
{
    if(msg->header.preamble_seq != DOLPHIN_DAEMON_MSG_PREAMBLE) return 0;

    // Correct msg ID
    if(msg->header.msg_id != DOLPHIN_DAEMON_ALLOC_RESP) return 0;

    // All required fields are present
    if(payload_len < sizeof(dolphin_mc_alloc_resp)) return 0;

    //Msg is long enough for all requested segments
    if (payload_len < GET_ALLOC_RESP_SZ(msg->num_addrs))
        return 0;

    return 1;
}


//
// Free all request
//
typedef struct dolphin_mc_free_all_req
{
    dolphin_mc_header header;
} dolphin_mc_free_all_req;

static inline int check_free_all_valid(dolphin_mc_free_all_req * msg, unsigned payload_len)
{
    if(msg->header.preamble_seq != DOLPHIN_DAEMON_MSG_PREAMBLE) return 0;

    // Correct msg ID
    if(msg->header.msg_id != DOLPHIN_DAEMON_FREE_REQ) return 0;

    // All required fields are present
    if(payload_len < sizeof(dolphin_mc_free_all_req)) return 0;

    return 1;
}



//
// Utility union for handling any msg type 
//
typedef union all_dolphin_daemon_msgs_union
{
    dolphin_mc_header         * as_hdr;
    dolphin_mc_alloc_req      * as_alloc_req;
    dolphin_mc_alloc_resp     * as_alloc_resp;
    dolphin_mc_free_all_req   * as_free_req;
} all_dolphin_daemon_msgs_union;


#endif //LIGO_DOLPHIN_DAEMON_MESSAGES
