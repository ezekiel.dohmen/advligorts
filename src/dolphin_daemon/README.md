# Dolphin Daemon
The dolphin daemon is a userspace application that listens for netlink multicast requests and satisfies them with the Dolphoin API(s).



## Kernel Side Dolphin API
To use the kernel side Dolphin API we run an additional kernel module `dolphin_proxy_km`. We proxy the requests the `dolphin_daemon` gets to this kernel module, and it makes the alloc/free requests from the Dolphin kernel space APIs. The `dolphin_daemon` then returns the request results (read/write) pointers back to the original requesting kernel module. 


## Userspace API
The Dolphin SISCI API will only return the virtual kernel read address to users, so this is not currently used by the `dolphin_daemon` If changes are made in the future, and the virtual kernel write address could also be retrieved from the userspace API, then we could switch to this method. The `DolphinNetlinkServer.cpp` and `Dolphin_SISCI_Resource.cpp` files have most of this implantation complete, we just need to add getting the write address (when suported). 

