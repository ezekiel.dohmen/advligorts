#include <stdio.h>
#include <stdlib.h>
#include "../daqd/crc8.cc"

void print_usage(const char *progname) {
    fprintf(stderr, "Usage:  %s <string> [<string2> ...]\n", progname);
}

int main (int argc, char **argv) {
    if (argc < 2) {
        print_usage(argv[0]);
        exit(1);
    }

    argc--, argv++;

    while (argc >= 1) {
        printf("%s\n", crc8_str( argv[0] ));
        argc--, argv++;
    }
    exit(0);
}
