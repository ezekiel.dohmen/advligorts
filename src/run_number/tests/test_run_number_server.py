import os
import os.path
import time
import typing
import integration

integration.Executable(name="run_number_server", hints=["/run_number_server",], description="run_number_server")
integration.Executable(name="run_number_test_client", hints=["/run_number_test_client",], description="run_number_test_client")

server_db = os.path.join(integration.state.temp_dir("db"), "server_db")

run_number_server = integration.Process("run_number_server",
                                        ["--file", server_db])

# functions


def test_hash(input_hash, expected, timeout=20):

    def do_check():
        end_time = time.time() + timeout
        number_test_1 = integration.Process("run_number_test_client", [
            "--hash", input_hash])

        number_test_1.ignore()
        number_test_1.run()

        while number_test_1.state() == integration.Process.RUNNING:
            print(integration.Process.RUNNING)

            if time.time() > end_time:
                number_test_1.stop()
                raise RuntimeError("Process timed out")

            time.sleep(.5)

        actual_out = number_test_1.get_output_log().strip()
        if actual_out != expected:
            raise RuntimeError("Actual output, '{0}' , not equal to expected output, '{1}'".format(actual_out, expected))


    return do_check


def echo(msg):
    def wrapper():
        print(msg)
    return wrapper

integration.Sequence([

    #integration.state.preserve_files,
    run_number_server.run,
    integration.wait(5),
    echo("Run number server is up and running"),
    test_hash("1111111111", "1"),
    test_hash("1111111111", "1"),
    test_hash("1111111112", "2"),
    test_hash("1111111112", "2"),
    test_hash("1111111111", "3"),
])





