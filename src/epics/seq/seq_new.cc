///	@file /src/epics/seq/main.c
///	@brief Contains required 'main' function to startup EPICS sequencers, along with supporting routines. 
///<		This code is taken from EPICS example included in the EPICS distribution and modified for LIGO use.
/* demoMain.c */
/* Author:  Marty Kraimer Date:    17MAR2000 */

/********************COPYRIGHT NOTIFICATION**********************************
This software was developed under a United States Government license
described on the COPYRIGHT_UniversityOfChicago file included as part
of this distribution.
****************************************************************************/

// TODO:
// Add command error checking, command out of range, etc.
//	- Particularly returns from functions.
//	- Add db pointer to cdTable.
//	- Add present value to cdTable.
//	- Fix filter monitor bit settings on change.
/*
 * Main program for demo sequencer
 */
#include "epics_channel.hh"
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <sys/stat.h>
#include <errno.h>
#include <ctype.h>
#include <algorithm>
#include <array>
#include <iterator>
#include <memory>
#include <functional>
#include <cstdio> //std::{fopen, fclose, fflush}


#ifdef USE_SYSTEM_TIME
#include <time.h>
#endif

#include "iocsh.h"
#include "dbStaticLib.h"
extern "C" {
    #include "crc.h"
}
#include "dbCommon.h"
#include "recSup.h"
#include "dbDefs.h"
#include "dbFldTypes.h"
#include "dbAccess.h"
#include "envDefs.h"
#include "math.h"
#include "rcgversion.h"
#include "burt_file.hh"
#include "sdf_file_loaded.h"
#include "util/user/check_file_crc.h"

#define epicsExportSharedSymbols
#include "asLib.h"
#include "asCa.h"
#include "asDbLib.h"

#include "fixed_size_string.hh"
#include "fixed_size_vector.hh"
#include "simple_range.hh"

using embedded::fixed_string;
using embedded::fixed_size_vector;

#ifdef CA_SDF
#include <mutex>

// Including the DB/IOC based code + CA based client code gives warnings on redefinitions
// so undef these before including the CA code.
#undef DBR_SHORT
#undef DBR_GR_LONG
#undef DBR_GR_DOUBLE
#undef DBR_GR_LONG
#undef DBR_CTRL_DOUBLE
#undef DBR_CTRL_LONG
#undef DBR_PUT_ACKT
#undef DBR_PUT_ACKS
#undef VALID_DB_REQ
#undef INVALID_DB_REQ
#include "cadef.h"
#endif

///< SWSTAT has 32 bits but only 17 of them are used (what does bit 14 do?)
#define ALL_SWSTAT_BITS		(0x1bfff)

#define SDF_LOAD_DB_ONLY	4
#define SDF_READ_ONLY		2
#define SDF_RESET		3
#define SDF_LOAD_PARTIAL	1

#define SDF_MAX_CHANS		125000	///< Maximum number of settings, including alarm settings.
#define SDF_MAX_TSIZE		20000	///< Maximum number of EPICS settings records (No subfields).
#define SDF_ERR_DSIZE		40	///< Size of display reporting tables.
#define SDF_ERR_TSIZE		20000	///< Size of error reporting tables.
#define SDF_MAX_FMSIZE		1000	///< Maximum number of filter modules that can be monitored.

#define MAX_CHAN_LEN		60

#define SDF_TABLE_DIFFS			0
#define SDF_TABLE_NOT_FOUND		1
#define SDF_TABLE_NOT_INIT		2
#define SDF_TABLE_NOT_MONITORED		3
#define SDF_TABLE_FULL			4
#ifdef CA_SDF
#define SDF_TABLE_DISCONNECTED		5
#endif

#define CA_STATE_OFF -1
#define CA_STATE_IDLE 0
#define CA_STATE_PARSING 1
#define CA_STATE_RUNNING 2
#define CA_STATE_EXIT 3

enum SDF_FILE_TYPE {
	SDF_FILE_UNKNOWN=0,
	SDF_FILE_SAFE=1,
	SDF_FILE_OBSERVE=2,
};

enum SDF_TYPE {
    SDF_NUM=0,
    SDF_STR=1,
    SDF_UNKNOWN=-1,
};

using PV_INT_TYPE = unsigned int;

#if 0
/// Pointers to status record
DBENTRY  *pdbentry_status[2][10];

/// Pointers to records currently alarmed
///< Two lists of 10 records, one inputs (ai, bi)
///< the other outputs (ao, bo)
DBENTRY  *pdbentry_alarm[2][10];


/// Alarm configuration CRC checksum.
DBENTRY  *pdbentry_crc = 0;
/// Number of setpoints in alarm..
DBENTRY  *pdbentry_in_err_cnt = 0;
/// Number of readbacks in alarm..
DBENTRY  *pdbentry_out_err_cnt = 0;
#endif

/// Quick look up table for filter module switch decoding
unsigned int fltrConst[17] = {16, 64, 256, 1024, 4096, 16384,
                              65536, 262144, 1048576, 4194304,
                              0x4, 0x8, 0x4000000,0x1000000,0x1,
                              0x2000000,0x8000000
};
union Data {
	double chval;
	fixed_string<64> strval{};
};

/// Structure for holding BURT settings in local memory.
typedef struct CDS_CD_TABLE {
	fixed_string<128> chname;
	int datatype;
	union Data data;
	int mask;
	int initialized;
	int filterswitch;
	int filterNum;
	int error;
	fixed_string<64> errMsg;
	int chFlag;
#ifdef CA_SDF
	int connected;
#endif
} CDS_CD_TABLE;

/// Structure for creating/holding filter module switch settings.
typedef struct FILTER_TABLE {
	fixed_string<64> fname;
	int swreq;
	int swmask;
	int sw[2];
	int newSet;
	int init;
	int mask;
} FILTER_TABLE;

/// Structure for table data to be presented to operators.
typedef struct SET_ERR_TABLE {
	fixed_string<64> chname;
	fixed_string<64> burtset;
	fixed_string<64> liveset;
	fixed_string<64> timeset;
	fixed_string<64> diff;
	double liveval;
	int sigNum;
	int chFlag;
	int filtNum{-1};
	unsigned int sw[2];
} SET_ERR_TABLE;

#ifdef CA_SDF
/// Structure for holding data from EPICS CA
typedef struct EPICS_CA_TABLE {
	int redirIndex;					// When a connection is in progress writes are redirected as the type may have changed
	int datatype;
	union Data data;
	int connected;
	time_t mod_time;
	chid chanid;
	int chanIndex;					// index of the channel in cdTable, caTable, ...
} EPICS_CA_TABLE;

/// Structure to hold start up information for the CA thread
typedef struct CA_STARTUP_INFO {
	char *fname;		// name of the request file to parse
} CA_STARTUP_INFO;

#endif

using table_range = embedded::range<SET_ERR_TABLE*>;

template <std::size_t entry_count>
table_range
make_range(fixed_size_vector<SET_ERR_TABLE, entry_count>& cont)
{
    return embedded::make_range(&cont[0], &cont[0] + cont.size());
}

// Gloabl variables		****************************************************************************************
int chNum = 0;			///< Total number of channels held in the local lookup table.
int chNotMon = 0;		///< Total number of channels not being monitored.
int alarmCnt = 0;		///< Total number of alarm settings loaded from a BURT file.
int chNumP = 0;			///< Total number of settings loaded from a BURT file.
int fmNum = 0;			///< Total number of filter modules found.
int fmtInit = 0;		///< Flag used to indicate that the filter module table needs to be initiialized on startup.
int chNotFound = 0;		///< Total number of channels read from BURT file which did not have a database entry.
int chNotInit = 0;		///< Total number of channels not initialized by the safe.snap BURT file.
int rderror = 0;
#ifndef USE_SYSTEM_TIME
std::unique_ptr< epics::DBEntry< epics::PVType::String > > timechannel{nullptr}; ///the GPS time channel for timestamping.
#endif
char reloadtimechannel[256];	///< Name of EPICS channel which contains the BURT reload requests.
struct timespec t;
char logfilename[128];

fixed_size_vector<CDS_CD_TABLE, SDF_MAX_TSIZE> cdTable;       ///< Table used to hold EPICS database info for monitoring settings.
fixed_size_vector<CDS_CD_TABLE, SDF_MAX_CHANS> cdTableP;      ///< Temp table filled on BURT read and set to EPICS channels.
fixed_size_vector<FILTER_TABLE, SDF_MAX_FMSIZE> filterTable;  ///< Table for holding filter module switch settings for comparisons.
fixed_size_vector<SET_ERR_TABLE, SDF_ERR_TSIZE> setErrTable;  ///< Table used to report settings diffs.
fixed_size_vector<SET_ERR_TABLE, SDF_ERR_TSIZE> unknownChans; ///< Table used to report channels not found in local database.
fixed_size_vector<SET_ERR_TABLE, SDF_ERR_TSIZE> uninitChans;  ///< Table used to report channels not initialized by BURT safe.snap.
fixed_size_vector<SET_ERR_TABLE, SDF_MAX_TSIZE> unMonChans;	  ///< Table used to report channels not being monitored.
fixed_size_vector<SET_ERR_TABLE, SDF_ERR_TSIZE> readErrTable; ///< Table used to report file read errors..
fixed_size_vector<SET_ERR_TABLE, SDF_MAX_TSIZE> cdTableList;  ///< Table used to report file read errors..
fixed_size_vector<time_t, SDF_MAX_CHANS> timeTable;           ///< Holds timestamps for cdTableP

fixed_size_vector<int, SDF_MAX_FMSIZE> filterMasks;		///< Filter monitor masks, as manipulated by the user

fixed_size_vector<dbAddr, SDF_MAX_FMSIZE> fmMaskChan;		///< We reflect the mask value into these channels
fixed_size_vector<dbAddr, SDF_MAX_FMSIZE> fmMaskChanCtrl;		///< We signal bit changes to the masks on these channels

#ifdef CA_SDF
fixed_size_vector<SET_ERR_TABLE, SDF_MAX_TSIZE> disconnectChans;

// caTable, caConnTAble, chConnNum, chEnumDetermined are protected by the caTableMutex
EPICS_CA_TABLE caTable[SDF_MAX_TSIZE];		    ///< Table used to hold the data returned by channel access
EPICS_CA_TABLE caConnTable[SDF_MAX_TSIZE]; ///< Table used to hold new connection data
EPICS_CA_TABLE caConnEnumTable[SDF_MAX_TSIZE];  ///< Table used to hold enums as their monitoring type (NUM/STR) is determined
												///< enums are treated as STR records only if they have non-null 0 and 1 strings

int  caStringSetpointInitted[SDF_MAX_TSIZE];
char caStringSetpoint[SDF_MAX_TSIZE][64];

// protected by caEvidMutex
evid caEvid[SDF_MAX_TSIZE];			///< Table to hold the CA subscription id for the channels

int chConnNum = 0;					///< Total number of entries to be processed in caConnTable
int chEnumDetermined = 0;			///< Total number of enums whos types have been determined

long chDisconnected = 0;		///< Total number of channels that are disconnected.  This is used as the max index for disconnectedChans
long chDisconnectedCount = 0;   ///< The count of the disconnected channels.  Seperate from chDisconnected so that it can be updated even when the table is not updated.


std::mutex caStateMutex;
std::mutex caTableMutex;
std::mutex caEvidMutex;

using lock_guard = std::lock_guard<std::mutex>;

int caThreadState;
long droppedPVCount;

using ADDRESS = int;


#else
using ADDRESS = dbAddr;
#endif


// Function prototypes		****************************************************************************************
bool isAlarmChannelRaw(const char *);
int checkFileMod( const char* , time_t* , int );
unsigned int filtCtrlBitConvert(unsigned int);
void getSdfTime(embedded::fixed_string<256>&);
void logFileEntry(const char *);
void getEpicsSettings();

template <std::size_t entry_count>
bool writeTable2File(const char *burtdir, const char *filename, int ftype, fixed_size_vector<CDS_CD_TABLE, entry_count>& myTable);

int savesdffile(int,int,const char *,const char *,const char *,const char *,const char *,epics::DBEntry<epics::PVType::String>&,epics::DBEntry<epics::PVType::String>&,epics::DBEntry<epics::PVType::String>&);
int createSortTableEntries(int,int,const char *,int *,time_t*);
template <std::size_t entry_count>
int reportSetErrors(char *,fixed_size_vector<SET_ERR_TABLE, entry_count>&,int,int);
template <std::size_t entry_count>
int spChecker(int,fixed_size_vector<SET_ERR_TABLE, entry_count>&,int,const char *,int,int *);
void newfilterstats();

template <std::size_t entry_count>
int writeEpicsDb(fixed_size_vector<CDS_CD_TABLE, entry_count>&,int);
int readConfig( const char *, const char *,int, const char *);
int modifyTable(table_range);
template <std::size_t entry_count>
int resetSelectedValues(fixed_size_vector<SET_ERR_TABLE, entry_count>&);
template <std::size_t entry_count>
void clearTableSelections(fixed_size_vector<SET_ERR_TABLE, entry_count>& dcsErrTable, int sc[]);
void setAllTableSelections(table_range, int *,int);
void changeSelectCB_uninit(SET_ERR_TABLE&, int *);
void decodeChangeSelect(int, int, table_range, int *, void (*)(SET_ERR_TABLE&, int *));
int appendAlarms2File(const char *,const char *,const char *);
void registerFilters();
void setupFMArrays(char *,fixed_size_vector<int, SDF_MAX_FMSIZE>&,
        fixed_size_vector<dbAddr, SDF_MAX_FMSIZE>&,
        fixed_size_vector<dbAddr, SDF_MAX_FMSIZE>&);
void resyncFMArrays(fixed_size_vector<int, SDF_MAX_FMSIZE>&,fixed_size_vector<dbAddr, SDF_MAX_FMSIZE>&);
void processFMChanCommands(fixed_size_vector<int, SDF_MAX_FMSIZE>&,
        fixed_size_vector<dbAddr, SDF_MAX_FMSIZE>&,
        fixed_size_vector<dbAddr, SDF_MAX_FMSIZE>&,
        int*,table_range);
void countDisconnectedChannels(int);
void saveStrSetPoint(int, const char*);
void updateStrVarSetPoint(int);
#ifdef CA_SDF
long countDisconnectedChans();

void nullCACallback(struct event_handler_args);

bool getCAIndex(const char *, ADDRESS *);

int canFindCAChannel(char *entry);
bool setCAValue(ADDRESS, SDF_TYPE, const void *);
bool setCAValueLong(ADDRESS, const PV_INT_TYPE *);

bool syncEpicsDoubleValue(ADDRESS, double *, time_t *, int *);
bool syncEpicsIntValue(ADDRESS, PV_INT_TYPE *, time_t *, int *);
bool syncEpicsStrValue(ADDRESS, char *, int, time_t *, int *);

void subscriptionHandler(struct event_handler_args);
void connectCallback(struct connection_handler_args);
void registerPV(char *);
int daqToSDFDataType(int);
void parseChannelListReq(char *);

int getCAThreadState();
void setCAThreadState(int state);

void copyConnectedCAEntry(EPICS_CA_TABLE *);
void syncCAConnections(long *);

void *caMainLoop(void *);


void initCAConnections(char *);
void setupCASDF();
void cleanupCASDF();

void SETUP()
{
    setupCASDF();
}
void CLEANUP()
{
    cleanupCASDF();
}

bool GET_ADDRESS(const char* name, ADDRESS* addr)
{
    return getCAIndex(name, addr);
}
bool PUT_VALUE(ADDRESS addr, SDF_TYPE type, const void* pval)
{
    return setCAValue(addr, type, pval);
}
bool PUT_VALUE_INT(ADDRESS addr, const PV_INT_TYPE* val)
{
    return setCAValueLong(addr, val);
}
bool GET_VALUE_NUM(ADDRESS addr, double* dest, time_t* tp, int* connp)
{
    return syncEpicsDoubleValue(addr, dest, tp, connp);
}
bool GET_VALUE_INT(ADDRESS addr, PV_INT_TYPE* dest, time_t* tp, int* connp)
{
    return syncEpicsIntValue(addr, dest, tp, connp);
}
bool GET_VALUE_STR(ADDRESS addr, char* dest, int dest_size, time_t* tp, int *connp) {
    return syncEpicsStrValue(addr, dest, dest_size, tp, connp);
}
#else
bool getDbValueDouble(ADDRESS*,double *,time_t *);
bool getDbValueString(ADDRESS*,char *, int, time_t *);
bool getDbValueLong(ADDRESS *,PV_INT_TYPE * ,time_t *);
void dbDumpRecords(DBBASE *,const char *);

void SETUP()
{}

void CLEANUP()
{}

bool GET_ADDRESS(const char* name, ADDRESS* addr)
{
    return dbNameToAddr(name, addr) == 0;
}

bool PUT_VALUE(ADDRESS addr, SDF_TYPE type, const void* pval)
{
    return dbPutField(&addr,(type==SDF_NUM ? DBR_DOUBLE : DBR_STRING),pval,1) == 0;
}
bool PUT_VALUE_INT(ADDRESS addr, const PV_INT_TYPE *val)
{
    return dbPutField(&addr, DBR_LONG, val, 1) == 0;
}
bool GET_VALUE_NUM(ADDRESS addr, double* dest, time_t* tp, int* connp)
{
    return getDbValueDouble(&addr, dest, tp);
}
bool GET_VALUE_INT(ADDRESS addr, PV_INT_TYPE* dest, time_t* tp, int* connp)
{
    return getDbValueLong(&addr, dest, tp);
}
bool GET_VALUE_STR(ADDRESS addr, char* dest, int max_len, time_t *tp, int* connp)
{
    return getDbValueString(&addr, dest, max_len, tp);
}
#endif

template <typename StringLike>
bool
PUT_VALUE_STR(ADDRESS addr, const StringLike& value)
{
    return PUT_VALUE(addr, SDF_STR, reinterpret_cast<void*>(const_cast<char*>(value.c_str())));
}

template <typename StringLike>
bool
GET_ADDRESS(const StringLike& name, ADDRESS* addr)
{
    return GET_ADDRESS(name.c_str(), addr);
}

template <typename StringLike>
bool
GET_VALUE_STR(ADDRESS addr, StringLike& dest, time_t *tp, int* connp)
{
    auto buffer = dest.get_buffer();
    return GET_VALUE_STR(addr, buffer.data(), buffer.capacity(), tp, connp);
}

#ifdef VERBOSE_DEBUG
#define D_NOW (__output_iter == 0)
#define D(...) {if (__output_iter == 0) { fprintf(stderr, __VA_ARGS__); } }

#define TEST_CHAN "X1:PSL-PMC_BOOST_CALI_SW1S"

int __output_iter = 0;
int __test_chan_idx = -1;
void iterate_output_counter()
{
	++__output_iter;
	if (__output_iter > 10) __output_iter = 0;
}
#else
#define D_NOW (false)
#define D(...) {}
void iterate_output_counter() {}
#endif


// Some helper macros
#define CHFLAG_CURRENT_MONITORED_BIT_VAL 1
#define CHFLAG_REVERT_BIT_VAL 2
#define CHFLAG_ACCEPT_BIT_VAL 4
#define CHFLAG_MONITOR_BIT_VAL 8

#define CHFLAG_CURRENT_MONITORED_BIT(x) ((x) & 1)
#define CHFLAG_REVERT_BIT(x) ((x) & 2)
#define CHFLAG_ACCEPT_BIT(x) ((x) & 4)
#define CHFLAG_MONITOR_BIT(x) ((x) & 8)

// End Header **********************************************************************************************************
//

/// Given a channel name return 1 if it is an alarm channel, else 1
template <typename StringLike>
bool isAlarmChannel(const StringLike& chname) {
	return isAlarmChannelRaw(chname.c_str());
//    return ((strstr(chname.c_str(),".HIGH") != NULL) ||
//		(strstr(chname.c_str(),".HIHI") != NULL) ||
//		(strstr(chname.c_str(),".LOW") != NULL) ||
//		(strstr(chname.c_str(),".LOLO") != NULL) ||
//		(strstr(chname.c_str(),".HSV") != NULL) ||
//		(strstr(chname.c_str(),".OSV") != NULL) ||
//		(strstr(chname.c_str(),".ZSV") != NULL) ||
//		(strstr(chname.c_str(),".LSV") != NULL) );
}

bool isAlarmChannelRaw(const char* chname) {
    if (chname == nullptr)
    {
        return false;
    }
    return ((strstr(chname,".HIGH") != nullptr) ||
            (strstr(chname,".HIHI") != nullptr) ||
            (strstr(chname,".LOW") != nullptr) ||
            (strstr(chname,".LOLO") != nullptr) ||
            (strstr(chname,".HSV") != nullptr) ||
            (strstr(chname,".OSV") != nullptr) ||
            (strstr(chname,".ZSV") != nullptr) ||
            (strstr(chname,".LSV") != nullptr) );
}



/// Common routine to clear all entries for table changes..
///	@param[in] numEntries		Number of entries in the table.	
///	@param[in] dcsErrtable 		Pointer to table
///	@param[out] sc 			Pointer to change counters.
template <std::size_t entry_count>
void clearTableSelections(fixed_size_vector<SET_ERR_TABLE, entry_count>& dcsErrTable, int sc[])
{
    int ii;
	for(ii=0;ii<3;ii++) sc[ii] = 0;
	for (auto& entry:dcsErrTable)
    {
	    entry.chFlag = 0;
	    entry.filtNum = -1;
    }
}

/// Common routine to set all entries for table changes..
///	@param[in] numEntries		Number of entries in the table.	
///	@param[in] dcsErrtable 		Pointer to table
///	@param[out] sc 			Pointer to change counters.
///	@param[in] selectOp 		Present selection.
void setAllTableSelections(table_range dcsErrTable, int sc[],int selectOpt)
{
    int filtNum = -1;
    long status = 0;

	switch(selectOpt) {
		case 1:
			sc[0] = 0;
			for (auto& entry:dcsErrTable)
			{
				entry.chFlag |= 2;
				sc[0] ++;
				if(entry.chFlag & 4) {
					entry.chFlag &= ~(4);
					sc[1] --;
				}
			}
			break;
		case 2:
			sc[1] = 0;
            for (auto& entry:dcsErrTable)
			{
				entry.chFlag |= 4;
				sc[1] ++;
				if(entry.chFlag & 2) {
					entry.chFlag &= ~(2);
					sc[0] --;
				}
			}
			break;
		case 3:
			sc[2] = std::distance(dcsErrTable.begin(), dcsErrTable.end());
            for (auto& entry:dcsErrTable)
			{
				entry.chFlag |= 8;
				
				filtNum = entry.filtNum;
				if (filtNum >= 0) {
					if (filterMasks[filtNum] == filterTable[filtNum].mask) {
						filterMasks[filtNum] = ((ALL_SWSTAT_BITS & filterMasks[filtNum]) > 0 ? 0 : ~0);
						status = dbPutField(&fmMaskChan[filtNum],DBR_LONG,&(filterMasks[filtNum]),1);
					}
				}
			}
			break;
		default:
			break;
	}
}

/// Callback function to ensure consistency of the select flags for entries in the uninit table
///	@param[in] index		Number providing the index into the dcsErrTable
///	@param[in] dcsErrTable		Pointer to table
///	@param[out] selectCounter	Pointer to change counters
void changeSelectCB_uninit(SET_ERR_TABLE &errTableEntry, int selectCounter[])
{
	int revertBit = errTableEntry.chFlag & 2;
	int acceptBit = errTableEntry.chFlag & 4;
	int monBit = errTableEntry.chFlag & 8;
	int otherBits = 0; //errTableEntry.chFlag | ~(2 | 4 | 8);

	// revert is not defined on this table
	if (revertBit) {
		revertBit = 0;
		selectCounter[0] --;
	}
	// if monitor is set, then accept must be set
	if (monBit && !acceptBit) {
		acceptBit = 4;
		selectCounter[1] ++;
	} 
	errTableEntry.chFlag = revertBit | acceptBit | monBit | otherBits;
}

/// Common routine to set/unset individual entries for table changes..
///	@param[in] selNum		Number providing line and column of table entry to change.
///	@param[in] page			Number providing the page of table to change.
///	@param[in] totalItems		Number items selected for change.
///	@param[in] dcsErrtable 		Pointer to table
///	@param[out] selectCounter 	Pointer to change counters.
void decodeChangeSelect(int selNum, int page, table_range dcsErrTable, int selectCounter[], void (*cb)(SET_ERR_TABLE&, int*))
{
    int selectBit;
    int selectLine;

	selectBit = selNum / 100;
	selectLine = selNum % 100 - 1;
	selNum += page * SDF_ERR_DSIZE;
	selectLine += page * SDF_ERR_DSIZE;

	auto selection = dcsErrTable.begin() + selectLine;
	if(selection < dcsErrTable.end()) {
		switch (selectBit) {
			case 0:
				selection->chFlag ^= 2;
				if(selection->chFlag & 2) selectCounter[0] ++;
				else selectCounter[0] --;
				if(selection->chFlag & 4) {
					selection->chFlag &= ~(4);
					selectCounter[1] --;
				}
				break;
			case 1:
				selection->chFlag ^= 4;
				if(selection->chFlag & 4) selectCounter[1] ++;
				else selectCounter[1] --;
				if(selection->chFlag & 2) {
					selection->chFlag &= ~(2);
					selectCounter[0] --;
				}
				break;
			case 2:
				/* we only handle non-filter bank entries here */
				if (selection->filtNum < 0) {
					selection->chFlag ^= 8;
					if(selection->chFlag & 8) selectCounter[2] ++;
					else selectCounter[2] --;
				}
				break;
			default:
				break;
		}
		if (cb)
			cb(*selection, selectCounter);
	}
}

// Check for file modification time changes
int
checkFileMod( const char* fName, time_t* last_mod_time, int gettime )
{
    struct stat statBuf;
    if ( !stat( fName, &statBuf ) )
    {
        if ( gettime )
        {
            *last_mod_time = statBuf.st_mtime;
            return 2;
        }
        if ( statBuf.st_mtime > *last_mod_time )
        {
            *last_mod_time = statBuf.st_mtime;
            return 1;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return 0;
    }
}



/// Quick convert of filter switch settings to match SWMASK
///	@param[in] v	UINT32 representation of filter module switch setting.
///	@return Input value converted to 16 bit representation of filter switch settings.
unsigned int filtCtrlBitConvert(unsigned int v) {
        unsigned int val = 0;
        int i;
        for (i = 0; i < 17; i++) {
                if (v & fltrConst[i]) val |= 1<<i;
        }
        return val;
}
/// Quick convert of filter switch SWSTAT to SWSTR value
///	@param[in] v			SWSTAT representation of switch setting.	
///	@param[out] filterstring	Returned string representation of switch setting.	
///	@return				Always zero at this point.
template <typename StringLike>
int filtStrBitConvert(int stype, unsigned int v, StringLike& filterstring) {
unsigned int val[5],jj;
unsigned int x;
char bitDefShort[16][8] = {"IN,","OF,","1,","2,",
			   "3,","4,","5,","6,",
			   "7,","8,","9,","10,",
			   "LT,","OT,","DC,","HD,"};
char bitDefLong[16][8] = {"IN,","OF,","F1,","F2,",
			   "F3,","F4,","F5,","F6,",
			   "F7,","F8,","F9,","F10,",
			   "LT,","OT,","DC,","HD,"};

	val[0] = (v >> 10) & 3;		//In and Offset
	val[1] = (v & 1023) << 2;		// F1 thru F10
	val[2] = (v & 0x2000) >> 1;		// Limit
	val[3] = (v & 0x1000) << 1;		// Output
	val[4] = (v & 0x18000) >> 1;		// Dec and Hold
	x = val[0] + val[1] + val[2] + val[3] + val[4];
	filterstring.clear();
	for(jj=0;jj<16;jj++) {
		if(x & 1 && stype == 0) filterstring += bitDefShort[jj];
		if(x & 1 && stype == 1) filterstring += bitDefLong[jj];
		x = x >> 1;
	}
	// strcat(filterstring,"\0");
        return(0);
}

/// Given the a filter table entry print out the bits set in the mask.
/// @param[in] filter the filter table entry to extract the current value from
/// @param[out] dest buffer to put the results in
template <typename StringLike>
void createSwitchText(FILTER_TABLE *filter, StringLike& dest) {
#if 1
	int sw1s=0;
	int sw2s=0;

	dest.clear();
	sw1s = (int)cdTableP[filter->sw[0]].data.chval;
	sw2s = (int)cdTableP[filter->sw[1]].data.chval;
#else
	ADDRESS paddr;
	ADDRESS paddr2;
	int sw1s=0;
	int sw2s=0;
	char name[256];
	char name2[256];
	
	if (snprintf(name, sizeof(name), "%s%s", filter->fname, "SW1S") > sizeof(name)) {	
		return;
	}
	if (snprintf(name2, sizeof(name2), "%s%s", filter->fname, "SW2S") > sizeof(name2)) {	
		return;
	}
	GET_ADDRESS(name, &paddr);
	GET_ADDRESS(name2, &paddr2);
	GET_VALUE_INT(paddr, &sw1s, NULL, NULL);
	GET_VALUE_INT(paddr2, &sw2s, NULL, NULL);
#endif
	filtStrBitConvert(1,filtCtrlBitConvert(sw1s + (sw2s << 16)), dest);
}

/// Routine to check filter switch settings and provide info on switches not set as requested.
/// @param[in] monitorAll	Global monitoring flag.
/// @return	Number of errant switch settings found.
template <std::size_t entry_count>
int checkFilterSwitches(fixed_size_vector<SET_ERR_TABLE, entry_count>& setErrTable, int monitorAll, int displayall, int wcflag, const char *wcstr, int *diffcntr)
{
unsigned int refVal=0;
unsigned int presentVal=0;
unsigned int x=0,y=0;
unsigned int mask = 0;
unsigned int maskedRefVal = 0;
unsigned int maskedSwReqVal = 0;
int jj=0;
int errCnt = 0;
fixed_string<64>swname[3];
fixed_string<64> tmpname;
time_t mtime=0;
fixed_string<64> swstrE;
fixed_string<64> swstrB;
fixed_string<64> swstrD;
struct buffer {
	time_t t;
	unsigned int rval;
	}buffer[2];
ADDRESS paddr;
ADDRESS paddr1;
ADDRESS paddr2;
long status=0;

    auto filtNum = -1;
    for (const auto& curFilt:filterTable)
    {
        ++filtNum;
		bzero(buffer, sizeof(struct buffer)*2);

		swname[0] = curFilt.fname;
		swname[0] += "SW1S";
		swname[1] = curFilt.fname;
		swname[1] += "SW2S";
		swname[2] = curFilt.fname;
		swname[2] += "SWSTR";
		GET_ADDRESS(swname[0],&paddr);
		GET_VALUE_INT(paddr,&(buffer[0].rval),&(buffer[0].t), 0);
		GET_ADDRESS(swname[1],&paddr1);
		GET_VALUE_INT(paddr1,&(buffer[1].rval),&(buffer[1].t), 0);
		for(jj=0;jj<2;jj++) {
			if(buffer[jj].rval > 0xffff || buffer[jj].rval < 0)	// Switch setting overrange
			{
			    setErrTable.emplace_back();
			    auto& curErr = setErrTable.back();
				curErr.chname = swname[jj];
				curErr.burtset = " ";
				curErr.liveset.printf("0x%x", buffer[jj].rval);
				curErr.diff = "OVERRANGE";
				curErr.liveval = 0.0;
				curErr.sigNum = curFilt.sw[jj];
				// take the latest change time between the SW1S & SW2S channels
				mtime = (buffer[0].t >= buffer[1].t ? buffer[0].t : buffer[1].t);
				curErr.timeset = ctime(&mtime);
				buffer[jj].rval &= 0xffff;
				errCnt ++;
			}
		}
		presentVal = buffer[0].rval + (buffer[1].rval << 16);
		refVal = filtCtrlBitConvert(presentVal);
		filtStrBitConvert(0,refVal,swstrE);
		x = refVal;
		GET_ADDRESS(swname[2],&paddr2);
		PUT_VALUE_STR(paddr2,swstrE);
		//setErrTable[errCnt].filtNum = -1;

		mask = (unsigned int)curFilt.mask;
		maskedRefVal = refVal & mask;
		maskedSwReqVal = curFilt.swreq & mask;
		if(( maskedRefVal != maskedSwReqVal && errCnt < SDF_ERR_TSIZE && (mask || monitorAll) && curFilt.init) )
			*diffcntr += 1;
		if(( maskedRefVal != maskedSwReqVal && errCnt < SDF_ERR_TSIZE && (mask || monitorAll) && curFilt.init) || displayall)
		{
			filtStrBitConvert(1,refVal,swstrE);
			filtStrBitConvert(1,curFilt.swreq,swstrB);
			x = maskedRefVal ^ maskedSwReqVal;
			filtStrBitConvert(1,x,swstrD);
			tmpname = curFilt.fname;

			if(!wcflag || (wcflag && (strstr(tmpname.c_str(),wcstr) != NULL))) {
			    setErrTable.emplace_back();
			    auto& curErr = setErrTable.back();
                curErr.chname = tmpname;
                curErr.burtset = swstrB;
                curErr.liveset = swstrE;
                curErr.diff = swstrD;

                curErr.liveval = 0.0;
                curErr.sigNum = curFilt.sw[0] + (curFilt.sw[1] * SDF_MAX_TSIZE);
                curErr.filtNum = filtNum;
                curErr.sw[0] = buffer[0].rval;
                curErr.sw[1] = buffer[1].rval;
                if(curFilt.mask & ALL_SWSTAT_BITS) curErr.chFlag |= 0x1;
                else curErr.chFlag &= 0xe;

                // take the latest change time between the SW1S & SW2S channels
                mtime = (buffer[0].t >= buffer[1].t ? buffer[0].t : buffer[1].t);
                curErr.timeset = ctime(&mtime);
                errCnt ++;
			}
		}
	}
	return(errCnt);
}

/// Routine for reading and formatting the time as a string.
///	@param[out] timestring 	Pointer to char string in which GPS time is to be written.
/// @note This can use the GPS time from the model, or if configured with USE_SYSTEM_TIME the system time.
void getSdfTime(embedded::fixed_string<256>& timestring)
{
#ifdef USE_SYSTEM_TIME
	time_t t=0;
	struct tm tdata;

	t = time(NULL);
	localtime_r(&t, &tdata);

    auto buf = timestring.get_buffer();
    strftime(buf.data(), buf.capacity(), "%a %b %e %H:%M:%S %Y", &tdata);
#else
	dbAddr paddr;
	long ropts = 0;
	long nvals = 1;
	long status;

    timechannel->get(timestring);
#endif
}

/// Routine for logging messages to ioc.log file.
/// 	@param[in] message Ptr to string containing message to be logged.
void logFileEntry(const char *message)
{
    static std::unique_ptr < std::FILE, std::function<void(std::FILE*) > > fp = nullptr;
    static bool open_attempted = false;

    embedded::fixed_string<256> timestring{};
    dbAddr paddr;
    getSdfTime(timestring);

    if(fp.get() == nullptr && !open_attempted )
    {
        open_attempted = true;
        fp = std::unique_ptr < std::FILE, std::function<void(std::FILE*)>>(
            std::fopen(logfilename, "a"),
            [](std::FILE * fp) { std::fclose(fp); });

        if(fp.get() == NULL) {
            dbNameToAddr(reloadtimechannel, &paddr);
            dbPutField(&paddr, DBR_STRING, "ERR - NO LOG FILE FOUND",1);
            embedded::fixed_string<256> error_msg( "Failed to open log file: ");
            error_msg += logfilename;
            error_msg +=  " : ";
            perror(error_msg.c_str());
        }
    }


    if(fp.get() != nullptr)
    {
        fprintf(fp.get(), "%s\n%s\n", timestring.c_str(), message);
        fprintf(fp.get(), "***************************************************\n");
        //We might not need this fflush, but adding it in case the 
        //flush as part of the fclose() was required
        fflush(fp.get()); 
    }
}


/// Function to read all settings, listed in main table, from the EPICS database.
///	@param[in] numchans The number of channels listed in the main table.
///	@param[out] times (Optional) An array of at least numchans time_t values to put change times in
void getEpicsSettings()
{
    cdTableP.clear();
    std::transform(cdTable.begin(), cdTable.end(), std::back_inserter( cdTableP),
                   [](const CDS_CD_TABLE& entry) -> CDS_CD_TABLE {
        CDS_CD_TABLE result;
        result.chname = entry.chname;
        result.datatype = entry.datatype;
        result.mask = entry.mask;
        result.initialized = entry.initialized;

        ADDRESS geaddr;
        if(GET_ADDRESS(result.chname,&geaddr)) {
            time_t* change_t = nullptr;
           if(result.datatype == SDF_NUM)
           {
               double dval;
               if (GET_VALUE_NUM(geaddr,&dval,change_t, NULL)) {
                   result.data.chval = (entry.filterswitch ? (int) dval & 0xffff : dval);
               }
           } else {
               GET_VALUE_STR(geaddr,result.data.strval, change_t, NULL);
           }
       }
        return result;
    });
}

// writeTable2File ftype options:
#define SDF_WITH_INIT_FLAG	0
#define SDF_FILE_PARAMS_ONLY	1
#define SDF_FILE_BURT_ONLY	2
/// Common routine for saving table data to BURT files.
template <std::size_t entry_count>
bool writeTable2File(const char *burtdir,
                     const char *filename, 		///< Name of file to write
		    int ftype, 			///< Type of file to write
		    fixed_size_vector<CDS_CD_TABLE, entry_count>& myTable)	///< Table to be written.
{
        int ii;
        FILE *csFile=nullptr;
        embedded::fixed_string<128> filemsg;
	embedded::fixed_string<256> timestring;
	embedded::fixed_string<128> monitorstring;

	embedded::fixed_string<64+2> burtString;
#ifdef CA_SDF
	int precision = 20;
#else
	int precision = 15;
#endif
    // Write out local monitoring table as snap file.
        errno=0;
	burtString = "";
        csFile = fopen(filename,"w");
        if (csFile == nullptr)
        {
            filemsg.printf("ERROR Failed to open %s - %s",filename,strerror(errno));
            logFileEntry(filemsg.c_str());
	    return(false);
        }
	// Write BURT header
	getSdfTime(timestring);
	fprintf(csFile,"%s\n","--- Start BURT header");
	fprintf(csFile,"%s%s\n","Time:      ",timestring.c_str());
	fprintf(csFile,"%s\n","Login ID: controls ()");
	fprintf(csFile,"%s\n","Eff  UID: 1001 ");
	fprintf(csFile,"%s\n","Group ID: 1001 ");
	fprintf(csFile,"%s\n","Keywords:");
	fprintf(csFile,"%s\n","Comments:");
	fprintf(csFile,"%s\n","Type:     Absolute  ");
	fprintf(csFile,"%s%s\n","Directory: ",burtdir);
	fprintf(csFile,"%s\n","Req File: autoBurt.req ");
	fprintf(csFile,"%s\n","--- End BURT header");

	for (const auto& entry:myTable)
	{
		if (entry.filterswitch) {
			if ((entry.mask & ALL_SWSTAT_BITS)== ~0) {
                            monitorstring = "1";
			} else if ((entry.mask & ALL_SWSTAT_BITS) == 0) {
                            monitorstring = "0";
			} else {
                            monitorstring.printf("0x%x", entry.mask);
                        }
		} else {
			if (entry.mask)
                        {
                            monitorstring = "1";
                        } else
                        {
                            monitorstring = "0";
                        }
                }
		switch(ftype)
		{
		   case SDF_WITH_INIT_FLAG:
			if(myTable[ii].datatype == SDF_NUM) {
				fprintf(csFile,"%s %d %.*e %s %d\n",entry.chname.c_str(),1,precision,entry.data.chval,monitorstring.c_str(),entry.initialized);
			} else {
                            BURT::encodeString( entry.data.strval, burtString );
				fprintf(csFile,"%s %d %s %s %d\n",entry.chname.c_str(),1,burtString.c_str(),monitorstring.c_str(),entry.initialized);
			}
			break;
		   case SDF_FILE_PARAMS_ONLY:
			if(entry.initialized) {
				if(entry.datatype == SDF_NUM) {
					fprintf(csFile,"%s %d %.*e %s\n",entry.chname.c_str(),1,precision,entry.data.chval,monitorstring.c_str());
				} else {
                                    BURT::encodeString( entry.data.strval,
                                                        burtString );
					fprintf(csFile,"%s %d %s %s\n",entry.chname.c_str(),1,burtString.c_str(),monitorstring.c_str());
				}
			}
			break;
		   case SDF_FILE_BURT_ONLY:
			if(entry.datatype == SDF_NUM) {
				fprintf(csFile,"%s %d %.*e\n",entry.chname.c_str(),1,precision,entry.data.chval);
			} else {
                            BURT::encodeString( entry.data.strval, burtString );
				fprintf(csFile,"%s %d %s \n",entry.chname.c_str(),1,burtString.c_str());
			}
			break;
		   default:
			break;
		}
	}
	fclose(csFile);
	return true;
}

/// Function to append alarm settings to saved files..
///	@param[in] sdfdir 	Associated model BURT directory.
///	@param[in] sdffile	Name of the file being saved.
///	@param[in] currentload	Base file name of the associated alarms.snap file..
int appendAlarms2File(
		const char *sdfdir,
		const char *sdffile,
		const char *currentload
		)
{
char sdffilename[256];
char alarmfilename[256];
FILE *cdf=nullptr;
FILE *adf=nullptr;
char line[128];
char errMsg[128];
int lderror = 0;
	sprintf(sdffilename,"%s%s.snap",sdfdir,sdffile);
	sprintf(alarmfilename,"%s%s_alarms.snap",sdfdir,currentload);
	// printf("sdffile = %s  \nalarmfile = %s\n",sdffilename,alarmfilename);
	adf = fopen(alarmfilename,"r");
	if(adf == nullptr) return(-1);
		cdf = fopen(sdffilename,"a");
		if(cdf == nullptr) {
			sprintf(errMsg,"New SDF request ERROR: FILE %s DOES NOT EXIST\n",sdffilename);
			logFileEntry(errMsg);
			lderror = 4;
			return(lderror);
		}
		while(fgets(line,sizeof line,adf) != nullptr)
		{
			fprintf(cdf,"%s",line);
		}
		fclose(cdf);
	fclose(adf);
	return(0);
}
// Savetype
#define SAVE_TABLE_AS_SDF	1
#define SAVE_EPICS_AS_SDF	2
// Saveopts
#define SAVE_TIME_NOW		1
#define SAVE_OVERWRITE		2
#define SAVE_AS			3
#define SAVE_BACKUP		4
#define SAVE_OVERWRITE_TABLE	5

/// Routine used to decode and handle BURT save requests.
int savesdffile(int saveType, 		///< Save file format definition.
		int saveOpts, 		///< Save file options.
		const char *sdfdir, 		///< Directory to save file in.
		const char *model, 		///< Name of the model used to build file name.
		const char *currentfile, 	///< Name of file last read (Used if option is to overwrite).
		const char *saveasfile,	///< Name of file to be saved.
		const char *currentload,	///< Name of file, less directory info.
		epics::DBEntry<epics::PVType::String>& save_file_entry,		///< Address of EPICS channel to write save file name.
		epics::DBEntry<epics::PVType::String>& save_time_entry,		///< Address of EPICS channel to write save file time.
		epics::DBEntry<epics::PVType::String>& reload_time_entry)
{
char filename[256];
char ftype[16];
int status;
char filemsg[128];
embedded::fixed_string<256> timestring{};
embedded::fixed_string<256> shortfilename{};

	time_t now = time(nullptr);
	struct tm *mytime  = localtime(&now);

	switch(saveOpts)
	{
		case SAVE_TIME_NOW:
			sprintf(filename,"%s%s_%d%02d%02d_%02d%02d%02d.snap", sdfdir,currentload,
			(mytime->tm_year - 100),  (mytime->tm_mon + 1),  mytime->tm_mday,  mytime->tm_hour,  mytime->tm_min,  mytime->tm_sec);
            shortfilename.printf("%s_%d%02d%02d_%02d%02d%02d", currentload,
			(mytime->tm_year - 100),  (mytime->tm_mon + 1),  mytime->tm_mday,  mytime->tm_hour,  mytime->tm_min,  mytime->tm_sec);
			// printf("File to save is TIME NOW: %s\n",filename);
			break;
		case SAVE_OVERWRITE:
			sprintf(filename,"%s",currentfile);
			shortfilename.printf("%s",currentload);
			// printf("File to save is OVERWRITE: %s\n",filename);
			break;
		case SAVE_BACKUP:
			sprintf(filename,"%s%s_%d%02d%02d_%02d%02d%02d.snap",sdfdir,currentload,
			(mytime->tm_year - 100),  (mytime->tm_mon + 1),  mytime->tm_mday,  mytime->tm_hour,  mytime->tm_min,  mytime->tm_sec);
			shortfilename.printf("%s",currentload);
			// printf("File to save is BACKUP: %s\n",filename);
			break;
		case SAVE_OVERWRITE_TABLE:
			sprintf(filename,"%s%s.snap",sdfdir,currentload);
			shortfilename.printf("%s",currentload);
			// printf("File to save is BACKUP OVERWRITE: %s\n",filename);
			break;
		case SAVE_AS:
			sprintf(filename,"%s%s.snap",sdfdir,saveasfile);
			shortfilename.printf("%s",saveasfile);
			// printf("File to save is SAVE_AS: %s\n",filename);
			break;

		default:
			return(-1);
	}
	// SAVE THE DATA TO FILE **********************************************************************
	switch(saveType)
	{
		case SAVE_TABLE_AS_SDF:
			// printf("Save table as sdf\n");
			if(!writeTable2File(sdfdir,filename,SDF_FILE_PARAMS_ONLY,cdTable)) {
                            sprintf(filemsg,"FAILED FILE SAVE %s",filename);
			    logFileEntry(filemsg);
                            return(-2);
			}
			status = appendAlarms2File(sdfdir,shortfilename.c_str(),currentload);
			if(status != 0) {
                            sprintf(filemsg,"FAILED To Append Alarms -  %s",currentload);
			    logFileEntry(filemsg);
                            return(-2);
			}
		        sprintf(filemsg,"Save TABLE as SDF: %s",filename);
                        break;
		case SAVE_EPICS_AS_SDF:
			// printf("Save epics as sdf\n");
			getEpicsSettings();
			if(!writeTable2File(sdfdir,filename,SDF_FILE_PARAMS_ONLY,cdTableP)) {
                            sprintf(filemsg,"FAILED EPICS SAVE %s",filename);
			    logFileEntry(filemsg);
                            return(-2);
			}
			sprintf(filemsg,"Save EPICS as SDF: %s",filename);
                        break;
		default:
			sprintf(filemsg,"BAD SAVE REQUEST %s",filename);
			logFileEntry(filemsg);
			return(-1);
	}
	logFileEntry(filemsg);
	getSdfTime(timestring);
	// printf(" Time of save = %s\n",timestring);
    save_file_entry.set(shortfilename);
	save_time_entry.set(timestring);
	reload_time_entry.set(timestring);
	return(0);

	// I can type above your caret
}


#if 0
// Routine to change ASG, thereby changing record locking
// Left here for possible future use to lock EPICS settings.
void resetASG(char *name, int lock) {
    
    DBENTRY  *pdbentry;
    dbCommon *precord;
    long status;
    char t[256];

    pdbentry = dbAllocEntry(*iocshPpdbbase);
    status = dbFindRecord(pdbentry, name);
    precord = pdbentry->precnode->precord;
    if(lock) sprintf(t, "DEFAULT");
    else sprintf(t, "MANUAL");
    status = asChangeGroup((ASMEMBERPVT *)&precord->asp,t);
    // printf("ASG = %s\n",t);
    dbFreeEntry(pdbentry);
}
#endif

/// Routine used to create local tables for reporting uninitialize and not monitored channels on request.
int createSortTableEntries(int numEntries,int wcval,const char *wcstring,int *noInit,time_t *times)
{
int ii,jj;
int notMon = 0;
int ret = 0;
int lna = 0;	// line a - uninit chan list index
int lnb = 0;	// line b - non mon chan list index
#ifdef CA_SDF
int lnc = 0;	// line c - disconnected chan list index
#endif
fixed_string<64> tmpname;
time_t mtime=0;
long nvals = 1;
fixed_string<64> liveset;
double liveval = 0.0;

	chNotInit = 0;
	chNotMon = 0;
#ifdef CA_SDF
	chDisconnected = 0;
#endif

	D("In createSortTableEntries filter:'%s'\n", (wcval ? wcstring : "None"));
	// Fill uninit and unmon tables.
//	printf("sort table = %d string %s\n",wcval,wcstring);
	for(ii=0;ii<fmNum;ii++) {
		if(!filterTable[ii].init) chNotInit += 1;
		if(filterTable[ii].init && !(filterTable[ii].mask & ALL_SWSTAT_BITS)) chNotMon += 1;
		if(wcval  && (ret = strstr(filterTable[ii].fname.c_str(),wcstring) == NULL)) {
			continue;
		}
		tmpname = filterTable[ii].fname;
		if(!filterTable[ii].init) {
			uninitChans[lna].chname.printf("%s",tmpname);
			uninitChans[lna].burtset.clear();
			uninitChans[lna].liveval = 0.0;
			uninitChans[lna].sw[0] = (int)cdTableP[filterTable[ii].sw[0]].data.chval;
			uninitChans[lna].sw[1] = (int)cdTableP[filterTable[ii].sw[1]].data.chval;
			uninitChans[lna].sigNum = filterTable[ii].sw[0] + (filterTable[ii].sw[1] * SDF_MAX_TSIZE);
			uninitChans[lna].filtNum = ii;
			createSwitchText(&(filterTable[ii]),uninitChans[lna].liveset);
			lna ++;
		}
		if(filterTable[ii].init && !(filterTable[ii].mask & ALL_SWSTAT_BITS)) {

		    unMonChans[lnb].chname = tmpname;
			unMonChans[lnb].liveval = 0.0;
			unMonChans[lnb].sw[0] = (int)cdTableP[filterTable[ii].sw[0]].data.chval;
			unMonChans[lnb].sw[1] = (int)cdTableP[filterTable[ii].sw[1]].data.chval;
			unMonChans[lnb].sigNum = filterTable[ii].sw[0] + (filterTable[ii].sw[1] * SDF_MAX_TSIZE);
			unMonChans[lnb].filtNum = ii;
			filtStrBitConvert(1,filterTable[ii].swreq, unMonChans[lnb].burtset);
			createSwitchText(&(filterTable[ii]),unMonChans[lnb].liveset);
			lnb ++;
		}
	}
	for(jj=0;jj<numEntries;jj++)
	{
#ifdef VERBOSE_DEBUG
		if (D_NOW) {
			if (strcmp(cdTable[jj].chname.c_str(), TEST_CHAN) == 0) {
				D("%s: init: %d mask: %d filter: %d\n", TEST_CHAN, cdTable[jj].initialized, cdTable[jj].mask, cdTable[jj].filterswitch);	
			}
		}
#endif
		if(cdTable[jj].filterswitch) continue;
		if(!cdTable[jj].initialized) chNotInit += 1;
		if(cdTable[jj].initialized && !cdTable[jj].mask) chNotMon += 1;
#ifdef CA_SDF
		if(!cdTable[jj].connected) chDisconnected += 1;
#endif
		if(wcval  && (ret = strstr(cdTable[jj].chname.c_str(),wcstring) == NULL)) {
			continue;
		}
		// Uninitialized channels
		if(!cdTable[jj].initialized && !cdTable[jj].filterswitch) {
			// printf("Chan %s not init %d %d %d\n",cdTable[jj].chname.c_str(),cdTable[jj].initialized,jj,numEntries);
			if(lna < SDF_ERR_TSIZE) {
				uninitChans[lna].chname = cdTable[jj].chname;
				if(cdTable[jj].datatype == SDF_NUM) {
					liveval = cdTableP[jj].data.chval;
					liveset.printf("%.10lf", liveval);

				} else {
					liveval = 0.0;
					liveset = cdTableP[jj].data.strval;
				}
				uninitChans[lna].liveset = liveset;
				uninitChans[lna].liveval = liveval;
				uninitChans[lna].sigNum = jj;
				uninitChans[lna].filtNum = -1;

    		    unMonChans[lna].timeset = (times ? ctime(&times[jj]) : " ");

				uninitChans[lna].timeset = " ";
				uninitChans[lna].diff = " ";
				lna++;
			}
		}
		// Unmonitored channels
		if(cdTable[jj].initialized && !cdTable[jj].mask && !cdTable[jj].filterswitch) {
			if(lnb < SDF_ERR_TSIZE) {
				unMonChans[lnb].chname = cdTable[jj].chname;
				if(cdTable[jj].datatype == SDF_NUM)
				{
					unMonChans[lnb].burtset.printf("%.10lf",cdTable[jj].data.chval);
					unMonChans[lnb].liveset.printf("%.10lf",cdTableP[jj].data.chval);
					unMonChans[lnb].liveval = cdTableP[jj].data.chval;
				} else {
					unMonChans[lnb].burtset = cdTable[jj].data.strval;
					unMonChans[lnb].liveset = cdTableP[jj].data.strval;
					unMonChans[lnb].liveval = 0.0;
				}

                unMonChans[lnb].sigNum = jj;
				unMonChans[lnb].filtNum = -1;

                unMonChans[lnb].timeset = (times ? ctime(&times[jj]) : " ");
                unMonChans[lnb].diff = " ";

				lnb ++;
			}
		}
#ifdef CA_SDF
		if(!cdTable[jj].connected && !cdTable[jj].filterswitch) {
			if (lnc < SDF_ERR_TSIZE) {
				disconnectChans[lnc].chname = cdTable[jj].chname;
				if(cdTable[jj].datatype == SDF_NUM) {
					liveval = cdTableP[jj].data.chval;
					liveset.printf("%.10lf", liveval);

				} else {
					liveval = 0.0;
					liveset = cdTableP[jj].data.strval;
				}
				disconnectChans[lnc].liveset = liveset;
				disconnectChans[lnc].liveval = liveval;
                disconnectChans[lnc].sigNum = jj;
				disconnectChans[lnc].filtNum = -1;
				disconnectChans[lnc].timeset = " ";
				disconnectChans[lnc].diff = " ";
				lnc++;
			}
		}
#endif
	}
	// Clear out the uninit tables.
	for(jj=lna;jj<(lna + 50);jj++)
	{
		uninitChans[jj].chname = " ";
		uninitChans[jj].burtset = " ";
		uninitChans[jj].liveset = " ";
		uninitChans[jj].liveval = 0.0;
		uninitChans[jj].timeset = " ";
		uninitChans[jj].diff = " ";
        uninitChans[jj].sigNum = 0;         // is this the right value, should it be -1?
		uninitChans[jj].filtNum = -1;
	}
	// Clear out the unmon tables.
	for(jj=lnb;jj<(lnb + 50);jj++)
	{
		unMonChans[jj].chname = " ";
		unMonChans[jj].burtset = " ";
		unMonChans[jj].liveset = " ";
		unMonChans[jj].liveval = 0.0;
		unMonChans[jj].timeset = " ";
		unMonChans[jj].diff = " ";
        unMonChans[jj].sigNum = 0;
		unMonChans[jj].filtNum = -1;
	}
#ifdef CA_SDF
	// Clear out the disconnected tables.
	for(jj=lnc;jj<(lnc + 50);jj++)
	{
		disconnectChans[jj].chname = " ";
		disconnectChans[jj].burtset = " ";
		disconnectChans[jj].liveset = " ";
		disconnectChans[jj].liveval = 0.0;
		disconnectChans[jj].timeset = " ";
		disconnectChans[jj].diff = " ";
        disconnectChans[jj].sigNum = 0;
		disconnectChans[jj].filtNum = -1;
	}
#endif
	*noInit = lna;
	return(lnb);
}

/// Common routine to load monitoring tables into EPICS channels for MEDM screen.
template <std::size_t entry_count>
int reportSetErrors(char *pref,			///< Channel name prefix from EPICS environment. 
		     fixed_size_vector<SET_ERR_TABLE , entry_count>& setErrTable,	///< Which table to report to EPICS channels.
		     int page,                      ///< Which page of 40 to display.
             int linkInFilters)				///< Should the SDF_FM_LINE values be set.
{

int ii;
dbAddr saddr;
dbAddr baddr;
dbAddr maddr;
dbAddr taddr;
dbAddr daddr;
dbAddr laddr;
dbAddr faddr;
char s[64];
char s1[64];
char s2[64];
char s3[64];
char s4[64];
char sl[64];
char sf[64];
//char stmp[128];
long status = 0;
char clearString[62] = "                       ";
int flength = 62;
int rc = 0;
int myindex = 0;
int numDisp = 0;
int lineNum = 0;
int mypage = 0;
int lineCtr = 0;
int zero = 0;
int minusOne = -1;


	// Get the page number to display
	mypage = page;
	// Calculate start index to the diff table.
	myindex = page *  SDF_ERR_DSIZE;
	int numEntries = setErrTable.size();
	if(myindex == numEntries && numEntries > 0) {
		mypage --;
		myindex = mypage *  SDF_ERR_DSIZE;
	}
	// If index is > number of entries in the table, then page back.
    if(myindex > numEntries) {
		mypage = numEntries / SDF_ERR_DSIZE;
		myindex = mypage *  SDF_ERR_DSIZE;
    }
	// Set the stop index to the diff table.
	rc = myindex + SDF_ERR_DSIZE;
	// If stop index beyond last diff table entry, set it to last entry.
    if(rc > numEntries) rc = numEntries;
	numDisp = rc - myindex;

	// Fill in table entries.
	for(ii=myindex;ii<rc;ii++)
	{
		sprintf(s, "%s_%s_STAT%d", pref,"SDF_SP", lineNum);
		status = dbNameToAddr(s,&saddr);
		//sprintf(stmp, "%s%s", (setErrTable[ii].filtNum >= 0 ? "* " : ""), setErrTable[ii].chname.c_str());
		status = dbPutField(&saddr,DBR_UCHAR,setErrTable[ii].chname.c_str(),flength);

		sprintf(s1, "%s_%s_STAT%d_BURT", pref,"SDF_SP", lineNum);
		status = dbNameToAddr(s1,&baddr);
		status = dbPutField(&baddr,DBR_UCHAR,&setErrTable[ii].burtset,flength);

		sprintf(s2, "%s_%s_STAT%d_LIVE", pref,"SDF_SP", lineNum);
		status = dbNameToAddr(s2,&maddr);
		status = dbPutField(&maddr,DBR_UCHAR,&setErrTable[ii].liveset,flength);

		sprintf(s3, "%s_%s_STAT%d_TIME", pref,"SDF_SP", lineNum);
		status = dbNameToAddr(s3,&taddr);
		status = dbPutField(&taddr,DBR_UCHAR,&setErrTable[ii].timeset,flength);

		sprintf(s4, "%s_%s_STAT%d_DIFF", pref,"SDF_SP", lineNum);
		status = dbNameToAddr(s4,&daddr);
		status = dbPutField(&daddr,DBR_UCHAR,&setErrTable[ii].diff,flength);

		sprintf(sl, "%s_SDF_BITS_%d", pref, lineNum);
                status = dbNameToAddr(sl,&daddr);
                status = dbPutField(&daddr,DBR_ULONG,&setErrTable[ii].chFlag,1);


		sprintf(sf, "%s_SDF_FM_LINE_%d", pref, lineNum);
		status = dbNameToAddr(sf,&faddr);
		status = dbPutField(&faddr,DBR_LONG,(linkInFilters ? &setErrTable[ii].filtNum : &minusOne),1);

		sprintf(sl, "%s_SDF_LINE_%d", pref, lineNum);
		lineNum ++;
		lineCtr = ii + 1;;
                status = dbNameToAddr(sl,&laddr);
                status = dbPutField(&laddr,DBR_LONG,&lineCtr,1);
	}

	// Clear out empty table entries.
	for(ii=numDisp;ii<SDF_ERR_DSIZE;ii++)
	{
		sprintf(s, "%s_%s_STAT%d", pref,"SDF_SP", ii);
		status = dbNameToAddr(s,&saddr);
		status = dbPutField(&saddr,DBR_UCHAR,clearString,flength);

		sprintf(s1, "%s_%s_STAT%d_BURT", pref,"SDF_SP", ii);
		status = dbNameToAddr(s1,&baddr);
		status = dbPutField(&baddr,DBR_UCHAR,clearString,flength);

		sprintf(s2, "%s_%s_STAT%d_LIVE", pref,"SDF_SP", ii);
		status = dbNameToAddr(s2,&maddr);
		status = dbPutField(&maddr,DBR_UCHAR,clearString,flength);


		sprintf(s3, "%s_%s_STAT%d_TIME", pref,"SDF_SP", ii);
		status = dbNameToAddr(s3,&taddr);
		status = dbPutField(&taddr,DBR_UCHAR,clearString,flength);

		sprintf(s4, "%s_%s_STAT%d_DIFF", pref,"SDF_SP", ii);
		status = dbNameToAddr(s4,&daddr);
		status = dbPutField(&daddr,DBR_UCHAR,clearString,flength);

		sprintf(sl, "%s_SDF_BITS_%d", pref, ii);
                status = dbNameToAddr(sl,&daddr);
                status = dbPutField(&daddr,DBR_ULONG,&zero,1);

		sprintf(sf, "%s_SDF_FM_LINE_%d", pref, ii);
		status = dbNameToAddr(sf,&faddr);
		status = dbPutField(&faddr,DBR_LONG,&minusOne,1);

		lineCtr ++;
		sprintf(sl, "%s_SDF_LINE_%d", pref, ii);
		status = dbNameToAddr(sl,&laddr);
		status = dbPutField(&laddr,DBR_LONG,&lineCtr,1);
		
	}
	// Return the number of the display page.
	return(mypage);
}

// This function checks that present setpoints match those set by BURT if the channel is marked by a mask
// setting in the BURT file indicating that this channel should be monitored. .
// If settings don't match, then this is reported to the Guardian records in the form:
//	- Signal Name
//	- BURT Setting
//	- Present Value
//	- Time the present setting was applied.
/// Setpoint monitoring routine.
template <std::size_t entry_count>
int spChecker(int monitorAll, fixed_size_vector<SET_ERR_TABLE, entry_count>& setErrTable,int wcVal, const char *wcstring, int listAll, int *totalDiffs)
{
   	int errCntr = 0;
	ADDRESS paddr;
	long status=0;
	//int ii;
	double rval;
	fixed_string<128> sval;
	time_t mtime;
	fixed_string<256> localtimestring;
	int localErr = 0;
	fixed_string<64> liveset;
	fixed_string<64> burtset;
	fixed_string<64> diffB2L;
	fixed_string<64> swName;
	double sdfdiff = 0.0;
	double liveval = 0.0;
	int filtDiffs=0;

#ifdef VERBOSE_DEBUG
/*
	char *__table_name = "setErrTable or unknown";
	if (setErrTable == unknownChans) __table_name = "unknownChans";
	else if (setErrTable == uninitChans) __table_name == "uninitChans";
	else if (setErrTable == unMonChans) __table_name == "unMonChans";
	else if (setErrTable == readErrTable) __table_name == "readErrTable";
	else if (setErrTable == cdTableList) __table_name == "cdTableList";
	D("In spChecker cdTable -> '%s' monAll(%d) listAll(%d)\n", __table_name, monitorAll, listAll);
*/
#endif
	// Check filter switch settings first
        setErrTable.clear();
	    errCntr = checkFilterSwitches(setErrTable,monitorAll,listAll,wcVal,wcstring,&filtDiffs);
	    *totalDiffs = filtDiffs;

	    int index = -1;
	    for (auto& curSP: cdTable)
        {
	        ++index;
			if((errCntr < SDF_ERR_TSIZE) && 		// Within table max size
			  ((curSP.mask != 0) || (monitorAll)) && 	// Channel is to be monitored
			   curSP.initialized && 			// Channel was set in BURT
			   curSP.filterswitch == 0 ||		// Not a filter switch channel
			   (listAll && curSP.filterswitch == 0))
			{
				localErr = 0;
				mtime = 0;
				// Find address of channel
				GET_ADDRESS(curSP.chname,&paddr);
				// If this is a digital data type, then get as double.
				if(curSP.datatype == SDF_NUM)
				{
					GET_VALUE_NUM(paddr,&rval,&mtime, NULL); //&(curSP.connected));
					if(curSP.data.chval != rval || listAll)
					{
						sdfdiff = fabs(curSP.data.chval - rval);
						burtset.printf("%.10lf",curSP.data.chval);
						liveset.printf("%.10lf",rval);
						liveval = rval;
						diffB2L.printf("%.8le",sdfdiff);
						localErr = 1;
					}
				// If this is a string type, then get as string.
				} else {
                    updateStrVarSetPoint(index);
					GET_VALUE_STR(paddr,sval,&mtime, NULL); //&(curSP.connected));
					if(sval != curSP.data.strval || listAll)
					{
						burtset = curSP.data.strval;
						liveset = sval;
						liveval = 0.0;
						diffB2L.printf("%s","                                   ");
						localErr = 1;
					}
				}
				if(localErr) *totalDiffs += 1;
				if(localErr && wcVal  && (strstr(curSP.chname.c_str(),wcstring) == NULL))
					localErr = 0;
				// If a diff was found, then write info the EPICS setpoint diff table.
				if(localErr)
				{
				    setErrTable.emplace_back();
				    auto& curErr = setErrTable.back();
					curErr.chname = curSP.chname;

					curErr.burtset = burtset;

					curErr.liveset = liveset;
					curErr.liveval = liveval;
					curErr.diff = diffB2L;
					curErr.sigNum = index;
					curErr.filtNum = -1;

					localtimestring = ctime(&mtime);
					curErr.timeset = localtimestring;
					if(curSP.mask) curErr.chFlag |= 1;
					else curErr.chFlag &= ~(1);
					errCntr ++;
				}
			}
		}
	return(errCntr);
}

/// Modify the global cdTable as directed by applying changes from modTable where the channel is marked as apply or monitor
///
/// @param numEntries[in] The number of entries in modTable
/// @param modTable[in] An array of SET_ERR_TABLE entries that hold the current views state to be merged with cdTable
///
/// @remark modifies cdTable
int modifyTable(table_range modTable)
{
    int jj;
    int fmIndex = -1;
    unsigned int sn,sn1;
    int found = 0;

	for (const auto& entry:modTable)
	{
		// if accept or monitor is set
		if(entry.chFlag > 3)
		{
			found = 0;
			for(jj=0;jj<chNum;jj++)
			{
				if (cdTable[jj].chname == entry.chname) {
					if ( CHFLAG_ACCEPT_BIT(entry.chFlag) ) {
						if(cdTable[jj].datatype == SDF_NUM) cdTable[jj].data.chval = entry.liveval;/* atof(entry.liveset);*/
						else cdTable[jj].data.strval = entry.liveset;
						cdTable[jj].initialized = 1;
						found = 1;
						fmIndex = cdTable[jj].filterNum;
					}
					if(CHFLAG_MONITOR_BIT(entry.chFlag)) {
						fmIndex = cdTable[jj].filterNum;
						if (fmIndex >= 0 && fmIndex < SDF_MAX_FMSIZE) {
							// filter module use the manualy modified state, cannot just toggle
							cdTable[jj].mask = filterMasks[fmIndex];
						} else {
							// regular channel just toggle on/off state
							cdTable[jj].mask = (cdTable[jj].mask ? 0 : ~0);
						}
						found = 1;
					}
				}
			}
			if(entry.filtNum >= 0 && !found) {
				fmIndex = entry.filtNum;
				// printf("This is a filter from diffs = %s\n",filterTable[fmIndex].fname);
				filterTable[fmIndex].newSet = 1;
				sn = entry.sigNum;
				sn1 = sn / SDF_MAX_TSIZE;
				sn %= SDF_MAX_TSIZE;
				if(CHFLAG_ACCEPT_BIT(entry.chFlag)) {
					cdTable[sn].data.chval = entry.sw[0];
					cdTable[sn].initialized = 1;
					cdTable[sn1].data.chval = entry.sw[1];
					cdTable[sn1].initialized = 1;
					filterTable[fmIndex].init = 1;
				}
				if(CHFLAG_MONITOR_BIT(entry.chFlag)) {
					filterTable[fmIndex].mask = filterMasks[fmIndex];
				 	cdTable[sn].mask = filterMasks[fmIndex];
				 	cdTable[sn1].mask = filterMasks[fmIndex];
				}
			}
		}
	}
	newfilterstats();
	return(0);
}

template <std::size_t entry_count>
int resetSelectedValues(fixed_size_vector<SET_ERR_TABLE, entry_count>& modTable)
{
long status;
int sn;
int sn1 = 0;
ADDRESS saddr;

	for(const auto& entry:modTable)
	{
		if (entry.chFlag & 2)
		{
			sn = entry.sigNum;
			if(sn > SDF_MAX_TSIZE) {
				sn1 = sn / SDF_MAX_TSIZE;
				sn %= SDF_MAX_TSIZE;
			}
			GET_ADDRESS(cdTable[sn].chname,&saddr);
			if (cdTable[sn].datatype == SDF_NUM) PUT_VALUE(saddr,SDF_NUM,&(cdTable[sn].data.chval));
			else PUT_VALUE(saddr,SDF_STR,cdTable[sn].data.strval.c_str());;
			if(sn1) {
				GET_ADDRESS(cdTable[sn1].chname,&saddr);
				PUT_VALUE(saddr,SDF_NUM,&(cdTable[sn1].data.chval));
			}
		}
	}
	return(0);
}

/// This function sets filter module request fields to aid in decoding errant filter module switch settings.
void newfilterstats() {
	ADDRESS paddr;
	long status;

	FILE *log=nullptr;
	fixed_string<128> chname;
	unsigned int mask = 0x1ffff;
	int tmpreq;
	int counter = 0;
	int rsw1,rsw2;
	unsigned int tmpL = 0;

	printf("In newfilterstats\n");
	for(auto& filterEntry:filterTable) {
		if(filterEntry.newSet) {
			counter ++;
			filterEntry.newSet = 0;
			filterEntry.init = 1;
			rsw1 = filterEntry.sw[0];
			rsw2 = filterEntry.sw[1];
			filterEntry.mask =  cdTable[rsw1].mask | cdTable[rsw2].mask;;
			cdTable[rsw1].mask = filterEntry.mask;
			cdTable[rsw2].mask = filterEntry.mask;
			tmpreq =  ((unsigned int)cdTable[rsw1].data.chval & 0xffff) + 
				(((unsigned int)cdTable[rsw2].data.chval & 0xffff) << 16);
			filterEntry.swreq = filtCtrlBitConvert(tmpreq);
			// Find address of channel
			chname  = filterEntry.fname;
			chname += "SWREQ";
			if(GET_ADDRESS(chname,&paddr)) {
				tmpL = (unsigned int)filterEntry.swreq;
				PUT_VALUE_INT(paddr,&tmpL);
			}
			// Find address of channel
			chname = filterEntry.fname;
			chname += "SWMASK";
			if(GET_ADDRESS(chname,&paddr)) {
				PUT_VALUE_INT(paddr,&mask);
			}
			// printf("New filter %d %s = 0x%x\t0x%x\t0x%x\n",ii,filterTable[ii].fname,filterTable[ii].swreq,filterTable[ii].sw[0],filterTable[ii].sw[1]);
		}
	}
	printf("Set filter masks for %d filter modules\n",counter);
}

/// This function writes BURT settings to EPICS records.
template <std::size_t entry_count>
int writeEpicsDb(fixed_size_vector<CDS_CD_TABLE, entry_count>& myTable,	///< Table with data to be written.
	     	 int command) 		///< Write request.
{
	ADDRESS paddr;
	long status;
	int ii;

	switch (command)
	{
		case SDF_LOAD_DB_ONLY:
		case SDF_LOAD_PARTIAL:

		    for (const auto& entry:myTable)
		    {
                        // Find address of channel
                        if (GET_ADDRESS(entry.chname,&paddr))
                        {
                            if (entry.datatype == SDF_NUM)
                            {
                                PUT_VALUE(paddr,SDF_NUM,&(entry.data.chval));
                            } else {
                                PUT_VALUE(paddr,SDF_STR,entry.data.strval.c_str());
                            }
                        }
                        else {				// Write errors to chan not found table.
                            printf("CNF for %s = %d\n",entry.chname.c_str(),status);
                        }
                    }
                    // Set the SDF_FILE_LOADED environment var to 1,
                    // signalling to the epipcs sequencer to
                    // trigger a BURT_RESTORE

                    {
                        set_sdf_file_loaded(1);
                    }
                    break;
		case SDF_READ_ONLY:
			// If request was only to re-read the BURT file, then don't want to apply new settings.
			// This is typically the case where only mask fields were changed in the BURT file to
			// apply setpoint monitoring of channels.
			return(0);
			break;
		case SDF_RESET:
			// Only want to set those channels marked by a mask back to their original BURT setting.
			for (const auto& entry: myTable)
			{
                // FIXME: check does this make sense w/ a bitmask in mask?
                // can filter modules get here?  It seems that they will, so would we need
                // to mask the value we set ?
                if(entry.mask) {
                    //Find address of channel
                    if (GET_ADDRESS(entry.chname,&paddr))
                    {
                        if (entry.datatype == SDF_NUM)
                        {
                            PUT_VALUE(paddr,SDF_NUM,&(entry.data.chval));
                        } else {
                            PUT_VALUE(paddr,SDF_STR,entry.data.strval.c_str());
                        }
                    }
                }
            }
			break;
		default:
			printf("writeEpicsDb setting routine got unknown request \n");
			return(-1);
			break;
	}
	return(0);
}



/// Function to read BURT files and load data into local tables.
int readConfig( const char *pref,		///< EPICS channel prefix from EPICS environment.
		const char *sdfile, 		///< Name of the file to read.
		int command,		///< Read file request type.
		const char *alarmfile)
{
	FILE *cdf=nullptr;
	FILE *adf=nullptr;
	char c=0;
	int ii=0;
	int lock=0;
	char ls[6][64];
	ADDRESS paddr;
	dbAddr reloadDbAddr;
	long status=0;
	int lderror = 0;
	int ropts = 0;
	int nvals = 1;
	int starttime=0,totaltime=0;
	embedded::fixed_string<256> timestring{};
	char line[128];
	char *fs=nullptr;
	char ifo[4];
	double tmpreq = 0;
	char fname[128];
	int fmatch = 0;
	int fmIndex = -1;
	char errMsg[128];
	int argcount = 0;
	int isalarm = 0;
	int lineCnt = 0;

    //Header vars
    int in_header = 0;
    const static char* header_start = "--- Start";
    const static char* header_end = "--- End";

	line[0]='\0';
	ifo[0]='\0';
	fname[0]=errMsg[0]='\0';
	for (ii = 0; ii < 6; ii++) ls[ii][0]='\0';
	rderror = 0;

	clock_gettime(CLOCK_REALTIME,&t);
	starttime = t.tv_nsec;

	getSdfTime(timestring);

	if(command == SDF_RESET) {
		lderror = writeEpicsDb(cdTable,command);
	} else {
		printf("PARTIAL %s\n",sdfile);
		cdf = fopen(sdfile,"r");
		if(cdf == nullptr) {
			snprintf(errMsg, 64, "New SDF request ERROR: FILE %s DOES NOT EXIST", sdfile);
			logFileEntry(errMsg);
			lderror = 4;
			perror(errMsg);
			fprintf(stderr, "sdfile : %s\n", sdfile);
			return(lderror);
		}
		adf = fopen(alarmfile,"w");
		if(adf == nullptr)
		{
			snprintf(errMsg, 64, "New SDF request ERROR: failed open alarm file");
			logFileEntry(errMsg);
			lderror = 4;
			perror(errMsg);
			fprintf(stderr, "alarmfile : %s\n", alarmfile);
			return(lderror);
		}
		chNumP = 0;
		alarmCnt = 0;
		strncpy(ifo,pref,3);
		chNotFound = 0;
		while(fgets(line,sizeof line,cdf) != nullptr)
		{

			if (in_header)
			{
				if (strstr(line, header_end) == line)
				{
					in_header = 0;
				}
				continue;
			}
			else if (strstr( line, header_start) == line)
			{
				in_header = 1;
				continue;
			}


			isalarm = 0;
			lineCnt ++;
                        BURT::parsed_line words{};
			argcount = BURT::parseLine(line, words);
                        // Only 3 = no monit flag
                        // >=4 count be monit flag or string with quotes
                        if ( argcount < 3 )
                        {
                            continue;
                        }
                        if ( argcount < 4 )
                        {
                            words.push_back(BURT::parsed_word("0"));
                        }
                        const char* s1 = words[0].c_str();
                        const char* s2 = words[1].c_str();
                        const char* s3 = words[2].c_str();
                        const char* s4 = words[3].c_str();
			// If 1st three chars match IFO ie checking this this line is not BURT header or channel marked RO
			if(
			// Don't allow load of SWSTAT or SWMASK, which are set by this program.
				strstr(s1,"_SWMASK") == nullptr &&
				strstr(s1,"_SDF_NAME") == nullptr &&
				strstr(s1,"_SWREQ") == nullptr &&
				argcount > 2)
			{
				// Load channel name into local table.
				cdTableP[chNumP].chname = s1;
				// Check if s4 (monitor or not) is set (0/1). If doesn/'t exist in file, set to zero in local table.
				if(argcount > 3 && isdigit(s4[0])) {
					// printf("%s %s %s %s\n",s1,s2,s3,s4);
					// 0x... -> bit mask
					// 0|1 -> 0|0xfffffffff
					if (s4[1] == 'x') {
						if (sscanf(s4, "0x%xd", &(cdTableP[chNumP].mask)) <= 0)
							cdTableP[chNumP].mask = 0;
					} else {
						cdTableP[chNumP].mask = atoi(s4);
						if((cdTableP[chNumP].mask < 0) || (cdTableP[chNumP].mask > 1))	
							cdTableP[chNumP].mask = 0;
						if (cdTableP[chNumP].mask > 0) cdTableP[chNumP].mask = ~0;
					}
					// printf("mask: %d %s\n", cdTableP[chNumP].mask, cdTableP[chNumP].chname.c_str());
				} else {
					cdTableP[chNumP].mask = 0;
				}
				// Find channel in full list and replace setting info
				fmatch = 0;
				// We can set alarm values, but do not put them in cdTable
				if( isAlarmChannel(cdTableP[chNumP].chname) )
				{
					alarmCnt ++;
					isalarm = 1;
					if(isdigit(s3[0])) {
						cdTableP[chNumP].datatype = SDF_NUM;
						cdTableP[chNumP].data.chval = atof(s3);
						// printf("Alarm set - %s = %f\n",cdTableP[chNumP].chname.c_str(),cdTableP[chNumP].data.chval);
					} else {
						cdTableP[chNumP].datatype = SDF_STR;
						cdTableP[chNumP].data.strval = s3;
						// printf("Alarm set - %s = %s\n",cdTableP[chNumP].chname.c_str(),cdTableP[chNumP].data.strval.c_str());
					}
					fprintf(adf,"%s %s %s\n",s1,s2,s3);
				} 
				if(!isalarm)
				{
				   // Add settings to local table.
				   for(ii=0;ii<chNum;ii++)
				   {
					if(cdTable[ii].chname == cdTableP[chNumP].chname)
					{
					    saveStrSetPoint(ii, s3);
						fmatch = 1;
						if(cdTable[ii].datatype == SDF_STR || (!isdigit(s3[0]) && strncmp(s3,"-",1) != 0))
						{
							// s3[strlen(s3) - 1] = 0;
							cdTableP[chNumP].datatype = SDF_STR;
							cdTableP[chNumP].data.strval = s3;
							if(command != SDF_LOAD_DB_ONLY)
							{
								cdTable[ii].data.strval = s3;
							}
						} else {
							cdTableP[chNumP].datatype = SDF_NUM;
							cdTableP[chNumP].data.chval = atof(s3);
							if(cdTable[ii].filterswitch) {
								if(cdTableP[chNumP].data.chval > 0xffff) {
									readErrTable[rderror].chname = cdTable[ii].chname;
									readErrTable[rderror].burtset.printf("0x%x", (int)cdTableP[chNumP].data.chval);
									readErrTable[rderror].liveset = "OVERRANGE";
									readErrTable[rderror].liveval = 0.0;
									readErrTable[rderror].diff = "MAX VAL = 0xffff";
									readErrTable[rderror].timeset = timestring;
									printf("Read error --- %s\n", cdTable[ii].chname.c_str());
									rderror ++;
								}
								cdTableP[chNumP].data.chval = (int) cdTableP[chNumP].data.chval & 0xffff;
							}
							if(command != SDF_LOAD_DB_ONLY)
								cdTable[ii].data.chval = cdTableP[chNumP].data.chval;
						}
						if(command != SDF_LOAD_DB_ONLY) {
							//if(cdTableP[chNumP].mask != -1)
								cdTable[ii].mask = cdTableP[chNumP].mask;
							cdTable[ii].initialized = 1;
						}
					}
				   }
				   // if(!fmatch) printf("NEW channel not found %s %d\n",cdTableP[chNumP].chname.c_str(),chNumP);
				}
				// The following loads info into the filter module table if a FM switch
				fmIndex = -1;
				if(((strstr(s1,"_SW1S") != nullptr) && (strstr(s1,"_SW1S.") == nullptr)) ||
					((strstr(s1,"_SW2S") != nullptr) && (strstr(s1,"_SW2S.") == nullptr)))
				{
				   	bzero(fname,sizeof(fname));
					strncpy(fname,s1,(strlen(s1)-4));
				   	for(ii=0;ii<fmNum;ii++)
				   	{
						if(filterTable[ii].fname == fname)
						{
							fmIndex = ii;
							filterTable[fmIndex].newSet = 1;
							break;
						}
					}
				}
				if(fmatch) {
					chNumP ++;
				} else {
					// printf("CNF for %s \n",cdTableP[chNumP].chname.c_str());
					if(chNotFound < SDF_ERR_TSIZE) {
						unknownChans[chNotFound].chname = cdTableP[chNumP].chname;
						if(GET_ADDRESS(cdTableP[chNumP].chname,&paddr)) {
							unknownChans[chNotFound].liveset  = "RO Channel ";
						} else {
							unknownChans[chNotFound].liveset = "  ";
						}
						unknownChans[chNotFound].liveval = 0.0;
						unknownChans[chNotFound].timeset = " ";
						unknownChans[chNotFound].diff = " ";
						unknownChans[chNotFound].chFlag = 0;
					}
					chNotFound ++;
				}
				if(chNumP >= SDF_MAX_CHANS)
				{
					fclose(cdf);
					fclose(adf);
					sprintf(errMsg,"Number of channels in %s exceeds program limit\n",sdfile);
					logFileEntry(errMsg);
					lderror = 4;
					return(lderror);
				}
		   	} 
		}
		fclose(cdf);
		fclose(adf);
		printf("Loading epics %d\n",chNumP);
		lderror = writeEpicsDb(cdTableP,command);
		sleep(2);
		newfilterstats();
		fmtInit = 1;
	}

	// Calc time to load settings and make log entry
	clock_gettime(CLOCK_REALTIME,&t);
	totaltime = t.tv_nsec - starttime;
	if(totaltime < 0) totaltime += 1000000000;
	if(command == SDF_LOAD_PARTIAL) {
		sprintf(errMsg,"New SDF request (w/table update): %s\nFile = %s\nTotal Chans = %d with load time = %d usec\n",timestring.c_str(), sdfile, chNumP, (totaltime/1000));
	} else if(command == SDF_LOAD_DB_ONLY){
		sprintf(errMsg,"New SDF request (No table update): %s\nFile = %s\nTotal Chans = %d with load time = %d usec\n",timestring.c_str(), sdfile, chNumP, (totaltime/1000));
	}
	logFileEntry(errMsg);
	status = dbNameToAddr(reloadtimechannel,&reloadDbAddr);
	status = dbPutField(&reloadDbAddr,DBR_STRING,timestring.c_str(),1);
	printf("Number of read errors = %d\n",rderror);
	if(rderror) lderror = 2;
	return(lderror);
}

void registerFilters() {
	int ii = 0;
	fixed_string<64> tmpstr;
	int amatch = 0, jj = 0;
	fmNum = 0;
	for(ii=0;ii<chNum;ii++) {
		if(cdTable[ii].filterswitch == 1)
		{
			filterTable[fmNum].fname = cdTable[ii].chname;
            filterTable[fmNum].fname.pop_back_n(4);
			tmpstr = filterTable[fmNum].fname;
			tmpstr += "SW2S";
			filterTable[fmNum].sw[0] = ii;
			cdTable[ii].filterNum = fmNum;
			amatch = 0;
			for(jj=0;jj<chNum;jj++) {
				if(tmpstr == cdTable[jj].chname)
				{
					filterTable[fmNum].sw[1] = jj;
					amatch = 1;
				}
			}
			if(!amatch) printf("No match for %s\n",tmpstr.c_str());
			fmNum ++;
		}
	}
	printf("%d filters\n",fmNum);
}

/// Provide initial values to the filterMasks arrray and the SDF_FM_MASK and SDF_FM_MASK_CTRL variables
///
void setupFMArrays(char *pref,fixed_size_vector<int, SDF_MAX_FMSIZE>& fmMasks,
                   fixed_size_vector<dbAddr, SDF_MAX_FMSIZE>& fmMaskAddr,
                   fixed_size_vector<dbAddr, SDF_MAX_FMSIZE>& fmCtrlAddr) {
	int ii = 0;
	int sw1 = 0;
	int sw2 = 0;
	fixed_string<256> name;
	fixed_string<256> ctrl;
	int zero = 0;

	fmMasks.clear();
	fmMaskAddr.clear();
	fmCtrlAddr.clear();
	for (ii = 0; ii < filterTable.size(); ++ii)
	{
        sw1 = filterTable[ii].sw[0];
        sw2 = filterTable[ii].sw[1];
        fmMasks[ii] = filterTable[ii].mask = cdTable[sw1].mask | cdTable[sw2].mask;

        name.printf("%s_SDF_FM_MASK_%d", pref, ii);
        fmMaskAddr.emplace_back();
        dbNameToAddr(name.c_str(), &fmMaskAddr[ii]);
        dbPutField(&fmMaskAddr[ii],DBR_LONG,&(fmMasks[ii]),1);

        ctrl.printf("%s_SDF_FM_MASK_CTRL_%d", pref, ii);
        fmCtrlAddr.emplace_back();
        dbNameToAddr(ctrl.c_str(), &fmCtrlAddr[ii]);
        dbPutField(&fmCtrlAddr[ii],DBR_LONG,&zero,1);

    }
}

/// Copy the filter mask information from cdTable into fmMasks
///
void resyncFMArrays(fixed_size_vector<int, SDF_MAX_FMSIZE>&fmMasks,
                    fixed_size_vector<dbAddr, SDF_MAX_FMSIZE>& fmMaskAddr)
{
	int ii = 0;
	int sw1 = 0;
	int sw2 = 0;
	long status = 0;


	for (ii = 0; ii < filterTable.size(); ++ii) {
		sw1 = filterTable[ii].sw[0];
		sw2 = filterTable[ii].sw[1];
		fmMasks[ii] = filterTable[ii].mask = (cdTable[sw1].mask | cdTable[sw2].mask) & ALL_SWSTAT_BITS;

		status = dbPutField(&fmMaskAddr[ii],DBR_LONG,&(fmMasks[ii]),1);
	}
}

/// Process a command to flip or set filter bank monitor bits
///
/// @param fMask[in/out] A pointer to a list of int filter monitoring masks
/// @param fmMaskAddr[in/out] A pointer to the list of monitor masks addresses
/// @param fmCtrlAddr[in/out] A pointer to the list of control word addresses
/// @param selectCounter[in/out] A array of 3 ints with the count of changes selected
/// @param errCnt[in] Number or entries in setErrTable
/// @param setErrTable[in/out] The array of SET_ERR_TABLE entries to update the chFlag field on
///
/// @remark The SDF_FM_MASK_CTRL channels signal a change in filter bank
/// monitoring.  This checks all of the MASK_CTRL signals and modifies the
/// filter mask in the fMask array as needed.
///
/// @remark Each of the input arrays is expected to have a length of SDF_MAX_FMSIZE
/// with fmNum in actual use.
void processFMChanCommands(
        fixed_size_vector<int, SDF_MAX_FMSIZE>& fMask,
        fixed_size_vector<dbAddr, SDF_MAX_FMSIZE>& fmMaskAddr,
        fixed_size_vector<dbAddr, SDF_MAX_FMSIZE>& fmCtrlAddr,
        int *selectCounter, table_range setErrTable) {
	int ii = 0;
	int jj = 0;
	int refMask = 0;
	int preMask = 0;
	int differsPre = 0, differsPost = 0;
	int ctrl = 0;
	long status = 0;
	long ropts = 0;
	long nvals = 1;
	int foundCh = 0;

	//std::distance(setErrTable.begin(), setErrTable.end());
	for (ii = 0; ii < filterTable.size(); ++ii) {
		status = dbGetField(&fmCtrlAddr[ii], DBR_LONG, &ctrl, &ropts, &nvals, NULL);

		// only do work if there is a change
		if (!ctrl) continue;

		refMask = filterTable[ii].mask;
		preMask = fMask[ii];
		differsPre = ((refMask & ALL_SWSTAT_BITS) != (fMask[ii] & ALL_SWSTAT_BITS));

		fMask[ii] ^= ctrl;

		differsPost = ((refMask & ALL_SWSTAT_BITS) != (fMask[ii] & ALL_SWSTAT_BITS));

		foundCh = 0;
		/* if there is a change, update4 the selection count) */
		if (differsPre != differsPost) {
		    // search through setErrTable
			//for (jj = 0; jj < errCnt && setErrTable[jj].filtNum >= 0 && setErrTable[jj].filtNum != ii; ++jj) {}
			auto match = std::find_if(setErrTable.begin(), setErrTable.end(), [ii](const SET_ERR_TABLE& cur) -> bool {
			    return cur.filtNum == ii;
			});
			// act on search
			//if (jj < errCnt && setErrTable[jj].filtNum == ii)
			if ( match != setErrTable.end())
			{
				match->chFlag ^= CHFLAG_MONITOR_BIT_VAL;
				foundCh = match->chFlag;
				selectCounter[2] += ( differsPre ? -1 : 1 );
			}
		}

		printf("Signal 0x%x set on '%s' ref=0x%x pre=0x%x post=0x%x pre/post diff(%d,%d) chFlag = 0x%x\n", ctrl, filterTable[ii].fname.c_str(), refMask, preMask, fMask[ii], differsPre, differsPost, foundCh);

		dbPutField(&fmMaskAddr[ii],DBR_LONG,&(fMask[ii]),1);

		ctrl = 0;
		dbPutField(&fmCtrlAddr[ii],DBR_LONG,&ctrl,1);
	}
}

#ifdef CA_SDF

/*!
 * @brief count the disconnected channels if we are not on the disconnected page
 */
void countDisconnectedChannels(int curTable)
{
    if (curTable != SDF_TABLE_DISCONNECTED)
    {
        chDisconnectedCount = countDisconnectedChans();
    }
}

/*!
 * @brief store the setpoint value for a pv
 * @note this stores the value that will be set by updateStrVarSetPoint
 */
void saveStrSetPoint(int index, const char* value)
{
    sprintf(caStringSetpoint[index],"%s",value);
    caStringSetpointInitted[index] = 1;
}

/*!
 * @brief Ensure that the setpoint value is set for the given str variable
 */
void updateStrVarSetPoint(int index)
{
    if (caStringSetpointInitted[index] == 1)
    {
        cdTable[index].data.strval = caStringSetpoint[index];
        caStringSetpointInitted[index] = 2;
    }
}

long countDisconnectedChans()
{
    int jj = 0;
    long count = 0;

    for (jj = 0; jj < chNum; jj++)
    {
        if (!cdTable[jj].connected)
        {
            ++count;
        }
    }
    return count;
}

void nullCACallback(struct event_handler_args args) {}

bool getCAIndex(const char *entry, ADDRESS *addr) {
	int ii = 0;

	if (!entry || !addr) return false;
	for (ii = 0; ii < chNum; ++ii) {
		if (cdTable[ii].chname == entry) {
			*addr = ii;
			return true;
		}
	}
	*addr = -1;
	return false;
}

bool setCAValue(ADDRESS ii, SDF_TYPE type, const void *data)
{
	bool result = false;
	int status = ECA_NORMAL;

	if (ii >= 0 && ii < chNum) {
		if (type == SDF_NUM) {
			status = ca_put_callback(DBR_DOUBLE, caTable[ii].chanid, (double *)data, nullCACallback, NULL);
		} else {
			status = ca_put_callback(DBR_STRING, caTable[ii].chanid, (char *)data, nullCACallback, NULL);
		}
		result = (status == ECA_NORMAL);
	}
	return result;
}

bool setCAValueLong(ADDRESS ii, const PV_INT_TYPE *data) {
	double tmp = 0.0;

	if (!data) return false;
	tmp = (double)*data;
	return setCAValue(ii,SDF_NUM,(const void*)&tmp);
}

bool syncEpicsDoubleValue(ADDRESS index, double *dest, time_t *tp, int *connp) {
	int debug = 0;
	if (!dest || index < 0 || index >= chNum) return false;
#if VERBOSE_DEBUG
	if (strcmp(cdTable[caTable[index].chanIndex].chname.c_str(), TEST_CHAN) == 0) {
		debug=1;
	}
#endif
	lock_guard l_(caTableMutex);
	if (caTable[index].datatype == SDF_NUM) {
		*dest = caTable[index].data.chval;
		if (tp) {
			*tp = caTable[index].mod_time;
		}
		if (connp) {
			*connp = caTable[index].connected;
		}
	}
	return true;
}

bool syncEpicsIntValue(ADDRESS index, PV_INT_TYPE *dest, time_t *tp, int *connp) {
	bool result = true;
	double tmp = 0.0;

	if (!dest) return false;
	result = syncEpicsDoubleValue(index, &tmp, tp, connp);
	*dest = (PV_INT_TYPE)(tmp);
	return result;
}

bool syncEpicsStrValue(ADDRESS index, char *dest, int dest_size, time_t *tp, int *connp) {
	if (!dest || index < 0 || index >= chNum || dest_size < 1) return false;
	dest[0] = '\0';
	lock_guard l_(caTableMutex);
	if (caTable[index].datatype == SDF_STR) {
		const int MAX_STR_LEN = caTable[index].data.strval.capacity();
		strncpy(dest, caTable[index].data.strval.c_str(), (dest_size < MAX_STR_LEN ? dest_size : MAX_STR_LEN));
		dest[dest_size-1] = '\0';
		if (tp) {
			*tp = caTable[index].mod_time;
		}
		if (connp) {
			*connp = caTable[index].connected;
		}
	}
	return true;
}

/// Routine to handle subscription callbacks
void subscriptionHandler(struct event_handler_args args) {
	float val = 0.0;
	EPICS_CA_TABLE *entry = reinterpret_cast<EPICS_CA_TABLE*>(args.usr);
	EPICS_CA_TABLE *origEntry = entry;
	int initialRedirIndex = 0;

	if (args.status != ECA_NORMAL ||  !entry) {
		return;
	}

	lock_guard l_(caTableMutex);

	// If this entry has just reconnected, then do not write the old copy, write to the temporary/redir dest
	initialRedirIndex = entry->redirIndex;
	if (entry->redirIndex >= 0) {
		if (entry->redirIndex < SDF_MAX_TSIZE) {
			entry = &(caConnTable[entry->redirIndex]);
		} else {
			entry = &(caConnEnumTable[entry->redirIndex - SDF_MAX_TSIZE]);
		}
	}

	// if we are getting data, we must be connected.
	entry->connected = 1;
	if (args.type == DBR_TIME_DOUBLE) {
		struct dbr_time_double *dVal = (struct dbr_time_double *)args.dbr;
		entry->data.chval = dVal->value;
		entry->mod_time = dVal->stamp.secPastEpoch + POSIX_TIME_AT_EPICS_EPOCH;
	} else if (args.type == DBR_TIME_STRING) {
		struct dbr_time_string *sVal = (struct dbr_time_string *)args.dbr;
		entry->data.strval = sVal->value;
		entry->mod_time = sVal->stamp.secPastEpoch + POSIX_TIME_AT_EPICS_EPOCH;
	} else if (args.type == DBR_GR_ENUM) {
		struct dbr_gr_enum *eVal = (struct dbr_gr_enum *)args.dbr;
		if (initialRedirIndex >= SDF_MAX_TSIZE) {			
			if (entry->datatype == SDF_UNKNOWN) {
				// determine the proper type
				entry->datatype = SDF_NUM;
				if (eVal->no_str >= 2) {
					// call it a string if there are two non-null distinct
					// strings for the first two entries
					if ((strlen(eVal->strs[0]) > 0 && strlen(eVal->strs[1]) > 0) &&
						strcmp(eVal->strs[0], eVal->strs[1]) != 0)
					{
						entry->datatype = SDF_STR;
					}
				}
				++chEnumDetermined;
			}
		}
		if (entry->datatype == SDF_NUM) {
			entry->data.chval = (double)(eVal->value);
		} else {
			if (eVal->value > 0 && eVal->value < eVal->no_str) {
                            entry->data.strval = eVal->strs[eVal->value];
			} else {
			    entry->data.strval.printf("Unexpected enum value received - %d", (int)eVal->value);
			}
		}
		// The dbr_gr_enum type does not have time information, so we use current time
		entry->mod_time = time(NULL);
	}
}

void connectCallback(struct connection_handler_args args) {
	EPICS_CA_TABLE *entry = (EPICS_CA_TABLE*)ca_puser(args.chid);
	EPICS_CA_TABLE *connEntry = 0;
	int dbr_type = -1;
	int sdf_type = SDF_NUM;
	int chanIndex = SDF_MAX_TSIZE;
	int typeChange = 0;
	int is_enum = 0;

	if (entry) {
		chanIndex = entry->chanIndex;

		// determine the field type for conn up events
		// do this outside of the critical section
		if (args.op == CA_OP_CONN_UP) {
			dbr_type = dbf_type_to_DBR(ca_field_type(args.chid));
			if (dbr_type_is_ENUM(dbr_type)) {
				is_enum = 1;
				sdf_type = SDF_UNKNOWN;
			} else if (dbr_type == DBR_STRING) {
				sdf_type = SDF_STR;
			}
		}

        {
            lock_guard l_(caTableMutex);
            if (args.op == CA_OP_CONN_UP) {
                // connection
                if (is_enum) {
                    entry->redirIndex = entry->chanIndex + SDF_MAX_TSIZE;
                    connEntry = &(caConnEnumTable[entry->chanIndex]);
                } else {
                    // reserve a new conn table entry if one is not already reserved
                    if (entry->redirIndex < 0) {
                        entry->redirIndex = chConnNum;
                        ++chConnNum;
                    }
                    connEntry = &(caConnTable[entry->redirIndex]);
                }
                entry->chanid = args.chid;

                // now copy items over to the connection record
                connEntry->redirIndex = -1;

                connEntry->datatype = sdf_type;
                connEntry->data.chval = 0.0;
                entry->connected = 1;
                connEntry->connected = 1;
                connEntry->mod_time = 0;
                connEntry->chanid = args.chid;
                connEntry->chanIndex = entry->chanIndex;
                typeChange = (connEntry->datatype != entry->datatype);

            } else {
                // disconnect
                entry->connected = 0;
            }
        }

        {
            // now register/clear the subscription callback
            lock_guard l_(caEvidMutex);
            if (args.op == CA_OP_CONN_UP) {
                // connect
                // if we are subscribed but the types are wrong, then unsubscribe
                if (caEvid[chanIndex] && typeChange) {
                    ca_clear_subscription(caEvid[chanIndex]);
                    caEvid[chanIndex] = 0;
                }
                // if we are not subscribed become subscribed
                if (!caEvid[chanIndex]) {
                    chtype subtype = (sdf_type == SDF_NUM ? DBR_TIME_DOUBLE : DBR_TIME_STRING);
                    if (is_enum) {
                        subtype = DBR_GR_ENUM;
                    }
                    ca_create_subscription(subtype, 0, args.chid, DBE_VALUE, subscriptionHandler, entry,
                                           &(caEvid[chanIndex]));
                }
            } else {
                // disconnect
                if (caEvid[chanIndex]) {
                    ca_clear_subscription(caEvid[chanIndex]);
                    caEvid[chanIndex] = 0;
                }
            }
        }
	}
}

/// Routine to register a channel
void registerPV(const char *PVname)
{
	long status=0;
	chid chid1;

	if (chNum >= SDF_MAX_TSIZE) {
		droppedPVCount++;
		return;
	}
	//printf("Registering %s\n", PVname);
    {
        lock_guard l_(caTableMutex);
        caTable[chNum].datatype = SDF_NUM;
        caTable[chNum].connected = 0;
        cdTable[chNum].chname = PVname;
        cdTable[chNum].datatype = SDF_NUM;
        cdTable[chNum].initialized = 0;
        cdTable[chNum].filterswitch = 0;
        cdTable[chNum].filterNum = -1;
        cdTable[chNum].error = 0;
        cdTable[chNum].initialized = 0;
        cdTable[chNum].mask = 0;
    }

	status = ca_create_channel(PVname, connectCallback, &(caTable[chNum]), 0, &chid1);

	if((strstr(PVname,"_SW1S") != NULL) && (strstr(PVname,"_SW1S.") == NULL))
	{
		cdTable[chNum].filterswitch = 1;
	}
	if((strstr(PVname,"_SW2S") != NULL) && (strstr(PVname,"_SW2S.") == NULL))
	{
		cdTable[chNum].filterswitch = 2;
	}

	caTable[chNum].chanid = chid1;
	++chNum;
}

/// Convert a daq data type to a SDF type
int daqToSDFDataType(int daqtype) {
	if (daqtype == 4) {
		return SDF_NUM;
	}
	if (daqtype == 128) {
		return SDF_STR;
	}
	return SDF_UNKNOWN;
}

/// Parse an BURT request file and register each channel to be monitored
/// @input fname - name of the file to open
/// @input pref - the ifo name, should be 3 characters
void parseChannelListReq(char *fname) {
	FILE *f = 0;
	char line[128];
	int argcount = 0;
        int in_header = 0;
        const static char* header_start = "--- Start";
        const static char* header_end = "--- End";

	line[0]='\0';
	f = fopen(fname, "r");
	if (!f) return;

        embedded::fixed_size_vector<embedded::fixed_string<128>, 1> words;

	while (fgets(line, sizeof(line), f) != NULL) {
                if (in_header)
                {
                    if (strstr(line, header_end) == line)
                    {
                        in_header = 0;
                    }
                    continue;
                }
                else if (strstr( line, header_start) == line)
                {
                    in_header = 1;
                    continue;
                }
                argcount = BURT::parseLine(line, words);
		if (argcount < 1) continue;
		if (strstr(words.front().c_str(),"_SWMASK") != NULL ||
			strstr(words.front().c_str(),"_SDF_NAME") != NULL ||
			strstr(words.front().c_str(),"_SWREQ") != NULL) continue;
		if (isAlarmChannelRaw(words.front().c_str())) continue;
		registerPV(words.front().c_str());
	}
	fclose(f);
}

/// Routine to get the state of the CA Thread
int getCAThreadState() {
	lock_guard l_(caStateMutex);
	return caThreadState;
}

/// Routine to set the state of the CA Thread
void setCAThreadState(int state) {
	lock_guard l_(caStateMutex);
	caThreadState = state;
}

// copy the given entry (which should be in caConnTable or caConnEnumTable) to caTable
// update cdTable with type information if needed.
void copyConnectedCAEntry(EPICS_CA_TABLE *src) {
	int cdt_jj = 0;
	EPICS_CA_TABLE *dest = 0;

	if (!src) return;

	dest = &(caTable[src->chanIndex]);
	src->connected = 1;
	// just copy the table entry into place
	memcpy((void *)(dest), (void *)(src), sizeof(*src));
	
	// clearing the redir field
	dest->redirIndex = -1;
	cdt_jj = src->chanIndex;
	cdTable[cdt_jj].connected = 1;
	caTable[cdt_jj].connected = 1;
	// now update cdTable type information
	// iff the type is different, clear the data field
	if (cdTable[cdt_jj].datatype != src->datatype) {
		cdTable[cdt_jj].datatype = src->datatype;
		cdTable[cdt_jj].data.chval = 0.0;
	}
}

void syncCAConnections(long *disconnected_count)
{
	int ii = 0;
	long tmp = 0;
	int chanIndex = SDF_MAX_TSIZE;
	chtype subtype = DBR_TIME_DOUBLE;

	lock_guard l_(caTableMutex);
	// sync all non-enum channel connections that have taken place
	for (ii = 0; ii < chConnNum; ++ii) {
		copyConnectedCAEntry(&(caConnTable[ii]));
	}
	chConnNum = 0;

	// enums take special work, as we must receive an value to determine if we can use it as string or numeric value
	// try to short circuit the reading of the entire table, most cycles we have nothing to do.
	if (chEnumDetermined > 0) {
		for (ii = 0; ii < chNum; ++ii) {
			if (caConnEnumTable[ii].datatype != SDF_UNKNOWN) {
				// we have a concrete type here, migrate it over
				copyConnectedCAEntry(&(caConnEnumTable[ii]));
				caConnEnumTable[ii].datatype = SDF_UNKNOWN;

				chanIndex = caTable[ii].chanIndex;
				ca_clear_subscription(caEvid[chanIndex]);
				caEvid[chanIndex] = 0;
				subtype = (caTable[ii].datatype == SDF_NUM ? DBR_TIME_DOUBLE : DBR_TIME_STRING);
				ca_create_subscription(subtype, 0, caTable[ii].chanid, DBE_VALUE, subscriptionHandler, &(caTable[ii]), &(caEvid[chanIndex]));

				chanIndex = SDF_MAX_TSIZE;
				subtype = DBR_TIME_DOUBLE;
			}
		}
	}
	chEnumDetermined = 0;
	for (ii = 0 ; ii < chNum ; ++ii)  {
		cdTable[ii].connected = caTable[ii].connected;
	}
	// count the disconnected chans
	//if (disconnected_count) {
	//	for (ii = 0; ii < chNum; ++ii) {
	//		tmp += !caTable[ii].connected;
	//	}
	//	*disconnected_count = tmp;
	//}
}

/// Main loop of the CA Thread
void *caMainLoop(void *param)
{
	CA_STARTUP_INFO *info = (CA_STARTUP_INFO*)param;
	
	if (!param) {
		setCAThreadState(CA_STATE_EXIT);
		return NULL;
	}
	ca_context_create(ca_enable_preemptive_callback);
	setCAThreadState(CA_STATE_PARSING);
	parseChannelListReq(info->fname);
	registerFilters();
	printf("Done with parsing, CA thread continuing\n");
	setCAThreadState(CA_STATE_RUNNING);
	return nullptr;
}

/// Routine used to read in all channels to monitor from an INI file and create local connections
void initCAConnections(char *fname)
{
	int err = 0;
	int state = CA_STATE_OFF;
	CA_STARTUP_INFO param;

	param.fname = fname;

	setCAThreadState(CA_STATE_OFF);
	caMainLoop(&param);
}

/// Routine to setup mutexes and other resources used by the CA SDF system
void setupCASDF()
{
	int ii;

	// set some defaults
	for (ii = 0; ii < SDF_MAX_TSIZE; ++ii) {
	    caStringSetpointInitted[ii] = 0;
	    strcpy(caStringSetpoint[ii], "");

		caTable[ii].redirIndex = -1;
		caTable[ii].datatype = SDF_NUM;
		caTable[ii].data.chval = 0.0;
		caTable[ii].connected = 0;
		caTable[ii].chanid = nullptr;
		caTable[ii].mod_time = (time_t)0;
		caTable[ii].chanIndex = ii;

		caConnTable[ii].redirIndex = -1;
		caConnTable[ii].datatype = SDF_NUM;
		caConnTable[ii].data.chval = 0.0;
		caConnTable[ii].connected = 0;
		caConnTable[ii].chanid = nullptr;
		caConnTable[ii].mod_time = (time_t)0;
		caConnTable[ii].chanIndex = 0;

		caConnEnumTable[ii].redirIndex = -1;
		caConnEnumTable[ii].datatype = SDF_UNKNOWN;
		caConnEnumTable[ii].data.chval = 0.0;
		caConnEnumTable[ii].connected = 0;
		caConnEnumTable[ii].chanid = nullptr;
		caConnEnumTable[ii].mod_time = (time_t)0;
		caConnEnumTable[ii].chanIndex = ii;

		bzero((void *)&(cdTable[ii]), sizeof(cdTable[ii]));
		bzero((void *)&(unMonChans[ii]), sizeof(unMonChans[ii]));
		bzero((void *)&(cdTableList[ii]), sizeof(cdTableList[ii]));
		bzero((void *)&(disconnectChans[ii]), sizeof(disconnectChans[ii]));

		caEvid[ii] = 0;
	}

	// set more defaults
	for (ii = 0; ii < SDF_MAX_CHANS; ++ii) {
		bzero((void *)&(cdTableP[ii]), sizeof(cdTableP[ii]));
	}
	for (ii = 0; ii < SDF_MAX_FMSIZE; ++ii) {
		bzero((void *)&(filterTable[ii]), sizeof(filterTable[ii]));
	}
	for (ii = 0; ii < SDF_ERR_TSIZE; ++ii) {
		bzero((void *)&(setErrTable[ii]), sizeof(setErrTable[ii]));
		bzero((void *)&(unknownChans[ii]), sizeof(unknownChans[ii]));
		bzero((void *)&(uninitChans[ii]), sizeof(uninitChans[ii]));
		bzero((void *)&(readErrTable[ii]), sizeof(readErrTable[ii]));
	}
	droppedPVCount = 0;
}

/// Routine to tear-down mutexes and other resources used by the CA SDF system
void cleanupCASDF()
{
}
#else
void countDisconnectedChannels(int curTable)
{
}

void saveStrSetPoint(int index, const char* value)
{
}

void updateStrVarSetPoint(int index)
{
}

bool getDbValueDouble(ADDRESS *paddr,double *dest,time_t *tp) {

	struct buffer {
		DBRtime
		double dval;
	} buffer;
	long options = DBR_TIME;
	long nvals = 1;
	bool result = false;

	if (dest && paddr) {
		result = dbGetField(paddr, DBR_DOUBLE, &buffer, &options, &nvals, NULL) == 0;
		*dest = buffer.dval;
		if (tp) {
			*tp = buffer.time.secPastEpoch + POSIX_TIME_AT_EPICS_EPOCH;
		}
	}
	return result;
}
bool getDbValueLong(ADDRESS *paddr,PV_INT_TYPE *dest,time_t *tp) {
	struct buffer {
		DBRtime
		unsigned int dval;
	} buffer;
	long options = DBR_TIME;
	long nvals = 1;
	bool result = false;

	if (dest && paddr) {
		result = dbGetField(paddr, DBR_LONG, &buffer, &options, &nvals, NULL) == 0;
		*dest = buffer.dval;
		if (tp) {
			*tp = buffer.time.secPastEpoch + POSIX_TIME_AT_EPICS_EPOCH;
		}
	}
	return result;
}
bool getDbValueString(ADDRESS *paddr,char *dest, int max_len, time_t *tp) {
	struct buffer {
		DBRtime
		char sval[128];
	} strbuffer;
	long options = DBR_TIME;
	long nvals = 1;
	bool result = false;
	if (dest && paddr && (max_len > 0)) {
		result = dbGetField(paddr,DBR_STRING,&strbuffer,&options,&nvals,NULL) == 0;
		strncpy(dest, strbuffer.sval, max_len);
		dest[max_len-1]='\0';
		if (tp) {
			*tp = strbuffer.time.secPastEpoch + POSIX_TIME_AT_EPICS_EPOCH;
		}
	}
	return result;
}

/// Routine used to extract all settings channels from EPICS database to create local settings table on startup.
void dbDumpRecords(DBBASE *pdbbase, const char *pref)
{
    DBENTRY  *pdbentry = 0;
    long  status = 0;
    char mask_pref[64];
    char mytype[4][64];
    int ii;
    int cnt = 0;
    size_t pref_len = strlen(pref);

    // By convention, the RCG produces ai and bi records for settings.
    sprintf(mytype[0],"%s","ai");
    sprintf(mytype[1],"%s","bi");
    sprintf(mytype[2],"%s","stringin");
    mytype[3][0]='\0';
    pdbentry = dbAllocEntry(pdbbase);

    sprintf(mask_pref, "%s_SDF_FM_MASK_", pref);
    pref_len = strlen(mask_pref);

    chNum = 0;
    fmNum = 0;
    for(ii=0;ii<3;ii++) {
    status = dbFindRecordType(pdbentry,mytype[ii]);

    if(status) {printf("No record descriptions\n");return;}
    while(!status) {
        printf("record type: %s",dbGetRecordTypeName(pdbentry));
        status = dbFirstRecord(pdbentry);
        if (status) printf("  No Records\n"); 
	cnt = 0;
        while (!status) {
	    cnt++;
            if (dbIsAlias(pdbentry)) {
                printf("\n  Alias:%s\n",dbGetRecordName(pdbentry));
            } else {
		//fprintf(stderr, "processing %s\n", dbGetRecordName(pdbentry));
		cdTable[chNum].chname = dbGetRecordName(pdbentry);
		// do not monitor the the SDF mask channels, they are part of this IOC
		if ( cdTable[chNum].chname == mask_pref ) {
            --cnt;
            continue;
		}
		cdTable[chNum].filterswitch = 0;
		cdTable[chNum].filterNum = -1;
		// Check if this is a filter module
		// If so, initialize parameters
		if((strstr(cdTable[chNum].chname.c_str(),"_SW1S") != NULL) && (strstr(cdTable[chNum].chname.c_str(),"_SW1S.") == NULL))
		{
			cdTable[chNum].filterswitch = 1;
			fmNum ++;
		}
		if((strstr(cdTable[chNum].chname.c_str(),"_SW2S") != NULL) && (strstr(cdTable[chNum].chname.c_str(),"_SW2S.") == NULL))
		{
			cdTable[chNum].filterswitch = 2;
		}
		if(ii == 0) {
			cdTable[chNum].datatype = SDF_NUM;
			cdTable[chNum].data.chval = 0.0;
		} else {
			cdTable[chNum].datatype = SDF_STR;
			cdTable[chNum].data.strval = "";
		}
		cdTable[chNum].mask = 0;
		cdTable[chNum].initialized = 0;
            }
	    
	    chNum ++;
            status = dbNextRecord(pdbentry);
        }
	printf("  %d Records, with %d filters\n", cnt,fmNum);
    }
}
	registerFilters();
    printf("End of all Records\n");
    dbFreeEntry(pdbentry);
}
#endif

void listLocalRecords(DBBASE *pdbbase) {
	DBENTRY  *pdbentry = 0;
	long status = 0;
	int ii = 0;

	pdbentry = dbAllocEntry(pdbbase);
	status = dbFindRecordType(pdbentry, "ao");
	if (status) {
		printf("No record descriptions\n");
		return;
	}
	status = dbFirstRecord(pdbentry);
	if (status) {
		printf("No records found\n");
		return;
	}
	while (!status) {
		// printf("%d: %s\n",ii, dbGetRecordName(pdbentry));
		++ii;
		status = dbNextRecord(pdbentry);
	}
	dbFreeEntry(pdbentry);
}

/// Called on EPICS startup; This is generic EPICS provided function, modified for LIGO use.
int main(int argc,char *argv[])
{
#ifdef CA_SDF
	// CA_SDF does not do a partial load on startup.
	int sdfReq = SDF_READ_ONLY;
#else
	// Initialize request for file load on startup.
	int sdfReq = SDF_LOAD_PARTIAL;
#endif
	int status = 0;
	int request = 0;
	long ropts = 0;
	long nvals = 1;
	int rdstatus = 0;
	int burtstatus = 0;
	embedded::fixed_string<256> loadedSdf{};
	embedded::fixed_string<256> sdffileloaded{};
	int sdf_type;
   	int sperror = 0;
	int noMon = 0;
	int noInit = 0;
	// FILE *csFile;
	int ii = 0;
	int setChans = 0;
	char tsrString[64];
	int tsrVal = 0;
	int wcVal = 0;
	int monFlag = 0;
	int sdfSaveReq = 0;
	int saveType = 0;
	embedded::fixed_string<256> saveTypeString{};
	int saveOpts = 0;
    embedded::fixed_string<256> saveOptsString{};
	int fivesectimer = 0;
	long daqFileCrc = 0;
	long coeffFileCrc = 0;
	long fotonFileCrc = 0;
	long prevFotonFileCrc = 0;
	long prevCoeffFileCrc = 0;
	long sdfFileCrc = 0;
    time_t      daqFileMt;
    time_t      coeffFileMt;
    time_t      fotonFileMt;
    time_t      sdfFileMt;
	char modfilemsg[] = "Modified File Detected ";
	struct stat st = {0};
	int reqValid = 0;
	int pageDisp = 0;
	int resetByte = 0;
	int resetBit = 0;
	int confirmVal = 0;
	int myexp = 0;
	int selectCounter[4] = {0,0,0,0};
	int selectAll = 0;
	int freezeTable = 0;
	int zero = 0;
	embedded::fixed_string<256> backupName{};
	int lastTable = 0;
	int cdSort = 0;
	int diffCnt = 0;
  	char errMsg[128];

    tsrString[0] = '\0';
    errMsg[0] = '\0';

    if(argc>=2) {
        iocsh(argv[1]);

	for (ii = 2; ii < argc; ++ii) {
		if (strcmp(argv[ii], "--no-sdf-restore") == 0) {
			sdfReq = SDF_READ_ONLY;
			printf("The SDF system will not force a restore of EPICS values at startup\n");
		} else if (strcmp(argv[ii], "--sdf-restore") == 0) {
			sdfReq = SDF_LOAD_PARTIAL;
			printf("The SDF system will force a restore of EPICS values at startup\n");
		}
	}

	// printf("Executing post script commands\n");
	// dbDumpRecords(*iocshPpdbbase);
	// Get environment variables from startup command to formulate EPICS record names.
	char *pref = getenv("PREFIX");
	char *sdfDir = getenv("SDF_DIR");
	char *sdfenv = getenv("SDF_FILE");
	char *modelname =  getenv("SDF_MODEL");
	char *targetdir =  getenv("TARGET_DIR");
	char *daqFile =  getenv("DAQ_FILE");
	char *coeffFile =  getenv("COEFF_FILE");
	char *fotonFile =  getenv("FOTON_FILE");
	char *fotonDiffFile = getenv("FOTON_DIFF_FILE");
	char *logdir = getenv("LOG_DIR");
	char myDiffCmd[256];
	table_range currentTable;


	if(stat(logdir, &st) == -1) mkdir(logdir,0777);
	// strcat(sdf,"_safe");
	embedded::fixed_string<256> sdf;
    embedded::fixed_string<256>  sdfile;
	embedded::fixed_string<256>  sdalarmfile;
	embedded::fixed_string<256>  bufile;
    embedded::fixed_string<256>  saveasfilename;
    embedded::fixed_string<256>  wcstring;

    sdf = sdfenv;
	
	printf("My prefix is %s\n",pref);
	sdfile.printf("%s%s%s", sdfDir, sdf.c_str(), ".snap");  // Initialize with BURT_safe.snap
	bufile.printf("%s%s", sdfDir, "fec.snap");					// Initialize table dump file
	sprintf(logfilename, "%s%s", logdir, "/ioc.log");					// Initialize table dump file
	printf("SDF FILE = %s\n",sdfile.c_str() );
	printf("CURRENt FILE = %s\n",bufile.c_str() );
	printf("LOG FILE = %s\n",logfilename);
	sleep(5);
	int majorversion = RCG_VERSION_MAJOR;
	int subversion1 = RCG_VERSION_MINOR;
	int subversion2 = RCG_VERSION_SUB;
	int myreleased = RCG_VERSION_REL;
	double myversion;

    auto common_name = [pref](const char* name) -> epics::channel_name {
        return epics::make_name(pref, name);
    };


	SETUP();
#ifndef USE_SYSTEM_TIME
        timechannel = std::make_unique< epics::DBEntry< epics::PVType::String > >( common_name("TIME_STRING") );
#endif

  	sprintf(reloadtimechannel,"%s_%s", pref, "SDF_RELOAD_TIME");			// Time of last BURT reload

	// listLocalRecords(*iocshPpdbbase);
	myversion = majorversion + 0.1 * subversion1 + 0.01 * subversion2;
	if(!myreleased)
        {
            myversion *= -1.0;
        }
	epics::DBEntry< epics::PVType::Float64 > rcgversion_channel(common_name("RCG_VERSION"), myversion);
        rcgversion_channel.set( myversion );


	// Create BURT/SDF EPICS channel names

    epics::DBEntry< epics::PVType::Int32 > reload_channel(common_name("SDF_RELOAD"), sdfReq); // Init request for startup.
    epics::DBEntry< epics::PVType::Int32 > reload_stat( common_name("SDF_RELOAD_STATUS"), rdstatus); // Init to zero

    // Initialize BURT file to be loaded next request = safe.snap
    epics::DBEntry<epics::PVType::String> sdfname_channel(common_name("SDF_NAME"), sdf);

    epics::DBEntry<epics::PVType::String> loadedfile_channel(common_name("SDF_LOADED"));

    epics::DBEntry<epics::PVType::Int32> sdftype_channel(common_name("SDF_TYPE"), sdf_type);

    epics::DBEntry< epics::PVType::String > edbloaded_channel(common_name("SDF_LOADED_EDB"));	// Name of file presently loaded

	epics::DBEntry< epics::PVType::Int32 > sperror_channel(common_name("SDF_DIFF_CNT"), sperror); // Setpoint diff counter
    epics::DBEntry< epics::PVType::Int32 > alrmchcount_channel(common_name("SDF_ALARM_CNT"), alarmCnt); // Number of alarm settings in a BURT file.
    epics::DBEntry< epics::PVType::Int32 > fulldbcnt_channel(common_name("SDF_FULL_CNT"));// Number of setting channels in EPICS db

    epics::DBEntry< epics::PVType::Int32 > filesetcnt_channel(common_name("SDF_FILE_SET_CNT")); // Number of settings inBURT file
    epics::DBEntry< epics::PVType::Int32 > unmonchancnt_channel(common_name("SDF_UNMON_CNT")); // Number of settings NOT being monitored.

#ifdef CA_SDF
    epics::DBEntry< epics::PVType::Int32 > system_dcuid( epics::make_name(epics::model_to_system(modelname),"DCU_ID") );
    system_dcuid.set(epics::extract_dcu_from_prefix(pref));
    epics::DBEntry< epics::PVType::Int32 > disconnectcount_channel(common_name("SDF_DISCONNECTED_CNT"));

    epics::DBEntry< epics::PVType::Int32 > droppedcount_channel(common_name("SDF_DROPPED_CNT"));
#endif

    epics::DBEntry< epics::PVType::UInt16 > tablesortreq_channel(common_name("SDF_SORT"));// SDF Table sorting request
    epics::DBEntry< epics::PVType::Int32 > wcreq_channel(common_name("SDF_WILDCARD"), 0);               // SDF Table sorting request
    epics::DBEntry< epics::PVType::Int32 > chnotfound_channel(common_name("SDF_DROP_CNT")); // Number of channels not found.
    epics::DBEntry< epics::PVType::Int32 > chnotinit_channel(common_name("SDF_UNINIT_CNT"));// Number of channels not initialized.
    epics::DBEntry< epics::PVType::Int32 > sorttableentries_channel(common_name("SDF_TABLE_ENTRIES")); // Number of entries in an SDF reporting table.
    epics::DBEntry< epics::PVType::Int32 > monflag_channel(common_name("SDF_MON_ALL"), rdstatus); // Request to monitor all channels.
    epics::DBEntry< epics::PVType::Int32 > savecmd_channel(common_name("SDF_SAVE_CMD"), rdstatus);	// SDF Save command.
    epics::DBEntry< epics::PVType::Int32 > pagelock_channel(common_name("SDF_TABLE_LOCK"), freezeTable); // SDF Save command.

	// Clear out the save as file name request
    epics::DBEntry< epics::PVType::String > saveas_channel(common_name("SDF_SAVE_AS_NAME"), epics::channel_name("default")); // Set as dummy 'default'

	epics::DBEntry< epics::PVType::String > wcstring_channel(common_name("SDF_WC_STR"), "");// SDF Save as file name.

    epics::DBEntry< epics::PVType::String > savetype_channel( common_name("SDF_SAVE_TYPE"));// SDF Save file type.
    epics::DBEntry< epics::PVType::String > saveopts_channel( common_name("SDF_SAVE_OPTS"));// SDF Save file options.

    epics::DBEntry<epics::PVType::String> savefile_channel(common_name("SDF_SAVE_FILE"), ""); // SDF Name of last file saved.
    epics::DBEntry<epics::PVType::String> savetime_channel( common_name("SDF_SAVE_TIME"), ""); // SDF Time of last file save.
    epics::DBEntry< epics::PVType::String > daqmsg_channel(common_name("MSGDAQ"));// Record to write if DAQ file changed.
    epics::DBEntry< epics::PVType::String > coeffmsg_channel(common_name("MSG2"));	// Record to write if Coeff file changed.

    epics::DBEntry< epics::PVType::String > msgstr_channel(common_name("SDF_MSG_STR"), "");// SDF Time of last file save.
	char msgstrname[128]; sprintf(msgstrname, "%s_%s", pref, "SDF_MSG_STR");	// SDF Time of last file save.

    epics::DBEntry<epics::PVType::String> reloadtime_channel(common_name("SDF_RELOAD_TIME"), ""); // Time of last BURT reload

	int pageNum = 0;
	int pageNumSet = 0;
	dbAddr pagereqaddr;
        char pagereqname[256]; sprintf(pagereqname, "%s_%s", pref, "SDF_PAGE"); // SDF Save command.
        status = dbNameToAddr(pagereqname,&pagereqaddr);                // Get Address.

	unsigned int resetNum = 0;
    epics::DBEntry< epics::PVType::UInt32 > resetone_channel(common_name("SDF_RESET_CHAN"), resetNum);	// SDF reset one value.

    std::array< epics::DBEntry< epics::PVType::Int32 >, 4> select_channel{
            epics::DBEntry< epics::PVType::Int32 >(common_name("SDF_SELECT_SUM0"), 0),
            epics::DBEntry< epics::PVType::Int32 >(common_name("SDF_SELECT_SUM1"), 0),
            epics::DBEntry< epics::PVType::Int32 >(common_name("SDF_SELECT_SUM2"), 0),
            epics::DBEntry< epics::PVType::Int32 >(common_name("SDF_SELECT_ALL"), selectAll),
    };

	dbAddr confirmwordaddr;
	char confirmwordname[64]; 
	sprintf(confirmwordname, "%s_%s", pref, "SDF_CONFIRM");	// Record to write if Coeff file changed.
	status = dbNameToAddr(confirmwordname,&confirmwordaddr);
	status = dbPutField(&confirmwordaddr,DBR_LONG,&resetNum,1);

#ifdef CA_SDF
	{
		char *buffer=0;
		char fname[]="/monitor.req";
		int len = strlen(sdfDir)+strlen(fname)+1;
		buffer = (char*)malloc(len);
		if (!buffer) {
			fprintf(stderr, "Unable to allocate memory to hold the path to monitor.req, aborting!");
			exit(1);
		}
		strcpy(buffer, sdfDir);
		strcat(buffer, fname);
		initCAConnections(buffer);
		free(buffer);
	}
#else
	dbDumpRecords(*iocshPpdbbase, pref);
#endif
	setupFMArrays(pref,filterMasks,fmMaskChan,fmMaskChanCtrl);

	sprintf(errMsg,"Software Restart \nRCG VERSION: %.2f",myversion);
	logFileEntry(errMsg);

	

	// Initialize DAQ and COEFF file CRC checksums for later compares.
	daqFileCrc = checkFileCrc(daqFile);
	coeffFileCrc = checkFileCrc(coeffFile);
	fotonFileCrc = checkFileCrc(fotonFile);
	prevFotonFileCrc = fotonFileCrc;
	prevCoeffFileCrc = coeffFileCrc;
    // Initialize file modification times
    status = checkFileMod( daqFile, &daqFileMt, 1 );
    status = checkFileMod( coeffFile, &coeffFileMt, 1 );
    status = checkFileMod( fotonFile, &fotonFileMt, 1 );

	reportSetErrors(pref,setErrTable,0,1);

	sleep(1);       // Need to wait before first restore to allow sequencers time to do their initialization.
	cdSort = spChecker(monFlag,cdTableList,wcVal,wcstring.c_str(),1,&status);

	// Start Infinite Loop 		*******************************************************************************
	for(;;) {
		usleep(100000);					// Run loop at 10Hz.
#ifdef CA_SDF
		{
			// even with a pre-emptive callback enabled we need to do a ca_poll to get writes to work
			// see bug 965
			ca_poll();
			syncCAConnections(NULL);
            disconnectcount_channel.set(chDisconnectedCount);
            droppedcount_channel.set(droppedPVCount);
		}
#endif
		fivesectimer = (fivesectimer + 1) % 50;		// Increment 5 second timer for triggering CRC checks.
		// Check for reload request
        reload_channel.get(request);

		// Get BURT Read File Name
        sdfname_channel.get(sdf);
		////status = dbNameToAddr(sdfFileName,&sdfname_addr);
		////status = dbGetField(&sdfname_addr,DBR_STRING,sdf,&ropts,&nvals,NULL);

		//  Create full filename including directory and extension.
        sdfile.printf("%s%s%s", sdfDir, sdf.c_str(), ".snap");
        sdalarmfile.printf("%s%s%s", sdfDir, sdf.c_str(), "_alarms.snap");

		// Check if file name != to one presently loaded

        if (sdf != loadedSdf) {
            burtstatus |= 1;
        }
        else
        {
            burtstatus &= ~(1);
        }
		//if(strcmp(sdf,loadedSdf) != 0) burtstatus |= 1;
		//else burtstatus &= ~(1);

		if(burtstatus == 0) {
            msgstr_channel.set(" ");
        }
		if(burtstatus == 1) {
            msgstr_channel.set("New SDF File Pending");
        }
		if(burtstatus & 2) {
            msgstr_channel.set("Read Error: Errant line(s) in file");
        }
		if(burtstatus & 4) {
            msgstr_channel.set("Read Error: File Not Found");
        }
		if(request != 0) {		// If there is a read file request, then:
			reload_channel.set(ropts);
			reqValid = 1;
			if(reqValid) {
				rdstatus = readConfig(pref,sdfile.c_str(),request,sdalarmfile.c_str());
				resyncFMArrays(filterMasks,fmMaskChan);
				if (rdstatus) burtstatus |= rdstatus;
				else burtstatus &= ~(6);
				if(burtstatus < 4) {
					switch (request){
						case SDF_LOAD_DB_ONLY:
                            loadedSdf = sdf;
                            edbloaded_channel.set(loadedSdf);
							break;
						case SDF_RESET:
							break;
						case SDF_LOAD_PARTIAL:
							loadedSdf = sdf;
                            loadedfile_channel.set(loadedSdf);
                            edbloaded_channel.set(loadedSdf);
							break;
						case SDF_READ_ONLY:
							loadedSdf = sdf;
                            loadedfile_channel.set(loadedSdf);
							break;
						default:
							logFileEntry("Invalid READ Request");
							reqValid = 0;
							break;
					}
                    reload_stat.set(rdstatus);
					// Get the file CRC for later checking if file changed.
					sdffileloaded.printf("%s%s%s", sdfDir, loadedSdf.c_str(), ".snap");
					sdfFileCrc = checkFileCrc(sdffileloaded.c_str());
                    // Get the file mod time
                    status = checkFileMod( sdffileloaded.c_str(), &sdfFileMt, 1 );
					// Calculate and report the number of settings in the BURT file.
					setChans = chNumP - alarmCnt;
                    filesetcnt_channel.set(setChans);
					// Report number of settings in the main table.
					setChans = chNum - fmNum;
                    fulldbcnt_channel.set(setChans);
					// Sort channels for data reporting via the MEDM table.
					getEpicsSettings();
					noMon = createSortTableEntries(chNum,0,"",&noInit,nullptr);
					// Calculate and report number of channels NOT being monitored.
					unmonchancnt_channel.set(chNotMon);
					alrmchcount_channel.set(alarmCnt);
					// Report number of channels in BURT file that are not in local database.
					chnotfound_channel.set(chNotFound);
					// Report number of channels that have not been initialized via a BURT read.
					chnotinit_channel.set(chNotInit);
					// Write out local monitoring table as snap file.
					writeTable2File(sdfDir,bufile.c_str(),SDF_WITH_INIT_FLAG,cdTable);
				}
			}
		}
        reload_stat.set(burtstatus);

		// sleep(1);
		// Check for SAVE requests
        savecmd_channel.get(sdfSaveReq);
		if(sdfSaveReq)	// If there is a SAVE file request, then:
		{
			// Clear the save file request
            savecmd_channel.set(ropts);
			// Determine file type
            savetype_channel.get(saveTypeString);
			saveType = 0;
            if(saveTypeString == "TABLE TO FILE") {
                saveType = SAVE_TABLE_AS_SDF;
            }
            if(saveTypeString == "EPICS DB TO FILE") {
                saveType = SAVE_EPICS_AS_SDF;
            }
			// Determine file options
                        saveOpts = 0;
			saveopts_channel.get(saveOptsString);
            if(saveOptsString == "TIME NOW") saveOpts = SAVE_TIME_NOW;
            if(saveOptsString == "OVERWRITE") saveOpts = SAVE_OVERWRITE;
            if(saveOptsString == "SAVE AS") saveOpts = SAVE_AS;
			// Determine if request is valid.
			if(saveType && saveOpts)
			{
				// Get saveas filename
                saveas_channel.get(saveasfilename);
				if(saveOpts == SAVE_OVERWRITE) {
                    savesdffile(saveType, SAVE_TIME_NOW, sdfDir, modelname, sdfile.c_str(), saveasfilename.c_str(), loadedSdf.c_str(),
                                savefile_channel, savetime_channel, reloadtime_channel);
                }
				// Save the file
				savesdffile(saveType,saveOpts,sdfDir,modelname,sdfile.c_str(),saveasfilename.c_str(),loadedSdf.c_str(),savefile_channel,savetime_channel,reloadtime_channel);
				if(saveOpts == SAVE_OVERWRITE)  {
						sdfFileCrc = checkFileCrc(sdffileloaded.c_str());
                        status = checkFileMod( sdffileloaded.c_str(), &sdfFileMt, 1 );
                }
			} else {
				logFileEntry("Invalid SAVE File Request");
			}
		}
		// Check present settings vs BURT settings and report diffs.
		// Check if MON ALL CHANNELS is set
        monflag_channel.get(monFlag);
		wcstring_channel.get(wcstring);
		saveasfilename = wcstring;
		// Call the diff checking function.
		if(!freezeTable)
			sperror = spChecker(monFlag,setErrTable,wcVal,wcstring.c_str(),0,&diffCnt);
		// Report number of diffs found.
        sperror_channel.set(diffCnt);
		// Table sorting and presentation
        tablesortreq_channel.set(tsrVal);
        wcreq_channel.set(wcVal);
		status = dbGetField(&pagereqaddr,DBR_LONG,&pageNumSet,&ropts,&nvals,NULL);
		status = dbGetField(&confirmwordaddr,DBR_LONG,&confirmVal,&ropts,&nvals,NULL);
		select_channel[3].get(selectAll);
		if(pageNumSet != 0) {
			pageNum += pageNumSet;
			if(pageNum < 0) pageNum = 0;
			status = dbPutField(&pagereqaddr,DBR_LONG,&ropts,1);                // Init to zero.
		}
        countDisconnectedChannels(tsrVal);
		switch(tsrVal) {
			case SDF_TABLE_DIFFS:
				// Need to clear selections when moving between tables.
				if(lastTable !=  SDF_TABLE_DIFFS) {
					clearTableSelections(setErrTable, selectCounter);
					resyncFMArrays(filterMasks,fmMaskChan);
					confirmVal = 0;
				}
				pageDisp = reportSetErrors(pref, setErrTable,pageNum,1);
				currentTable = make_range(setErrTable);

                sorttableentries_channel.set(sperror);
                resetone_channel.get(resetNum);
				if(selectAll) {
					setAllTableSelections(make_range(setErrTable), selectCounter,selectAll);
				}
				if(resetNum) {
					decodeChangeSelect(resetNum, pageDisp, make_range(setErrTable),selectCounter, NULL);
				}
				if(confirmVal) {
					if(selectCounter[0] && (confirmVal & 2)) status = resetSelectedValues(setErrTable);
					if((selectCounter[1] || selectCounter[2]) && (confirmVal & 2)) {
						// Save present table as timenow.
                        loadedfile_channel.get(backupName);
						// printf("BACKING UP: %s\n",backupName);
						savesdffile(SAVE_TABLE_AS_SDF,SAVE_TIME_NOW,sdfDir,modelname,sdfile.c_str(),saveasfilename.c_str(),
                                    backupName.c_str(),savefile_channel,savetime_channel,reloadtime_channel);
						// Overwrite the table with new values
						status = modifyTable(make_range(setErrTable));
						// Overwrite file
						savesdffile(SAVE_TABLE_AS_SDF,SAVE_OVERWRITE,sdfDir,modelname,sdfile.c_str(),saveasfilename.c_str(),
							    backupName.c_str(),savefile_channel,savetime_channel,reloadtime_channel);
						sdfFileCrc = checkFileCrc(sdffileloaded.c_str());
                        status = checkFileMod( sdffileloaded.c_str(), &sdfFileMt, 1 );
						writeTable2File(sdfDir,bufile.c_str(),SDF_WITH_INIT_FLAG,cdTable);
					}
					clearTableSelections(setErrTable, selectCounter);
					resyncFMArrays(filterMasks,fmMaskChan);
					confirmVal = 0;
					status = dbPutField(&confirmwordaddr,DBR_LONG,&confirmVal,1);
					noMon = createSortTableEntries(chNum,wcVal,wcstring.c_str(),&noInit,NULL);
                    unmonchancnt_channel.set(chNotMon);
				}
				lastTable = SDF_TABLE_DIFFS;
				break;
			case SDF_TABLE_NOT_FOUND:
				// Need to clear selections when moving between tables.
				if(lastTable != SDF_TABLE_NOT_FOUND) {
					clearTableSelections(setErrTable, selectCounter);
					resyncFMArrays(filterMasks,fmMaskChan);
					confirmVal = 0;
				}
				pageDisp = reportSetErrors(pref, unknownChans,pageNum,1);
				currentTable = make_range(unknownChans);

                sorttableentries_channel.set(chNotFound);
				/*if (resetNum > 200) {
					decodeChangeSelect(resetNum, pageDisp, make_range(unknownChans),selectCounter, NULL);
				}*/
				lastTable =  SDF_TABLE_NOT_FOUND;
				break;
			case SDF_TABLE_NOT_INIT:
				if(lastTable != SDF_TABLE_NOT_INIT) {
					clearTableSelections(setErrTable, selectCounter);
					resyncFMArrays(filterMasks,fmMaskChan);
					confirmVal = 0;
				}
				if (!freezeTable)
					getEpicsSettings();
				noMon = createSortTableEntries(chNum,wcVal,wcstring.c_str(),&noInit,NULL);
				pageDisp = reportSetErrors(pref, uninitChans,pageNum,1);
				currentTable = make_range(uninitChans);
                resetone_channel.get(resetNum);
				if(selectAll == 2 || selectAll == 3) {
					setAllTableSelections(make_range(uninitChans),selectCounter,selectAll);
					if (selectAll == 3)
						setAllTableSelections(make_range(uninitChans),selectCounter,2);
				}
				if(resetNum > 100) {
					decodeChangeSelect(resetNum, pageDisp, make_range(uninitChans),selectCounter, changeSelectCB_uninit);
				}
				if(confirmVal) {
					if(selectCounter[0] && (confirmVal & 2)) status = resetSelectedValues(uninitChans);
					if(selectCounter[1] && (confirmVal & 2)) {
						// Save present table as timenow.
						loadedfile_channel.get(backupName);
						// printf("BACKING UP: %s\n",backupName);
						savesdffile(SAVE_TABLE_AS_SDF,SAVE_TIME_NOW,sdfDir,modelname,sdfile.c_str(),saveasfilename.c_str(),backupName.c_str(),
							    savefile_channel,savetime_channel,reloadtime_channel);
						// Overwrite the table with new values
						status = modifyTable(make_range(uninitChans));
						// Overwrite file
						savesdffile(SAVE_TABLE_AS_SDF,SAVE_OVERWRITE,sdfDir,modelname,sdfile.c_str(),saveasfilename.c_str(),
							    backupName.c_str(),savefile_channel,savetime_channel,reloadtime_channel);
						sdfFileCrc = checkFileCrc(sdffileloaded.c_str());
                        status = checkFileMod( sdffileloaded.c_str(), &sdfFileMt, 1 );
						writeTable2File(sdfDir,bufile.c_str(),SDF_WITH_INIT_FLAG,cdTable);
					}
					clearTableSelections(uninitChans, selectCounter);
					resyncFMArrays(filterMasks,fmMaskChan);
					confirmVal = 0;
					status = dbPutField(&confirmwordaddr,DBR_LONG,&confirmVal,1);
				}
                sorttableentries_channel.set(noInit);
				chnotinit_channel.set(chNotInit);
				lastTable = SDF_TABLE_NOT_INIT;
				break;
			case SDF_TABLE_NOT_MONITORED:
				D("In not mon\n");
				if(lastTable != SDF_TABLE_NOT_MONITORED) {
					clearTableSelections(unMonChans, selectCounter);
					resyncFMArrays(filterMasks,fmMaskChan);
					confirmVal = 0;
					status = dbPutField(&confirmwordaddr,DBR_LONG,&confirmVal,1);
				}
				if (!freezeTable)
					getEpicsSettings(); //timeTable);
				noMon = createSortTableEntries(chNum,wcVal,wcstring.c_str(),&noInit,NULL);//timeTable);
				unmonchancnt_channel.set(chNotMon);
				pageDisp = reportSetErrors(pref, unMonChans,pageNum,1);
				currentTable = make_range(unMonChans);
                resetone_channel.get(resetNum);
				if(selectAll) {
					setAllTableSelections(make_range(unMonChans),selectCounter,selectAll);
				}
				if(resetNum) {
					decodeChangeSelect(resetNum, pageDisp, make_range(unMonChans),selectCounter, NULL);
				}
				if(confirmVal) {
					if(selectCounter[0] && (confirmVal & 2)) status = resetSelectedValues(unMonChans);
					if((selectCounter[1] || selectCounter[2]) && (confirmVal & 2)) {
						// Save present table as timenow.
                        loadedfile_channel.get(backupName);
						// printf("BACKING UP: %s\n",backupName);
						savesdffile(SAVE_TABLE_AS_SDF,SAVE_TIME_NOW,sdfDir,modelname,sdfile.c_str(),saveasfilename.c_str(),backupName.c_str(),
							    savefile_channel,savetime_channel,reloadtime_channel);
						// Overwrite the table with new values
						status = modifyTable(make_range(unMonChans));
						// Overwrite file
						savesdffile(SAVE_TABLE_AS_SDF,SAVE_OVERWRITE,sdfDir,modelname,sdfile.c_str(),saveasfilename.c_str(),
							    backupName.c_str(),savefile_channel,savetime_channel,reloadtime_channel);
						sdfFileCrc = checkFileCrc(sdffileloaded.c_str());
                        status = checkFileMod( sdffileloaded.c_str(), &sdfFileMt, 1 );
						writeTable2File(sdfDir,bufile.c_str(),SDF_WITH_INIT_FLAG,cdTable);
					}
					// noMon = createSortTableEntries(chNum,wcVal,wcstring);
					clearTableSelections(unMonChans, selectCounter);
					resyncFMArrays(filterMasks,fmMaskChan);
					confirmVal = 0;
					status = dbPutField(&confirmwordaddr,DBR_LONG,&confirmVal,1);
					// Calculate and report number of channels NOT being monitored.
				}
                sorttableentries_channel.set(noMon);
				lastTable = SDF_TABLE_NOT_MONITORED;
				break;
			case SDF_TABLE_FULL:
				if(lastTable != SDF_TABLE_FULL) {
					clearTableSelections(cdTableList, selectCounter);
					resyncFMArrays(filterMasks,fmMaskChan);
					confirmVal = 0;
				}
				if (!freezeTable)
					cdSort = spChecker(monFlag,cdTableList,wcVal,wcstring.c_str(),1,&status);
				pageDisp = reportSetErrors(pref, cdTableList,pageNum,1);
				currentTable = make_range(cdTableList);
                resetone_channel.get(resetNum);
				if(selectAll == 3) {
					setAllTableSelections(make_range(cdTableList),selectCounter,selectAll);
				}
				if(resetNum) {
					decodeChangeSelect(resetNum, pageDisp, make_range(cdTableList),selectCounter, NULL);
				}
				if(confirmVal) {
					if(selectCounter[0] && (confirmVal & 2)) status = resetSelectedValues(cdTableList);
					if((selectCounter[1] || selectCounter[2]) && (confirmVal & 2)) {
						// Save present table as timenow.
                        loadedfile_channel.get(backupName);
						// printf("BACKING UP: %s\n",backupName);
						savesdffile(SAVE_TABLE_AS_SDF,SAVE_TIME_NOW,sdfDir,modelname,sdfile.c_str(),saveasfilename.c_str(),
							    backupName.c_str(),savefile_channel,savetime_channel,reloadtime_channel);
						// Overwrite the table with new values
						status = modifyTable(make_range(cdTableList));
						// Overwrite file
						savesdffile(SAVE_TABLE_AS_SDF,SAVE_OVERWRITE,sdfDir,modelname,sdfile.c_str(),saveasfilename.c_str(),
							    backupName.c_str(),savefile_channel,savetime_channel,reloadtime_channel);
						sdfFileCrc = checkFileCrc(sdffileloaded.c_str());
                        status = checkFileMod( sdffileloaded.c_str(), &sdfFileMt, 1 );
						writeTable2File(sdfDir,bufile.c_str(),SDF_WITH_INIT_FLAG,cdTable);
					}
					clearTableSelections(cdTableList, selectCounter);
					resyncFMArrays(filterMasks,fmMaskChan);
					confirmVal = 0;
					status = dbPutField(&confirmwordaddr,DBR_LONG,&confirmVal,1);
					noMon = createSortTableEntries(chNum,wcVal,wcstring.c_str(),&noInit,NULL);
					// Calculate and report number of channels NOT being monitored.
					unmonchancnt_channel.set(chNotMon);
				}
                sorttableentries_channel.set(cdSort);
				lastTable = SDF_TABLE_FULL;
				break;
#ifdef CA_SDF
			case SDF_TABLE_DISCONNECTED:
				if(lastTable != SDF_TABLE_DISCONNECTED) {
					clearTableSelections(cdTableList, selectCounter);
					resyncFMArrays(filterMasks,fmMaskChan);
					confirmVal = 0;
				}
				noMon = createSortTableEntries(chNum,wcVal,wcstring.c_str(),&noInit,NULL);
				pageDisp =  reportSetErrors(pref, disconnectChans, pageNum,0);
				chDisconnectedCount = chDisconnected;
				currentTable = make_range(disconnectChans);
                sorttableentries_channel.set(chDisconnected);
				if (resetNum > 200) {
					decodeChangeSelect(resetNum, pageDisp, make_range(disconnectChans), selectCounter, NULL);
				}
				break;
#endif
			default:
				pageDisp = reportSetErrors(pref, setErrTable,pageNum,1);
				sorttableentries_channel.set(sperror);
				currentTable = make_range(setErrTable);
				break;
		}
		if (selectAll) {
			selectAll = 0;
            select_channel[3].set(selectAll);
		}
		if (resetNum) {
			resetNum = 0;
            resetone_channel.set(resetNum);
		}
		if(pageDisp != pageNum) {
			pageNum = pageDisp;
		}
		freezeTable = 0;
		for(ii=0;ii<3;ii++) {
			freezeTable += selectCounter[ii];
            select_channel[ii].set(selectCounter[ii]);
		}
        pagelock_channel.set(freezeTable);

		if (loadedSdf == "safe") {
			sdf_type = SDF_FILE_SAFE;
		}
		else if (loadedSdf == "OBSERVE") {
			sdf_type = SDF_FILE_OBSERVE;
		}
		else {
			sdf_type = SDF_FILE_UNKNOWN;
		}
		sdftype_channel.set(sdf_type);


		processFMChanCommands(filterMasks, fmMaskChan,fmMaskChanCtrl,selectCounter, currentTable);

		// Check file CRCs every 5 seconds.
		// DAQ and COEFF file checking was moved from skeleton.st to here RCG V2.9.
		if(!fivesectimer) {
            // Check DAQ ini file modified
            int fm_flag = checkFileMod( daqFile, &daqFileMt, 0 );
            if ( fm_flag )
            {
			status = checkFileCrc(daqFile);
			if(status != daqFileCrc) {
				daqFileCrc = status;
                daqmsg_channel.set(modfilemsg);
				logFileEntry("Detected Change to DAQ Config file.");
			}
            }
            // Check Foton file modified
            fm_flag = checkFileMod( fotonFile, &fotonFileMt, 0 );
            int fm_flag1 = checkFileMod( coeffFile, &coeffFileMt, 0 );
            if ( fm_flag  || fm_flag1)
            {
			coeffFileCrc = checkFileCrc(coeffFile);
			fotonFileCrc = checkFileCrc(fotonFile);
			if(fotonFileCrc != coeffFileCrc) {
                coeffmsg_channel.set(modfilemsg);
			} else {
                coeffmsg_channel.set("");
			}
			if(fotonFileCrc != prevFotonFileCrc || prevCoeffFileCrc != coeffFileCrc) {
				sprintf(myDiffCmd,"%s %s %s %s %s","diff",fotonFile,coeffFile," > ",fotonDiffFile);
				status = system(myDiffCmd);
				prevFotonFileCrc = fotonFileCrc;
				prevCoeffFileCrc = coeffFileCrc;
			}
            }
            // Check SDF file modified
            fm_flag = checkFileMod( sdffileloaded.c_str(), &sdfFileMt, 0 );
            if ( fm_flag )
            {
			status = checkFileCrc(sdffileloaded.c_str());
			if(status == -1) {
				sdfFileCrc = status;
				logFileEntry("SDF file not found.");
			}
			if(status != sdfFileCrc) {
				sdfFileCrc = status;
				logFileEntry("Detected Change to SDF file.");
				reloadtime_channel.set(modfilemsg);
			}
            }
		}
		iterate_output_counter();
	}
	sleep(0xfffffff);
    } else
    	iocsh(NULL);
    CLEANUP();
    return(0);
}
