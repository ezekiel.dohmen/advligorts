//
// Created by jonathan.hanks on 11/13/20.
//

#include "catch.hpp"

#include "fixed_size_vector.hh"

#include <algorithm>

TEST_CASE( "You can create a fixed size vector" )
{
    embedded::fixed_size_vector< int, 20 > v0;

    REQUIRE( v0.size( ) == 0 );
    REQUIRE( v0.empty( ) );
    REQUIRE( v0.capacity( ) == 20 );
    REQUIRE( v0.begin( ) == v0.end( ) );

    const embedded::fixed_size_vector< int, 20 > v1;
    REQUIRE( v1.begin( ) == v1.end( ) );
    REQUIRE( v1.cbegin( ) == v1.cend( ) );
    REQUIRE( v1.begin( ) == v1.cbegin( ) );
    REQUIRE( v1.end( ) == v1.cend( ) );

    static_assert(
        std::is_same<
            int*,
            typename embedded::fixed_size_vector< int, 20 >::iterator >::value,
        "The iterator is a pointer" );
    static_assert(
        std::is_same< const int*,
                      typename embedded::fixed_size_vector< int, 20 >::
                          const_iterator >::value,
        "The iterator is pointer to const" );
}

TEST_CASE( "You can add elements to the back of a fixed size vector" )
{
    embedded::fixed_size_vector< int, 20 > v0;
    REQUIRE( v0.push_back( 5 ) );
    REQUIRE( v0.size( ) == 1 );
    REQUIRE( v0.capacity( ) == 20 );
    REQUIRE( v0.push_back( 6 ) );
    REQUIRE( v0.size( ) == 2 );
    REQUIRE( v0.capacity( ) == 20 );
    REQUIRE( v0.push_back( 7 ) );
    REQUIRE( v0.size( ) == 3 );
    REQUIRE( v0.capacity( ) == 20 );
    REQUIRE( v0.end( ) == v0.begin( ) + v0.size( ) );
    REQUIRE( *v0.begin( ) == 5 );
    REQUIRE( *( v0.begin( ) + 1 ) == 6 );
    REQUIRE( *( v0.begin( ) + 2 ) == 7 );
}

TEST_CASE( "You can add elements to the back of a fixed size vector with "
           "emplace_back" )
{
    embedded::fixed_size_vector< int, 20 > v0;
    REQUIRE( v0.emplace_back( 5 ) );
    REQUIRE( v0.size( ) == 1 );
    REQUIRE( v0.capacity( ) == 20 );
    REQUIRE( v0.emplace_back( 6 ) );
    REQUIRE( v0.size( ) == 2 );
    REQUIRE( v0.capacity( ) == 20 );
    REQUIRE( v0.emplace_back( 7 ) );
    REQUIRE( v0.size( ) == 3 );
    REQUIRE( v0.capacity( ) == 20 );
    REQUIRE( v0.end( ) == v0.begin( ) + v0.size( ) );
    REQUIRE( *v0.begin( ) == 5 );
    REQUIRE( *( v0.begin( ) + 1 ) == 6 );
    REQUIRE( *( v0.begin( ) + 2 ) == 7 );
}

TEST_CASE( "Attempting to add elements when full returns a false value" )
{
    embedded::fixed_size_vector< int, 20 > v0;
    for ( auto i = 0; i < v0.capacity( ); ++i )
    {
        REQUIRE( v0.push_back( i ) );
    }
    REQUIRE( v0.size( ) == v0.capacity( ) );
    REQUIRE( v0.push_back( 42 ) == false );
    REQUIRE( v0.emplace_back( 43 ) == false );
    REQUIRE( v0.size( ) == v0.capacity( ) );
}

TEST_CASE( "You can access elements via []" )
{
    embedded::fixed_size_vector< int, 20 > v0;
    for ( auto i = 0; i < v0.capacity( ); ++i )
    {
        REQUIRE( v0.push_back( i ) );
    }
    for ( auto i = 0; i < v0.capacity( ); ++i )
    {
        REQUIRE( v0[ i ] == i );
    }
}

TEST_CASE( "You can pop an entry off of the end of a vector" )
{
    embedded::fixed_size_vector< int, 20 > v0;
    for ( auto i = 0; i < v0.capacity( ); ++i )
    {
        REQUIRE( v0.push_back( i ) );
    }
    REQUIRE( v0.end( ) == v0.begin( ) + v0.capacity( ) );
    REQUIRE( v0.size( ) == v0.capacity( ) );

    v0.pop_back( );
    REQUIRE( v0.end( ) == v0.begin( ) + v0.capacity( ) - 1 );
    REQUIRE( v0.size( ) == v0.capacity( ) - 1 );
}

TEST_CASE( "You can clear a fixed sized vector to empty it" )
{
    embedded::fixed_size_vector< int, 20 > v0;
    for ( auto i = 0; i < v0.capacity( ); ++i )
    {
        REQUIRE( v0.push_back( i ) );
    }
    REQUIRE( v0.end( ) == v0.begin( ) + v0.capacity( ) );
    REQUIRE( v0.size( ) == v0.capacity( ) );

    v0.clear( );
    REQUIRE( v0.size( ) == 0 );
    REQUIRE( v0.begin( ) == v0.end( ) );
    REQUIRE( v0.capacity( ) == 20 );
}

class DestructionNotifier
{
public:
    explicit DestructionNotifier( int* counter_dest ) : counter_{ counter_dest }
    {
    }
    DestructionNotifier( const DestructionNotifier& ) noexcept = default;
    DestructionNotifier&
    operator=( const DestructionNotifier& ) noexcept = default;

    ~DestructionNotifier( )
    {
        ( *counter_ )++;
    }

private:
    int* counter_;
};

TEST_CASE( "Destructors are called on items in the fixed size vector when it "
           "is destroyed or elements are popped off" )
{
    int counter = 0;

    {
        embedded::fixed_size_vector< DestructionNotifier, 10 > v0;

        v0.emplace_back( &counter );
        REQUIRE( !v0.empty( ) );
        REQUIRE( counter == 0 );
        v0.pop_back( );
        REQUIRE( v0.empty( ) );
        REQUIRE( counter == 1 );

        counter = 0;
        while ( v0.emplace_back( &counter ) )
        {
        }
        REQUIRE( v0.size( ) == v0.capacity( ) );
    }
    REQUIRE( counter == 10 );
}

TEST_CASE( "We have defined enough to make the this usable by std::algorithms" )
{
    embedded::fixed_size_vector< int, 20 > v0;
    v0.emplace_back( 5 );
    v0.emplace_back( 4 );
    v0.emplace_back( 3 );
    v0.emplace_back( 2 );
    v0.emplace_back( 1 );
    std::sort( v0.begin( ), v0.end( ) );
    REQUIRE( v0[ 0 ] == 1 );
    REQUIRE( v0[ 1 ] == 2 );
    REQUIRE( v0[ 2 ] == 3 );
    REQUIRE( v0[ 3 ] == 4 );
    REQUIRE( v0[ 4 ] == 5 );
}

TEST_CASE( "We can use front()/back() to reference the first/last elements when there are more than one.")
{
    embedded::fixed_size_vector<int, 20> v0;
    v0.push_back(5);
    REQUIRE( v0.front() == v0.back() );
    v0.front() = 42;
    REQUIRE( v0[0] == 42 );
    v0.back() = 44;
    REQUIRE( v0[0] == 44 );
    v0.push_back(10);
    REQUIRE( v0.front() == 44 );
    REQUIRE( v0.back() == 10 );
    REQUIRE( v0[0] == v0.front() );
    REQUIRE( v0[1] == v0.back() );
}

TEST_CASE( "We can use front()/back() in a const setting as well")
{
    embedded::fixed_size_vector<int, 20> v0;
    v0.push_back(5);

    const auto& vconst = v0;
    REQUIRE( vconst.front() == vconst.back() );
}

// static_assert(std::is_trivially_destructible<embedded::fixed_size_vector<int,
// 20> >::value, "For trivial destructable types the fixed sized vector should
// be trivially destructable");
static_assert(
    !std::is_trivially_destructible<
        embedded::fixed_size_vector< DestructionNotifier, 10 > >::value,
    "for non-trivially destructable types the fixed size vector is not "
    "trivially destructable" );