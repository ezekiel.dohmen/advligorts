//
// Created by jonathan.hanks on 8/16/18.
//
#include "catch.hpp"

#include "fixed_size_string.hh"

#include <cstring>
#include <type_traits>
#include <vector>

static_assert(
    std::is_trivially_copyable< embedded::fixed_string< 64 > >::value,
    "Must be trivial" );
static_assert( std::is_standard_layout< embedded::fixed_string< 64 > >::value,
               "Must be standard layout" );
static_assert(
    std::is_nothrow_constructible< embedded::fixed_string< 64 > >::value,
    "Must be nothrow constructable" );
static_assert(
    std::is_nothrow_copy_constructible< embedded::fixed_string< 64 > >::value,
    "Must be nothrow copy constructable" );
static_assert(
    std::is_nothrow_copy_assignable< embedded::fixed_string< 64 > >::value,
    "Must be nothrow copy assignable" );
static_assert( std::is_nothrow_swappable< embedded::fixed_string< 64 > >::value,
               "Must be nothrow swappable" );

TEST_CASE( "You can create a fixed_size_string" )
{

    SECTION( "Create an empty string" )
    {
        embedded::fixed_string< 64 > s;
        REQUIRE( s.capacity( ) == 64 );
        REQUIRE( s.remaining( ) == 63 );
        REQUIRE( sizeof( s ) >= 64 );
        REQUIRE( s.size( ) == 0 );
        REQUIRE( std::strcmp( s.c_str( ), "" ) == 0 );
        REQUIRE( s.c_str( ) == s.data( ) );
    }
    SECTION( "Create a string from a literal" )
    {
        std::vector< const char* > tests;
        tests.push_back( "This is a test" );
        tests.push_back( (char*)0 );
        tests.push_back( "" );
        tests.push_back( " " );
        tests.push_back(
            "123456789012345678901234567890123456789012345678901234567890123" );
        tests.push_back( "12345678901234567890123456789012345678901234567890123"
                         "45678901234" );
        tests.push_back( "12345678901234567890123456789012345678901234567890123"
                         "456789012345" );

        for ( int i = 0; i < tests.size( ); ++i )
        {
            embedded::fixed_string< 64 > s( tests[ i ] );
            REQUIRE( s.capacity( ) == 64 );
            REQUIRE( s.c_str( ) == s.data( ) );
            if ( tests[ i ] )
            {
                if ( strlen( tests[ i ] ) >= s.capacity( ) )
                {
                    REQUIRE( strncmp( s.c_str( ),
                                      tests[ i ],
                                      s.capacity( ) - 1 ) == 0 );
                    REQUIRE( strlen( s.c_str( ) ) == s.capacity( ) - 1 );
                    REQUIRE( s.size( ) == s.capacity( ) - 1 );
                }
                else
                {
                    REQUIRE( strcmp( s.c_str( ), tests[ i ] ) == 0 );
                    REQUIRE( strlen( tests[ i ] ) == s.size( ) );
                }
            }
            else
            {
                REQUIRE( strcmp( s.c_str( ), "" ) == 0 );
                REQUIRE( s.size( ) == 0 );
            }
        }
    }
    SECTION( "Create via copy constructor" )
    {
        embedded::fixed_string< 64 > s0( "Hello World!" );
        embedded::fixed_string< 64 > s1( s0 );
        REQUIRE( s0.size( ) == s1.size( ) );
        REQUIRE( s0.capacity( ) == s1.capacity( ) );
        REQUIRE( strcmp( s0.c_str( ), s1.c_str( ) ) == 0 );
        REQUIRE( strcmp( s0.c_str( ), "Hello World!" ) == 0 );
    }
    SECTION( "Create via copy constructor from different size "
             "embedded::fixed_string using copy constructor" )
    {
        embedded::fixed_string< 32 > s0( "Hello World!" );
        embedded::fixed_string< 64 > s1( s0 );
        REQUIRE( s0.size( ) == s1.size( ) );
        REQUIRE( s0.capacity( ) == 32 );
        REQUIRE( s1.capacity( ) == 64 );
        REQUIRE( strcmp( s0.c_str( ), s1.c_str( ) ) == 0 );
        REQUIRE( strcmp( s0.c_str( ), "Hello World!" ) == 0 );
    }
    SECTION( "Create via copy constructor to a smaller size "
             "embedded::fixed_string using copy constructor" )
    {
        embedded::fixed_string< 32 > s0( "Hello World!" );
        embedded::fixed_string< 6 >  s1( s0 );
        REQUIRE( s0.size( ) == 12 );
        REQUIRE( s1.size( ) == 5 );
        REQUIRE( s0.capacity( ) == 32 );
        REQUIRE( s1.capacity( ) == 6 );
        REQUIRE( strcmp( s0.c_str( ), s1.c_str( ) ) != 0 );
        REQUIRE( strcmp( s1.c_str( ), "Hello" ) == 0 );
    }
    SECTION( "Create via copy constructor from different size "
             "embedded::fixed_string using c_str()" )
    {
        embedded::fixed_string< 32 > s0( "Hello World!" );
        embedded::fixed_string< 64 > s1( s0.c_str( ) );
        REQUIRE( s0.size( ) == s1.size( ) );
        REQUIRE( s0.capacity( ) == 32 );
        REQUIRE( s1.capacity( ) == 64 );
        REQUIRE( strcmp( s0.c_str( ), s1.c_str( ) ) == 0 );
        REQUIRE( strcmp( s0.c_str( ), "Hello World!" ) == 0 );
    }
    SECTION( "Copy operators should work" )
    {
        embedded::fixed_string< 64 > s;
        REQUIRE( s.size( ) == 0 );
        embedded::fixed_string< 64 > other( "Hello World!" );
        s = other;
        REQUIRE( s.size( ) == other.size( ) );
        REQUIRE( strcmp( s.c_str( ), other.c_str( ) ) == 0 );

        const char* goodbye = "Goodbye World!";
        s = goodbye;
        REQUIRE( s.size( ) == strlen( goodbye ) );
        REQUIRE( strcmp( s.c_str( ), goodbye ) == 0 );

        std::vector< const char* > tests;
        tests.push_back( "This is a test" );
        tests.push_back( (char*)0 );
        tests.push_back( "" );
        tests.push_back( " " );
        tests.push_back(
            "123456789012345678901234567890123456789012345678901234567890123" );
        tests.push_back( "12345678901234567890123456789012345678901234567890123"
                         "45678901234" );
        tests.push_back( "12345678901234567890123456789012345678901234567890123"
                         "456789012345" );

        for ( int i = 0; i < tests.size( ); ++i )
        {
            s = tests[ i ];
            if ( tests[ i ] )
            {
                if ( strlen( tests[ i ] ) >= s.capacity( ) )
                {
                    REQUIRE( strncmp( s.c_str( ),
                                      tests[ i ],
                                      s.capacity( ) - 1 ) == 0 );
                    REQUIRE( strlen( s.c_str( ) ) == s.capacity( ) - 1 );
                    REQUIRE( s.size( ) == s.capacity( ) - 1 );
                }
                else
                {
                    REQUIRE( strcmp( s.c_str( ), tests[ i ] ) == 0 );
                    REQUIRE( strlen( tests[ i ] ) == s.size( ) );
                }
            }
            else
            {
                REQUIRE( strcmp( s.c_str( ), "" ) == 0 );
                REQUIRE( s.size( ) == 0 );
            }
        }
    }
}

TEST_CASE( "Copy operators work even when the sizes are different, it just may "
           "truncate" )
{
    embedded::fixed_string< 64 > s0( "123456" );
    embedded::fixed_string< 32 > s1;
    embedded::fixed_string< 6 >  s2;
    embedded::fixed_string< 64 > s3( "123456" );

    s1 = s0;
    REQUIRE( s1 == "123456" );
    s2 = s0;
    REQUIRE( s2 == "12345" );
    s3 = s2;
    REQUIRE( s3 == "12345" );
}

TEST_CASE( "You can append to a fixed size string" )
{
    embedded::fixed_string< 64 > s;
    embedded::fixed_string< 6 >  single( "0" );
    embedded::fixed_string< 6 >  five( "00000" );

    for ( int i = 1; i <= 100; ++i )
    {
        s += '0';
        std::size_t expected_zeros =
            ( i < s.capacity( ) ? i : s.capacity( ) - 1 );
        REQUIRE( s.size( ) == expected_zeros );
        std::size_t j = 0;
        for ( ; j < expected_zeros && s.c_str( )[ j ] == '0'; ++j )
        {
        }
        REQUIRE( j == expected_zeros );
    }
    s.clear( );
    for ( int i = 1; i <= 100; ++i )
    {
        s += "0";
        std::size_t expected_zeros =
            ( i < s.capacity( ) ? i : s.capacity( ) - 1 );
        REQUIRE( s.size( ) == expected_zeros );
        std::size_t j = 0;
        for ( ; j < expected_zeros && s.c_str( )[ j ] == '0'; ++j )
        {
        }
        REQUIRE( j == expected_zeros );
    }
    s = "";
    for ( int i = 1; i <= 100; ++i )
    {
        s += "00000";
        std::size_t expected_zeros =
            ( i * 5 < s.capacity( ) ? i * 5 : s.capacity( ) - 1 );
        REQUIRE( s.size( ) == expected_zeros );
        std::size_t j = 0;
        for ( ; j < expected_zeros && s.c_str( )[ j ] == '0'; ++j )
        {
        }
        REQUIRE( j == expected_zeros );
    }
    s = "";
    for ( int i = 1; i <= 100; ++i )
    {
        s += single;
        std::size_t expected_zeros =
            ( i < s.capacity( ) ? i : s.capacity( ) - 1 );
        REQUIRE( s.size( ) == expected_zeros );
        std::size_t j = 0;
        for ( ; j < expected_zeros && s.c_str( )[ j ] == '0'; ++j )
        {
        }
        REQUIRE( j == expected_zeros );
    }
    s = "";
    for ( int i = 1; i <= 100; ++i )
    {
        s += five;
        std::size_t expected_zeros =
            ( i * 5 < s.capacity( ) ? i * 5 : s.capacity( ) - 1 );
        REQUIRE( s.size( ) == expected_zeros );
        std::size_t j = 0;
        for ( ; j < expected_zeros && s.c_str( )[ j ] == '0'; ++j )
        {
        }
        REQUIRE( j == expected_zeros );
    }
    s = "abab";
    s += s;
    REQUIRE( s.size( ) == 8 );
    REQUIRE( strcmp( s.c_str( ), "abababab" ) == 0 );
    REQUIRE( s.remaining( ) == 63 - 8 );
}

TEST_CASE( "You can do printf style formatted printing" )
{
    embedded::fixed_string< 64 > s;
    s.printf( "%d", 123456 );
    REQUIRE( strcmp( s.c_str( ), "123456" ) == 0 );
    s.printf( "%s", "abcdefg" );
    REQUIRE( strcmp( s.c_str( ), "abcdefg" ) == 0 );
    s.printf(
        "%s",
        "12345678901234567890123456789012345678901234567890123456789012345" );
    REQUIRE( s.size( ) == 63 );
    REQUIRE( strcmp( s.c_str( ),
                     "123456789012345678901234567890123456789012345678901234567"
                     "890123" ) == 0 );
    s.printf(
        "%d %s",
        -1,
        "12345678901234567890123456789012345678901234567890123456789012345" );
    REQUIRE( s.size( ) == 63 );
    REQUIRE(
        strcmp(
            s.c_str( ),
            "-1 "
            "123456789012345678901234567890123456789012345678901234567890" ) ==
        0 );
}

TEST_CASE( "You can check for a zero length string by using empty()" )
{
    embedded::fixed_string< 64 > s0( "" );
    REQUIRE( s0.empty( ) );

    s0 = "abc";
    REQUIRE( !s0.empty( ) );
}

TEST_CASE( "You can use begin/end on a fixed_size_string" )
{
    embedded::fixed_string< 64 > s0( "" );

    REQUIRE( s0.begin( ) == s0.end( ) );
    REQUIRE( s0.begin( ) == s0.c_str( ) );

    s0 = "a";
    REQUIRE( s0.end( ) == s0.begin( ) + 1 );
    REQUIRE( s0.begin( ) == s0.c_str( ) );

    s0 = "abc";
    REQUIRE( s0.end( ) == s0.begin( ) + 3 );
    REQUIRE( s0.begin( ) == s0.c_str( ) );
}

TEST_CASE( "You can assign and append via char arrays" )
{
    embedded::fixed_string< 64 > s0( "123456" );
    char                         blob[ 1 ][ 8 ] = { "abc" };

    s0 += blob[ 0 ];
    REQUIRE( s0 == "123456abc" );
}

TEST_CASE( "clear will reset the fixed_string to be empty" )
{
    embedded::fixed_string< 64 > s0( "123456" );
    REQUIRE( s0.size( ) > 0 );
    s0.clear( );
    REQUIRE( s0.size( ) == 0 );
    REQUIRE( s0 == "" );
}

TEST_CASE(
    "printf with a null format string is equal to a format string of \"\"" )
{
    embedded::fixed_string< 64 > s0( "123456" );
    REQUIRE( s0.printf( nullptr ) == 0 );
    REQUIRE( s0 == "" );
}

TEST_CASE(
    "You can compare with a fixed_string using operator== and operator!=" )
{
    embedded::fixed_string< 64 > s0( "123456" ), s1( "123456789" );

    REQUIRE( s0 == s0 );
    REQUIRE( s0 != s1 );
}

TEST_CASE( "You can compare with a fixed_string using operator== and "
           "operator!= even when capacities are different" )
{
    embedded::fixed_string< 64 > s0( "1234" ), s1( "123456789" );
    embedded::fixed_string< 6 >  s2( "1234" );

    REQUIRE( s0 == s2 );
    REQUIRE( s1 != s2 );
}

TEST_CASE( "You can compare with a char* using operator== and operator!=" )
{
    embedded::fixed_string< 64 > s0( "123456" );

    REQUIRE( s0 == "123456" );
    REQUIRE( s0 != "1234567890" );
}

TEST_CASE( "It is safe to compare with a null char*, it matches an empty "
           "fixed_string" )
{
    embedded::fixed_string< 64 > s0( "123456" );
    embedded::fixed_string< 64 > s1;
    REQUIRE( s0.size( ) > 0 );
    REQUIRE( s1.size( ) == 0 );
    REQUIRE( s0 != nullptr );
    REQUIRE( s1 == "" );
    REQUIRE( s1 == nullptr );
}

TEST_CASE( "You can remove characters from the end" )
{
    embedded::fixed_string< 64 > s0( "1234567890" );

    REQUIRE( s0.size( ) == 10 );
    s0.pop_back_n( 4 );
    REQUIRE( s0.size( ) == 6 );
    REQUIRE( s0 == "123456" );
    s0.pop_back_n( 1 );
    REQUIRE( s0.size( ) == 5 );
    s0.pop_back_n( 10 );
    REQUIRE( s0.size( ) == 0 );
}

TEST_CASE( "For direct manipulation you can get a buffer which cleans up when "
           "it is done" )
{
    embedded::fixed_string< 10 > s0( "abc" );

    {
        auto buf = s0.get_buffer( );
        std::fill( buf.data( ), buf.data( ) + buf.capacity( ), 1 );
        REQUIRE( buf.data( )[ buf.capacity( ) - 1 ] == 1 );
        REQUIRE( s0.data( )[ s0.capacity( ) - 1 ] == 1 );
    }
    REQUIRE( s0.data( )[ s0.capacity( ) - 1 ] == '\0' );
}