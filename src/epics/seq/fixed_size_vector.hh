//
// Created by jonathan.hanks on 11/13/20.
//

#ifndef DAQD_TRUNK_FIXED_SIZE_VECTOR_HH
#define DAQD_TRUNK_FIXED_SIZE_VECTOR_HH

#include <cstddef>
#include <type_traits>
#include <utility>

namespace embedded
{

    /*!
     * @brief a simple vector with a fixed backing store.
     * @details
     * @tparam T the type being stored, some constraints are place on T to keep
     * the vector noexcept
     * @tparam max_size
     */
    template < typename T, std::size_t max_size >
    class fixed_size_vector
    {
        static_assert( std::is_nothrow_copy_constructible< T >::value,
                       "T must be no throw copy constructable" );
        static_assert( std::is_nothrow_copy_assignable< T >::value,
                       "T must be no throw copy assignable" );
        static_assert( std::is_nothrow_move_constructible< T >::value,
                       "T must be no throw move constructable" );
        static_assert( std::is_nothrow_move_assignable< T >::value,
                       "T must be no throw move assignable" );
        static_assert( std::is_nothrow_destructible< T >::value,
                       "T must be no throw destructable" );

        using storage_type =
            typename std::aligned_storage< sizeof( T ), alignof( T ) >::type;

    public:
        using value_type = T;
        using size_type = std::size_t;
        using iterator = typename std::add_pointer< T >::type;
        using const_iterator = typename std::add_pointer<
            typename std::add_const< T >::type >::type;

        ~fixed_size_vector( )
        {
            clear( );
        }

        size_type
        size( ) const noexcept
        {
            return entries_;
        }

        bool
        empty( ) const noexcept
        {
            return size( ) == 0;
        }

        constexpr size_type
                  capacity( ) const noexcept
        {
            return max_size;
        }

        iterator
        begin( ) noexcept
        {
            return reinterpret_cast< T* >( &storage_[ 0 ] );
        }

        iterator
        end( ) noexcept
        {
            return begin( ) + entries_;
        }

        const_iterator
        begin( ) const noexcept
        {
            return reinterpret_cast< const_iterator >( &storage_[ 0 ] );
        }

        const_iterator
        end( ) const noexcept
        {
            return begin( ) + entries_;
        }

        const_iterator
        cbegin( ) const noexcept
        {
            return begin( );
        }

        const_iterator
        cend( ) const noexcept
        {
            return end( );
        }

        /*!
         * @brief push an entry onto the back of the vector if space is
         * available.
         * @param val
         * @return true on success, else false (no change made to the vector)
         */
        bool
        push_back( const T& val ) noexcept
        {
            if ( size( ) < capacity( ) )
            {
                T* dest = reinterpret_cast< T* >( &storage_[ entries_ ] );
                new ( dest ) T( std::move( val ) );
                ++entries_;
                return true;
            }
            return false;
        }

        /*!
         * @brief push an entry onto the back of the vector if space is
         * available.
         * @param val
         * @return true on success, else false (no change made to the vector)
         */
        bool
        push_back( T&& val ) noexcept
        {
            if ( size( ) < capacity( ) )
            {
                T* dest = reinterpret_cast< T* >( &storage_[ entries_ ] );
                new ( dest ) T( std::move( val ) );
                ++entries_;
                return true;
            }
            return false;
        }

        /*!
         * @brief emplace an entry onto the back of the vector if space is
         * available.
         * @tparam Args The argument types to pass to T's constructor
         * @param args The argument to pass to T's constructor
         * @return true on success, else false (no change made to the vector)
         */
        template < typename... Args >
        bool
        emplace_back( Args... args ) noexcept
        {
            if ( size( ) < capacity( ) )
            {
                T* dest = reinterpret_cast< T* >( &storage_[ entries_ ] );
                new ( dest ) T( std::forward< Args >( args )... );
                ++entries_;
                return true;
            }
            return false;
        }

        T& operator[]( size_type index ) noexcept
        {
            return *reinterpret_cast< T* >( &storage_[ index ] );
        }

        const T& operator[]( size_type index ) const noexcept
        {
            return *reinterpret_cast< const T* >( &storage_[ index ] );
        }

        void
        pop_back( ) noexcept
        {
            if ( !empty( ) )
            {
                size_type index = entries_ - 1;
                T*        cur = reinterpret_cast< T* >( &storage_[ index ] );
                cur->~T( );
                entries_--;
            }
        }

        template < typename T_ = T,
                   typename std::enable_if<
                       std::is_trivially_destructible< T_ >::value,
                       int >::type = 0 >
        void
        clear( )
        {
            entries_ = 0;
        }

        template < typename T_ = T,
                   typename std::enable_if<
                       !std::is_trivially_destructible< T_ >::value,
                       int >::type = 0 >
        void
        clear( )
        {
            while ( !empty( ) )
            {
                pop_back( );
            }
        }

        T&
        front() noexcept
        {
            return operator[](0);
        }

        T&
        back() noexcept
        {
            return operator[](entries_-1);
        }

        const T&
        front() const noexcept
        {
            return operator[](0);
        }

        const T&
        back() const noexcept
        {
            return operator[](entries_-1);
        }

    private:
        storage_type storage_[ max_size ]{};
        size_type    entries_{ 0 };
    };

} // namespace embedded

#endif // DAQD_TRUNK_FIXED_SIZE_VECTOR_HH
