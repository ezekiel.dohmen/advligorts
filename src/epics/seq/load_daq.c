//
// Created by erik.vonreis on 2/21/23.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "param.h"
#include "epics_map.h"
#include "sequencer.h"


/// call before running through the other check functions
int check_epics_start ()
{
    for ( int i = 0; i < EPICS_MAP_SIZE; ++i )
    {
        epics_map[ i ].in_ini = 0;
    }

    return 1;
}

/// Return a pointer to an EPICS_MAP struct for the channel name
/// see include/
/// \param channel_name
/// \return The EPICS map struct associated with the channel name, or NULL if not found.
static EPICS_MAP *
find_epics_map(const char *channel_name) {
    for(int i=0; i < EPICS_MAP_SIZE; ++i) {
        if(!strcmp(channel_name, epics_map[i].var_name)) {
            return &epics_map[i];
        }
    }
    return NULL;
}

/// Return true if EPICS int var C structure
/// matches the INI file order
/// Assumes this function is called in INI file order
/// \param channel_name EPICS channel name
/// \return 1 if EPICS C structure is in order with the INI file
static int
check_epics_int(const char *channel_name, DAQ_INFO_BLOCK *info) {
    EPICS_MAP *emap = find_epics_map(channel_name);
    char errMsg[128];

    // check if not mapped in C structure
    if (NULL == emap) {
        logFileEntryFmt("EPICS channel '%s' in INI file not mapped in C epics structure",
                         channel_name);
        return 0;
    }

    // check that it really is an integer type
    if (emap->var_type != EPICS_MAP_INT) {
        logFileEntryFmt( "EPICS channel %s is an integer in the INI file but not an integer in the C struct");
        return 0;
    }

    // check if already in INI file
    if (emap->in_ini > 0) {
        logFileEntryFmt(  "EPICS channel '%s' was found twice while loading the INI file.",
                         channel_name);
        return 0;
    }
    else
    {
        emap->in_ini += 1;
    }

    // check if doubles already seen
    // all integers must appear before all doubles
    if (info->epicsdblDataOffset >= 0) {
        logFileEntryFmt( "EPICS channel '%s' was an integer, but was read after a floating point channel from the INI file.",
                         channel_name);
        return 0;
    }

    // check if channel is in sequence
    // ini sequence must align with sequence in the C structure
    // we allow one gap in integers between EPICS_OUT and %SYS% structures

    int expected_offset = 1000000;
    if (info->cpyepics2times) {
        expected_offset = epics_map_out_offset + info->cpy2IntOffset + info->numEpicsInts*sizeof(int) - info->cpyIntSize[0];
    }
    else
    {
        expected_offset = epics_map_out_offset + info->numEpicsInts * sizeof(int);
    }

    // if we're greater than expected and haven't seen a gap yet
    // we're ok, but we now have a gap
    if ((info->cpyepics2times == 0) && (emap->struct_offset >= epics_map_sys_offset)) {
        info->cpyepics2times = 1;

        info->cpyIntSize[0] = info->numEpicsInts * sizeof(int);

        if (emap->struct_offset != epics_map_sys_offset) {
            logFileEntryFmt("EPICS channel '%s' was at offset 0x%d but 0x%d was expected.",
                             emap->struct_offset, epics_map_sys_offset);
        }

        // this is an offset from the start of the epics output,
        // not the start of the epics structure,
        // hence the subtraction of epics_map_out_offset.
        info->cpy2IntOffset = emap->struct_offset - epics_map_out_offset;
        fprintf(stdout,"found first variable in system struct: %s at offset %d\n", channel_name, emap->struct_offset);
    }
    else if (expected_offset != emap->struct_offset) {
        logFileEntryFmt( "EPICS channel '%s' was out of sequence in the INI file compared to the C struct\n"
                         "    expected offset was %d but actual offset was %d", channel_name, expected_offset, emap->struct_offset);
        return 0;
    }

    return 1;
}

/// Return true if the EPICS float var C structure
/// matches the INI file order
/// Assumes this function is called in INI file order
/// \param channel_name EPICS channel name
/// \return 1 if EPICS C structure is in order with the INI file
static int
check_epics_float(const char *channel_name, DAQ_INFO_BLOCK *info) {
    EPICS_MAP *emap = find_epics_map(channel_name);

    // check if not mapped in C structure
    if (NULL == emap) {
        logFileEntryFmt( "EPICS channel '%s' in INI file not mapped in C epics structure",
                         channel_name);
        return 0;
    }

    // check that it really is a floating point type
    if (emap->var_type != EPICS_MAP_DBL) {
        logFileEntryFmt( "EPICS channel %s is an floating point in the INI file but not a floating point in the C struct");
        return 0;
    }

    // check if already in INI file
    if (emap->in_ini > 0) {
        logFileEntryFmt( "EPICS channel '%s' was found twice while loading the INI file.\n",
                         channel_name);
        return 0;
    }
    else
    {
        emap->in_ini += 1;
    }

    // check that it's after all the ints
    int minimum_offset = info->numEpicsInts * sizeof(int) + epics_map_out_offset;
    if (minimum_offset > emap->struct_offset) {
        logFileEntryFmt( "EPICS channel '%s' is floating point but appears before the end of integer channels", channel_name);
        return 0;
    }

    // handle first double differently: we don't care where it is and we need
    // to specially mark its position
    //
    // otherwise, make sure it's in sequence.
    if (info->epicsdblDataOffset < 0) {
        // reference from start of output, not start of epics structure
        info->epicsdblDataOffset = emap->struct_offset - epics_map_out_offset;
    }
    else {
        int expected_offset = info->numEpicsFloats * sizeof(double) + info->epicsdblDataOffset + epics_map_out_offset;
        if (expected_offset != emap->struct_offset) {
            logFileEntryFmt("EPICS channel '%s' was out of sequence in the INI file compared to the C struct\n"
                             "Expected %d but offset was %d\n", channel_name, expected_offset, emap->struct_offset);
            return 0;
        }
    }

    return 1;
}

/// return 0 if end-of-load checks of epics channels fails for some reason
static int
epics_finalize(DAQ_INFO_BLOCK *info) {
    int all_found = 1;
    for (int i=0; i < EPICS_MAP_SIZE; ++i) {
        if(epics_map[i].in_ini < 1) {
            logFileEntryFmt("EPICS channel '%s' was not found in the INI file", epics_map[i].var_name);
            all_found = 0;
        }
    }

    if (!all_found) {
        return 0;
    }

    //calculate final integer copy
    if (info->cpyepics2times) {
        info->cpyIntSize[1] = info->numEpicsInts*sizeof(int) - info->cpyIntSize[0];
    }
    else
    {
        // only one copy of integers needed
        info->cpyIntSize[0] = info->numEpicsInts*sizeof(int);
    }

    return 1;
}

/*
 * Callback function should return 1 if OK and 0 if not.
 */
int testCallback(const char *channel_name, struct CHAN_PARAM *params, void *user) {
#if 0
  printf("channel_name=%s\n", channel_name);
  printf("dcuid=%d\n", params->dcuid);
  printf("datarate=%d\n", params->datarate);
  printf("acquire=%d\n", params->acquire);
  printf("ifoid=%d\n", params->ifoid);
  printf("datatype=%d\n", params->datatype);
  printf("chnnum=%d\n", params->chnnum);
  printf("testpoint=%d\n", params->testpoint);
  printf("gain=%f\n", params->gain);
  printf("slope=%f\n", params->slope);
  printf("offset=%f\n", params->offset);
  printf("units=%s\n", params->units);
  printf("system=%s\n", params->system);
#endif
    return 1;
}

static int
infoCallback(const char *channel_name, struct CHAN_PARAM *params, void *user) {
    DAQ_INFO_BLOCK *info = (DAQ_INFO_BLOCK *) user;
    testCallback(channel_name, params, user);
    if (info->numChans >= DCU_MAX_CHANNELS) {
        fprintf(stderr, "Too many channels. Hard limit is %d", DCU_MAX_CHANNELS);
        return 0;
    }
    if((params->chnnum >= FIRST_EPICS_INT_CHANNEL) &&
         (params->chnnum < FIRST_EPICS_DOUBLE_CHANNEL) )
    {
        if ((params->datatype != 2) && (params->datatype != 7)) {
            logFileEntryFmt("EPICS channel '%s' is not an integer type but\n"
                             "channel num %d is in the integer channel range",
                channel_name, params->chnnum);
            return 0;
        }

        if (!check_epics_int(channel_name, info)) {
            return 0;
        }

        info->numEpicsInts ++;
        info->numEpicsTotal ++;
    } else if ((params->chnnum >= FIRST_EPICS_DOUBLE_CHANNEL) &&
              (params->chnnum < FIRST_EPICS_FM_CHANNEL)) {
        if (params->datatype != 4)  {
            logFileEntryFmt("EPICS channel '%s' is not a floating point type but\n"
                             "channel num %d is in the floating point channel range",
                             channel_name, params->chnnum);
            return 0;
        }
        if ( !check_epics_float(channel_name, info)) {
            return 0;
        }

        info->numEpicsFloats ++;
        info->numEpicsTotal ++;
    } else if (params->chnnum >= FIRST_EPICS_FM_CHANNEL) {
        info->numEpicsFilts ++;
        info->numEpicsTotal ++;
    } else {
        sprintf(info->tp[info->numChans].channel_name,"%s",channel_name);
        info->tp[info->numChans].tpnum = params->chnnum;
        info->tp[info->numChans].dataType = params->datatype;
        info->tp[info->numChans].dataRate = params->datarate;
        info->tp[info->numChans].dataGain = (int)params->gain;
        info->numChans++;
    }
    return 1;
}

static int
parseGdstpFile(char *fname,GDS_INFO_BLOCK *ginfo) {

    char *cp;
    char cbuf[128];
    char val[64];
    char id[64];
    int channum;
    int totalchans = 0;
    int inloop = 0;
    unsigned int cr;
    char chan_name[60];
    FILE *fp = fopen(fname, "r");
    if (fp == NULL) {
        return 0;
    }
    // Read file up to first TP channel name
    while((cp = fgets(cbuf, 128, fp)) && cbuf[0] != '[') {
    }
    while(!feof(fp)) {
        /* :TODO: there will be a problem if the closing square bracket is missing */
        // Capture the channel name
        for (cp = cbuf; *cp && *(cp+1) && *(cp+1) != ']'; cp++) *cp = *(cp+1);
        *cp = 0;
        strncpy(chan_name, cbuf, 60);
        chan_name[59] = 0;

        // Search for corresponding TP channel number, which should occur before next TP channel name.
        while((cp = fgets(cbuf, 128, fp)) && cbuf[0] != '[' ) {
            for (cp = cbuf, cr = 0; *cp && *cp != '=' && cr < 64; cp++)  if (!isspace(*cp)) id[cr++] = *cp;
            if (*cp != '=') continue;
            // Get info descriptor and value based on '=' separator
            id[cr]=0;
            for (cp++, cr = 0; *cp && cr < 64; cp++)  if (!isspace(*cp)) val[cr++] = *cp;
            val[cr]=0;

            if(!strcasecmp(id,"chnnum")) {
                char *endptr;
                channum = strtol(val,&endptr,0);
                // Load TP info from channel name and number info
                strcpy(ginfo->tpinfo[totalchans].tpname,chan_name);
                ginfo->tpinfo[totalchans].tpnumber = channum;
                totalchans ++;
                ginfo->totalchans = totalchans;
            }
        }
    }
    printf("EOF found\n");
    fclose(fp);

    return(totalchans);

}

/// Load DAQ configuration file and store data in `info'.
/// Input and archive file names are determined based on provided site, ifo
/// and system names.
int
loadDaqConfigFile(DAQ_INFO_BLOCK *info, GDS_INFO_BLOCK *gdsinfo, char *site, char *ifo, char *sys)
{
    unsigned long crc = 0;
    char fname[256];         /* Input file name */
    char gds_fname[256];         /* Input file name */
    char archive_fname[256]; /* Archive file name */
    char perlCommand[256];   /* String that will contain the Perl command */
    int returnValue = -999;

    strcat(strcat(strcpy(fname, "/opt/rtcds/"), site), "/");
    strcat_lower(fname, ifo);

    strcpy(perlCommand, "iniChk.pl ");

    strcpy(gds_fname,fname);
    strcat(fname, "/chans/daq/");

    strcpy(archive_fname, fname);

    strcat(fname,sys);
    strcat(fname, ".ini");

    strcat(archive_fname, "archive/");
    strcat(archive_fname, sys);
    printf("%s\n%s\n", fname, archive_fname);

    strcat(perlCommand, fname);
    /*  fprintf(stderr, "pC=%s\n", perlCommand);  */
    returnValue = system(perlCommand);
    if (returnValue != 0)  {
        fprintf(stderr, "Failed to load DAQ configuration file\n");
        return 0;
    }
    strcat(gds_fname,"/target/gds/param/tpchn_");
    strcat_lower(gds_fname,sys);
    strcat(gds_fname,".par");
    // printf("GDS TP FILE = %s\n",gds_fname);
    int mytotal = parseGdstpFile(gds_fname,gdsinfo);
    // printf("total gds chans = %d\n",mytotal);

    info->numChans = 0;
    info->numEpicsInts = 0;
    info->numEpicsTotal = 0;
    info->numEpicsFloats = 0;
    info->numEpicsFilts = 0;
    info->epicsdblDataOffset = -1;
    info->cpyepics2times = 0;
    info->cpyIntSize[0] = 0;
    info->cpyIntSize[1] = 0;

    if (!check_epics_start()) {
        fprintf(stderr, "loadDaqConfigFileEx(): check_epics_start() failed\n");
        return 0;
    }

    if (0 == parseConfigFile(fname, &crc, infoCallback, 0,
                               archive_fname, info)) return 0;
    if (0 == epics_finalize(info)) {
        fprintf(stderr, "loadDaqConfigFileEx(): Failed epics_finalize\n");
        return 0;
    }
    fprintf(stdout, "Passed epics_finalize()\n");
    info->configFileCRC = crc;
    printf("CRC=0x%lx\n", crc);
    printf("dataSize=0x%lx 0x%lx\n", sizeof(DAQ_INFO_BLOCK),
            (0x1fe0000 + sizeof(DAQ_INFO_BLOCK)));
    printf("EPICS: INT=%d  FLOAT=%d  FILTERS=%d FAST=%d SLOW=%d\n",
            info->numEpicsInts,info->numEpicsFloats,info->numEpicsFilts,
            info->numChans,info->numEpicsTotal);
    printf("\tDBL_OFF=%d CPY_TWICE=%d CPY2_OFF=%d CPY1SZ=%d CPY2SZ=%d\n",
            info->epicsdblDataOffset, info->cpyepics2times, info->cpy2IntOffset,
            info->cpyIntSize[0], info->cpyIntSize[1]);
    return 1;
}