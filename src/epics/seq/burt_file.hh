//
// Created by jonathan.hanks on 4/2/22.
//

#ifndef DAQD_TRUNK_BURT_FILE_HH
#define DAQD_TRUNK_BURT_FILE_HH

#include "fixed_size_string.hh"
#include "fixed_size_vector.hh"

namespace BURT
{

    using parsed_word = embedded::fixed_string< 128 >;
    using parsed_line = embedded::fixed_size_vector< parsed_word, 4 >;

    /**
     * @brief Encode a string for output into a BURT file.
     * @note BURT encoding of strings does not escape quotes,
     *       and lists empty strings as \0 (backslash and 0)
     *
     * @tparam SRC_SIZE
     * @tparam DEST_SIZE Must be at least 2 greater than SRC_SIZE
     * @param src Input string to encode.
     * @param dest Destination for an encoded version of src.
     */
    template < std::size_t SRC_SIZE, std::size_t DEST_SIZE >
    void
    encodeString( const embedded::fixed_string< SRC_SIZE >& src,
                  embedded::fixed_string< DEST_SIZE >&      dest )
    {
        static_assert(
            DEST_SIZE >= ( SRC_SIZE + 2 ),
            "The destination buffer must be at least bytes larger than "
            "the source to account for quoting" );
        static_assert(
            DEST_SIZE >= 2,
            "The destination buffer must be long enough to burt encode "
            "a NULL string" );

        if ( src.empty( ) )
        {
            dest = "\\0";
            return;
        }

        bool expand = false;

        for ( char ch : src )
        {
            if ( ch == ' ' || ch == '\t' || ch == '\n' || ch == '"' )
            {
                expand = true;
                break;
            }
        }
        // we already bounds check this above
        if ( expand )
        {
            dest.printf( "\"%s\"", src.c_str( ) );
        }
        else
        {
            dest = src;
        }
    }

    /// Common routine to parse lines read from BURT/SDF files..
    ///	@param[in] *s	Pointer to line of chars to parse.
    ///	@param[out]     Output containing all the identified words in the line
    ///	@return wc	Number of words in the line.
    template < std::size_t N_WORDS, std::size_t WORD_LEN >
    static inline int
    parseLine( const char*                             s,
               embedded::fixed_size_vector< embedded::fixed_string< WORD_LEN >,
                                            N_WORDS >& out )
    {
        int lastwasspace = 1;

        const char* lastquote = nullptr;
        const char* qch = nullptr;

        out.clear( );
        embedded::fixed_string< WORD_LEN > cur_word{ };

        while ( *s != 0 && *s != '\n' && out.capacity( ) > out.size( ) )
        {
            if ( *s == ' ' || *s == '\t' )
            {
                if ( !lastwasspace )
                {
                    out.push_back( cur_word );
                    cur_word.clear( );
                }
                lastwasspace = 1;
            }
            else if ( *s != '"' || !lastwasspace )
            {
                cur_word += *s;
                lastwasspace = 0;
            }
            else
            {
                // quote
                // burt does not escape quotes, you have to look for the last
                // quote and just take it.
                lastquote = nullptr;
                qch = s + 1;

                while ( *qch && *qch != '\n' )
                {
                    if ( *qch == '"' )
                        lastquote = qch;
                    ++qch;
                }
                if ( !lastquote )
                    lastquote = qch;
                ++s;
                // copy from (s,qch) then set lastwasspace
                while ( s < lastquote )
                {
                    cur_word += *s;
                    ++s;
                }
                out.push_back( cur_word );
                cur_word.clear( );
                lastwasspace = 1;
                if ( !( *s ) || *s == '\n' || out.size( ) == out.capacity( ) )
                    break;
            }
            ++s;
        }
        if ( !cur_word.empty( ) )
        {
            out.push_back( cur_word );
        }
        for ( auto& word : out )
        {
            if ( word == "\\0" )
            {
                word = "";
            }
        }
        return out.size( );
    }

} // namespace BURT

#endif // DAQD_TRUNK_BURT_FILE_HH
