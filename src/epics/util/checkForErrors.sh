#!/bin/bash 
this_script_path=`realpath $0`
this_script_dir=`dirname ${this_script_path}`
err_logs=`find $this_script_dir/../models/*/logs/ -name build_success`
any_errors=0

for file in $err_logs
do
    contents=`cat $file`
    if [ $contents -eq "0" ]
    then
        any_errors=$((any_errors+1))
        echo "Errors in: $(dirname ${file})"
    fi
done

if [ $any_errors -eq 0 ] 
then
    echo "No Errors in built models."
fi

