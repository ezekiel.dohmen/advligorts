#!/usr/bin/perl

my $total_args = $#ARGV + 1;
if($total_args ne 3)
{
    die "***ERROR - You need to pass in three arguments : <IFO> <EPICS_SRC_DIR> <EPICS_BUILD_DIR>\n";
}

#
#  We will need the ifo name in upper case as part of the file path.
#
$ifoName = uc(substr($ARGV[0], 0, 2) );

$model_epics_src_dir = $ARGV[1];
$model_epics_build_dir = $ARGV[2];

#
#  Assemble file path & name of the .db file.
#
$fileName = "$model_epics_build_dir/db/";
$fileName .= $ifoName;
$fileName .= "/";
$fileName .= $ARGV[0];
$fileName .= "1";
$fileName .= "\.db";
 
#
#  Open the .db file, read its contents, and close it.
#
if (-e $fileName)  {
   open(IN, "<" . $fileName) || die "***ERROR: Can NOT open $fileName\n";
 
   chomp(@inData=<IN>);
 
   close IN;
 
   $tooLong = 0;
   $maxPartNameLength = 54;
   print "\n";
 
#
#  Loop through the lines in the .db file, looking for lines that
#  begin with 'grecord'.
#
   foreach $value (@inData)  {
      if ($value =~ /^grecord/)  {
 
#
#  Extract the part name and check its length.  Increment counter
#  if it is too long and print an error message.
#
         if ($value =~ /\,\"([\w\:\-]+)\"\)/)  {
            $partName = $1;
 
            if (length($partName) > $maxPartNameLength)  {
               $tooLong++;
               print "\n***ERROR: Part name too long: $partName\n";
            }
         }
      }
   }
}
else  {
   die "\n***ERROR: $fileName does NOT exist\n";
}

#
#  Assemble file path & name of the .ini file.
#
$fileName = "$model_epics_src_dir/";
$fileName .= $ARGV[0];
$fileName .= "\.ini";
 
#
#  Open the .ini file, read its contents, and close it.
#
if (-e $fileName)  {
   open(IN, "<" . $fileName) || die "***ERROR: Can NOT open $fileName\n";
 
   chomp(@inData=<IN>);
 
   close IN;
 
#
#  Loop through the lines in the .ini file, looking for lines that
#  begin with '[' or '#['.
#
   foreach $value (@inData)  {
      if ($value =~ /^\[|^\#\[/)  {
 
#
#  Extract the part name and check its length.  Increment counter
#  if it is too long and print an error message.
#
         if ($value =~ /\[([\w\:\-]+)\]/)  {
            $partName = $1;
 
            if (length($partName) > $maxPartNameLength)  {
               $tooLong++;
               print "\n***ERROR: Part name too long: $partName\n";
            }
         }
      }
   }
}
else  {
   die "\n***ERROR: $fileName does NOT exist\n";
}


#
#  Abort if any part names were found to be too long.
#
if ($tooLong)  {
   die"\n***ERROR - Too long part name(s) - ABORTING\n";
}
