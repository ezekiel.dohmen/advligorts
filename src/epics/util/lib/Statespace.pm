package CDS::Statespace;
use Exporter;
use Env;
use File::Basename;

#//     \page Statespace Statespace.pm
#//     Documentation for Statespace.pm
#//
#// \n


@ISA = ('Exporter');

# This one gets node pointer (with all parsed info) as the first arg
# and as the second arguments this gets part number
sub partType {
    my ($node, $i) = @_;
    
    $::partnum_to_ss_index_map[$i] = $::statespace_num_parts;
    $::statespace_num_parts += 1;

    return Statespace;
}

# Print Epics communication structure into a header file
# Current part number is passed as first argument
sub printHeaderStruct {
    my ($i) = @_;
    # Make sure input is a MUX and Output is DEMUX
    if ($::partInCnt[$i] != 1) {
        die "Part $::xpartName[$i] needs a single input\n";
    }
    if ($::partInputType[$i][0] ne "MUX") {
        die "Part $::xpartName[$i] needs a single MUX input, detected $::partInputType[$i][0]\n";
    }
    if ($::partOutCnt[$i] != 1) {
        die "Part $::xpartName[$i] needs a single output\n";
    }
    if ($::partOutputType[$i][0] ne "DEMUX") {
        die "Part $::xpartName[$i] needs a single MUX input, detected $::partOutputType[$i][0]\n";
    }
    print ::OUTH "\tint $::xpartName[$i]_RESET;\n";
}


# return an array of EpicsVariable objects
# this function should be idempotent: calling it over and over should give the same
# result and have no side effects.
sub getEpicsVariables {
    my ($i) = @_;
    @epics_vars = ();

    # Add EPICS input to reset the state of the statespace part
    push @epics_vars, CDS::EpicsVariable::new("$::xpartName[$i]\_RESET", "$::xpartName[$i]\_RESET", "int", "ai", 1, 0, {}, 1);

    return @epics_vars;
}

# Print variable declarations int front-end file
# Current part number is passed as first argument
sub printFrontEndVars  {
    my ($i) = @_;

    print ::OUT "#define $::xpartName[$i]_INDEX $::partnum_to_ss_index_map[$i] //statespace part index\n";

}

# Check inputs are connected
sub checkInputConnect {
    my ($i) = @_;
    return "";
}

# Return front end initialization code
# Argument 1 is the part number
# Returns calculated code string
sub frontEndInitCode {
    my ($i) = @_;
    my $ins = $::partInCnt[$::partInNum[$i][0]]; # The number of inputs to the part
    my $outs = $::partOutputs[$::partOutNum[$i][0]];

    return "globStateSpaceConfigPart($::xpartName[$i]_INDEX, \"$::xpartName[$i]\", $ins, $outs);\n";
}

# Figure out part input code
# Argument 1 is the part number
# Argument 2 is the input number
# Returns calculated input code
sub fromExp {
    my ($i, $j) = @_;
    my $from = $::partInNum[$i][$j];

    return "";
}

# Return front end code
# Argument 1 is the part number
# Returns calculated code string

sub frontEndCode {
    my ($i) = @_;
    
    my $ret="";

    $ret .= "//Statespace: $::xpartName[$i]\n";
    $ret .= "calcStateSpace($::xpartName[$i]_INDEX, ";
    $ret .=  "pLocalEpics->$::systemName\.$::xpartName[$i]\_RESET, $::fromExp[0],";
    $ret .= " \L$::partOutput[$i][0]);\n";
    $ret .= "pLocalEpics->$::systemName\.$::xpartName[$i]\_RESET = 0;\n";
 
    return $ret;
}
