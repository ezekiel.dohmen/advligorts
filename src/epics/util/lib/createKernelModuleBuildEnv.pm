package CDS::KernelMakefileUtils;
use Exporter;
@ISA = ('Exporter');

#// \b sub \b createKernelMakefiles \n
#// Move the standard kernel makefile into the build dir
#// and generate KernelVars.mk build env file.  \n\n
sub createKernelMakefiles{
my ($makefileDir) = @_;

# Compile options common to all runtime configurations
system ("/bin/cp Makefile.kernel $::modelCodeKernDir/Makefile");

open(OUTM,">".$makefileDir."KernelVars.mk") || die "cannot open Makefile (".$makefileDir."KernelVars.mk) file for writing";

print OUTM "# CPU-Shutdown Real Time Linux\n";
print OUTM "MODEL_NAME:=$::skeleton\n";
if ($::iopModel > -1) {
    print OUTM "IOP_MODEL:=1\n";
}
if ($::dolphinTiming > -1 or $::virtualiop == 2) {
    if ( $::pciNet < 0 )
    {
        die "Error: Model is configured to receive dolphin time (dolphin_time_rcvr) but is not configured to be a dolphin node (pciRfm).";
    }
    print OUTM "USE_DOLPHIN_TIMING:=1\n";
}
if ($::dolphin_time_xmit > 0) {
    if ( $::pciNet < 0 )
    {
        die "Error: Model is configured to transmit dolphin time (dolphin_time_xmit) but is not configured to be a dolphin node (pciRfm).";
    }
    print OUTM "XMIT_DOLPHIN_TIME:=1\n";
}
if ($::virtualiop != 1) {
    if ($::pciNet > 0) {
        print OUTM "BUILD_WITH_DOLPHIN:=1\n";
    }
}

#
# Start KBUILD_EXTRA_SYMBOLS for the kernel module build
print OUTM "KBUILD_EXTRA_SYMBOLS = $::mbufsymfile\n";
print OUTM "KBUILD_EXTRA_SYMBOLS += $::gpssymfile\n";
print OUTM "KBUILD_EXTRA_SYMBOLS += $::cpuisolatorfile\n";
print OUTM "KBUILD_EXTRA_SYMBOLS += $::rtsloggerfile\n";

print OUTM "KBUILD_EXTRA_SYMBOLS += \$(mkfile_dir)/../../../utils/ModuleIOP.symvers\n";


print OUTM "ccflags-y += -std=gnu11 \n";
print OUTM "ccflags-y += -O \n";
print OUTM "ccflags-y += -Wno-date-time \n";
print OUTM "ccflags-y += -Wno-declaration-after-statement #Off explicitly because kbuild sets it on\n";
print OUTM "ccflags-y += -Wframe-larger-than=4096 #KBuild'd default is 2048, but we are making it a bit larger\n";
print OUTM "ccflags-y += -Wno-unused-function #Pattern of c file inclusion and usage of some of the functions makes this a hard fix\n";
print OUTM "ccflags-y += -I$::rcg_src_dir/src/drv/\n";

# set vectorization level
if($::vectorization eq "avx512")
{
	print "vectorization set to avx512\n";
	print OUTM "ccflags-y += -march=native -mavx512f -mavx512bw -mavx512cd -mavx512dq -mfma \n";
}
elsif($::vectorization eq "avx2")
{
	print "vectorization set to avx2\n";
	print OUTM "ccflags-y += -march=native -mavx2 -mfma\n";
}
elsif($::vectorization eq "sse3")
{
	print "vectorization set to sse3\n";
	print OUTM "ccflags-y += -march=native -msse3 -mfma\n";
}
elsif($::vectorization eq "none")
{
	print "no vectorization used\n";
	#don't add anything if no vectorization
}
else
{
	die "Unknown vectorization level: $::vectorization\n";
}



print OUTM "ccflags-y += -D";
print OUTM "\U$::skeleton";
print OUTM "_CODE\n";
print OUTM "ccflags-y += -DFE_HEADER=\\\"\L$::skeleton.h\\\"\n";

# Model uses FIR filters
if($::useFIRs)
{
    print OUTM "ccflags-y += -DFIR_FILTERS\n";
}
print OUTM "ccflags-y += -g\n";

if ($::remoteGPS) {
  print OUTM "ccflags-y += -DREMOTE_GPS\n";
}
if ($::requireIOcnt) {
  print OUTM "#Comment out to disenable exact IO module count requirement\n";
  print OUTM "ccflags-y += -DREQUIRE_IO_CNT\n";
} else {
  print OUTM "#Uncomment to enable exact IO module count requirement\n";
  print OUTM "#ccflags-y += -DREQUIRE_IO_CNT\n";
}
if (0 == $::dac_testpoint_names && 0 == $::extraTestPoints && 0 == $::filtCnt) {
	print "Not compiling DAQ into the front-end\n";
	$::no_daq = 1;
}
if ($::edcu) {
    $::no_daq = 1;
}
if ($::no_daq) {
  print OUTM "#Comment out to enable DAQ\n";
  print OUTM "ccflags-y += -DNO_DAQ\n";
}

# Set to flip polarity of ADC input signals
if ($::flipSignals) {
  print OUTM "ccflags-y += -DFLIP_SIGNALS=1\n";
}

# Set BIQUAD as default starting RCG V2.8
  print OUTM "ccflags-y += -DALL_BIQUAD=1 -DCORE_BIQUAD=1\n";

if ($::rfmDelay >= 0) {
  print OUTM "ccflags-y += -DRFM_DELAY=$::rfmDelay\n";
}

print OUTM "ccflags-y += -DMODEL_RATE_HZ=$::modelrateHz\n";

#********* END OF COMMON COMPILE OPTIONS

if ($::iopModel > -1) {  #************ SETUP FOR IOP ***************

  print OUTM "ccflags-y += -DIOP_MODEL\n";
  print OUTM "ccflags-y += -DSLOW_ADC_SAMPLE_RATE_POW_2=16\n";
  print OUTM "ccflags-y += -DFAST_ADC_SAMPLE_RATE_POW_2=$::fastAdcSampleRate_2power\n";


  $modelType = "IOP";
  if($::diagTest > -1) {
  print OUTM "ccflags-y += -DDIAG_TEST\n";
  }
  if($::internalclk) {
  print OUTM "ccflags-y += -DUSE_ADC_CLOCK\n";
  }
  # Invoked if IOP cycle rate slower than ADC clock rate
  print OUTM "ccflags-y += -DUNDERSAMPLE=$::clock_div\n";
  $adccopyrate = $::modelrateHz / $::adcclockHz;
  print OUTM "ccflags-y += -DADC_MEMCPY_RATE=$adccopyrate\n";

  #Following used for IOP running at 128K 
  if($::adcclockHz ==131072) {
  print OUTM "ccflags-y += -DIOP_IO_RATE=131072\n";
  } else {
  print OUTM "ccflags-y += -DIOP_IO_RATE=65536\n";
  }

  #Following used for testing 
  if($::dacWdOverride > -1) {
  print OUTM "ccflags-y += -DDAC_WD_OVERRIDE\n";
  }
  #Following set to run as standard kernel module
  if ($::no_cpu_shutdown > 0) {
    print OUTM "ccflags-y += -DNO_CPU_SHUTDOWN\n";
  } else {
    print OUTM "#ccflags-y += -DNO_CPU_SHUTDOWN\n";
  }
  # ADD DAC_AUTOCAL to IOPs
  print OUTM "ccflags-y += -DDAC_AUTOCAL\n";

  # Set to run without LIGO timing system
  if ($::no_sync == 1) {
    print OUTM "ccflags-y += -DNO_SYNC\n";
  }  
  # Test mode to force sync to 1pps even if TDS present
  if ($::test1pps == 1) {
    print OUTM "ccflags-y += -DTEST_1PPS\n";
  }  
  # Set to run IOP as time master for the Dolphin Network
  if ($::dolphin_time_xmit > 0) {
    if ( $::pciNet < 0 )
    {
      die "Error: Model is configured to transmit dolphin time (dolphin_time_xmit) but is not configured to be a dolphin node (pciRfm).";
    }

    print OUTM "ccflags-y += -DXMIT_DOLPHIN_TIME=1\n";
  }
  # Set to run IOP as time receiver on the Dolphin Network
  if ($::dolphinTiming > -1 or $::virtualiop == 2) {
    if ( $::pciNet < 0 )
    {
        die "Error: Model is configured to receive dolphin time (dolphin_time_rcvr) but is not configured to be a dolphin node (pciRfm).";
    }
    print OUTM "ccflags-y += -DUSE_DOLPHIN_TIMING\n";
    print OUTM "ccflags-y += -DNO_DAC_PRELOAD=1\n";
  }
  # Set to run IOP on internal clock ie no IO modules
  if ($::virtualiop == 1) {
    print OUTM "ccflags-y += -DRUN_WO_IO_MODULES=1\n";
    print OUTM "ccflags-y += -DNO_DAC_PRELOAD=1\n";
  }
  # Set IOP to map Dolphin Networks
  # Dolphin Gen2 is the default
  if ($::virtualiop != 1) {
    if ($::pciNet > 0) {

      print OUTM "ccflags-y += -DDOLPHIN_TEST=1\n";
  }
 }

  if ($::optimizeIO) {
    print OUTM "ccflags-y += -DNO_DAC_PRELOAD=1\n";
  }
  if ($::lhomid) {
    print OUTM "ccflags-y += -DADD_SEC_ON_START=1\n";
  }

  #Non-zero in cases where there is a large delay in querying the time
  print OUTM "ccflags-y += -DTIME0_DELAY_US=$::time0_delay_us\n";

}
# End of IOP compile options

if ($::iopModel < 1) {   #************ SETUP FOR USER APP ***************
  print OUTM "ccflags-y += -DCONTROL_MODEL\n";
  print OUTM "ccflags-y += -DUNDERSAMPLE=1\n";
  print OUTM "ccflags-y += -DADC_MEMCPY_RATE=1\n";
  $modelType = "CONTROL";

  if ($::noZeroPad) {
    print OUTM "ccflags-y += -DNO_ZERO_PAD=1\n";
  }
  if ($::biotest) {
    print OUTM "ccflags-y += -DDIO_TEST=1\n";
  }

  #Following used with IOP running at 128K 
  if($::adcclockHz ==131072) {
    print OUTM "ccflags-y += -DIOP_IO_RATE=131072\n";
    if($::modelrateHz < 131072) {
        my $drate = 131072/$::modelrateHz;
        if($drate == 8 or $drate == 2 ) {
            die "RCG does not support a user model rate $::modelrateHz" . " Hz with IOP data at $::adcclockHz" ." Hz\n"  ;
        }
        print OUTM "ccflags-y += -DOVERSAMPLE\n";
        print OUTM "ccflags-y += -DOVERSAMPLE_DAC\n";
        print OUTM "ccflags-y += -DOVERSAMPLE_TIMES=$drate\n";
        print OUTM "ccflags-y += -DFE_OVERSAMPLE_COEFF=feCoeff$drate"."x\n";
    }
  }

  #Following used with IOP running at 64K (NORMAL) 
  if($::adcclockHz ==65536) {
    print OUTM "ccflags-y += -DIOP_IO_RATE=65536\n";
    if($::modelrateHz < 65536) {
        my $drate = 65536/$::modelrateHz;

        if($drate == 8 ) {
            die "RCG does not support a user model rate $::modelrateHz" . " Hz with IOP data at $::adcclockHz" ." Hz\n"  ;
        }

        print OUTM "ccflags-y += -DOVERSAMPLE\n";
        print OUTM "ccflags-y += -DOVERSAMPLE_DAC\n";
        print OUTM "ccflags-y += -DOVERSAMPLE_TIMES=$drate\n";
        print OUTM "ccflags-y += -DFE_OVERSAMPLE_COEFF=feCoeff$drate"."x\n";
    }
  }

}  #******************* END SETUP FOR USER APP


print OUTM "ccflags-y += -DMODULE -DNO_RTL=1\n";
print OUTM "ccflags-y += -I\$(SUBDIRS)/../../../models/" . "\L$::skeleton" . "/include -I$::rcg_src_dir/src/include\n";
print OUTM "ccflags-y += -I$::rcg_src_dir/src/fe/demod\n";
print OUTM "ccflags-y += -ffast-math -m80387 -msse2 -fno-builtin-sincos -mpreferred-stack-boundary=4\n";
print OUTM "\n";

if ($::extra_cflags) {
    print OUTM "# Extra CFLAGS from cdsParameters in the model:\n";
    print OUTM "ccflags-y += $::extra_cflags\n";
}


close OUTM;
}




