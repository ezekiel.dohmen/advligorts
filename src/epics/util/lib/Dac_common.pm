package Dac_common;

use warnings;
use strict;


%Dac_common::board_num_chans = (
    GSC_16AO16 => 16, # General Standards board
    GSC_18AO8 => 8, # 18-bit General Standards DAC board
    GSC_20AO8 => 8, # 20-bit General Standards DAC board
    LIGO_28AO32 => 32
);
$Dac_common::default_board_type = "GSC_16AO16";

%Dac_common::supported_board_types = (
    "Dac" => 1,
    "Dac18" => 1,
    "Dac20" => 1,
    "Dacligo28" => 1
);

#This constant needs to be matched to the MAX_DAC_CHN_PER_MOD constant
#in cdsHardware.h, so the c module allocates buffers appropriately
$Dac_common::max_dac_channels = 32;