package CDS::GaussianNoiseGenerator;
use Exporter;
@ISA = ('Exporter');

#//     \page GaussianNoiseGenerator Noise.pm
#//     Documentation for GaussianNoiseGenerator.pm
#//     This GaussianNoiseGenerator part generates gaussian 
#//     random noise with 0 mean and 1 standard deviation.
#//     
#//     
#//
#// \n


$printed = 0;
$init_code_printed = 0;
1;

sub partType {
    return GaussianNoiseGenerator;
}

# Print Epics communication structure into a header file
# Current part number is passed as first argument
sub printHeaderStruct {
    my ($i) = @_;
}

# Print Epics variable definitions
# Current part number is passed as first argument
sub printEpics {
    my ($i) = @_;
}


# Print variable declarations int front-end file
# Current part number is passed as first argument
sub printFrontEndVars  {
    my ($i) = @_;

    print ::OUT "static double \L$::xpartName[$i];\n";
    if ($printed) { return; }

    $state_name = "gauss_gen_prng_state";
    print ::OUT "static uint64_t $state_name\[2\];\n";
    print ::OUT "static inline double gaussianRandomDouble( void ) \n";
    print ::OUT "{\n";
    print ::OUT "   return qnorm5_s( ((double)xoroshiroPP_next( $state_name )/0xFFFFFFFFFFFFFFFF), 1, 0);\n";
    print ::OUT "}\n";
    #We don't want to have a zero seed for the RNG, so we take any user seed and 
    #add a constant to it, that way we can support a 0 gaussNoiseGeneratorSeed
    print ::OUT "void gauss_set_seed(unsigned long long int seed)\n";
    print ::OUT "{\n";
    print ::OUT "    $state_name\[0\] = 0x38ECAC5FB3251641ULL;\n";
    print ::OUT "    $state_name\[1\] = 0x145E556BD545B56BULL + seed;\n";
    print ::OUT "}\n";

    $printed = 1;
}

# Check inputs are connected
sub checkInputConnect {
    my ($i) = @_;
    return "";
}

# Figure out part input code
# Argument 1 is the part number
# Argument 2 is the input number
# Returns calculated input code
sub fromExp {
    my ($i, $j) = @_;
    my $from = $::partInNum[$i][$j];
    return "\L$::xpartName[$from]";
}

# Return front end initialization code
# Argument 1 is the part number
# Returns calculated code string
sub frontEndInitCode {
    my ($i) = @_;

    if ($init_code_printed) { return ""; }
    my $calcExp  =  "\L$::xpartName[$i] = 0;\n";
    if( $::gaussNoiseGeneratorSeed eq "none")
    {
        $calcExp .= "ligo_get_random_bytes(gauss_gen_prng_state, sizeof(gauss_gen_prng_state));\n";
    }
    else
    {
        $calcExp .= "gauss_set_seed($::gaussNoiseGeneratorSeed);\n";
    }

    $init_code_printed = 1;
    return $calcExp;
}


# Return front end code
# Argument 1 is the part number
# Returns calculated code string
sub frontEndCode {
    my ($i) = @_;
    my $calcExp = "// GaussiaNoiseGenerator\n";
    $calcExp .= "\L$::xpartName[$i] " . "= gaussianRandomDouble();\n";
    return $calcExp;
}
