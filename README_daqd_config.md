# Declaratively Configuring the Daqd

Previously the daqd was configured using a daqdrc file.
This file was written using the daqd command language and
was written in an imperative style.  To configure the daqd 
systems administrators must be familiar with the internals
of the daqd in order to not only ask that the proper
subsystems of the daqd get started for their task, but they
must also be very aware of the ordering of the tasks.

The daqd can now be configured via a yaml file.  This config
is a declarative config.  The system administrators need only
specify what they want the dadq to do and the daqd will
start up the needed subsystems in an appropriate manner.

This document will discuss the new declarative config file.

This document will **not** discuss the daqdrc file format.

## Default Configuration file

It is recommended that the user use the following file path
to hold the configuration for the daqd.

<pre>
/etc/advligorts/daqd.yaml
</pre>

This can be overridden on the command line with the '-c' option.
If the config filename ends in a '.yaml' suffix it is parsed as
a declarative yaml file, otherwise it is parsed as legacy daqdrc
file.

*Note:* Previously the default config had been ~/.daqdrc (or .daqdrc
if the HOME environment variable was not set).

## File layout

The config file is a yaml dictionary with four keys:

 * parameters
 * detector
 * services
 * inputs

### The Parameters section

The parameters section is a list of key value pairs used to tweak
various bits of behaviour in the daqd.

There is not a fixed set of parameters accepted, the restriction
is that the values must be boolean values, integers, floating point numbers,
or strings.  No objects are allowed.

This section is kept open ended to allow tweaks to be added easier.

Some common parameters with default values.

<table>
<tr><th>Parameter</th><th>Default</th><th>Notes</th></tr>
<tr>
    <td>thread_stack_size_mb</td>
    <td>10</td>
    <td>Sets the default thread stack 
        size when starting sub threads</td>
</tr>
<tr>
    <td>dcu_status_check</td>
    <td>true</td>
    <td>Calculate status checks in the frame writer</td>
</tr>
<tr>
    <td>debug</td>
    <td>10</td>
    <td>Debugging level, the higher the number the greater the output</td>
</tr>
<tr>
    <td>log</td>
    <td>2</td>
    <td>Syslogs, the higher the number the greater the output</td>
</tr>
<tr>
    <td>zero_bad_data</td>
    <td>true</td>
    <td>Should bad daq data be zeroed.  If false then old
    data will be potentially show up in the output streams
    if the model status is bad.</td>
</tr>
</table>

### The Detector section

The detector section defines the physical location of the detector.  If a daqd
is writing frames it should have the full definition.  However, if it is only an
NDS1 server only the site and ifo fields are required.

This will be shown by example with the settings for LHO and LLO.

The LHO detector should be defined as thus:
<pre>
detector:
  site: LHO
  ifo: H1
  longitude: -2.08407676917
  latitude: 0.81079526383
  elevation: 142.554
  azimuths:
    - 5.65487724844
    - 4.08408092164
  altitudes:
    - -0.0006195 
    - 0.0000125
  midpoints:
    - 1997.542
    - 1997.522
</pre>

The LLO detector should be defined as thus:
<pre>
detector:
  site: LLO
  ifo: L1
  longitude: -1.584309
  latitude: 0.533423
  elevation: -6.57399988174
  azimuths:
    - 4.40317821503
    - 2.83238005638
  altitudes:
    - -0.0003121
    - -0.0006107
  midpoints:
    - 1997.575
    - 1997.575
</pre>

### The Services section

This section is where the system administrators will enable
the desired behavior of the daqd.

The services section is a list of service objects.  Generally each
service object will have its parameters as sub fields.

The available services are:

 * configuration_number_client
 * frame_writer
 * strend_writer
 * mtrend_writer
 * raw_mtrend_writer
 * nds1
 * epics
 * gds_broadcast
 * daqd_control

Services are defined as an object.  You must at least specify the
service field.  In many cases if the defaults work well then that is the
only field that is needed.

As an example here are two equivalent service definitions to start
a frame writer.  The first uses the default values, the second 
explicitly lists the defaults.  The frame type default is derived from
the detector/ifo setting.  For this example we are assuming the ifo is
configured to be L1.

<pre>
services:
 - service: frame_writer
</pre>

Now with explicit defaults

<pre>
services:
 - service: frame_writer
   frame_dir: /frames/full
   frame_type: L-L1_R
   seconds_per_frame: 64
</pre>

#### configuration_number_client

This service enables the use of a run/configuration number server
to manage the run_number field in the output frames.  It is only
useful if frame_writer, strend_writer, or mtrend_writer is set.

The purpose of the configuration number server is to enable an
optimization when reading consecutive frames.  If the run number in
the frames are the same then some of the FrameCPP structures can
be reused between frames instead of being read from each frame.
This can noticeably speed up frame reads.

Fields
<table>
<tr><th>Field</th><th>Default</th><th>Notes</th></tr>
<tr>
    <td>server</td>
    <td></td>
    <td>The hostname:port of the server.</td>
</tr>
</table>

If no server is set, then the configuration number is set to 0.

#### frame_writer

The frame_writer service starts a raw frame writer process.

Fields
<table>
<tr><th>Field</th><th>Default</th><th>Notes</th></tr>
<tr>
    <td>frame_dir</td>
    <td>/frames/full</td>
    <td>The directory that frames are written under.  This
    directory will contain subdirectories so that the frames
    are broken out by gps times.<br/><br/>
    The final files will be &lt;frame_dir&gt;/&lt;5 digit gps prefix&gt;/&lt;frame_type&gt;&lt;gpstime-duration&gt;.gwf
    </td>
</tr>
<tr>
    <td>frame_type</td>
    <td>?-??_R-</td>
    <td>The prefix for the frames.  The full
        default will use input from the detector
        ifo field.  So an 'H1' detector would default
        to 'H-H1_R-' and an 'X6' detector would default
        to a frame type of 'X-X6_R-'.</td>
</tr>
<tr>
    <td>seconds_per_frame</td>
    <td>64</td>
    <td>How many seconds of data to put in each frame.</td>
</tr>
<tr>
   <td>checksum_dir</td>
   <td></td>
   <td>The frame writer can write md5 checksum files with each
   frame.  Set this to a directory to receive the files (in a 
   similar layout as the frames).  If empty no checksum files are
   generated.  It is safe to use the same directory as frame_dir 
   as the generated files have different extensions.</td>
</tr>
</table>

#### strend_writer

The strend_writer writes second trend frames.  For every
channel in daqd it will record the following 5 trends of the
raw data, with 1 entry summarizing each second of data:

 * min
 * max
 * mean
 * rms
 * n

Fields
<table>
<tr><th>Field</th><th>Default</th><th>Notes</th></tr>
<tr>
    <td>frame_dir</td>
    <td>/frames/trend/second</td>
    <td>The directory that frames are written under.  This
    directory will contain subdirectories so that the frames
    are broken out by gps times.<br/><br/>
    The final files will be &lt;frame_dir&gt;/&lt;5 digit gps prefix&gt;/&lt;frame_type&gt;&lt;gpstime-duration&gt;.gwf
    </td>
</tr>
<tr>
    <td>frame_type</td>
    <td>?-??_T-</td>
    <td>The prefix for the frames.  The full
        default will use input from the detector
        ifo field.  So an 'H1' detector would default
        to 'H-H1_T-' and an 'X6' detector would default
        to a frame type of 'X-X6_T-'.</td>
</tr>
<tr>
    <td>minutes_per_frame</td>
    <td>10</td>
    <td>How many minutes of data to put in each frame.</td>
</tr>
<tr>
   <td>checksum_dir</td>
   <td></td>
   <td>The strend writer can write md5 checksum files with each
   frame.  Set this to a directory to receive the files (in a 
   similar layout as the frames).  If empty no checksum files are
   generated.  It is safe to use the same directory as frame_dir 
   as the generated files have different extensions.</td>
</tr>
</table>

#### mtrend_writer

The mtrend_writer writes minute trend frames.  For every
channel in daqd it will record the following 5 trends, with one
entry summarizing 1 minute of raw data:

* min
* max
* mean
* rms
* n

Fields
<table>
<tr><th>Field</th><th>Default</th><th>Notes</th></tr>
<tr>
    <td>frame_dir</td>
    <td>/frames/trend/minute</td>
    <td>The directory that frames are written under.  This
    directory will contain subdirectories so that the frames
    are broken out by gps times.<br/><br/>
    The final files will be &lt;frame_dir&gt;/&lt;5 digit gps prefix&gt;/&lt;frame_type&gt;&lt;gpstime-duration&gt;.gwf
    </td>
</tr>
<tr>
    <td>frame_type</td>
    <td>?-??_M-</td>
    <td>The prefix for the frames.  The full
        default will use input from the detector
        ifo field.  So an 'H1' detector would default
        to 'H-H1_M-' and an 'X6' detector would default
        to a frame type of 'X-X6_M-'.</td>
</tr>
<tr>
    <td>minutes_per_frame</td>
    <td>60</td>
    <td>How many minutes of data to put in each frame.</td>
</tr>
<tr>
   <td>checksum_dir</td>
   <td></td>
   <td>The mtrend writer can write md5 checksum files with each
   frame.  Set this to a directory to receive the files (in a 
   similar layout as the frames).  If empty no checksum files are
   generated.  It is safe to use the same directory as frame_dir 
   as the generated files have different extensions.</td>
</tr>
</table>

#### raw_mtrend_writer

The raw minute trends are a control room optimization.  Frames
provide quick access to a large number of channels over a short
time frame.  The raw minute trends provide quick access to data
from a small number of channels over a long time frame.

Each daqd channel gets one file that contains records of all the
statistical fields (min, max, mean, rms, n) + gps time concatenated
together.

Fields
<table>
<tr><th>Field</th><th>Default</th><th>Notes</th></tr>
<tr>
    <td>trend_dir</td>
    <td>/frames/trend/raw</td>
    <td>The directory that raw trends are written under.  This
    directory will contain subdirectories that hash the checksum on the
    channel names.  These subdirectories contain the actual raw trends.
    </td>
</tr>
<tr>
    <td>save_period_minutes</td>
    <td>5</td>
    <td>How many minutes of data to accumulate before writing.</td>
</tr>
</table>

Note: For now when the sysadmin creates the trend_dir it must contain 256
subdirectories used to hash the channel names.  It must contain the directories
named 0 through ff in hex.

#### nds1

The NDS1 service serves data back to the end users.  It will
return live data or serve back data from the frame files.

Fields
<table>
<tr><th>Field</th><th>Default</th><th>Notes</th></tr>
<tr>
    <td>job_dir</td>
    <td>/var/run/nds</td>
    <td>Directory that the daqd writes nds archival requests to.
    The nds process should be set to read from this directory.</td>
</tr>
<tr>
    <td>trend_dirs</td>
    <td></td>
    <td>This is the list of directories to read raw trends from.
    This list should contain one entry for the current directory,
    plus one entry for each archived directory of raw trends to read from.
    It should be ordered from the newest to the oldest.<br/><br/>
    Note: This is different from the daqdrc config.
    </td>
</tr>
<tr>
    <td>frame_dir</td>
    <td>same as frame_writer trend_dir</td>
    <td>The directory to read raw frames from.</td>
</tr>
<tr>
    <td>frame_type</td>
    <td>same as frame_writer frame_type</td>
    <td>The frame type/prefix of the raw frames.</td>
</tr>
<tr>
    <td>strend_dir</td>
    <td>same as strend_writer frame_dir</td>
    <td>The directory to read second-trend frames from.</td>
</tr>
<tr>
    <td>strend_type</td>
    <td>same as strend_writer frame_type</td>
    <td>The frame type/prefix of the second trend frames.</td>
</tr>
<tr>
    <td>mtrend_dir</td>
    <td>same as mtrend_writer frame_dir</td>
    <td>The directory to read minute-trend frames from.</td>
</tr>
<tr>
    <td>mtrend_type</td>
    <td>same as mtrend_writer frame_type</td>
    <td>The frame type/prefix of the minute trend frames.</td>
</tr>
<tr>
    <td>port</td>
    <td>8088</td>
    <td>The port number to accept nds1 requests on.  The
    nds1 system will listen on 0.0.0.0:&lt;port&gt;</td>
</tr>
</table>

Note: Please keep the trend_dirs in order.  The NDS1 subsystem may
have issues with reading data if they are out of order.

#### epics

The daqd can export status information as EPICS PVs.  Enable 
this service to make the daqd a read-only EPICS IOC.

Fields
<table>
<tr><th>Field</th><th>Default</th><th>Notes</th></tr>
<tr>
    <td>prefix</td>
    <td></td>
    <td>The prefix to apply to epics PV names.
    There is no default as it would incorporate the
    ifo name + the role of daqd.<br/>
    An example for an H1 frame writer would be
    H1:DAQ-FW0_</td>
</tr>
</table>

#### gds_broadcast

This is the gds broadcast service.  This should be
a single purpose box, ie do not configure a frame writer,
trend writer, ... on a gds_broadcaster (epics/daqd_control
are fine).

The gds broadcaster creates a 1s frame of a subset of the
channel list and transmits this frame over the network.
Due to using the frame writing code you cannot both write
frames and do a gds broadcast.

Fields
<table>
<tr><th>Field</th><th>Default</th><th>Notes</th></tr>
<tr>
    <td>address</td>
    <td></td>
    <td>There is no default.  This is the broadcast
    address to send to.  As an example to broadcast
    over localhost use 127.255.255.255</td>
</tr>
<tr>
    <td>network</td>
    <td></td>
    <td>There is no default.  This is the network to
    broadcast to.  As an example to broadcast over
    localhost use 127.0.0.0</td>
</tr>
<tr>
    <td>port</td>
    <td>7096</td>
    <td>The broadcast port to use</td>
</tr>
<tr>
    <td>broadcast_ini</td>
    <td>/etc/advligorts/gds_broadcast.ini</td>
    <td>An ini file with the names of the channels
    to include in the 1s frame.</td>
</tr>
<tr>
    <td>frame_dir</td>
    <td>/dev/shm</td>
    <td>The directory to write the 1s frame to.  It will
    be written and then deleted.</td>
</tr>
<tr>
    <td>frame_type</td>
    <td>?-?-</td>
    <td>The prefix for the frames.  The full
        default will use input from the detector
        ifo field.  So an 'H1' detector would default
        to 'H-R-' and an 'X6' detector would default
        to a frame type of 'X-R-'.</td>
</tr>
</table>

#### daqd_control

The daqd_control port.  This defines what ports the daqd listens
on to receive commands.

*Note:* This is not needed on a nds1 server.  The nds1 port
is the daqd_control port.

Fields
<table>
<tr><th>Field</th><th>Default</th><th>Notes</th></tr>
<tr>
    <td>binary_port</td>
    <td>8088</td>
    <td>The main daqd port.  This speaks the nds1/daqd
    protocol.  Set to 0 to disable.</td>
</tr>
<tr>
    <td>text_port</td>
    <td>8087</td>
    <td>The daqd can also speak a more human friendly
    varient of the nds1/daqd protocol.  Set to 0 to disable.
    In general this is rarily used.</td>
</tr>
</table>

### The Inputs section

The inputs section is where the daqd is configured to accept
channel lists.

This is an object that is defined with the following fields:

Fields
<table>
<tr><th>Field</th><th>Default</th><th>Notes</th></tr>
<tr>
    <td>type</td>
    <td>ini</td>
    <td>Defines reading channel lists from ini files.</td>
</tr>
<tr>
    <td>ini_list</td>
    <td>/etc/advligorts/master</td>
    <td>This file contains a list of ini files to load
    defining the channel list.</td>
</tr>
<tr>
    <td>testpoints</td>
    <td>/etc/advligorts/testpoint.par</td>
    <td>This file contains a list of par files to load
    defining the test point list.<br/>
    Set to the empty string or null to disable testpoints.<br/>
    </td>
</tr>
</table>

An example that overrides the default paths.

<pre>
inputs:
  type: ini
  ini_list: "/etc/advligorts/channels.ini"
  testpoints: "/etc/advligorts/tp_list.par"
</pre>

### Configuration Notes

Some notes and guidelines when writing a configuration file.

 * Just put in the services you need.
 * You do not have to specify all the options to a service if
   the defaults work.
 * There are some common fields/entries between the frame/trend writers
   and the nds1 system.  If you are doing a system with both (ie a cymac)
   you only need to specify them once (in nds1 or the writers) the system
   will make sure the same value is used for each.

### Configuration examples

Following are some example configurations to help give direction.

#### Sample Cymac config

This is a standalone system reading data, writing, frames, and
serving it out via NDS1.

We will use default settings for frame directories and frame types.

<pre>
# sample config for a cymac
---
# Notice that no parameters section is used
# it is not needed, the defaults will work

# a detector definition is needed
# this will allow the frame types to default to
# X-X7_R-, X-X7_T-, X-X7_M-
detector:
  site: TST
  ifo: X7
  longitude: -2.08407676917
  latitude: 0.81079526383
  elevation: 142.554
  azimuths:
    - 5.65487724844
    - 4.08408092164
  altitudes:
    - -0.0006195 
    - 0.0000125
  midpoints:
    - 1997.542
    - 1997.522

# services to run
services:
 - service: frame_writer
 - service: strend_writer
 - service: mtrend_writer
 - service: raw_mtrend_writer
 - service: nds1
 - service: epics
   prefix: X7:DAQ-CY0_

# input section
# use the defaults so no need to specify the ini list or
# testpoint list
</pre>

#### Sample FW config

This config will write raw, strend, and mtrend frames.
Default frame directories (/frames/full, /frames/trend/second, frames/trend/minute)
will be used.  Default frame types will be used.

<pre>
# sample config for a frame writer
---
# Notice that no parameters section is used
# it is not needed, the defaults will work

# a detector definition is needed
# this will allow the frame types to default to
# H-H1_R-, H-H1_T-, H-H1_M-
detector:
  site: LHO
  ifo: H1
  longitude: -2.08407676917
  latitude: 0.81079526383
  elevation: 142.554
  azimuths:
    - 5.65487724844
    - 4.08408092164
  altitudes:
    - -0.0006195 
    - 0.0000125
  midpoints:
    - 1997.542
    - 1997.522

# services to run
services:
 - service: frame_writer
 - service: strend_writer
 - service: mtrend_writer
 - service: daqd_control
 - service: epics
   prefix: H1:DAQ-FW0_

# input section
# use the default ini list and
# disable testpoint support
inputs:
  testpoints: null
</pre>

#### Sample NDS1 config

This config will read raw and strend frames + raw mtrends.
Default frame directories (/frames/full, /frames/trend/second, /frames/trend/raw)
will be used.  Default frame types will be used.

<pre>
# sample config for a NDS1 system
---
# Notice that no parameters section is used
# it is not needed, the defaults will work

# a detector definition is needed
# this will allow the frame types to default to
# H-H1_R-, H-H1_T-, H-H1_M-
detector:
  site: LHO
  ifo: H1

# services to run
# here we will specify a raw trends list
# this is to allow reading archived trends
services:
 - service: nds1
   trend_dirs:
    - /frames/trend/raw
    - /archives/trend/raw/20230101
    - /archives/trend/raw/20220701
    - /archives/trend/raw/20220101
    - /archives/trend/raw/20210701
    - /archives/trend/raw/20210101
 - service: epics
   prefix: H1:DAQD_NDS0_

# No input section is needed the defaults
# will work to load a channel list and enable
# testpoints

</pre>

#### Sample raw minute trend writer config

This config will write raw minute trends.
Default frame directories (/frames/trend/raw)
will be used.

<pre>
# sample config for a raw trend writer system
---
# Notice that no parameters section is used
# it is not needed, the defaults will work

# a detector definition is needed
# this will allow the frame types to default to
# H-H1_R-, H-H1_T-, H-H1_M-
detector:
  site: LHO
  ifo: H1

# services to run
services:
 - service: raw_mtrend_writer
 - service: epics
   prefix: H1:DAQD_TW0_
 - service: daqd_controls

# input section
# use the default ini list and testpoint list
inputs:
  testpoints: null
</pre>

#### Sample gds broadcast config

This config will write 1s low latency frames
to the network.

<pre>
# sample config for a gds broadcast system
---
# Notice that no parameters section is used
# it is not needed, the defaults will work

# a detector definition is needed
# this will allow the frame types to default to
# H-R-
detector:
  site: LHO_4k
  ifo: H1
  longitude: -2.08407676917
  latitude: 0.81079526383
  elevation: 142.554
  azimuths:
    - 5.65487724844
    - 4.08408092164
  altitudes:
    - -0.0006195 
    - 0.0000125
  midpoints:
    - 1997.542
    - 1997.522

# services to run
services:
 - service: gds_broadcast
   address: 127.255.255.255
   network: 127.0.0.0
 - service: epics
   prefix: H1:DAQD_GDS0_
 - service: daqd_controls

# input section
# use the default ini list and testpoint list
inputs:
  testpoints: null
</pre>