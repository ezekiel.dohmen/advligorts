# Functions to help use the integration test python code

function(integration_test)
    cmake_parse_arguments( "ARG"
            ""
            "NAME;SCRIPT;RESOURCE"
            ""
            ${ARGN})

    message("integration_test requested")
    message("    NAME=${ARG_NAME}")
    message("    SCRIPT=${ARG_SCRIPT}")
    message("    RESOURCE=${ARG_RESOURCE}")
    message("SRC=${CMAKE_CURRENT_SOURCE_DIR}")
    message("BIN=${CMAKE_CURRENT_BINARY_DIR}")
    message("PRJ=${CMAKE_SOURCE_DIR}")

    add_test(NAME "${ARG_NAME}"
            COMMAND ${PYTHON_EXECUTABLE} -B "${CMAKE_CURRENT_SOURCE_DIR}/${ARG_SCRIPT}"
            WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")
    set_property(TEST "${ARG_NAME}"
            PROPERTY ENVIRONMENT "PYTHONPATH=${CMAKE_SOURCE_DIR}/test/python_modules")

    install(FILES "${CMAKE_CURRENT_SOURCE_DIR}/${ARG_SCRIPT}"
            DESTINATION "${CMAKE_INSTALL_PREFIX}/libexec/advligorts/tests/")

    if (ARG_RESOURCE)
        MESSAGE("Installing ${ARG_RESOURCE} as part of a external test")
        install(FILES "${ARG_RESOURCE}"
                DESTINATION "${CMAKE_INSTALL_PREFIX}/libexec/advligorts/tests/")
    endif (ARG_RESOURCE)
endfunction()

install(FILES  "${CMAKE_SOURCE_DIR}/test/python_modules/integration/__init__.py"
        DESTINATION "${CMAKE_INSTALL_PREFIX}/libexec/advligorts/python_modules/integration/")
install(PROGRAMS "${CMAKE_SOURCE_DIR}/test/external_test_runner.sh"
        DESTINATION "${CMAKE_INSTALL_PREFIX}/libexec/advligorts/")