#!/usr/bin/env python3


import argparse
import os
import hashlib
import logging
import json
import subprocess

BUILD_INFO_DIR = 'src'
#BUILD_INFO_DIR = 'build_info'

def verify_exists(file_path):
    if not os.path.exists(file_path):
        logging.error(f"Expected file/dir {file_path} does not exist")
        exit(1)

def get_sha256_hash(file_path):

    verify_exists(file_path)

    with open( file_path, "rb") as f:
        bytes = f.read()
        return  hashlib.sha256(bytes).hexdigest()

def record_dir(dir_path, file_struct):
    verify_exists(dir_path)
    for root, dirs, files in os.walk( dir_path ):

        for file in files:
            file_path = os.path.join(root, file)
            file_struct[os.path.join(root, file)] = {'hash': get_sha256_hash( file_path ), 'isLink': os.path.islink(file_path) }

    return 

def record_installed_files(target_dir, model_name):

    file_struct = {}

    #RCG_TARGET/chans/MODEL.txt filter file
    file_path = os.path.join(target_dir, 'chans', model_name.upper()+".txt")
    file_struct[file_path] = {'hash': get_sha256_hash(file_path), 'isLink': os.path.islink(file_path) }

    #RCG_TARGET/chans/daq .ini file
    file_path = os.path.join(target_dir, 'chans', 'daq', model_name.upper()+".ini")
    file_struct[file_path] = {'hash': get_sha256_hash(file_path), 'isLink': os.path.islink(file_path) }

    #TODO: Do we need to verify adls?
    #RCG_TARGET/medm/
    dir_path = os.path.join(target_dir, 'medm',  model_name)
    record_dir(dir_path, file_struct)


    #RCG_TAGET/target/gds/param/
    dir_path = os.path.join(target_dir, 'target', 'gds', 'param')
    verify_exists(dir_path)
    file_path = os.path.join(dir_path, 'testpoint.par')
    file_struct[file_path] = {'hash': get_sha256_hash( file_path ), 'isLink': os.path.islink(file_path) }
    file_path = os.path.join(dir_path, 'tpchn_'+model_name+'.par')
    file_struct[file_path] = {'hash': get_sha256_hash( file_path ), 'isLink': os.path.islink(file_path) }

    #RCG_TAGET/target/<model>/bin
    dir_path = os.path.join(target_dir, 'target', model_name, 'bin')
    record_dir(dir_path, file_struct)

    #RCG_TAGET/target/<model>/build_info
    #TODO
    #Pre RCG 5 uses 'src' dir to store build info, so we conver the path for compatability 
    tmp_files = {}
    dir_path = os.path.join(target_dir, 'target', model_name, 'build_info')
    if not os.path.exists(dir_path):
        dir_path = os.path.join(target_dir, 'target', model_name, 'src')
    record_dir(dir_path, tmp_files)
    for key in tmp_files:
        if os.path.join(target_dir, 'target', model_name, 'src') in key:
            new_key = key.replace(os.path.join(target_dir, 'target', model_name, 'src'), os.path.join(target_dir, 'target', model_name, 'build_info') )
        else:
            new_key = key
        file_struct[new_key] = tmp_files[key]

    #RCG_TAGET/target/<model>/param/
    dir_path = os.path.join(target_dir, 'target', model_name, 'param')
    record_dir(dir_path, file_struct)

    #RCG_TAGET/target/<model>epics
    dir_path = os.path.join(target_dir, 'target', model_name, model_name+"epics")
    record_dir(dir_path, file_struct)


    #print(f"All keys? {file_struct.keys()}\n\n")

    #print(json.dumps(file_struct, indent=4))
    return file_struct

def check_links(orig_files, new_files):

    result = True

    for key in orig_files:
        if key not in new_files:
            logging.error(f"Expected file '{key}' not found")
            result =  False
        if orig_files[key]['isLink'] != new_files[key]['isLink']:
            logging.error(f"Symlink mismatch for file '{key}' old was {orig_files[key]['isLink']} new is {new_files[key]['isLink']}")
            result =  False

    return result 

def check_hash(orig_files, new_files):
    result = True

    for key in orig_files:
        if key not in new_files:
            logging.error(f"Expected file '{key}' not found")
            result = False
        if orig_files[key]['hash'] != new_files[key]['hash']:
            logging.error(f"Hash mismatch for file '{key}' old was {orig_files[key]['hash'][0:8]} new is {new_files[key]['hash'][0:8]}")
            result =  False

    return result


def load_models_from_file(file_path):
    models = []
    with open(file_path, "r") as f:
        data = f.read()
        data.replace("\n", "")
        models = data.split()
    return models



if __name__ == "__main__":

    
    parser = argparse.ArgumentParser()

    model_selection = parser.add_mutually_exclusive_group(required=True)
    model_selection.add_argument("-model", help = "The name of the model we are doing a test install for.")
    model_selection.add_argument("-models-from-file", help = "A filepath to a file containing a space seperated list of models")

    action_selection = parser.add_mutually_exclusive_group()
    action_selection.add_argument("--install-test", help = "Do an install of the target, and verify expected files and symlinks", action='store_true')
    action_selection.add_argument("--file-compare", help = "Compare what is currently installed vs. this file as the original")

    parser.add_argument("--outFile", type=str, help = "Filename where the current install record should be written")
    parser.add_argument("-hash-check", help = "Does a hash comparison of files. Not useful for new installs, but good for revert verification", action='store_true')
    args = parser.parse_args()


    logging.basicConfig(level=logging.DEBUG)

    RCG_TARGET = os.environ.get('RCG_TARGET')
    if(RCG_TARGET == None):
        logging.error("No RCG_TARGET defined")
        exit(1)
    logging.debug(f"Using RCG_TARGET: {RCG_TARGET}")

    return_code = 0

    
    if args.models_from_file is None:
        models = [args.model]
    else:
        models = load_models_from_file(args.models_from_file)

    for model in models:

        cur_install = record_installed_files(RCG_TARGET, model)
        
        if args.outFile != None:
            if len(models) > 1:
                file_path = model+"_"+args.outFile
            else:
                file_path = args.outFile
            with open(file_path, "w") as f:
                f.write(json.dumps(cur_install, indent=4))
                f.close()
                logging.debug(f"Wrote current install of {model} to {file_path}")

        #Do a real install of the model and compare
        if args.install_test is True:
            process = subprocess.Popen(f"rtcds install {model}", shell=True, stdout=subprocess.PIPE)
            process.wait()
            if process.returncode != 0:
                logging.error(f"Install {model} failed, will not continue")
                return_code = 1
            else:
                post_install_files = record_installed_files(RCG_TARGET, model)
                logging.debug("Passed post install file check")
                if check_links(cur_install, post_install_files) == True:
                    logging.debug("Passed symlink preservation check")
                else:
                    return_code = 1


        #Compare vs file
        if args.file_compare != None:
            logging.debug(f"Comparing currently installed model {model} vs file {args.file_compare}")
            with open(args.file_compare, "r") as f:
                read_file = f.read()
                read_json = json.loads(read_file)
                if check_links(read_json, cur_install) == True:
                    logging.debug("Passed symlink preservation check")
                else:
                    return_code = 1

                if args.hash_check:
                    if check_hash(read_json, cur_install) == True:
                        logging.debug("Passed hash match check")
                    else:
                        return_code = 1

    exit( return_code )









