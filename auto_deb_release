#!/bin/bash

set -e

version=$1

if [[ -z $version ]]
then
  echo "ERROR: a tagged version must be passed on the command line"
  exit
fi

deb_names="buster bullseye bookworm"
first_deb=`echo ${deb_names} | cut -d " " --fields 1`
declare -A deb_versions
deb_versions=( ["buster"]=10 ["bullseye"]=11 ["bookworm"]=12 )

src_dir=$(pwd)
echo "Source directory is ${src_dr}"

# get version tag
version_tag=$(echo $version | sed 's/~/_/g')
echo "Releasing version ${version} from git tag '${version_tag}'"

# check if tag exists
git_tags=$(git tag)
found=false
for check_tag in $git_tags
do
  if [[ $check_tag == $version_tag ]]
  then
    found=true
    break
  fi
done
if ! $found
then
  echo "ERROR: Version tag not found.  Be sure to tag the version before releasing!"
  exit
fi

# check if tag is ancestor of master
if ! git merge-base --is-ancestor ${version_tag} master
then
  echo "ERROR: version tag must be an ancestor of the master branch"
  exit
fi

echo "Version tag was an ancestor of the master branch"

# check if tag already released
target_deb_tag="debian/${first_deb}/${version_tag}-"
for check_tag in $git_tags
do
  if [[ "${check_tag}" == *"${target_deb_tag}"* ]]
  then
    echo "ERROR: version already released in tag ${check_tag}"
    exit
  fi
done

# check out the tag
if ! git checkout ${version_tag}
then
  echo "Couldn't checkout tag '${version_tag}'"
  exit
fi

# make sure checkout is clean
num_changes=$(git diff-index HEAD | wc -l)
if [[ $num_changes -gt 0 ]]
then
  echo "ERROR: Source directory is not clean."
  exit
fi

# Check actual version number in source to see if it's correct.  Don't release packages with wrong version number.

major_ver=$(grep RCG_VERSION_MAJOR src/include/rcgversion.h | cut -d ' ' -f 3)
minor_ver=$(grep RCG_VERSION_MINOR src/include/rcgversion.h | cut -d ' ' -f 3)
sub_ver=$(grep RCG_VERSION_SUB src/include/rcgversion.h | cut -d ' ' -f 3)
src_version="${major_ver}.${minor_ver}.${sub_ver}"
src_release=$(grep RCG_VERSION_REL src/include/rcgversion.h | cut -d ' ' -f 3)


if [[ "${version}" == *"~"* ]]
then
   release=0
   cut_version=$(echo $version | cut -d "~" -f 1)
else
   release=1
   cut_version=$version
fi

echo "cut version = ${cut_version}"

if [[ $src_version != $cut_version ]]
then
  echo "Error: Requested version was ${version} but source version at tag ${version_tag} was ${src_version}"
  exit
fi

if [[ $release != $src_release ]]
then
  echo "Error: Release status of version doesn't match release status in source."
  echo
  echo "If this is a development package then version should end with '~devN'"
  echo "and src/include/rcgversion.h RCG_VERSION_REL should be 0"
  echo
  echo "If this is a release package then version should be only the numerical part, no '~'"
  echo "and src/include/rcgversion.h RCG_VERSION_REL should be 1"
  exit
fi

# start the release
first_run=true
tmp_chng_entry='/tmp/_dtt_deb_changelog_entry.txt'
deb_chglog='debian/changelog'
tmp_chng_log='/tmp/_dtt_deb_changelog.txt'

for deb_name in $deb_names
do
  deb_ver=${deb_versions[$deb_name]}
  git switch "debian/${deb_name}"
  git pull
  gbp import-ref master -u $version --upstream-tag='%(version)s' --debian-branch=debian/$deb_name
  if $first_run
  then
    # manually edit first changelog
    old_line_count=$(cat ${deb_chglog} | wc -l)
    gbp dch -Rc -N ${version}-1+deb$deb_ver --debian-branch debian/$deb_name
    new_line_count=$(cat ${deb_chglog} | wc -l)
    new_entry_count=$(($new_line_count-$old_line_count))
    head -n $new_entry_count $deb_chglog > $tmp_chng_entry
    first_ver=$deb_ver
    first_run=false
  else
    # copy entry to subsequent changelogs
    # make sure to change deb number in first line
    head -n 1 $tmp_chng_entry | sed "s/deb${first_ver}/deb${deb_ver}/" > $tmp_chng_log
    tail_lc=$(($new_entry_count-1))
    tail -n $tail_lc $tmp_chng_entry >> $tmp_chng_log
    cat $deb_chglog >> $tmp_chng_log
    cp $tmp_chng_log $deb_chglog
    git add $deb_chglog
    git commit -m "updated change log from auto release script"
  fi
  git clean -f
  gbp tag --debian-branch=debian/$deb_name
  git push
done
git push --tags
git switch master
